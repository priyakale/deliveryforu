<!-- Footer -->

  <div class="footer__bg no__imgfooter">

    <div class="footer__inner">
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">DELIVER4U</a>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="http://deliveryforu.test:81/">Home</a></li>
            <li><a href="#">Jobs</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Become a vendor</a></li>
          </ul>
        </div>
      </nav>
      <hr>
      <div class="footer__copy">
        <p>Copyright 2019 Deliver4u</p>
        <p>Powered by I-Serve</p>
      </div>
    </div>
  </div>

<!-- Footer -->