<!-- Main Menu -->
<nav class="navbar navbar-inverse navbar__inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">Deliver4u</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Jobs</a></li>
        <li class="seprate__menu"><a href="#">Become A vendor</a></li>
        <li class="rounded__in"><a href="{{ route('login') }}">SIGN IN</a></li>
        <li class="rounded__up"><a href="{{ route('register') }}">SIGN UP</a></li>
      </ul>
    </div>
  </div>
</nav>
<!-- Main Menu -->