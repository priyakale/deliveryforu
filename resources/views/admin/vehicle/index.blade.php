@extends('admin.layout')

@section('htmlheader_title')
Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">
        <a href="{{ route('admin.vehicle.create') }}" class="btn btn-info"> Add New </a>
        <table class="display" id="data-table">
            <thead>
                <th>Vehicle Type</th>
                <th>Brand</th>
                <th>Action</th>
            </thead>
            <tbody>
                @forelse($vehicles as $vehicle)
                <tr>
                    <td>{{ $vehicle->type }}</td>
                    <td>{{ $vehicle->brand }}</td>
                    <td>
                        <a href="{{ route('admin.vehicle.edit', $vehicle->id) }}">Edit</a> /
                        <a href="{{ route('admin.vehicle.destroy', $vehicle->id) }}" onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">Delete</a>
                    </td>
                    <form id="delete-form" action="{{ route('admin.vehicle.destroy', $vehicle->id) }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                </tr>
                @empty
                <tr>
                    <td colspan="2">No Data Found!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript" >
    $(document).ready(function() {
        $('#data-table').dataTable();
    })
</script>

@endsection
