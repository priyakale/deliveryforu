@extends('admin.layout')

@section('htmlheader_title')
Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">

        <form action="{{ @$vehicle ? route('admin.vehicle.update', $vehicle->id) : route('admin.vehicle.store') }}" method="POST">
            {{ csrf_field() }}
            @if(@$vehicle)
                {{ method_field('PATCH') }}
            @endif

            <div class="form-group">
                <label for="">Type: </label>
                <input type="text" name="type" value="{{ old('type', @$vehicle->type) }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Brand: </label>
                <input type="text" name="brand" value="{{ old('brand', @$vehicle->brand) }}" class="form-control">
            </div>

            <button type="submit" class="btn btn-info">Create</button>

        </form>

    </div>
</div>


<script type="text/javascript" >
    $(document).ready(function() {

    })
</script>

@endsection
