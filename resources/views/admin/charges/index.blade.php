@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<div class="container full_width">
	<div class="row">
		<div class="panel panel-custom">
			<div class="panel-heading panel-heading-custom">
				{{ Form::open(array('url'=> URL::route("save_charges") 
	                , 'method' => 'POST', 'id'=>'charges_form')) }}
                <table class="full_width">
                    <tr>
                        <td>
                            <div class="form-group float-right">
                                <div class="btn-group input-group">
                                    <button type="button" id="save-charges" class="float-right btn btn-success">
                                    	<i class="fa fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
		<div class="col-lg-4">
			<div class="row well">
				<legend style="margin-bottom: 8px !important;">
		            <a href="javascript:void(0)">
		            <i class="glyphicon glyphicon-briefcase"></i>
		            </a> Charges
	            </legend>
				
				<div class="form-group{{ $errors->has('app_name') ? ' has-error' : '' }}">
	              	<label for="credit_card">Credit Card Fee (%)</label>
	              	<input type="hidden" name="id" value="{{ $data['record']['id'] }}">
	              	<input type="number" class="form-control" placeholder="Credit Card Fee (%)" id="credit_card" name="credit_card" value="{!! ($data['record']['credit_card'] ? $data['record']['credit_card'] : '') !!}" required>       
	              	@if ($errors->has('credit_card'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('credit_card') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
	          	<div class="form-group{{ $errors->has('app_name') ? ' has-error' : '' }}">
	              	<label for="debit_card">Debit Card Fee (%)</label>
	              	<input type="number" class="form-control" placeholder="Debit Card Fee (%)" id="debit_card" name="debit_card" value="{!! ($data['record']['debit_card'] ? $data['record']['debit_card'] : '') !!}" required>       
	              	@if ($errors->has('debit_card'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('debit_card') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
	          	<div class="form-group{{ $errors->has('app_name') ? ' has-error' : '' }}">
	              	<label for="fpx">FPX Fee (%)</label>
	              	<input type="number" class="form-control" placeholder="FPX Fee (%)" id="fpx" name="fpx" value="{!! ($data['record']['fpx'] ? $data['record']['fpx'] : '') !!}" required>       
	              	@if ($errors->has('fpx'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('fpx') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="row well"  style="margin-left:0.5%; margin-right:0.5%;">
	          	<legend style="margin-bottom: 8px !important;">
		            <a href="javascript:void(0)">
		            <i class="fa fa-columns" aria-hidden="true"></i>
		            </a> Charges
	            </legend>
	          	
				<div class="form-group{{ $errors->has('app_name') ? ' has-error' : '' }}">
	              	<label for="convenience_fee">Convenience Fee (RM)</label>
	              	<input type="number" class="form-control" placeholder="Convenience Fee (RM)" id="convenience_fee" name="convenience_fee" value="{!! ($data['record']['convenience_fee'] ? $data['record']['convenience_fee'] : '') !!}" required>       
	              	@if ($errors->has('convenience_fee'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('convenience_fee') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
			</div>
		</div>
		
	</div>
</div>
<script>
$(document).ready(function() {
	
	$('#save-charges').on('click',function(){
        var data = new FormData($('#charges_form')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_charges') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                window.location.href = "{{ URL::route('index_charges') }}";
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
        //return true;
    });

	
});
</script>
@endsection
