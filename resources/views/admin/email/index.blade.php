@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">
        <div class="panel panel-custom">
            <div class="panel-heading panel-heading-custom">
                {{ Form::open(array('url'=> URL::route("save_email") , 'method' => 'POST', 'id'=>'emailform')) }}
                <table class="full_width">
                    <tr>
                        <td>
                            <div class="form-group float-right">
                                <div class="btn-group input-group">
                                    <button type="button" id="save_email_setting" class="float-right btn btn-success">
                                        <i class="fa fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row well">
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="glyphicon glyphicon-briefcase"></i>
                    </a> Email
                </legend>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" >Name</label>
                    <input type="hidden" name="id" value="{{ $data['records']['id'] }}">
                    <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{!! ($data['records']['name'] ? $data['records']['name'] : '') !!}" required>       
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif                        
                </div>
                <div class="form-group{{ $errors->has('driver_id') ? ' has-error' : '' }}">
                    <label for="driver_id" >Driver</label>
                    <select name="driver_id" id="driver_id" class="form-control chosen-select" >
                        <option value="">Select Driver</option>
                        @if(!empty($data['email_driver']))
                            @foreach($data['email_driver'] as $index => $record)
                                <option value="{{ $record->id }}" {{ !empty($data['records']['driver_id']) && ($data['records']['driver_id']==$record->id) ? 'selected' : '' }}>{{ $record->name }}</option>
                            @endforeach
                        @endif
                    </select>      
                    @if ($errors->has('skin'))
                        <span class="help-block">
                        <strong>{{ $errors->first('skin') }}</strong>
                        </span>
                    @endif                  
                </div>
                <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                    <label for="port" >Port</label>
                    <input type="text" class="form-control" placeholder="Port" id="port" name="port" value="{!! ($data['records']['port'] ? $data['records']['port'] : '') !!}" required>       
                    @if ($errors->has('port'))
                        <span class="help-block">
                            <strong>{{ $errors->first('port') }}</strong>
                        </span>
                    @endif                        
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row well"  style="margin-left:0.5%; margin-right:0.5%;">
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="fa fa-columns" aria-hidden="true"></i>
                    </a> SMTP Setting
                </legend>
                <div class="form-group{{ $errors->has('smtp_host') ? ' has-error' : '' }}">
                    <label for="smtp_host" >SMTP Host</label>
                    <input type="text" class="form-control" placeholder="SMTP Host" id="smtp_host" name="smtp_host" value="{!! ($data['records']['smtp_host'] ? $data['records']['smtp_host'] : '') !!}" required>       
                    @if ($errors->has('smtp_host'))
                        <span class="help-block">
                            <strong>{{ $errors->first('smtp_host') }}</strong>
                        </span>
                    @endif                        
                </div>
                <div class="form-group{{ $errors->has('smtp_username') ? ' has-error' : '' }}">
                    <label for="smtp_username" >SMTP Username</label>
                    <input type="text" class="form-control" placeholder="SMTP Username" id="smtp_username" name="smtp_username" value="{!! ($data['records']['smtp_username'] ? $data['records']['smtp_username'] : '') !!}" required>       
                    @if ($errors->has('smtp_username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('smtp_username') }}</strong>
                        </span>
                    @endif                        
                </div>
                <div class="form-group{{ $errors->has('smtp_password') ? ' has-error' : '' }}">
                    <label for="smtp_password" >SMTP Password</label>
                    <input type="text" class="form-control" placeholder="SMTP Password" id="smtp_password" name="smtp_password" value="{!! ($data['records']['smtp_password'] ? $data['records']['smtp_password'] : '') !!}" required>       
                    @if ($errors->has('smtp_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('smtp_password') }}</strong>
                        </span>
                    @endif                        
                </div>
                <div class="form-group{{ $errors->has('encryption_id') ? ' has-error' : '' }}">
                    <label for="encryption_id" >Encryption</label>
                    <select name="encryption_id" id="encryption_id" class="form-control chosen-select" >
                        <option value="">Select Encryption</option>
                        @if(!empty($data['email_encryption']))
                            @foreach($data['email_encryption'] as $index => $record)
                                <option value="{{ $record->id }}" {{ !empty($data['records']['encryption_id']) && ($data['records']['encryption_id']==$record->id) ? 'selected' : '' }}>{{ $record->name }}</option>
                            @endforeach
                        @endif
                    </select>      
                    @if ($errors->has('encryption_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('encryption_id') }}</strong>
                        </span>
                    @endif                  
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row well" >
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a> Sendmail Setting
                </legend>
                <div class="form-group{{ $errors->has('owner') ? ' has-error' : '' }}">
                    <label for="sendmail_path" >Sendmail Path</label>
                    <input type="text" class="form-control" placeholder="Owner Name" id="owner" name="owner" value="{!! ($data['records']['sendmail_path'] ? $data['records']['sendmail_path'] : '') !!}">
                    @if ($errors->has('sendmail_path'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sendmail_path') }}</strong>
                        </span>
                    @endif                        
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    
    $('#save_email_setting').on('click',function(){
        var data = new FormData($('#emailform')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_email') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                if(data['result']){
                    window.location.href = "{{ URL::route('index_email') }}";
                }else{
                    alert('Fail : ' + data['errMsg']);
                    $('#'+data['error_field']).focus();
                    $('#'+data['error_field']).parents('div .form-group').addClass('has-error'); 
                    $('#'+data['error_field']).parents('div .form-group').find('label').css("color", "#a94442");
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
        //return true;
    });
});
</script>
@endsection

