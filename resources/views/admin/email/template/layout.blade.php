@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => URL('/') ])
<!--<img src="{{ asset('/assets/img/yakin-logo.png') }}" width="20%" alt="MCM!" />-->
<b>UPayMe</b>
@endcomponent
@endslot

{!! $data['description'] !!}

<!-- Subcopy -->
@if (!empty($data['actionText']))
@component('mail::subcopy')
If you’re having trouble clicking the "{{ $data['actionText'] }}" button, copy and paste the URL below
into your web browser: [{{ $data['actionUrl'] }}]({{ $data['actionUrl'] }})
@endcomponent
@endif

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<!--© {{ date('Y') }} Upayme. All rights reserved.-->
@endcomponent
@endslot
@endcomponent