@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<!-- Ionicons -->
<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet" type="text/css" />

<!-- SEARCH CRITERIA -->
<div class="row">
	<div class="col-md-12">
	 	<div class="widget widget-default widget-item-icon" >
            <div class="widget-item-left">
                <span class="fa fa-search"></span>
            </div>
            <div class="widget-data">
                {{ Form::open(array('url'=> URL::route("index_blast_email") 
                , 'method' => 'GET', 'id'=>'searchform')) }}
                	<div class="input-group form-width-full padding-top">
                		<div class="input-group form-width-full">
                			<div class="col-md-3">
	                            <label for="name" >Name</label>
                         		<input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ !empty($data['search']['name']) ? $data['search']['name'] : '' }}" >
	                        </div>
							<div class="col-md-3">
	                            <label for="email" >Email</label>
                         		<input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ !empty($data['search']['email']) ? $data['search']['email'] : '' }}" >
	                        </div>
	                        <div class="col-md-3">
	                            <label for="role_id">Role</label>
	                            <select name="role_id" id="role_id" class="form-control selectpicker" data-live-search="true">
	                            @if(!empty($data['app_name'][0]))
	                            	<option value="">Select role_id</option>
		                            @foreach($data['app_name'] as $index => $record)
		                            	<option value="{{ $record->id }}" {{ !empty($data['search']['role_id']) && ($data['search']['role_id']==$record->id) ? 'selected' : '' }}>{{ $record->name }}</option>
									@endforeach
							 	@else
							 		<option value="">You dont have access to any application</option>
							 	@endif
	                            </select>
	                        </div>
						</div>
                	</div>
                    <div class="input-group form-width-full padding-top padding-bottom padding-right">
                        <div class="form-group float-right">
                            <div class="input-group">
                                <button type="submit" class="btn btn-success">
                                <i class="fa fa-search"></i>Search</button>
                                &nbsp;&nbsp;
                                <button type="button" id="valuereset" class="btn btn-defult">
                                <i class="fa fa-times"></i>Reset</button>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>                
        </div>  
    </div>
</div>
<!-- END SEARCH CRITERIA -->
<!-- SEARCH RESULT -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-custom">
			<div class="panel-heading panel-heading-custom">
                <table class="full_width">
                    <tr>
                        <td>
                            Search Result  
                            <span class="badge custom-badge">{{ $data['records']->total() }}</span>
                        </td>
                        <td>	
                            <div class="form-group float-right">
                            	
								<button type="button" class="btn btn-success btn-sm open-send-modal"><span class="fa fa-bell" aria-hidden="true"></span> Send</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-body progress-panel ">
	            <div class="float-left">
	                {{ $data['records']->appends(Request::except('page'))->links() }}
	            </div>
            	{{ Form::open(array('url'=>'/fcm/notification/','id'=>'fcm_form','enctype' => 'multipart/form-data','files' => 'true')) }}
            	@if (!empty($data['search']['name']))
            		<input type="hidden" name="name" value="{{ $data['search']['name'] }}">
				@endif
				@if (!empty($data['search']['email']))
            		<input type="hidden" name="email" value="{{ $data['search']['email'] }}">
				@endif
				@if (!empty($data['search']['role_id']))
            		<input type="hidden" name="role_id" value="{{ $data['search']['role_id'] }}">
				@endif
           		<table class="table table-hover">
					<thead>
				  		<tr>
							<th><input type="checkbox" id="selectAll" ></th>
							<th>Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Action</th>
					  	</tr>
					</thead>
					<tbody>

					@if (empty($data['records'][0]))
                        <tr>
                            <th colspan="6" class="empty-record text-center"><label>No Result</label></th>
                        </tr>
                    @endif
					@foreach($data['records'] as $index => $record)
						<tr id="fcm{{$record->id}}">
							<td>
							<input type="checkbox" name="token[]" id="{{$record->id}}"  
							value="{{ $record->id }}">
							</td>
							<td>{{ $record->name }}</td>
							<td>{{ $record->email }}</td>
							<td>{{ $record->role->name }}</td>
							<td>
								<button type="button" class="btn btn-success btn-sm btn-edit" 
								id="{{ $record->id }}" >
								<i class="fa fa-pencil-alt"></i>
								</button>
							</td>									
						</tr> 
				 	@endforeach
					</tbody>
				</table>
				<br>
				<div class='row padding-top'>
                    <div class="col-lg-2"><label for="description">Description</label></div>
                    <div class="col-lg-8">
                        <textarea rows="30" class="form-control text-area-edit" name="description" id='description' >{!! (!empty($data['records']['description']) ? $data['records']['description'] : '') !!}</textarea>
                    </div>
                    <div class="col-lg-2">
                        <label for="description">Available Variables for Description</label>
                        <p>%NAME</p>
                        <p>%BUTTON</p>
                        <p>%EMAIL</p>
                    </div>
                </div>
				<div class="modal fade" id="sendModal" data-backdrop="static" data-keyboard="false" data-focus-on="input:first" tabindex="-1" role="dialog" aria-labelledby="sendModalLabel">
				    <div class="modal-dialog" role="document">
					    <div class="modal-content">
					        <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="sendModalLabel">Send Fcm</h4>
					        </div>
					        <div class="modal-body" id="sendModal_body">
					      	    <div class="panel-body">
									<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
								        <label for="description">Description</label>
								        <textarea rows="30" class="form-control text-area-edit" name="description" id='description' >{!! (!empty($data['records']['description']) ? $data['records']['description'] : '') !!}</textarea>    
								    </div>
								    
								</div>
					        </div>
					        <div class="modal-footer">
					      	    <button type="button" class="btn btn-primary" id="btn-send-fcm"><span class="fa fa-bell" aria-hidden="true"></span> Send</button>
					      	    <button type="button" class="btn btn-primary" id="btn-send-to-all-fcm"><span class="fa fa-bell" aria-hidden="true"></span> Send to all</button>
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				            </div>
					    </div>
				    </div>
	        	</div>
				{{ Form::close() }}
			</div>
			<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
			    <div class="modal-dialog" role="document">
			    	<div class="modal-content">
			        	<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="editModalLabel">Edit</h4>
			      		</div>
				        <div class="modal-body" id="editModal_body">
				        </div>
			    	</div>
			    </div>
    	    </div>
    	    <div class="modal fade" id="urlModal" tabindex="-1" role="dialog" aria-labelledby="urlModalLabel">
			    <div class="modal-dialog" role="document">
			    	<div class="modal-content">
			        	<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="selectModalLabel">Image Url</h4>
			      		</div>
				        <div class="modal-body" id="urlModal_body">
					        <div class="form-group">
					            <label for="url" >Url</label>
					            <input type="name" class="form-control" id="url" name="url" value="">  
					        </div>    
				        </div>
				        <div class="modal-footer">
				      	    <button type="button" class="btn btn-primary btn-ok-url" id="select">Ok</button>
			                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			            </div>
			    	</div>
			    </div>
    	    </div>
    	    <div class="modal fade" id="deleteModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
			    <div class="modal-dialog" role="document">
				    <div class="modal-content">
				        <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="deleteModalLabel">Delete</h4>
				        </div>
				        <div class="modal-body" id="deleteModal_body">
				      	    <h5>Are you sure want to delete this fcm?</h5>
				        </div>
				        <div class="modal-footer">
				        	<input type="hidden" id="delete_id" />
				      	    <button class="btn btn-danger" id="delete">Delete</button>
			                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			            </div>
				    </div>
			    </div>
        	</div>
    	</div>
	</div>
</div>
<!-- END SEARCH RESULT -->
<!-- Script -->
<script>
$(document).ready(function(){
    $('.btn-edit').click(function(e){
  	    var id = $(this).attr('id');
  	    // AJAX
  	    $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'GET',
      	    contentType: false,
  		    processData: false,
            url : "{{ URL::route('edit_blast_email') }}/" + id,
            dataType: 'json',
            success: function (data) {
                if(data['result']){
              	    $('#editModal_body').empty();
                    $('#editModal_body').html(data['returnHTML']);
                    $('#editModal').modal();
                }else{
                    alert('Fail : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
  	    // END AJAX
    });
    $('.btn-delete').on('click', function(e) {
    	var id = $(this).data('id');
    	$('#delete_id').val(id);
    	$('#deleteModal').modal('show');
	});
	$('#delete').on('click', function(e) {
    	var id = $('#delete_id').val();
    	// AJAX
  	    $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'GET',
      	    contentType: false,
  		    processData: false,
           	url : "{{ URL::route('delete_blast_email') }}/" + id,
            dataType: 'json',
            success: function (data) {
            	if(data['result']){
            		localStorage.removeItem("selected-"+id);
            		location.reload();
            	}else{
            		alert(data['errMsg']);
            	}
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
  	    // END AJAX
	});
	$('.url').click(function(e){
		$('#urlModal').appendTo("body").modal('show');
    });
    $('.open-send-modal').click(function(){
    	$('#sendModal').modal('show');
    });
    $('.btn-ok-url').on('click', function(){
    	
        var url = $( "#url" ).val();   
        $("label[for='selectedURL']").text(url);     
	 	$('#showURL').attr( "value", url );
	 	$('#url').attr( "value", url );
	 	$("#image").val("");
	 	$(".modal  #img").attr("src","");
	 	$(".modal #img").hide();
		$('#urlModal').modal('toggle');
    });  
    $("input:file").change(function (){
    	$('#showURL').attr("value","");
    	$('#url').attr("value","");
    	$('#url').val("");
    	var fileName = $(this).val().split('\\').pop();;
    	$("label[for='selectedURL']").text(fileName);
    	if (this.files && this.files[0]) {
            var reader = new FileReader();
              
            reader.onload = function (e) {
                $('.modal #img').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
            $('#img').show();
          }
        else{
            $(".modal  #img").attr("src","");
            $('#img').hide();
          }
    });  
   
    $("#selectAll").on( "click",function() {
    	var ischecked= $("#selectAll").is(':checked');
    	$("input[name='token[]']").each(function () {
    		var id = $(this).attr('id');
    		var checkedItem = $(this).val();
            if (ischecked) {
            	$(this).prop('checked', true);
                localStorage.setItem("selected-"+id, checkedItem);
            }
            else{
           		$(this).prop('checked', false);
           		localStorage.removeItem("selected-"+id);
           	}
		});
	});
	$("input[name='token[]']").on( "click",function() {
		if ($("input[name='token[]']:checked").length != $("input[name='token[]']").length) {
	       $("#selectAll").prop('checked', false);
	    }
	    else if ($("input[name='token[]']:checked").length == $("input[name='token[]']").length){
	    	$("#selectAll").prop('checked', true);
	    }
		var ischecked= $(this).is(':checked');
		var id = $(this).attr('id');
	    if(ischecked) {
	        var checkedItem = $(this).val();
			
			localStorage.setItem("selected-"+id, checkedItem);
		}
		else{
        	localStorage.removeItem("selected-"+id);
	    }
	});
	
	$("#sendModal").on("click", "#btn-send-fcm", function () {
		$(".modal-body #sendto").val("selected");
		var arrayData = [];		
		var title = $("#title").val();
      	var message = $("#message").val();
      	for (var key in localStorage){
			
			if(key.indexOf('selected-') >= 0 ){
			    var key_value = localStorage.getItem(key);

			    if ($("input[name='selected[]'][value='"+key_value+"']").length < 1){
			    	var input = $("<input>").attr({"type":"hidden","name":"selected[]"}).val(key_value);
			    	$('#fcm_form').append(input);
			    }
			}
			else{
				$("input[name='selected[]']").remove();
			}
		}
		var uploadfile = new FormData($("#fcm_form")[0]);
	      	// AJAX
	      	$.ajax({
      			contentType: false,
				processData: false,
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                url : "{{ URL::route('delete_blast_email') }}",
                data: uploadfile,
                dataType: 'json',
                success: function (data) {
	            	if(data['result']){
	            		for (var key in localStorage){
                			if (/^selected-/.test(key)) {
				               localStorage.removeItem(key);
				            }		
						}
	                	location.reload();	 
	            	}else{
	            		alert(data['errMsg']);
	            	}          
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
	      	// END AJAX
    });

    $("#sendModal").on("click", "#btn-send-to-all-fcm", function () {
    	$(".modal-body #sendto").val("all");
		var arrayData = [];		
		var title = $("#title").val();
      	var message = $("#message").val();
		var uploadfile = new FormData($("#fcm_form")[0]);
	      	// AJAX
	      	$.ajax({
      			contentType: false,
				processData: false,
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                url : "{{ URL::route('delete_blast_email') }}",
                data: uploadfile,
                dataType: 'json',
                success: function (data) {
	            	if(data['result']){
	            		for (var key in localStorage){
                			if (/^selected-/.test(key)) {
				            	localStorage.removeItem(key);
				            }			
						}
	                	location.reload();	 
	            	}else{
	            		alert(data['errMsg']);
	            	}          
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
	      	// END AJAX
    });
    $('#valuereset').click(function(){
	    window.location.href = "{{URL::to('fcm/')}}" 
    });
    for (var key in localStorage){
		if(key.indexOf('selected-') >= 0 ){
			var key = key.substr(9);
			$( "#"+ key ).prop( "checked", true );
		}
	}
	if($("input[name='token[]']").length > 0){
		if ($("input[name='token[]']:checked").length != $("input[name='token[]']").length) {
	       $("#selectAll").prop('checked', false);
	    }
	    else if ($("input[name='token[]']:checked").length == $("input[name='token[]']").length){
	    	$("#selectAll").prop('checked', true);
	    }
	}

});
</script>  
<!-- End Script -->

<!-- role js -->
<script src="{!!asset('/js/summernote.min.js')!!}"></script>
<script src="{!!asset('/js/editor.js')!!}"></script>	
     
@endsection




    