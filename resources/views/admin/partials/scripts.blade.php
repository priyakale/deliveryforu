<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>
<!-- bootstrap-switch -->
<script src="{{ asset('/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<!-- bootstrap-datepicker js -->
<script src="{{ asset('/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<!-- main js -->
<script src="{{ asset('/js/main.js') }}" type="text/javascript"></script>
<!-- select chosen js -->
<script src="{{ asset('/js/chosen.jquery.min.js') }}" type="text/javascript"></script>
<!-- Date js -->
<script src="{{ asset('/js/date-format.js') }}" type="text/javascript"></script>
<!-- select bootstrap-slider js -->
<script src="{{ asset('/js/bootstrap-slider.js') }}" type="text/javascript"></script>
<!-- Date Time picker js -->
<script src="{{ asset('/js/jquery.timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/custom.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="{{ asset('/assets/js/add_merchant.js') }}" type="text/javascript"></script>
<!-- Datatables js -->
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>

<script>
	var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }	
</script>