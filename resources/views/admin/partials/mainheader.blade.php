<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/admin') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img style="width: 32px !important;" src="{{asset('/img/'.SettingHelper::getIcon())}}" alt="{{ SettingHelper::getAppName() }}" /></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img style="width: 32px !important;" src="{{asset('/img/'.SettingHelper::getIcon())}}" alt="{{ SettingHelper::getAppName() }}" /><b> {{ SettingHelper::getAppName() }}</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="collapse_action()">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->                          

                         <img src="{{ (!empty(Auth::user()->profile_pic) ? asset('/uploads/profile_pic/'.Auth::user()->profile_pic) : asset('/uploads/profile_pic/default_user.png') ) }}" class="user-image" alt="My Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ (!empty(Auth::user()->profile_pic) ? asset('/uploads/profile_pic/'.Auth::user()->profile_pic) : asset('/uploads/profile_pic/default_user.png') ) }}" class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>{{ Auth::user()->last_login_at }}</small>  
                                 </p>
                            </li>
                           
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    @if(!RoleHelper::isCustomer())
                                    <a href="javascript:void(0)" class="btn btn-default btn-flat" 
                                    onclick="popDetail({{ Auth::user()->id }})">Profile</a>
                                    @endif
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>
    </nav>
    <div class="modal fade" role="dialog" id="user-detail-model" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-body" id="user-detail-content">

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function collapse_action(){
            $.ajax({
                type: 'GET',
                url : "{{ URL::route('collapse') }}",
                data: {'_token' : '{{ csrf_token() }}' },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
        function popDetail(id){
            $('#user-detail-content').html();
            $.ajax({
                type: 'GET',
                url : "{{ URL::route('user_detail') }}/" + id,
                data: { 'id' : id, '_token' : '{{ csrf_token() }}' },
                dataType: 'json',
                success: function (data) {
                    if(data['result']){
                        $('#user-detail-content').html(data['returnHTML']);
                        $("#user-detail-model").modal();
                    }else{
                        alert('Fail to get user detail : ' + data['errMsg']);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }//End popup detail 
        

    </script>
</header>
