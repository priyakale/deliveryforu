<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ (!empty(Auth::user()->profile_pic) ? asset('/uploads/profile_pic/'.Auth::user()->profile_pic) : asset('/uploads/profile_pic/default_user.png') ) }}" class="img-circle" alt="My Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" id="side-menu">
            <li class="header">{{ RoleHelper::getUserRole() }}</li>

            @foreach(RoleHelper::getRoleMenu() as $key_record => $record)
                @if($key_record == 'parent')
                    @foreach($record as $key_parent => $record_parent)
                    <li class="treeview parent-li" id="li-parent-{{ $key_parent }}">
                        @php
                            $parant_icon = '';
                            $parent_name = '';
                            $parent_data_menu = explode("||", $key_parent);
                            $parent_name = $parent_data_menu[0];
                        @endphp
                        @if(count($parent_data_menu) > 1)
                             @php $parant_icon = $parent_data_menu[1]; @endphp
                        @endif
                        <a href="#"><i class='fa {{ $parant_icon }}'></i>
                            <span>{{ $parent_name }}</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                        @foreach($record_parent as $key_value => $record_value)
                            @if(!empty($record_value['menu_array']['link']) && Request::url() == URL::route($record_value['menu_array']['link']))
                                <li class="child-li" data-js="child-li-selected" >
                            @else
                                <li class="child-li">
                            @endif
                                @if(empty($record_value['menu_array']['link']))
                                <a href="#">
                                @else
                                <a href="{{ URL::route($record_value['menu_array']['link']) }}">
                                @endif
                                <i class="fa {{ $record_value['menu_array']['icon'] }}"></i>
                                    <span>{{ $record_value['menu_array']['name'] }}</span>
                                </a>
                            </li>
                        @endforeach
                        </ul>
                    </li>
                    @endforeach
                @elseif($key_record == 'menu_array')
                    <!-- NO PARENT -->
                    @foreach($record as $key_value => $record_value)
                        @if(!empty($record_value['link']) && Request::url() == URL::route($record_value['link']))
                            <li class="active">
                        @else
                            <li>
                        @endif
                            @if(empty($record_value['link']))
                            <a href="#">
                            @else
                            <a href="{{URL::route($record_value['link'])}}">
                            @endif
                            <i class="fa {{ $record_value['icon'] }}"></i> <span>{{ $record_value['name'] }}</span>
                            </a>
                        </li>
                    @endforeach
                    <!-- END NO PARENT -->
                @endif
            @endforeach
        </ul><!-- /.sidebar-menu -->
         @endif
    </section>
    <!-- /.sidebar -->
</aside>
<script type="text/javascript" >
    $( document ).ready(function() {
        $('.child-li[data-js="child-li-selected"]')
        .parent()
        .parent()
        .removeClass()
        .addClass('treeview parent-li active');

        $('.child-li[data-js="child-li-selected"]')
        .removeClass()
        .addClass('child-li active');
    });
</script>
