<!-- Content Header (Page header) -->
<section class="content-header"> 
    <h3 class="head">
        @yield('contentheader_title', '' ) {{ isset($data['page_title'] ) ? $data['page_title']  : '' }}
        <small>@yield('contentheader_description')</small>
    </h3>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-tachometer-alt"></i> Level</a></li>
        <li class="active">CMS Page</li>
    </ol>
</section>