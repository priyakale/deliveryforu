@if (Session::has('message') || Session::has('success') || Session::has('warning') || Session::has('danger'))
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('message'))
            <div class="alert alert-info">
                <span>
                    <i class="fa fa-info-circle"></i>  {{ Session::get('message') }}
                    <a href="#" class="close close-alert">&times;</a>
                </span>
            </div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success">
                <span>
                    <i class="fa fa-check"></i>  {{ Session::get('success') }}
                    <a href="#" class="close close-alert">&times;</a>
                </span>
            </div>
            @endif
            @if (Session::has('warning'))
            <div class="alert alert-warning">
                <span>
                    <i class="fa fa-exclamation-circle"></i>  {{ Session::get('warning') }}
                    <a href="#" class="close close-alert">&times;</a>
                </span>
            </div>
            @endif
            @if (Session::has('danger'))
            <div class="alert alert-danger">
                <span>
                    <i class="fa fa-ban"></i>  {{ Session::get('danger') }}
                    <a href="#" class="close close-alert">&times;</a>
                </span>
            </div>
            @endif
        </div> 
    </div>  
@endif

@if(Session::has('message'))
<div class="alert alert-danger fade in">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{ Session::get('message') }}
</div>
@endif

@if (!empty($error) && count($errors) > 0)
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif