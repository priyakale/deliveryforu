<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version {{ SettingHelper::getVersion() }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">{{ SettingHelper::getOwner() }}</a>
</footer>

