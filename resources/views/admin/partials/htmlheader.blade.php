<head>
    <meta charset="UTF-8">
     <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/upay_me.png')}}" />
    <title>{{ SettingHelper::getAppName() }} - @yield('htmlheader_title', 'Your title here') </title>
    <link href="{{ asset('/img/logo.ico') }}" type="image/x-icon" rel="icon">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset('/css/fontawesome-all.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">-->
    <!-- Loader CSS -->
    <link href="{{ asset('/css/loader.css') }}" rel="stylesheet" type="text/css" />
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>


    <!-- ADDED EXTRA CSS -->
        <!-- Date Time Picker-->
        <link href="{{ asset('/css/jquery.timepicker.css') }}" rel="stylesheet" type="text/css" />
        <!-- Bootshrap  -->
        <link href="{{ asset('/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Bootshrap  Datepicker-->
        <link href="{{ asset('/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="{{ asset('css/jquery-datatable.min.css') }}" rel="stylesheet" type="text/css">

        <!-- Theme style -->
        <link href="{{ asset('/css/vendor.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/css/skins/skin-'.SettingHelper::getSkin().'.css') }}" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{ asset('/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Select Chosen -->
        <link href="{{ asset('/css/chosen.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Bootstrap slider-->
        <link href="{{ asset('/css/bootstrap-slider.css') }}" rel="stylesheet" type="text/css" />
      <!-- Custom CSS -->
        <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css" />

</head>
