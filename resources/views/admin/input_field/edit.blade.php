@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
        <form class="" role="update" name="save_input_field" id="save_input_field" 
                    action="{{ URL::route('save_input_field') }}" method="POST">
            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td></td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <button type="button" class="float-right btn btn-success" id="save-input-field">
                                            <i class="fa fa-save"></i>Save
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#general">General</a></li>
                        <li ><a data-toggle="tab" href="#options" style="display: none">Options</a></li>
                    </ul>
                    <!-- FORM UPDATE POSITION -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row tab-content" >
                        <!-- START GENERAL DETAIL-->
                        <div class="col-md-12 tab-pane fade in active" 
                        id="general">
                            <div class="widget widget-default widget-item-icon padding-left">
                                <div class='row float-left detaill-inner full_width'>
                                    <div class='row padding-top'>
                                        <div class="col-lg-2">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="form-group col-lg-10">
                                            <input type="hidden" class="form-control" name="id" id='id' value="{!! (!empty($data['records']['id']) ? $data['records']['id'] : '') !!}"  >
                                            <input type="text" class="form-control" placeholder="Name" name="name" id='name' value="{!! (!empty($data['records']['name']) ? $data['records']['name'] : '') !!}" required >
                                        </div>
                                    </div>

                                    <div class='row padding-top'>
                                        <div class="col-lg-2"><label for="name">Data Type</label></div>
                                        <div class="form-group col-lg-10">
                                            <select name="data_type_id" id="data_type_id" class="form-control chosen-select">
                                                <option value="">Select Data Type</option>
                                                @if(!empty($data['data_types']))
                                                    @foreach($data['data_types'] as $key => $value )
                                                      <option value="{{ $value['id'] }}" {{ !empty($data['records']['data_type_id']) && ($data['records']['data_type_id']==$value['id']) ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class='row padding-top'>
                                        <div class="col-lg-2"><label for="name">Status</label></div>
                                        <div class="form-group col-lg-10">
                                            <select name="status_id" id="status_id" class="form-control chosen-select">
                                                <option value="">Select Status</option>
                                                @if(!empty($data['status_records']))
                                                    @foreach($data['status_records'] as $key => $value )
                                                      <option value="{{ $value['id'] }}" {{ !empty($data['records']['status_id']) && ($data['records']['status_id']==$value['id']) ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>             
                            </div>
                        </div>
                        <!-- END GENERAL DETAIL -->
                        <!-- END GENERAL TAB -->
                        <form id="input_options" name="input_options"> 
                        <div class="col-md-12 tab-pane fade in" id="options">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class='row float-left detaill-inner full_width'>
                                    <div class="btn-group input-group">
                                        <button type='button' class='btn btn-default' id='btn_add_outlet'><i class='fa fa-plus'></i>Add Options
                                        </button>
                                    </div>
                                    <hr>
                                </div>


                                <div class='row float-left detaill-inner full_width'>
                                    <ul id="ul_option" class="list-group">
                                        <?php //echo '<pre>';print_r($data['records']['outlets']); ?>
                                        @if(!empty($data['records']['options']) && count($data['records']['options']) > 0)
                                            @php $count = 1; $cnt = 0; @endphp
                                            @foreach($data['records']['options'] as $record)
                                                <li id='li_option-{{ $count }}' class="list-group-item" style="margin-bottom: 10px;">
                                                    <div class="row li_condition_row" id='li_option_row-{{ $count }}' >

                                                        <input type="hidden" class="form-control" id="options[{{ $count }}][id]"  name="options[{{ $count }}][id]" value="{{!empty($record['id']) ? $record['id'] : ''}}">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="name">Outlet</label>
                                                                    <input type="text" class="form-control" id="options[{{ $count }}][name]"  name="options[{{ $count }}][name]" value="{{!empty($record['name']) ? $record['name'] : ''}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-1 float-right">
                                                            <button type="button" class="btn btn-danger btn_remove_added_outlet" data-id="{{!empty($record['id']) ? $record['id'] : ''}}" id="btn_remove_outlet-{{ $count }}">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </li>
                                                @php $count++; $cnt++; @endphp
                                            @endforeach
                                        @else
                                            <li id='li_option-1' class="list-group-item" style="margin-bottom: 10px;">
                                                <div class="row li_condition_row" id='li_option_row-1' >
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="name">Options</label>
                                                                <input type="text" class="form-control" id="options[1][name]"  name="options[1][name]" value="">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-1">
                                                            <button type="button" class="btn btn-danger btn_remove_outlet" id="btn_remove_outlet-1">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                        
                                                </div>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div><!-- END TAB -->
                    <!-- END FORM UPDATE input-field -->
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" >
$(document).ready(function() {

    $('#save-input-field').on('click',function(){
        var data = new FormData($('#save_input_field')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_input_field') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                if(data['result']==1){
                    window.location.href = "{{ URL::route('index_input_field') }}";
                }
                else{
                    alert(data['errMsg']);
                }
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['errors']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
        //return true;
    });

    if($('#data_type_id').val()==73){
        $('.nav-tabs a:last').show();
    }

    $('#data_type_id').on('change',function(){
        var value = $(this).val();

        if(value==73){
            $('.nav-tabs a:last').show();
        }
        else{
            $('.nav-tabs a:last').hide();
        }
        
    });


    $('#deleteModal').on('click','#delete',function(){
        $('#deleteModal').modal('hide');

        var id_array = $('#delete_array_id').val();
        
        var data = new FormData($('#delete_outlet_form')[0]);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('remove_added_outlet') }}",
            data: data,
            dataType: 'json',
            success: function (data) {

                var rowCount = $('#ul_option li').length;
                
                if(rowCount < 2){
                    
                    $("#ul_option #li_option-"+id_array+" .li_condition_row").find('input').val("");

                }else{
                    $('#li_option-'+id_array).remove();
                }
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
       
    });

    $("#btn_add_outlet").click(function(){
        var total_outlet = $('#ul_option li').length;
        
        if (total_outlet < 40) { ///maximum 40 outlets only
            cloneUL('ul_option');
        }
        
    });

    $("#ul_option").on("click", ".btn_remove_outlet", function () {
    
        var rowCount = $('#ul_option li').length;
        var id_array =  $(this).attr('id').split("-");
        
        if(rowCount < 2){
            
            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row").find('input').val("");
        }else{
            $('#li_option-'+id_array[1]).remove();
        }
    });
});

function cloneUL(table_id){ 
    var rowCount = $('#'+table_id+' li').length;
    var clone_ele = $('#'+table_id+' li:last').clone();
    
    clone_ele.find("textarea").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('textarea[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) { 
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('select[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });

    }).end();

    clone_ele.find("input").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) { 
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find("img").each(function () {
        $(this).val('').attr('id', function (_, id) {
            var id_array = id.split("-");
            if (id_array.length > 1) {
                var new_id = id_array[0] + '-' + rowCount;
                while ($('#' + table_id + ' #' + new_id).length) {
                    rowCount++;
                    new_id = id_array[0] + '-' + rowCount;
                }
                return new_id;
            } else {
                return id;
            }
        });
        $(this).attr('name', function (_, id) {
            var id_array = id.split("-");
            if (id_array.length > 1) {
                var new_id = id_array[0] + '-' + rowCount;
                while ($('#' + table_id + ' #' + new_id).length) {
                    rowCount++;
                    new_id = id_array[0] + '-' + rowCount;
                }
                return new_id;
            } else {
                return id;
            }
        });
    }).end();

    clone_ele.find(".li_condition_row").each(function() {
        $(this).attr('id', function(_, id) { 

            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_outlet").each(function() {
        $(this).attr('id', function(_, id) { 
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_added_outlet").each(function() {
        $(this).attr('id', function(_, id) { 
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.attr('id', function(_, id) { 
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    })

    $("#"+table_id+" li:last").after(clone_ele);
    
    clone_ele.attr('id', function(_, id) {
        var id_array = id.split("-");
       
        if(table_id == 'ul_option'){
                                                        
            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row").find('input').val("");
            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row").find('textarea').val("");

            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row #outlets-"+id_array[1]).attr('src','/assets/img/no_img.png');
            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeClass('btn_remove_added_outlet');
            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeAttr("data-id");
            $("#ul_option #li_option-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).addClass('btn_remove_outlet');
        }
    })
}//End Clone Table
</script>

@endsection
