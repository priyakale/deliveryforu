@extends('admin.layout')
 

@section('main-content')


<!-- SEARCH RESULT -->
<div class="container full_width">
       
    <!-- SEARCH RESULT -->
    <div class="row">
        <div class="col-md-12">
            
           <div class="panel panel-custom">
              
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td>
                                Search Result  
                                <span class="badge custom-badge"></span>
                            </td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <a href="{{ URL::route('add_input_field') }}" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span>Add</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="panel-body progress-panel ">
                    <div class="float-left">
                         {{ $data['records']->appends(Request::except('page'))->links() }}    
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (empty($data['records'][0]))
                            <tr>
                                <th colspan="6" class="empty-record text-center"><label>No Result</label></th>
                            </tr>
                        @endif
                        @foreach($data['records'] as $index => $record) 
                            <tr id="carrier{{$record->id}}">
                                <td>{{ $index+1 }}</td>
                                <td>{{ $record['name'] }}</td>
                                <td>{{ $record['status']['name'] }}</td>
                                <td>{{ $record['created_at'] }}</td>
                                <td>
                                    <a href="{{ URL::route('edit_input_field',$record->id) }}" class="btn btn-success btn-sm"><span class="fa fa-pencil-alt"></span></a>
                                    <button type="button" class="btn btn-danger btn-sm btn-delete" 
                                    data-id="{{ $record->id }}" >
                                    <span class="fa fa-trash"></span>
                                    </button>  
                                </td>                                   
                            </tr> 
                        @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{ $data['records']->appends(Request::except('page'))->links() }}    
                    </div>
                    <div class="modal fade" id="deleteModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="deleteModalLabel">Delete</h4>
                                </div>
                                <div class="modal-body" id="deleteModal_body">
                                    <h5>Are you sure want to delete this input field?</h5>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" id="delete_id" />
                                    <button class="btn btn-danger" id="delete">Delete</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                      
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@push('custom_script')
<!-- SCRIPT -->
<script>
$(document).ready(function(){
    $('.btn-delete').on('click', function(e) {
        var id = $(this).data('id');
        $('#delete_id').val(id);
        $('#deleteModal').modal('show');
    });
    $('#delete').on('click', function(e) {
        var id = $('#delete_id').val();
        // AJAX
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'GET',
            contentType: false,
            processData: false,
            url : "{{ URL::route('delete_input_field') }}/" + id,
            dataType: 'json',
            success: function (data) {
                window.location.href = "{{ URL::route('index_input_field') }}";
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['errors']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
        // END AJAX
    });
    $('#valuereset').click(function(){
        window.location.href = "{{URL::route('index_input_field')}}" 
    });
});
</script> 
<!-- END SCRIPT --> 
@endpush