@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<!-- Ionicons -->
<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet" type="text/css" />

<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
        <form class="" role="update" name="save_notification" id="save_notification" 
                    action="{{ URL::route('save_notification') }}" method="POST">
            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td></td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <button type="button" class="float-right btn btn-success" id="save-notification">
                                            <i class="fa fa-save"></i>Save
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#email">Email</a></li>
                    </ul>
                    <!-- FORM UPDATE NOTIFICATION -->
                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row tab-content" >
                            <!-- START GENERAL TAB-->
                            <div class="col-md-12 tab-pane fade in active" 
                            id="email">
                                <div class="widget widget-default widget-item-icon padding-left">
                                    <div class='row float-left detaill-inner full_width'>
                                        <div class='row padding-top'>
                                            <div class="col-lg-2"><label for="subject">Subject</label></div>
                                            <div class="col-lg-10">
                                            <input type="hidden" class="form-control" name="id" id='id' value="{!! (!empty($data['records']['id']) ? $data['records']['id'] : '') !!}"  >
                                            <input type="text" class="form-control" placeholder="Subject" 
                                            name="subject" id='subject' value="{!! (!empty($data['records']['subject']) ? $data['records']['subject'] : '') !!}" required >
                                            </div>
                                        </div>
                                        <div class='row padding-top'>
                                            <div class="col-lg-2"><label for="name">Status</label></div>
                                            <div class="col-lg-3">
                                                <select name="status_id" id="status_id" class="form-control chosen-select">
                                                    <option value="">Select Status</option>
                                                    @if(!empty($data['status_records']))
                                                        @foreach($data['status_records'] as $key => $value )
                                                          <option value="{{ $value['id'] }}" {{ !empty($data['records']['status_id']['id']) && ($data['records']['status_id']['id']==$value['id']) ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class='row padding-top'>
                                            <div class="col-lg-2"><label for="type_id">Email For</label></div>
                                            <div class="col-lg-3">
                                                <select name="email_for_id" id="email_for_id" class="form-control chosen-select">
                                                    <option value="">Select Email</option>
                                                    @if(!empty($data['email_for'][0]))
                                                        @foreach($data['email_for'] as $key => $value )
                                                          <option value="{{ $value['id'] }}" {{ !empty($data['records']['email_for_id']['id']) && ($data['records']['email_for_id']['id']==$value['id']) ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                
                                            </div>
                                        </div>
                                        <div class='row padding-top'>
                                            <div class="col-lg-2"><label for="description">Description</label></div>
                                            <div class="col-lg-8">
                                                <textarea rows="30" class="form-control text-area-edit" name="description" id='description' >{!! (!empty($data['records']['description']) ? $data['records']['description'] : '') !!}</textarea>
                                            </div>
                                            <div class="col-lg-2">
                                                <label for="description">Available Variables for Description</label>
                                                <p>%NAME</p>
                                                <p>%BUTTON</p>
                                                <p>%EMAIL</p>
                                            </div>
                                        </div>
                                    </div>             
                                </div>
                            </div>
                            <!-- END GENERAL TAB -->
                        </div><!-- END TAB -->
                    <!-- END FORM UPDATE NOTIFICATION -->
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" >
$(document).ready(function() {
    $("#selectAll").on( "click",function() {
        var ischecked= $("#selectAll").is(':checked');
        $("input[name='role_ids[]']").each(function () {
            if (ischecked) {
                $(this).prop('checked', true);
            }
            else{
                $(this).prop('checked', false);
            }
        });
    });

    $('#save-notification').on('click',function(){
        var data = new FormData($('#save_notification')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_notification') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if(data['result']){
                   window.location.href = "{{ URL::route('index_notification') }}";
                }else{
                    alert('Fail : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
        //return true;
    });
});

</script>

<!-- role js -->
<script src="{!!asset('/js/summernote.min.js')!!}"></script>
<script src="{!!asset('/js/editor.js')!!}"></script>

@endsection
