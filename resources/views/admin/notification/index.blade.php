@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<!-- SEARCH RESULT -->
<div class="container full_width">
	<div class="row">
		<div class="col-md-12">
			<div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    {{ Form::open(array('url'=> URL::route("index_notification") 
                    , 'method' => 'GET', 'id'=>'searchform')) }}
                    	<div class="input-group form-width-full padding-top">
                    		<div class="input-group form-width-full">
								<div class="col-md-3">
		                            <label for="subject" >Subject</label>
	                         		<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" 
	                         		value="{{ !empty($data['search']['subject']) ? $data['search']['subject'] : '' }}" >
		                        </div>
		                        <div class="col-md-3">
		                            <label for="status">Status</label>
									<select name="status" id="status" class="form-control chosen-select">
										<option value="">Select Status</option>
										<option value="A" {{ !empty($data['search']['status']) && ($data['search']['status']=='A') ? 'selected' : '' }}>Active</option>
										<option value="D" {{ !empty($data['search']['status']) && ($data['search']['status']=='D') ? 'selected' : '' }}>Disable</option>
									</select>
				                </div>
							</div>
                    	</div>
                        <div class="input-group form-width-full padding-top padding-bottom padding-right">
                            <div class="form-group float-right">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-success">
                                    <i class="fa fa-search"></i>Search</button>
                                    &nbsp;&nbsp;
                                    <button type="button" id="valuereset" class="btn btn-defult">
                                    <i class="fa fa-times"></i>Reset</button>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>                
            </div>  
	    </div>
	</div>
	<!-- END SEARCH CRITERIA -->
	<!-- SEARCH RESULT -->
	<div class="row">
	    <div class="col-md-12">
	       <div class="panel panel-custom">
				<div class="panel-heading panel-heading-custom">
	                <table class="full_width">
	                    <tr>
	                        <td>
	                            Search Result  
	                            <span class="badge custom-badge"></span>
	                        </td>
	                        <td>
	                            <div class="form-group float-right">
	                                <div class="btn-group input-group">
	                                    <a href="{{ URL::route('add_notification') }}" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span>Add</a>
	                                </div>
	                            </div>
	                        </td>
	                    </tr>
	                </table>
	            </div>
	            <div class="panel-body progress-panel ">
	            	<div class="float-left">
	                	 {{ $data['records']->appends(Request::except('page'))->links() }}    
	                </div>
	               	<table class="table table-hover">
						<thead>
						  	<tr>
								<th>No.</th>
								<th>Subject</th>
								<th>Email for</th>
								<th>Status</th>
								<th>Action</th>
						  	</tr>
						</thead>
						<tbody>
						@if (empty($data['records'][0]))
	                        <tr>
	                            <th colspan="6" class="empty-record text-center"><label>No Result</label></th>
	                        </tr>
	                    @endif
						@foreach($data['records'] as $index => $record)	
							<tr id="carrier{{$record->id}}">
								<td>{{ $index+1 }}</td>
								<td>{{ $record->subject }}</td>
								<td>{{ $record->email_for_id->name }}</td>
								<td>{{ $record->status_id->name }}</td>
								<td>
									<a href="{{ URL::route('edit_notification',$record->id) }}" class="btn btn-success btn-sm"><span class="fa fa-pencil-alt"></span></a>
									<button type="button" class="btn btn-danger btn-sm btn-delete" 
									data-id="{{ $record->id }}" >
									<span class="fa fa-trash-alt"></span>
									</button>  
								</td>									
							</tr> 
					 	@endforeach
						</tbody>
					</table>
					<div class="float-right">
	                	{{ $data['records']->appends(Request::except('page'))->links() }}    
	                </div>
	                <div class="modal fade" id="deleteModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
					    <div class="modal-dialog" role="document">
						    <div class="modal-content">
						        <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title" id="deleteModalLabel">Delete</h4>
						        </div>
						        <div class="modal-body" id="deleteModal_body">
						      	    <h5>Are you sure want to delete this notification?</h5>
						        </div>
						        <div class="modal-footer">
						        	<input type="hidden" id="delete_id" />
						      	    <button class="btn btn-danger" id="delete">Delete</button>
					                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					            </div>
						    </div>
					    </div>
	        	    </div>
	        	</div>
	    	</div>
		</div>
	</div>
</div>
<!-- END SEARCH RESULT -->
<!-- SCRIPT -->
<script>
$(document).ready(function(){
  	$('.btn-delete').on('click', function(e) {
    	var id = $(this).data('id');
    	$('#delete_id').val(id);
    	$('#deleteModal').modal('show');
	});
	$('#delete').on('click', function(e) {
    	var id = $('#delete_id').val();
    	// AJAX
  	    $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'GET',
      	    contentType: false,
  		    processData: false,
           	url : "{{ URL::route('delete_notification') }}/" + id,
            dataType: 'json',
            success: function (data) {
            	if(data['result']){
            		location.reload();
            	}else{
            		alert(data['errMsg']);
            	}
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
  	    // END AJAX
	});
	$('#valuereset').click(function(){
        window.location.href = "{{URL::route('index_notification')}}" 
    });
});
</script> 
<!-- END SCRIPT --> 

@endsection