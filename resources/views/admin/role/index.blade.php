@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<div class="container full_width">
    <div class="row">
        <div class="col-md-3">
            <!-- START USER-->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $data['total_role'] }}</div>
                    <div class="widget-title">Role</div>
                    <div class="widget-subtitle">Total : </div>
                </div>                
            </div>                            
            <!-- END USER --> 
        </div>
         <div class="col-md-9">
            <!-- START SERARCH CATERIA -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    <form class="padding-right" role="search">
                        <div class="row col-lg-12">
                            <div class="col-md-6">
                                <label for="keyword">Keyword</label>
                                <input type="text" class="form-control" placeholder="Search Keyword" name="keyword" value="{{ !empty($data['input']['keyword']) ? $data['input']['keyword'] : '' }}" >
                            </div>
                            <div class="col-md-4 padding-left-zero">
                                <label for="status_id">Status</label>
                                <select name="status_id" id="status_id" class="form-control chosen-select">
                                    <option value="">Select Status</option>
                                    @if(!empty($data['status_records']))
                                        @foreach($data['status_records'] as $key => $value )
                                          <option value="{{ $value->id }}" {{ !empty($data['input']['status_id']) && ($data['input']['status_id']==$value['id']) ? 'selected' : '' }}>{{ $value->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="input-group form-width-full padding-top padding-bottom padding-right">
                            <div class="form-group float-right">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-success">
                                    <i class="fa fa-search"></i>Search</button>
                                    &nbsp;&nbsp;
                                    <button type="button" id="valuereset" class="btn btn-defult">
                                    <i class="fa fa-times"></i>Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>                            
            <!-- END SERARCH CATERIA --> 
        </div>
    </div>
     <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-custom">
                    <div class="panel-heading panel-heading-custom">
                        <table class="full_width">
                            <tr>
                                <td>
                                    Search Result  
                                    <span class="badge custom-badge">{{ $data['records']->total() }}</span>
                                </td>
                                <td>
                                    <div class="form-group float-right">
                                        <div class="btn-group input-group">
                                            <a 
                                            class="float-right btn btn-success" id="btn_add" 
                                            href="{{ URL::route('add_role') }}">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body">
                        <div class="float-left">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                        <table class="table table-hover"> 
                            <thead> 
                                <tr> 
                                    <th>No.</th> 
                                    <th>Role Name</th>
                                    <th>Total User</th>
                                    <th>Last Updated</th> 
                                    <th class="text-center">Status</th> 
                                </tr> 
                            </thead> 
                            <tbody>
                                @if (empty($data['records'][0]))
                                    <tr>
                                        <th colspan="6" class="empty-record text-center"><label>No Result</label></th>
                                    </tr>
                                @endif
                                @php
                                    $count = 1;
                                @endphp
                                @foreach($data['records'] as $record)
                                <tr>
                                    <td>{{ $count++ }}</td>
                                    <td>
                                        <a href="{{ URL::route('edit_role').'/'.$record->id }}">{{ $record->name }}</a>
                                    </td>
                                    <td class="text-center">{{ $record->total_user }}</td>
                                    <td>{{ $record->updated_at_value }}</td>
                                    <td class="text-center status-{{ $record->status_id->name }}">
                                        {{ $record->status_id->name }}
                                    <td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript" >
$( document ).ready(function() {
    $('#valuereset').click(function(){
        window.location.href = "{{URL::route('index_role')}}/";
    });
});
</script>
@endsection
