@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<div class="container full_width">
     <div class="row">
        <div class="col-lg-12">
            <form class="" role="save" name="save_user" id="save_user" 
            action="{{ URL::route('save_role') }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="panel panel-custom">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-9">
                                <!-- START ROLE DETAIL-->
                                <div class="widget widget-default widget-item-icon" >
                                    <div class="widget-item-left">
                                        <span class="fa fa-file"></span>
                                    </div>
                                    <div class="widget-data">
                                        <div class='row float-left detaill-inner full_width'>
                                            <div class='row padding-top'>
                                                <div class="col-lg-2"><label for="name">Role Name</label></div>
                                                <div class="col-lg-7">
                                                <input type="hidden" class="form-control" 
                                                name="role_id" id='role_id' value="{!! (!empty($data['record']->id) ? $data['record']->id : '') !!}"  >
                                                <input type="text" class="form-control" placeholder="Name" 
                                                name="name" id='name' value="{!! (!empty($data['record']->name) ? $data['record']->name : '') !!}" required >
                                                </div>
                                            </div>
                                            <div class='row padding-top'>
                                                <div class="col-lg-2"><label for="name">Status</label></div>
                                                <div class="form-group col-lg-10">
                                                    <select name="status_id" id="status_id" class="form-control chosen-select">
                                                        <option value="">Select Status</option>
                                                        @if(!empty($data['general_status']))
                                                            @foreach($data['general_status'] as $key => $value )
                                                              <option value="{{ $value['id'] }}" {{ !empty($data['record']['status_id']) && ($data['record']['status_id']==$value['id']) ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class='row padding-top'>
                                                <div class="col-lg-2"><label for="description">Description</label></div>
                                                <div class="col-lg-9">
                                                <textarea rows="2" class="form-control" name="description" id='description' >{!! (!empty($data['record']->description) ? $data['record']->description : '') !!}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                
                                </div>                            
                                <!-- END ROLE DETAIL --> 
                            </div>
                            <div class="col-md-3">
                                <!-- USER COUNT BASE ON ROLE -->
                                <div class="widget widget-default widget-item-icon" >
                                    <div class="widget-item-left">
                                        <span class="fa fa-users"></span>
                                    </div>
                                    <div class="widget-data">
                                        <div class="widget-int num-count">
                                        {!! (!empty($data['record']->total_user) ? $data['record']->total_user : 0) !!}
                                        </div>
                                        <div class="widget-title">Total User</div>
                                    </div>                
                                </div>                            
                                <!-- END USER COUNT BASE ON ROLE --> 
                            </div>
                        </div> <!-- END ROLE -->
                        <div class="panel-heading panel-heading-custom">
                            <table class="full_width">
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="form-group float-right">
                                            <div class="btn-group input-group">
                                                <button type="submit" class="float-right btn btn-success" id="btn-save-form">
                                                    <i class="fa fa-save"></i>Save
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        @if(!empty($data['record']['id']))
                        <div class="row">
                            <div class="col-md-12">
                                <!-- START ROLE DETAIL-->
                                <a class="access-edit" href="javascript:void(0)" title="Add Function" id="btn_add_function_menu">
                                    <button type="button" class="float-right btn btn-success">
                                        Add Function
                                    </button>
                                </a>
                                <div class="panel-body">
                                    <ul class="nav nav-tabs">
                                        <li data-value="function" class="active"><a data-toggle="tab" class="tabs" href="#function_tab">Functionality</a></li>
                                        <li data-value="menu"><a data-toggle="tab" class="tabs" href="#menu_tab">Menu Accessible</a></li>
                                    </ul>
                                    <!-- FORM UPDATE PROMOTION -->
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row tab-content" >
                                            <!-- START GENERAL DETAIL-->
                                            <div class="col-md-12 tab-pane fade in active" 
                                            id="function_tab">
                                                <div class="widget widget-default widget-item-icon padding-left">
                                                    <div class='row float-left detaill-inner full_width'>
                                                        <!-- TEMPORARY CSS CLASS CSS CLASS--> 
                                                        <div class="col-md-12">
                                                            @if(!empty($data['controller_role_detail_records']))
                                                              @foreach($data['controller_role_detail_records'] as $record_controller_key => $record_controller_value)
                                                                <div class="row" style="margin-bottom: 10px;">
                                                                    <div class="col-md-12">
                                                                        <h3 class="controler_title">
                                                                          <a href="JavaScript:void(0);">{{ $record_controller_key  }}</a>
                                                                          <a class="function-action" href="javascript:void(0)" title="Delete All Function" data-value="{{ $record_controller_key  }}">
                                                                            <i class="fa fa-trash"></i>
                                                                          </a>
                                                                        </h3>
                                                                        <p class="scrum_task_info"><span class="uk-text-muted">Action:</span>
                                                                          <a href="JavaScript:void(0);" class="showActionList" data-value="{{ $record_controller_key  }}">
                                                                            <span id="div_action_text-{!! $record_controller_key !!}">View More</span>
                                                                          </a>
                                                                        </p>
                                                                        <div id="div_action-{{ $record_controller_key }}" hidden>
                                                                            <div class="row">
                                                                                @foreach ($record_controller_value as $value)
                                                                                <div class="col-md-4">
                                                                                    <div class="scrum_task">
                                                                                        {{ $value['function'] }}
                                                                                        <a class="md-fab md-fab-small md-fab-danger md-action-sub_function function-action-sub" href="javascript:void(0)" style="float: right;"
                                                                                        data-uk-tooltip="{pos:'bottom'}" title="Delete Selected Function" data-value="{{ $value['id'] }}">
                                                                                          <i class="fa fa-trash"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                              @endforeach
                                                            @else
                                                              <div class="scrum_task minor" style="margin-bottom: 10px; background: beige;">
                                                                  <h3 class="controler_title" style="text-align: center;">
                                                                    <a href="JavaScript:void(0);" data-uk-modal="{ center:true }">Empty Record</a>
                                                                  </h3>
                                                              </div>
                                                            @endif
                                                        </div>
                                                        <!-- TEMPORARY CSS CLASS --> 
                                                    </div>             
                                                </div>
                                            </div>
                                            <!-- END GENERAL DETAIL -->
                                            <!-- START DESCRIPTION DETAIL-->
                                            <div class="col-md-12 tab-pane fade in" 
                                            id="menu_tab">
                                                <div class="widget widget-default widget-item-icon padding-left">
                                                    <div class='row float-left detaill-inner full_width'>
                                                        <!-- TEMPORARY CSS CLASS --> 
                                                        <div class="scrum_column">
                                                            @if(!empty($data['menu_list_records']) && sizeof($data['menu_list_records']) > 0)
                                                              @foreach($data['menu_list_records'] as $menu_list_record)
                                                                <div class="scrum_task minor" style="margin-bottom: 10px;">
                                                                    <h3 class="controler_title">
                                                                      <a href="JavaScript:void(0);" data-uk-modal="{ center:true }">
                                                                        {{ $menu_list_record->menu_value }}
                                                                      </a>
                                                                      <a class="md-fab md-fab-small md-fab-danger md-action-sub menu-action" href="javascript:void(0)"
                                                                      data-uk-tooltip="{pos:'bottom'}" title="Delete Selected Menu" data-value="{{ $menu_list_record->id }}">
                                                                        <i class="material-icons"></i>
                                                                      </a>
                                                                    </h3>
                                                                    <p class="scrum_task_info"><span class="uk-text-muted">Action:</span>
                                                                      <a class="role_menu_action" href="JavaScript:void(0);"
                                                                      data-uk-modal="{target:'#model_access',bgclose:false}"
                                                                      data-value="{{ $menu_list_record->id }}">
                                                                        <span>Edit Detail</span>
                                                                      </a>
                                                                    </p>
                                                                     <div class="row padding-top">
                                                                        
                                                                          <div class="col-lg-12">
                                                                            <label for="position">Position:</label>{{ $menu_list_record->position }}        
                                                                        </div>
                                                                     </div>
                                                                </div>
                                                              @endforeach
                                                            @else
                                                              <div class="scrum_task minor" style="margin-bottom: 10px; background: beige;">
                                                                  <h3 class="controler_title" style="text-align: center;">
                                                                    <a href="JavaScript:void(0);" data-uk-modal="{ center:true }">Empty Record</a>
                                                                  </h3>
                                                              </div>
                                                            @endif
                                                        </div>
                                                        <!-- TEMPORARY CSS CLASS --> 
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END DESCRIPTION DETAIL -->
                                        </div><!-- END TAB -->
                                    <!-- END FORM UPDATE PROMOTION -->
                                </div>                    
                                <!-- END ROLE DETAIL --> 
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODEL POPUP FOR ADD AND EDIT -->
<div class="modal fade" role="dialog" id="model_access" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-body" id="model_content_access">

            </div>
        </div>
    </div>
</div>
<!-- END MODEL POPUP FOR ADD AND EDIT -->


<script type="text/javascript" >
      $(document).ready(function() {

        $('.access-edit').on('click',function(){
            var value = $('.nav-tabs').find('li.active').attr("data-value");
            if(value == 'function'){
              renderAccessDetail();
            }else if(value == 'menu'){
              renderMenuDetail(0); //New Role Menu
            }
        });//end Click

        $('.role_menu_action').on('click',function(){
            var value = $(this).attr("data-value");
            renderMenuDetail(value); //New Role Menu
        });//end Click

        $('.menu-action').on('click',function(){
            var menuRole_id = $(this).attr("data-value");
            var role_id = $('#role_id').val();
            if (confirm("Are you sure to delete this role menu ?") == true) {
              $.ajax({
                  type: 'POST',
                  url : "{{ URL::route('delete_role_menu') }}",
                  data : { 'id': menuRole_id , 'role_id' : role_id ,'_token' : '{{ csrf_token() }}'},
                  dataType: 'json',
                  success: function (data) {
                      if(data['result']){
                        window.location.href = "{{ URL::route('edit_role') }}/"+role_id;
                      }else{
                          alert('Fail to delete role menu : ' + data['errMsg']);
                      }
                  },
                  error: function (data) {
                      console.error('Error:', data);
                  }
              });
            }
        });//end Click

        $('.function-action').on('click',function(){
            var function_name = $(this).attr("data-value");
            var role_id = $('#role_id').val();
            if (confirm("Are you sure to delete this role function ?") == true) {
              $.ajax({
                  type: 'POST',
                  url : "{{ URL::route('delete_role_function') }}",
                  data : { 'function_name': function_name , 'role_id' : role_id ,'_token' : '{{ csrf_token() }}'},
                  dataType: 'json',
                  success: function (data) {
                      if(data['result']){
                        window.location.href = "{{ URL::route('edit_role') }}/"+role_id;
                      }else{
                          alert('Fail to delete role function : ' + data['errMsg']);
                      }
                  },
                  error: function (data) {
                      console.error('Error:', data);
                  }
              });
            }
        });//end Click

        $('.function-action-sub').on('click',function(){
            var id = $(this).attr("data-value");
            var role_id = $('#role_id').val();
            if (confirm("Are you sure to delete this role selected function ?") == true) {
              $.ajax({
                  type: 'POST',
                  url : "{{ URL::route('delete_role_function_selected') }}",
                  data : { 'id': id , 'role_id' : role_id ,'_token' : '{{ csrf_token() }}'},
                  dataType: 'json',
                  success: function (data) {
                      if(data['result']){
                        window.location.href = "{{ URL::route('edit_role') }}/"+role_id;
                      }else{
                          alert('Fail to delete role selected function: ' + data['errMsg']);
                      }
                  },
                  error: function (data) {
                      console.error('Error:', data);
                  }
              });
            }
        });//end Click
    
        $(".tabs").click(function() {
            var value = $(this).parent('li').attr("data-value");
            if(value == 'function'){
              $('#btn_add_function_menu button').text('Add Function');
            }else if(value == 'menu'){
              $('#btn_add_function_menu button').text('Add Menu');
            }
        });

        $("#btn_add_function_menu").mouseover(function() {
            var value = $('.nav-tabs').find('li.active').attr("data-value");
            if(value == 'function'){
              $('#btn_add_function_menu').prop('title', 'Add Function');
              $('#btn_add_function_menu button').text('Add Function');
            }else if(value == 'menu'){
              $('#btn_add_function_menu').prop('title', 'Add Menu');
              $('#btn_add_function_menu button').text('Add Menu');
            }
        });

        $(".showActionList").click(function() {
            var id = $(this).attr('data-value').replace(/\\/g, "\\\\$&");
            var element = $('#div_action-'+id);
             if(element.is(':visible')){
               element.hide();
               $('#div_action_text-'+id).text('View More');
             }else{
               element.show();
               $('#div_action_text-'+id).text('Hide Detail');
             }
        });


      });//End Hover

      function renderAccessDetail(){
        $.ajax({
            type: 'GET',
            url : "{{ URL::route('add_function_model') }}",
            data: { '_token' : '{{ csrf_token() }}' },
            dataType: 'json',
            success: function (data) {
                if(data['result']){
                    $('#model_content_access').html(data['returnHTML']);
                    $("#model_access").modal();
                }else{
                    alert('Fail to show Access : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
      }

      function renderMenuDetail(id){
        $.ajax({
            type: 'GET',
            url : "{{ URL::route('add_menu_model') }}/"+id,
            data: { '_token' : '{{ csrf_token() }}' },
            dataType: 'json',
            success: function (data) {
                if(data['result']){
                    $('#model_content_access').html(data['returnHTML']);
                    $("#model_access").modal();
                }else{
                    alert('Fail to show Menu Role : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
      }
</script>

<!-- role js -->
<script src="{{ asset('/js/role.js') }}" type="text/javascript"></script>
@endsection
