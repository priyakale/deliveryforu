<section class="content-header"><h1>{{ $data['page_title'] }}</h1></section>

<div class="model-menu" style="margin-left: auto; margin-right: auto; width: 90%; padding-top: 3%;">
    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ URL::route('save_role_menu') }}" id='menu_form'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="row padding-top">
            <div class="col-lg-12">
                <label for="parent_id">Menu</label>
                <input type="hidden" id="id" name="id" value="{{ !empty($data['record']['id']) ? $data['record']['id'] : '' }}" />
                <select name="menu_id" id="menu_id" class="form-control chosen-select">
                    <option value="">Select Menu *</option>
                    @if(!empty($data['menu_records']))
                      @foreach($data['menu_records'] as $key => $value )
                        <option value="{{ $value->id }}"
                        {!! (!empty($data['record']['menu_id']) && $data['record']['menu_id'] == $value->id ? 'selected' : '') !!}>{{ $value->name }}</option>
                      @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row padding-top">
            <div class="col-lg-12">
                <label for="position">Position *</label>
                <input type="text" class="md-input" name="position" id="position" value="{!! (!empty($data['record']['position']) ? $data['record']['position'] : '') !!}">
            </div>
        </div>
        

        <div class="row padding-top text-right">
            <div class="btn-group">
                <button type="button" class="btn btn-default" id="btn-close" data-dismiss="modal">Close</button>
                @if(empty($data['record']['id']))
                  <button type="button" class="btn btn-success save_role_menu" id="btn-create">New Role Menu</button>
                @else
                  <button type="button" class="btn btn-success save_role_menu" id="btn-create">Update Role Menu</button>
                @endif
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" >
    $( document ).ready(function() {
      
      $('.save_role_menu').on('click',function(){
          var role_id = $('#role_id').val();
          var menu_id = $('#menu_id :selected').val();
          var position = $('#position :selected').val();
          varifyRoleMenu(role_id,menu_id, position);
      });//end Click


    });//end document ready

    function varifyRoleMenu(role_id,menu_id, position){
      if(role_id == ''){
        alert('Error: No role selected!');
        return false;
      }else if(menu_id == ''){
        alert('Please select menu');
        return false;
      }else if(position == ''){
        alert('Please fill in position of menu');
        return false;
      }else{
        var data = new FormData($('#menu_form')[0]);
        data.append('role_id', role_id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_role_menu') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                if(data['result']){
                   window.location.href = "{{ URL::route('edit_role') }}/"+role_id;
                }else{
                    alert('Fail : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
      }
    }
</script>
