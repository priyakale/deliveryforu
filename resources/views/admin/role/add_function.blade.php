<section class="content-header"><h1>{{ $data['page_title'] }}</h1></section>

<div class="model-menu" style="margin-left: auto; margin-right: auto; width: 90%; padding-top: 3%;">
    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ URL::route('save_role_function') }}" id='function_form'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="row padding-top">
            <div class="col-lg-12">
                <label for="controller">Controller *</label>
                <select name="controller"
                  id="controller" class="form-control">
                    <option value="">Select Controller</option>
                    @foreach($data['link_records'] as $key => $value )
                     <option value="{{ $key }}">{{ $key }}</option>
                    @endforeach
                  </select>
            </div>
        </div>
        <div class="row padding-top">
            <div class="col-lg-12">
                <label for="function">Function *</label>
                <select name="function" id="function" class="form-control chosen-select">
                    <option value="">Select Function</option>
                </select>
            </div>
        </div>
        

        <div class="row padding-top text-right">
            <div class="btn-group">
                <button type="button" class="btn btn-default" id="btn-close" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btn-create">New Function</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" >
var array_link = {};
@foreach( $data['link_records'] as $key_controller => $value_function)
    array_link.{{ $key_controller }} = [];
    @foreach($value_function as $key => $value)
        array_link.{{ $key_controller }}.push('{{ $value }}');
    @endforeach
@endforeach

$( document ).ready(function() {

    $('#controller').change(function() {
        setValueAction(this.value);
    });

    $("#btn-close").click(function() {
        $('#menu-model').modal('hide');
    });

    $('#btn-create').on('click',function(){
        var role_id = $('#role_id').val();
        var controller = $('#controller :selected').val();
        var action = $('#function :selected').val();

        if($.trim(controller) == ''){
            alert("Please make sure to select controller!");
            return false;
        }else if($.trim(action) == ''){
            alert("Please make sure to select action!");
            return false;
        }else{
            var data = new FormData($('#function_form')[0]);
            data.append('role_id', role_id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                contentType: false,
                processData: false,
                url : "{{ URL::route('save_role_function') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if(data['result']){
                        window.location.href = "{{ URL::route('edit_role') }}/"+role_id;
                    }else{
                        alert('Fail : ' + data['errMsg']);
                    }
                },
                error: function (data) {
                    console.error('Error:', data);
                }
            });
            //return true;
        }
        });
    });//end document ready

    function setValueAction(control_selected){
        $("#function option").remove();
        setTimeout(
        function(){
            $("#function").append('<option value="">Select Function</option>');
            $("#function").append('<option value="ALL">All</option>');
            if(array_link.hasOwnProperty(control_selected)){
            var dataTmp = [];
            dataTmp.push({'id': 'ALL' , 'title': 'All'});
                $.each(array_link[control_selected], function( index, value ) {
                $("#function").append('<option ' +'value="'+value+'">'+ value +'</option>');
                dataTmp.push({'id': value , 'title': value});
                });
            }
        }, 10);
    }//End set value link


</script>
