<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('admin.partials.htmlheader')
@show

<body class="skin-{{ SettingHelper::getSkin() }} sidebar-mini">
<div class="loader-container">
    <div class="loader">
      <div class="side"></div>
      <div class="side"></div>
      <div class="side"></div>
      <div class="side"></div>
      <div class="side"></div>
      <div class="side"></div>
      <div class="side"></div>
      <div class="side"></div>
    </div>
</div>
<div class="wrapper">

    @include('admin.partials.mainheader')

    @include('admin.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('admin.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            @include('admin.partials.alertinfo')
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    @include('admin.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('admin.partials.scripts')

@show
    @stack('custom_script')

</body>
</html>
