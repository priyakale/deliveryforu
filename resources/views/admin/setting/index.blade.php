@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<div class="container full_width">
	<div class="row">
		<div class="panel panel-custom">
			<div class="panel-heading panel-heading-custom">
				{{ Form::open(array('url'=> URL::route("save_setting") 
	                , 'method' => 'POST', 'id'=>'settingform','enctype' => 'multipart/form-data','files' => 'true')) }}
                <table class="full_width">
                    <tr>
                        <td>
                            <div class="form-group float-right">
                                <div class="btn-group input-group">
                                    <button type="submit" class="float-right btn btn-success">
                                    	<i class="fa fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
		<div class="col-lg-4">
			<div class="row well">
				<legend style="margin-bottom: 8px !important;">
		            <a href="javascript:void(0)">
		            <i class="glyphicon glyphicon-briefcase"></i>
		            </a> Project
	            </legend>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-12">
						<label for="icon" >Application icon</label>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="btn-group" role="group" aria-label="...">
								<label class="btn btn-default">
								Local <input name="icon" id="icon" type="file" accept="image/*" style="display: none;"/>
								</label>
							</div>
							<img id="image" width="60" height="60" src="{!! (!empty($data['setting']['icon_name']) ? '/img/'.$data['setting']['icon_name'] : '/img/no_img.png') !!}"/>
               				<label for="selectedURL" id="selectedURL">{!! (!empty($data['setting']['icon_name']) ? $data['setting']['icon_name'] : 'no_img.png') !!}</label>
						</div>
					</div>
				</div> 	
				<div class="form-group{{ $errors->has('app_name') ? ' has-error' : '' }}">
	              	<label for="app_name" >Application Name</label>
	              	<input type="hidden" name="id" value="{{ $data['setting']['id'] }}">
	              	<input type="text" class="form-control" placeholder="Application Name" id="app_name" name="app_name" value="{!! ($data['setting']['app_name'] ? $data['setting']['app_name'] : '') !!}" required>       
	              	@if ($errors->has('app_name'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('app_name') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="row well"  style="margin-left:0.5%; margin-right:0.5%;">
	          	<legend style="margin-bottom: 8px !important;">
		            <a href="javascript:void(0)">
		            <i class="fa fa-columns" aria-hidden="true"></i>
		            </a> Layout
	            </legend>
	          	<div class="form-group{{ $errors->has('skin') ? ' has-error' : '' }}">
					<div class="row">
		          		<div class="col-lg-6">
							<label for="skin">Layout Colour</label>
							<select name="skin" id="skin" class="form-control chosen-select" >
								<option value="">Select Colour</option>
								@foreach($data['skin_colours'] as $index => $record)
	                            	<option value="{{ $record->value }}" {{ !empty($data['setting']['skin']) && ($data['setting']['skin']==$record->value) ? 'selected' : '' }}>{{ $record->value }}</option>
								@endforeach
							</select>
							@if ($errors->has('skin'))
								<span class="help-block">
								<strong>{{ $errors->first('skin') }}</strong>
								</span>
							@endif  
						</div>
						<div class="col-lg-6">
							<label for="platform">Preview</label><br>
							<label for="previewColor" id="previewColor" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	</label>
						</div>
					</div>
				</div>
				<div class="form-group{{ $errors->has('datetime_format') ? ' has-error' : '' }}">
					<div class="row">
		          		<div class="col-lg-6">
							<label for="datetime_format">Time Format</label>
							<input type="text" class="form-control" placeholder="d/m/YYYY H:i:s" id="datetime_format" name="datetime_format" value="{!! ($data['setting']['datetime_format'] ? $data['setting']['datetime_format'] : '') !!}" required>
							@if ($errors->has('datetime_format'))
								<span class="help-block">
								<strong>{{ $errors->first('datetime_format') }}</strong>
								</span>
							@endif  
						</div>
						<div class="col-lg-6">
							<label for="platform">Preview</label><br>
							<label for="previewFormat" id="previewFormat"></label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="row well" >
				<legend style="margin-bottom: 8px !important;">
		            <a href="javascript:void(0)">
		           	<i class="fa fa-question-circle" aria-hidden="true"></i>
		            </a>Other
	            </legend>
				<div class="form-group{{ $errors->has('owner') ? ' has-error' : '' }}">
	              	<label for="owner" >Owner Name</label>
	              	<input type="text" class="form-control" placeholder="Owner Name" id="owner" name="owner" value="{!! ($data['setting']['owner'] ? $data['setting']['owner'] : '') !!}" required>
	              	@if ($errors->has('owner'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('owner') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
	          	<div class="form-group{{ $errors->has('version') ? ' has-error' : '' }}">
	              	<label for="version" >Version</label>
	              	<input type="text" class="form-control" placeholder="Version" id="version" name="version" value="{!! ($data['setting']['version'] ? $data['setting']['version'] : '') !!}" required>
	              	@if ($errors->has('version'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('version') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
	          	<div class="form-group{{ $errors->has('per_page') ? ' has-error' : '' }}">
	              	<label for="per_page" >Per Page</label>
	              	<input type="number" class="form-control" placeholder="Per Page" id="per_page" name="per_page" value="{!! ($data['setting']['per_page'] ? $data['setting']['per_page'] : '') !!}" required>
	              	@if ($errors->has('per_page'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('per_page') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>
	          <!--	<div class="form-group{{ $errors->has('per_page_api') ? ' has-error' : '' }}">
	              	<label for="per_page_api" >Per Page Api</label>
	              	<input type="number" class="form-control" placeholder="Per Page Api" id="per_page_api" name="per_page_api" value="{!! ($data['setting']['per_page_api'] ? $data['setting']['per_page_api'] : '') !!}" required>
	              	@if ($errors->has('per_page_api'))
	                  	<span class="help-block">
	                      	<strong>{{ $errors->first('per_page_api') }}</strong>
	                  	</span>
	              	@endif                        
	          	</div>-->
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	
	loadDate();
	loadColor();

	$('#datetime_format').change(function(){
		loadDate();
	});

	$('#skin').change(function(){
		loadColor();
	});

	$("input:file").change(function (){
      var fileName = $(this).val().split('\\').pop();;
      $("label[for='selectedURL']").text(fileName);

        if (this.files && this.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#image').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(this.files[0]);
          }
        else{
            $("#image").attr("src","");
          }
    });

	function loadDate(){
		var time_format = $('#datetime_format').val();
		if(time_format==""){
			$("label[for='previewFormat']").text("");    
		}
		else{	
			var time_format = time_format.replace("Y", "yyyy");
			var time_format = time_format.replace("i", "M");
			today_date = new Date();
			var formatted_date = today_date.format(time_format);
			$("label[for='previewFormat']").text(formatted_date);     
		}
	}

	function loadColor(){
		var color = $('#skin').val();
		$('#previewColor').attr('style','background-color:'+color);
	}

	$('#limit_records').on('change', function(){
		var limit = $('#limit_records').val();
		if ($.isNumeric(limit)){
			if(limit>100){
				alert("Limit 100 will make the web slow");
			}
		}else{
			alert("Limit must be numeric");
			$('#limit_records').val('');
		}
	});
});
</script>
@endsection
