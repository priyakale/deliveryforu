<section class="content-header"><h1>{{ $data['page_title'] }}</h1></section>

<div class="model-menu" style="margin-left: auto; margin-right: auto; width: 90%; padding-top: 3%;">
    <form class="form-horizontal" method="POST" enctype="multipart/form-data" 
    action="{{ URL::route('save_menu') }}" id='menu_form'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="row padding-top">
            <div class="col-lg-4"><label for="parent_menu">Parent Menu</label></div>
            <div class="col-lg-8">
                <input type="hidden" class="form-control"
                name="id" id="id" 
                value="{!! (!empty($data['record']['id']) ? $data['record']['id'] : '') !!}">
                <div class="dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" id="select-parent" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                   {{ !empty($data['record']['parent_id']) ? 
                   $data['record']['parent_name'] :
                   'Select Parent' }} 
                    <span class="caret"></span>
                  </button>
                  <input type="hidden" class="hidden-value" name="parent_name" id="parent_name"
                   value="{{ !empty($data['record']['parent_id']) ? 
                  $data['record']['parent_name'] : '' }}" >
                    <ul class="dropdown-menu" aria-labelledby="select-parent">
                        <li>
                            <a href="javascript:void(0)" class="a-select-value" value="">Select Parent</a>
                        </li>
                        @foreach($data['menu_record'] as $key => $value )
                        <li>
                            <a href="javascript:void(0)" class="a-select-value" value="{{ $key }}">{{ $value['name'] }}</a>
                        </li>
                        @endforeach
                     </ul>
                </div>
            </div>
        </div>
        <div class="row padding-top">
            <div class="col-lg-4"><label for="name">Name</label></div>
            <div class="col-lg-8">
                <input type="text" class="form-control" placeholder="Menu Name" 
                name="name" id="name" 
                value="{!! (!empty($data['record']['name']) ? $data['record']['name'] : '') !!}" required>
            </div>
        </div>
        <div class="row padding-top">
            <div class="col-lg-4"><label for="link">Link</label></div>
            <div class="col-lg-4">
                <div class="dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" id="select-controller" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                   {{ !empty($data['record']['controller_name']) ? 
                   $data['record']['controller_name'] :
                   'Select Controller' }} 
                    <span class="caret"></span>
                  </button>
                  <input type="hidden" class="hidden-value" name="controller" id="controller"
                   value="{{ !empty($data['record']['controller_name']) ? 
                   $data['record']['controller_name'] : '' }}" >
                    <ul class="dropdown-menu" aria-labelledby="select-controller">
                        <li>
                        <a href="javascript:void(0)" class="a-select-controller" value="">Select Controller</a>
                        </li>
                        @foreach($data['link_records'] as $key => $record )
                        <li>
                            <a href="javascript:void(0)" class="a-select-controller" value="{{ $key }}">{{ $key }}</a>
                        </li>
                        @endforeach
                     </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" id="select-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                   {{ !empty($data['record']['link_name']) ? 
                   $data['record']['link_name'] :
                   'Select Link' }} 
                    <span class="caret"></span>
                  </button>
                  <input type="hidden" class="hidden-value" name="link_name" id="link_name"
                   value="{{ !empty($data['record']['link_name']) ? 
                   $data['record']['link_name'] : '' }}" >
                    <ul class="dropdown-menu" aria-labelledby="select-link" id="select-link-ui">
                        <li>
                        <a href="javascript:void(0)" class="a-select-value" value="">Select Link</a>
                        </li>
                     </ul>
                </div>
            </div>
        </div>
        <div class="row padding-top">
            <div class="col-lg-4"><label for="icon">Icon</label></div>
            <div class="col-lg-8 input-group" style="padding-left: 4%;">
                <input type="text" class="form-control" placeholder="Font Awesome Text" 
                name="icon" id="icon" 
                value="{!! (!empty($data['record']['icon']) ? $data['record']['icon'] : '') !!}">
                <div class="input-group-btn override-mini-btn-search">
                    <a class="btn btn-success" href="http://fontawesome.io/icons/" target="_blank"><i class="fa fa-windows"></i></a>
                </div>
            </div>
        </div>
        <div class="row padding-top text-right">
            <div class="btn-group">
                <button type="button" class="btn btn-default" id="btn-close" data-dismiss="modal">Close</button>
                @if(empty($data['record']['id']))
                <button type="button" class="btn btn-success" id="btn-create">New Menu</button>
                @else
                <button type="button" class="btn btn-success" id="btn-update">Update Menu</button>
                @endif
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" >
    var array_link = {};
    @foreach( $data['link_records'] as $key_controller => $value_function)
        array_link.{{ $key_controller }} = [];
        @foreach($value_function as $key => $value)
            array_link.{{ $key_controller }}.push('{{ $value }}');
        @endforeach
    @endforeach
</script>

<script type="text/javascript" >
    $( document ).ready(function() {

        $("#btn-close").click(function() {
            $('#menu-model').modal('hide');
        });

        $('#btn-create').on('click',function(){
            var id = 0;
            var parent_name = $('#category').val();
            var name = $('#name').val();
            var link_name = $('#link_name').val();
            var icon = $('#icon').val();

            validateMenu(id,parent_name, name, link_name, icon);
        });

        $('#btn-update').on('click',function(){
            var id = $('#id').val();
            var parent_name = $('#category').val();
            var name = $('#name').val();
            var link_name = $('#link_name').val();
            var icon = $('#icon').val();

            validateMenu(id,parent_name, name, link_name, icon);
        });

        $("#menu_form").on("click", ".a-select-controller", function () {
            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
            $(this).parents(".dropdown").find('.hidden-value').val($(this).text());
            setValueLink(this);
        });

        $("#menu_form").on("click", ".a-select-value", function () {
            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
            $(this).parents(".dropdown").find('.hidden-value').val($(this).text());
        });
       
    });//end document ready


function setValueLink(element){
    var element = $(element).parents(".dropdown").find('.hidden-value');
    $('#select-link').html('Select Link <span class="caret"></span>');
    $('#link').next().val('');
    $("#select-link-ui li").remove();
    setTimeout(
    function(){
        var value_select = element.val();
        $.each(array_link[value_select], function( index, value ) {
            $("#select-link-ui")
            .append('<li><a href="javascript:void(0)" class="a-select-value"' +
            'value="'+value+'">'+ value +'</a></li>');
        });
    }, 100);
}//End set value link

function validateMenu(id,parent_name, name, link_name, icon){
    if($.trim(name) == ''){
        alert("Please make sure to insert name!");
        return false;
    }else{
        var msg_success = 'Success add/update menu';
        var data = new FormData($('#menu_form')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_menu') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                if(data['result']){
                   window.location.href = "{{ URL::route('index_menu') }}";
                }else{
                    alert('Fail : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
        
        //return true;
    }
}//end validate menu
</script>