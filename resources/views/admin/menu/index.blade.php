@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<div class="container full_width">
    <div class="row">
        <div class="col-md-3">
            <!-- START USER-->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-bars"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $data['total_menu'] }}</div>
                    <div class="widget-title">Menu</div>
                    <div class="widget-subtitle">Total : </div>
                </div>                
            </div>                            
            <!-- END USER --> 
        </div>
         <div class="col-md-9">
            <!-- START SERARCH CATERIA -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    <form class="padding-right" role="search">
                        <div class="input-group form-width-full">
                            <input type="text" class="form-control" placeholder="Search Keyword" name="keyword" 
                            value="{{ !empty($data['input']['keyword']) ? $data['input']['keyword'] : '' }}" >
                            <div class="input-group-btn override-mini-btn-search">
                                <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="input-group padding-top float-right">
                            <div class="float-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>Search</button>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>                            
            <!-- END SERARCH CATERIA --> 
        </div>
    </div>
     <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-custom">
                    <div class="panel-heading panel-heading-custom">
                        <table class="full_width">
                            <tr>
                                <td>
                                    Search Result  
                                    <span class="badge custom-badge">{{ $data['records']->total() }}</span>
                                </td>
                                <td>
                                    <div class="form-group float-right">
                                        <div class="btn-group input-group">
                                            <a 
                                            class="float-right btn btn-success" id="btn_add" 
                                            href="javascript:void(0)">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body">
                        <div class="float-left">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                        <table class="table table-hover"> 
                            <thead> 
                                <tr> 
                                    <th>No.</th> 
                                    <th>Menu Name</th>
                                    <th>Parent Menu</th>
                                    <th>Link</th> 
                                    <th>Icon</th>
                                    <th>Last Updated</th>
                                    <th></th>
                                </tr> 
                            </thead> 
                            <tbody>
                                @php  $count = 1 @endphp
                                @foreach($data['records'] as $record)
                                <tr>
                                    <td>{{ $count++ }}</td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="edit({{ $record->id }})">
                                        {{ $record->name }}
                                        </a>
                                    </td>
                                    <td>{{ $record->parent_name }}</td>
                                    <td>{{ $record->link_name }}</td>
                                    <td>
                                        <i class="fa {{ $record->icon }}">
                                            <span>{{ $record->icon }}</span>
                                        </i>
                                    </td>
                                    <td>{{ $record->updated_at_value }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                                @if(empty($data['records']))
                                <tr>
                                    <td colspan="7" class="text-center empty-record"><div>Empty Data</div>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- MODEL MENU CREATE / EDIT -->
<div class="modal fade" role="dialog" id="menu-model" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-body" id="menu-content">

            </div>
        </div>
    </div>
</div>
<!-- END MODEL MENU CREATE / EDIT -->

<script type="text/javascript" >
    $( document ).ready(function() {
        $('#btn_add').on('click',function(){
            $('#menu-content').html();
            $.ajax({
                type: 'GET',
                url : "{{ URL::route('add_menu') }}",
                data: { '_token' : '{{ csrf_token() }}' },
                dataType: 'json',
                success: function (data) {
                    if(data['result']){
                        $('#menu-content').html(data['returnHTML']);
                        $("#menu-model").modal();
                    }else{
                        alert('Fail to show menu : ' + data['errMsg']);
                    }
                },
                error: function (data) {
                    console.error('Error:', data);
                }
            });
        });
    });

    function edit(id){
        $('#menu-content').html();
        $.ajax({
            type: 'GET',
            url : "{{ URL::route('edit_menu') }}/" + id,
            data: { '_token' : '{{ csrf_token() }}' },
            dataType: 'json',
            success: function (data) {
                if(data['result']){
                    $('#menu-content').html(data['returnHTML']);
                    $("#menu-model").modal();
                }else{
                    alert('Fail to show menu : ' + data['errMsg']);
                }
            },
            error: function (data) {
                console.error('Error:', data);
            }
        });
    }
</script>
@endsection
