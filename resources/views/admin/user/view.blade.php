@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<style type="text/css" >
    .chosen-container{
        min-width: 100px !important;
    }
</style>
<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td>{!! (!empty($data['record']['status_id']['name']) ? $data['record']['status_id']['name'] : '-') !!}</td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        
                                        <button class="float-right btn btn-warning" id="back" type="button">Back</button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>USER INFORMATION</h3>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['name']) ? $data['record']['name'] : '-') !!}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['role']['name']) ? $data['record']['role']['name'] : '-') !!}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['email']) ? $data['record']['email'] : '-') !!}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="phone">Mobile Number</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['phone']) ? $data['record']['phone'] : '-') !!}</p>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ACCEPT MODAL -->
<div class="modal fade" id="approveModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approveModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="approveModalLabel">Confirm</h4>
            </div>
            <div class="modal-body" id="approveModal_body">
                <h5>Are you sure want to approve this user?</h5>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="proceed">Proceed</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- END ACCEPT MODAL -->

<!-- REJECT MODAL -->
<div class="modal fade" id="rejectModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="rejectModalLabel">Confirm</h4>
            </div>
            <div class="modal-body" id="rejectModal_body">
                <h5>Are you sure want to reject this user?</h5>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="proceed">Proceed</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- END REJECT MODAL -->

<script>
$(document).ready(function() {  

    $('.chosen-select').chosen({
        no_results_text: "Oops, Nothing Found!",
        width: "200px"
    }); 

    $('#approve').on('click', function() {
        $('#approveModal').modal('show');
    });

    $('#reject').on('click', function() {
        $('#rejectModal').modal('show');
    });

    

    $('#back').click(function(){
        window.location.href = "{{ URL::route('index_user') }}" 
    });
});
</script>


@endsection