<div class="main-login main-center">
	<form class="form-horizontal" method="POST" enctype="multipart/form-data" 
    action="{{ URL::route('update_user') }}" id='user_detail_form'>
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<div class="form-group text-center">
			<img src="{{ 
			(!empty($data['records']['profile_pic']) ? 
			asset('/uploads/profile_pic/'.$data['records']['profile_pic']) : 
			asset('/uploads/profile_pic/default_user.png') ) }}" 
			class="img-circle img-user-profile" 
			alt="Click here to change image" href="javascript:void(0)" id="img_profile_pic" style="cursor: pointer;"/>
    		<input type="file" id="profile_pic" name="profile_pic" class="hidden" accept="image/*" />
    		<input type="hidden" class="hidden-value" name="id" id="txtId"
                       value="{{ !empty($data['records']['id']) ? $data['records']['id'] : '' }}" >
		</div>
		<div class="form-group">
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
					<input type="text" class="form-control" name="name" 
					id="txtName" placeholder="Please enter your user name" 
					value="{{ !empty($data['records']['name']) ? $data['records']['name'] : '' }}">
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
					<input type="email" class="form-control" name="email" id="txtEmail" placeholder=" Please enter your email address.(An email address must contain a single @)"
					value="{{ !empty($data['records']['email']) ? $data['records']['email'] : '' }}">
				</div>
			</div>
		</div>
        @if(RoleHelper::isAdministrator())
        <div class="form-group">
            <div class="cols-sm-10">
              <div class="input-group full_width">
                    <div class="dropdown full_width">
              <button class="btn btn-default dropdown-toggle full_width" type="button" 
              id="select-active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
               {{ !empty($data['records']['status_id']) ? 
               $data['records']['status_id']['name'] : 'Select Status' }} 
                <span class="caret"></span>
              </button>
              <input type="hidden" class="hidden-value" name="status_id" id="status_id"
               value="{{ !empty($data['records']['status_id']) ? 
               $data['records']['status_id']['name'] : '' }}" >
                <ul class="dropdown-menu" aria-labelledby="select-active">
                    @foreach($data['status_records'] as $key => $value )
                        <li>
                            <a href="javascript:void(0)" class="a-select-value" 
                            value="{{ $value['id'] }}">{{ $value['name'] }}</a>
                        </li>
                    @endforeach
                 </ul>
            </div>
                </div>
            </div>
        </div>

        <div class="form-group" id="div_role">
            <div class="cols-sm-10">
                <div class="input-group full_width">
                    <div class="dropdown full_width">
              <button class="btn btn-default dropdown-toggle full_width" type="button" id="select-role" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
               {{ !empty($data['records']['role_name']) ? $data['records']['role_name'] : 'Select Role' }} 
                <span class="caret"></span>
              </button>
              <input type="hidden" class="hidden-value" name="role_name" id="txtRole_Name"
               value="{{ !empty($data['records']['role_name']) ? $data['records']['role_name'] : '' }}" >
                <ul class="dropdown-menu" aria-labelledby="select-role">
                    <li>
                        <a href="javascript:void(0)" class="a-select-value" value="">Select Role</a>
                    </li>
                    @foreach($data['role_records'] as $role_record)
                    <li>
                        <a href="javascript:void(0)" class="a-select-value role_name"
                        value="{{ $role_record->id }}">{{ $role_record->name }}</a>
                    </li>
                    @endforeach
                 </ul>
            </div>
                </div>
            </div>
        </div>
        @endif

		<div class="form-group">
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
					<input type="password" class="form-control" name="password" id="txtPassword" 
					placeholder="Please enter your password {{ !empty($data['records']['id']) ?  '': '' }}">
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
					<input type="password" class="form-control" name="confirm" id="txtConfirm" 
					placeholder="Please confirm your password {{ !empty($data['records']['id']) ?  '': '' }}">
				</div>
			</div>
		</div>
		
		<div class="form-group text-right">
			<div class="btn-group">
				<button type="button" class="btn btn-default" id="btn-close">Close</button>
				@if(empty($data['records']['name']))
				<button type="button" class="btn btn-success" id="btn-create">Register</button>
				@else
				<button type="button" class="btn btn-success" id="btn-update">Update</button>
				@endif
			</div>
		</div>
	</form>
</div>
<script type="text/javascript" >
    $( document ).ready(function() {
        $(".switch-checkbox").bootstrapSwitch();

     	$( "#btn-close" ).click(function() {
            $('#user-detail-model').modal('hide');
        });

     	$( "#btn-create" ).click(function() {
            var id = $('#txtId').val();
     		var name = $('#txtName').val();
     		var email = $('#txtEmail').val();
     		var active_name = $('#txtActive_Name').val();
     		var role_name = $('#txtRole_Name').val();
     		var password = $('#txtPassword').val();
     		var confirmPassword = $('#txtConfirm').val();

        	validateUserDetail(id,name, email, active_name, role_name, password, confirmPassword);
        });

     	$( "#btn-update" ).click(function() {
     		var id = $('#txtId').val();
     		var name = $('#txtName').val();
     		var email = $('#txtEmail').val();
     		var active_name = $('#txtActive_Name').val();
     		var role_name = $('#txtRole_Name').val();
     		var password = $('#txtPassword').val();
     		var confirmPassword = $('#txtConfirm').val();

     		validateUserDetail(id,name, email, active_name, role_name, password, confirmPassword);
        });

        $("#img_profile_pic").click(function() {
		    $("#profile_pic").click();
		});

		$('#profile_pic').change(function(){
		    var file = this.files[0];
			var imagefile = file.type;
			var match= ["image/jpeg","image/png","image/jpg"];
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
				alert('Invalid File, Only accept png, jpeg and jpg!');
				$('#profile_pic').val('');
			}else{
				var reader = new FileReader();
		        reader.onload = function (e) {
		           $('#img_profile_pic').attr('src', e.target.result);
		           $('#img_profile_pic_change').val(1)
		        }
				reader.readAsDataURL(this.files[0]);
			}
	  	});

  	  	$(".a-select-value").click(function(){
		  	$(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
	 		$(this).parents(".dropdown").find('.hidden-value').val($(this).text());
		});
    });

    function validateUserDetail(id,name, email, active_name, role_name, password, confirmPassword){
    	if($.trim(name) == ''){
    		alert("Please enter your user name");
    		return false;
    	}else if($.trim(email) == ''){
    		alert("Please enter your email address.(An email address must contain a single @)");
    		return false;
    	}else if(!validateEmail(email)){
    		alert("Your email address is invalid.(An email address must contain a single @)");
    		return false;
    	}else if($.trim(id) == '' && $.trim(password) == '' && $.trim(confirmPassword) == ''){
    		alert("Please enter your password and confirm the password");
    		return false;
    	}else if($.trim(password) != '' && $.trim(password).length < 6){
    		alert("The password must contain at least 6 characters");
    		return false;
    	}else if($.trim(password) != '' && $.trim(confirmPassword) != '' 
    		&& $.trim(password) != $.trim(confirmPassword)){
    		alert("The passwords does not match");
    		return false;
    	}else{
    		var profile_pic = '';
    		var data = new FormData($('#user_detail_form')[0]);
    		$.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
             	contentType: false,
        		processData: false,
                url : "{{ URL::route('update_user') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if(data['result']){
                        location.reload();
                    }else{
                        alert(data['errMsg']);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
    		return true;
    	}
    }

    // Function that validates email address through a regular expression.
	function validateEmail(sEmail) {
		var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		if (filter.test(sEmail)) {
			return true;
		}else {
			return false;
		}
	}//end function validates email
</script>