@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<div class="container full_width">
    <div class="row">
        <div class="col-md-3">
            <!-- START USER-->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $data['total_user'] }}</div>
                    <div class="widget-title">User</div>
                    <div class="widget-subtitle">Total : </div>
                </div>                
            </div>                            
            <!-- END USER --> 
        </div>
         <div class="col-md-9">
            <!-- START SERARCH CATERIA -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    <form action="{{ URL::route('index_user') }}">
                        <div class="input-group form-width-full padding-top">
                            <div class="input-group form-width-full">
                        
                                <div class="col-md-4">
                                    <label for="keyword">Keyword</label>
                                    <input type="text" class="form-control" placeholder="Search Keyword" name="keyword" value="{{ !empty($data['search']['keyword']) ? $data['search']['keyword'] : '' }}" >
                                </div>
                            
                                <div class="col-md-4">
                                    <label for="status_id">Status</label>
                                    <select name="status_id" id="status_id" class="form-control chosen-select">
                                        <option value="">Select Status</option>
                                        @if(!empty($data['status_records']))
                                            @foreach($data['status_records'] as $key => $value)
                                              <option value="{{ $value->id }}" {{ !empty($data['search']['status_id']) && ($data['search']['status_id']==$value['id']) ? 'selected' : '' }}>{{ $value->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="role_id">Role</label>
                                    <select name="role_id" id="role_id" class="form-control chosen-select">
                                        <option value="">Select Role</option>
                                        @if(!empty($data['role_records']))
                                            @foreach($data['role_records'] as $key => $value)
                                              <option value="{{ $value->id }}" {{ !empty($data['search']['role_id']) && ($data['search']['role_id']==$value['id']) ? 'selected' : '' }}>{{ $value->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-group form-width-full padding-top padding-bottom padding-right">
                            <div class="form-group float-right">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-success">
                                    <i class="fa fa-search"></i>Search</button>
                                    &nbsp;&nbsp;
                                    <button type="button" id="valuereset" class="btn btn-defult">
                                    <i class="fa fa-times"></i>Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>                            
            <!-- END SERARCH CATERIA --> 
        </div>
    </div>
     <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-custom">
                    <div class="panel-heading panel-heading-custom">
                        <table class="full_width">
                            <tr>
                                <td>
                                    Search Result  
                                    <span class="badge custom-badge">{{ $data['records']->total() }}</span>
                                </td>
                                <td>
                                    <div class="form-group float-right">
                                        <div class="btn-group input-group">
                                            <button type="button" class="float-right btn btn-success" id="btn_add">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body">
                        <div class="float-left">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                        <table class="table table-hover"> 
                            <thead> 
                                <tr> 
                                    <th>No.</th> 
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Last Updated</th> 
                                    <th>Role</th> 
                                    <th class="text-center">Status</th>
                                    <th>Action</th> 
                                </tr> 
                            </thead> 
                            <tbody>
                               <!-- @php $count = 1 @endphp-->
                                @if (empty($data['records'][0]))
                                    <tr>
                                        <th colspan="8" class="empty-record text-center"><label>No Result</label></th>
                                    </tr>
                                @endif
                                @foreach($data['records'] as $key=>$record)
                                <tr>
                                    <td> {{ ($data['records']->currentpage()-1) * $data['records']->perpage() + $key + 1 }}
                                    </td>
                                    <td>
                                    <a href="javascript:void(0)" 
                                    onclick="popDetail({{ $record->id }})">{{ $record->name }}
                                    </a>
                                    </td>
                                    <td>{{ $record->email }}</td>
                                    <td>{{ $record->updated_at_value }}</td>
                                    <td>{{ $record->role_desc }}</td>
                                    <td class="text-center {{ !empty($record->status_id) ? 'status-'.$record->status_id->name : 'empty-record' }}">
                                        {{ $record->status_id->name }}
                                    </td>
                                    <td>
                                        <a href="{{ URL::route('view_user', $record['id']) }}" class="btn btn-success btn-sm"><span class="fa fa-eye"></span></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript" >
    $( document ).ready(function() {
       $( "#btn_add" ).click(function() {
            popDetail(0);
        });
        $('#valuereset').click(function(){
            window.location.href = "{{URL::route('index_user')}}" 
        });
    });
</script>
@endsection
