<!doctype html>
<html>
    <head>
        <style type="text/css">
            body{
                font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                size: 30%;
            }
            #footer {
                position : absolute;
                bottom : 0;
            }
            .logo{
                top: 10px;
                left: 480px;
                position : absolute;
            }

        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Transaction Statement</title>
        
    </head>
    <body>
        <hr>
        <img src="{{ asset('assets/img/mobieco-logo.png') }}" class="logo" height="60">
        <table width="100%" style="font-size: 80%" cellpadding="0" cellspacing="0">
            <tr> 
                <td width="20%"><b>MOBIECO WIRELESS SDN BHD</b></td>
            </tr>
        </table>
        <table width="100%" style="font-size: 70%" cellpadding="0" cellspacing="0">
            <tr> 
                <td width="5%">Unit 2.02, 2nd Floor, Dataran Hamodal Bock B</td>
            </tr>
            <tr> 
                <td width="5%">Lot 4, Jalan Bersatu 13/4,Section 13</td>
            </tr>
            <tr> 
                <td width="5%">46200 Petaling Jaya, Selangor, Malaysia</td>
            </tr>
            <tr> 
                <td width="5%">Tel: 603-7932 1233 Fax: 603-7932 8133</td>
            </tr>
        </table>
        <hr>
        <table width="100%" style="font-size: 80%" cellpadding="0" cellspacing="0">
            <tr><td>&nbsp;</td></tr>
            <tr> 
                <td width="80%"><b>RECEIPT </b></td>
                <td width="20%">Serial No: {{$data['receipt_num']}}</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
        <hr>
        <table width="100%" style="font-size: 80%" cellpadding="0" cellspacing="0">
            <tr><td>&nbsp;</td></tr>
            <tr> 
                <td width="20%">DATE & TIME:</td>
                <td width="80%" style="text-align: left">{{$data['datetime']}}</td>
            </tr>
            <tr><td>&nbsp;</td></tr>

            <tr> 
                <td width="20%">RECEIVED FROM:</td>
                <td width="80%">{{$data['user']['name']}}</td>
            </tr>
            <tr><td>&nbsp;</td></tr>

            <tr> 
                <td width="20%">PAYMENT FOR:</td>
                <td width="80%">{{$data['payment_for']}}</td>
            </tr>
            <tr> 
                <td width="20%">PLAN:</td>
                <td width="80%">{{$data['payment_for']}}</td>
            </tr>
            <tr> 
                <td width="20%">MODEL:</td>
                <td width="80%">{{$data['payment_for']}}</td>
            </tr>
            <tr> 
                <td width="20%">TIER:</td>
                <td width="80%">{{$data['payment_for']}}</td>
            </tr>
            <tr> 
                <td width="20%">MDN:</td>
                <td width="80%">{{$data['payment_for']}}</td>
            </tr>
            <tr><td>&nbsp;</td></tr>

            <tr> 
                <td width="20%">AMOUNT:</td>
                <td width="80%">MYR {{$data['records']['amount']}}</td>
            </tr>

            <tr> 
                <td width="20%"></td>
                <td width="80%">{{$data['records']['amount_word']}}</td>
            </tr>
            
            <tr><td>&nbsp;</td></tr>
        </table>
        <hr>
        <table width="100%" style="font-size: 80%" cellpadding="0" cellspacing="0">
            <tr> 
                <td width="20%">This is a system generated receipt. No signature is required.</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr> 
                <td width="20%">Confidential information may be contained in this email and any files transmitted in it (Message). If you are not the addressee indicated in this Message (or responsible for delivery of this Message to such person), you are hereby notified that any dissemination, distribution, printing or copying of this Message or any party thereof is strictly prohibited. In such a case, you should delete this Message immediately and advice the sender immediately by return email at legal@yakin2u.com. No assumption of responsibility or liability whatsoever is undertaken by Mobieco Wireless Sdn Bhd in respect of prohibited or unauthorized use by any other person.</td>
            </tr>
        </table>
    </body>
</html>