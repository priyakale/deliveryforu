@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">
        <div class="panel panel-custom">
            <div class="panel-heading panel-heading-custom">
                {{ Form::open(array('url'=> URL::route("save_payment_setting") , 'method' => 'POST', 'id'=>'payment_setting_form')) }}
                <table class="full_width">
                    <tr>
                        <td>
                            <div class="form-group float-right">
                                <div class="btn-group input-group">
                                    <button type="button" id="btn-payment-save" class="float-right btn btn-success">
                                        <i class="fa fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row well">
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="glyphicon glyphicon-briefcase"></i>
                    </a> General
                </legend>

                <div class="form-group{{ $errors->has('merchant_identify') ? ' has-error' : '' }}">
                    <label for="merchant_identify">Merchant Identify</label>
                    <input type="hidden" class="form-control" id="id" name="id" value="{!! (!empty($data['record']['id']) ? $data['record']['id'] : '') !!}" required autofocus />
                    <input type="text" class="form-control" id="merchant_identify" name="merchant_identify" value="{!! (!empty($data['record']['merchant_identify']) ? $data['record']['merchant_identify'] : '') !!}" required autofocus />     
                    @if ($errors->has('merchant_identify'))
                        <span class="help-block">
                            <strong>{{ $errors->first('merchant_identify') }}</strong>
                        </span>
                    @endif                        
                </div>

                <div class="form-group{{ $errors->has('verify_key') ? ' has-error' : '' }}">
                    <label for="verify_key">Verify Key</label>
                    <input type="text" class="form-control" id="verify_key" name="verify_key" value="{!! (!empty($data['record']['verify_key']) ? $data['record']['verify_key'] : '') !!}" required autofocus />     
                    @if ($errors->has('verify_key'))
                        <span class="help-block">
                            <strong>{{ $errors->first('verify_key') }}</strong>
                        </span>
                    @endif                        
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="row well"  style="margin-left:0.5%; margin-right:0.5%;">
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="fa fa-columns" aria-hidden="true"></i>
                    </a> Currency
                </legend>
                <div class="form-group{{ $errors->has('currency_id') ? ' has-error' : '' }}">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                @foreach($data['currency_record'] as $record)
                                    <div class="row">
                                        <div class="col-xs-4">
                                        <input type="hidden" name="currency_id" id="currency_id" value="{{ $record['id'] }}">
                                            <label style="padding-top: 5px;">{{ $record['name'] }}</label>
                                        </div>
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" name="currency_rate" id="currency_rate" placeholder="currency rate" value="{!! (!empty($data['record']['currency_rate']) ? $data['record']['currency_rate'] : '') !!}" required />
                                        </div>
                                        <div class="col-xs-2 status-{{ $record['status']['name'] }}" style="text-align: center;height: 30px;">
                                            <label style="padding-top: 5px;">{{ $record['status']['name'] }}</label>
                                        </div>
                                        <div class="col-xs-2 status-Hidden" style="text-align: center;height: 30px;">
                                            <label style="padding-top: 5px;">Primary</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{ asset('/assets/img/payment_logos.png') }}" 
                            class="img-responsive" style="float: right; width: 60%;" 
                            href="javascript:void(0)"/>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="row well" >
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    </a> Mode
                </legend>

                <div class="form-group{{ $errors->has('mode') ? ' has-error' : '' }}">
                    <label for="mode">Mode</label>
                    <select name="mode" id="mode" class="form-control chosen-select" >
                        <option value="">Select Mode</option>
                        <option value="L" {{ !empty($data['record']['mode']) && $data['record']['mode'] == 'L' ? 'selected' : '' }}>Live</option>
                        <option value="T" {{ !empty($data['record']['mode']) && $data['record']['mode'] == 'T' ? 'selected' : '' }}>Test</option>
                    </select>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script>
$( document ).ready(function() {
    $('#btn-payment-save').on('click',function(){

        if($.trim($('#merchant_identify').val()) == ''){
            alert("Please make sure to key in merchant identify");
            return false;
        }else if($.trim($('#verify_key').val()) == ''){
            alert("Please make sure to key in verify key");
            return false;
        }if($.trim($('#currency_rate').val()) == ''){
            alert("Please make sure to key in currency rate");
            return false;
        }else{
            var data = new FormData($('#payment_setting_form')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                contentType: false,
                processData: false,
                url : "{{ URL::route('save_payment_setting') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if(data['result']){
                       window.location.href = "{{ URL::route('index_payment_setting') }}";
                    }else{
                        alert('Fail : ' + data['errMsg']);
                    }
                },
                error: function (data) {
                    console.error('Error:', data);
                }
            });
            //return true;
        }
    });
});
</script>
@endsection