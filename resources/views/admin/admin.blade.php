@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<div class="container full_width">
	<div class="row">
        <div class="col-md-3">
            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-file-text"></span>
                </div>                             
                <div class="widget-data">
                    <div class="widget-int num-count">0</div>
                    <div class="widget-title">Total Order</div>
                    <div class="widget-subtitle">New : </div>
                </div>
            </div>                            
            <!-- END WIDGET MESSAGES -->
        </div>

        <div class="col-md-3">
            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-usd"></span>
                </div>                             
                <div class="widget-data">
                    <div class="widget-int num-count">0</div>
                    <div class="widget-title">Month</div>
                </div>
            </div>                            
            <!-- END WIDGET MESSAGES -->
        </div>

        <div class="col-md-3">
            <!-- START WIDGET REGISTRED -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-users"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">0</div>
                    <div class="widget-title">Customer</div>
                    <div class="widget-subtitle">New : </div>
                </div>                
            </div>                            
            <!-- END WIDGET REGISTRED --> 
        </div>

        <div class="col-md-3">
            <!-- START WIDGET CLOCK -->
            <div class="widget widget-danger widget-padding-sm">
                <div class="widget-big-int plugin-clock">
                <span id="txtHour">12</span>
                <span>:</span>
                <span id="txtMinute">33</span>
                </div>
                <div class="widget-subtitle plugin-date"> <span id="txtDay">Tuesday</span>,  <span id="txtMonth">July</span>  <span id="txtDate">19</span>,  <span id="txtYear">2016</span></div>
                <div class="widget-buttons widget-c3">
                    <div class="col">
                        <a href="#"><span class="fa fa-clock-o"></span></a>
                    </div>
                    <div class="col">
                        <a href="#"><span class="fa fa-bell"></span></a>
                    </div>
                    <div class="col">
                        <a href="#"><span class="fa fa-calendar"></span></a>
                    </div>
                </div>                            
            </div>                        
            <!-- END WIDGET CLOCK -->
        </div>
    </div>
</div>
<script type="text/javascript" >
$( document ).ready(function() {
    change_time(); //for first time
    timedMsg();   
});

function timedMsg(){
    var t=setInterval("change_time();",60 * 1000); //Every minute run
}

function change_time(){
    var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"
    ];

    var weekday = ["Sunday","Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    var d = new Date();
    var curr_hour = d.getHours();
    var curr_min = d.getMinutes();
    var curr_date = d.getDate();
    var curr_day = weekday[d.getDay()];
    var curr_month = monthNames[d.getMonth()];
    var curr_year = d.getFullYear();
    if(curr_min < 10 ) curr_min = '0' + curr_min;
    if(curr_day < 10 ) curr_day = '0' + curr_day;
    $('#txtHour').text(curr_hour);
    $('#txtMinute').text(curr_min);
    $('#txtDate').text(curr_date);
    $('#txtMonth').text(curr_month);
    $('#txtYear').text(curr_year);
     $('#txtDay').text(curr_day);
}
</script>
@endsection
