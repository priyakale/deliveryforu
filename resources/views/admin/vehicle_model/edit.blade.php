@extends('admin.layout')

@section('htmlheader_title')
Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">

        <form action="{{ @$model ? route('admin.vehicle-model.update', $model->id) : route('admin.vehicle-model.store') }}" method="POST">
            {{ csrf_field() }}
            @if(@$model)
            {{ method_field('PATCH') }}
            @endif

            <div class="form-group">
                <label for="">Vehicle</label>
                <select class="form-control" name="vehicle_id">
                    <option value="" selected> Please Select </option>
                    @forelse($vehicles as $vehicle)
                    <option
                            value="{{ $vehicle->id }}"
                            {{ old('vehicle_id', @$model->vehicle->id) == $vehicle->id ? 'selected' : '' }}>
                        {{ $vehicle->type }} - {{ $vehicle->brand }}
                    </option>
                    @empty
                    @endforelse
                </select>
            </div>

            <div class="form-group">
                <label for="">Model: </label>
                <input type="text" name="model" value="{{ old('model', @$model->model) }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="">Status: </label>
                <input type="text" name="status" value="{{ old('status', @$model->status) }}" class="form-control">
            </div>

            <button type="submit" class="btn btn-info">Create</button>

        </form>

    </div>
</div>


<script type="text/javascript" >
    $(document).ready(function() {

    })
</script>

@endsection
