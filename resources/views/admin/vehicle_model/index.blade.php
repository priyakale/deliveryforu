@extends('admin.layout')

@section('htmlheader_title')
Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">
        <a href="{{ route('admin.vehicle-model.create') }}" class="btn btn-info"> Add New </a>
        <table class="display" id="data-table">
            <thead>
            <th>Model</th>
            <th>Status</th>
            <th>Action</th>
            </thead>
            <tbody>
            @forelse($models as $model)
            <tr>
                <td>{{ $model->model }}</td>
                <td>{{ $model->status }}</td>
                <td>
                    <a href="{{ route('admin.vehicle-model.edit', $model->id) }}">Edit</a> /
                    <a href="{{ route('admin.vehicle-model.destroy', $model->id) }}" onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">Delete</a>
                </td>
                <form id="delete-form" action="{{ route('admin.vehicle-model.destroy', $model->id) }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </tr>
            @empty
            <tr>
                <td colspan="2">No Data Found!</td>
            </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript" >
    $(document).ready(function() {
        $('#data-table').dataTable();
    })
</script>

@endsection
