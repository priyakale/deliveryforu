<html>
    <table border="1" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>
                <td colspan="1" style="width: 10px;height: 1px;"></td>                
            </tr>
            <tr>
                <td colspan="5" style="height:70px;text-align: right;">
                   <h2><img align="right" id="image" width="50" height="80" src="assets/img/upay_me.png"/>UPayMe</h2> 
                </td>
                <td colspan="6" style="height:70px;text-align: left;">
                   <i style="font-size: 10px !important;font-weight: normal !important;"></i>
                </td>
            </tr> 
            <tr>
                <td colspan="11" class="text-center" style="font-size: 8px;vertical-align: bottom;">
                    HQ: 54-3, Jalan USJ 9/5P, Subang Business Centre, 47620 UEP Subang Jaya, Selangor Darul Ehsan, Malaysia.   Tel: 603-8023 2846 / 8025 9919   Fax: 603-8023 2587
                </td>
            </tr> 
            <tr> 
                <td colspan="11" class="text-center" style="font-size: 8px;vertical-align: top;">
                    BANGSAR: Unit 8-2, Level 8, Avenue 5, Tower 9, Bangsar South, No. 8, Jalan Kerinchi, 59200 Kuala Lumpur, Malaysia.   Tel: 603-2242 0831 / 2242 1372   Fax: 603-2242 1346
                </td>
            </tr> 
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" class="text-center" style="height:18px;font-size:14px;font-weight: bold;">
                   TRANSACTION REPORT
                </td>
            </tr>
            <tr>
                <td style="width: 10px;height: 18px;"></td>
            </tr>
            <tr>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Merchant Name</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Salesperson Name</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Merchant Registration Number</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">User Name</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">User Email</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Currency</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Amount</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Status</th>
                <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Updated At</th>
                 <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">Sign Off Date</th>
                  <th style="border: 10px solid #000000; text-align: center;width: 18px;height:50px;vertical-align: middle;">End Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data['records'] as $record)
            <tr>
              <td>{{ $record['merchant']['company_registered_name'] }}</td>
              <td>{{ $record['merchant']['salesperson_name'] }}</td>
              <td>{{ $record['merchant']['business_registration_number'] }}</td>
              <td>{{ $record['user']['name'] }}</td>
              <td>{{ $record['user']['email'] }}</td>
              <td>{{ $record['currency'] }}</td>
              <td>{{ number_format($record['amount'],2) }}</td>
              <td>{{ $record['status']['name'] }}</td>
              <td>{{ $record->created_at }}</td>
              <td>{{ $record['merchant']['sign_off'] }}</td> 
              <td>{{ $record['merchant']['end_date'] }}</td>
            </tr>  
          @endforeach 
            <tr>
                <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>         
        </tbody>
    </table>
</html>