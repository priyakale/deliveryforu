@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<div class="container full_width">
    <div class="row">
        <form role="search" method="POST" action="{{ URL::route('download_report') }}" target="_blank" >
        {{ csrf_field() }}   
        <div class="panel panel-custom">
            <div class="panel-heading panel-heading-custom"> 
                <table class="full_width">
                    <tr>
                        <td>
                            <div class="form-group float-right">
                                <div class="btn-group input-group">
                                    <button type="submit" class="float-right btn btn-success">
                                        <i class="fa fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row well">
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="glyphicon glyphicon-briefcase"></i>
                    </a> Report
                </legend>
                <div class="form-group{{ $errors->has('report') ? ' has-error' : '' }}">
                    <label for="report" >Report</label>
                    <select name="report" id="report" class="form-control chosen-select">
                       <!-- <option value="">Select Report</option>-->
                        @if(!empty($data['report']))
                            @foreach($data['report'] as $key => $value )
                              <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach
                        @endif
                    </select>                     
                </div>
                <div class="form-group">
                    <label for="date_from">Date From</label>
                    <div class="input-group">
                        <input class="form-control datepicker" type="text" placeholder="Date From"  name="date_from" id="date_from" 
                               value="{!! (!empty($data['records']['user_detail_record']['date_from']) ? $data['records']['user_detail_record']['date_from'] : '') !!}"  autocomplete="off">
                        <div class="input-group-btn">
                            <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date_to">Date To</label>
                    <div class="input-group">
                        <input class="form-control datepicker" type="text" placeholder="Date To"  name="date_to" id="date_to" 
                               value="{!! (!empty($data['records']['user_detail_record']['date_to']) ? $data['records']['user_detail_record']['date_to'] : '') !!}" autocomplete="off">
                        <div class="input-group-btn">
                            <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row well">
                <legend style="margin-bottom: 8px !important;">
                    <a href="javascript:void(0)">
                    <i class="glyphicon glyphicon-briefcase"></i>
                    </a> Download Type
                </legend>
                <div class="form-group{{ $errors->has('download_type') ? ' has-error' : '' }}">
                    <label for="download_type" >Download Type</label>
                    <select name="download_type" id="download_type" class="form-control chosen-select">
                        <!--<option value="">Select Download Type</option>-->
                        @if(!empty($data['download_type']))
                            @foreach($data['download_type'] as $key => $value )
                              <option value="{{ $value['name'] }}">{{ $value['name'] }}</option>
                            @endforeach
                        @endif
                    </select>                     
                </div>
                <div class="form-group float-right">
                    <div class="btn-group input-group">
                        <button type="submit" id="submit" class="float-right btn btn-success">
                            <i class="fa fa-save"></i>Download
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<!-- END ACTION FLOAT -->
<script type="text/javascript" >
$( document ).ready(function() {

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('.btn-date').click(function () {
        var ele_name = $(this).parents(".input-group-btn").parents(".input-group").first().find('.datepicker').attr('id');
        $('#' + ele_name).datepicker("show");
    });   
       
    $("#submit").attr('disabled', 'disabled'); 

    $('#date_from, #date_to').on('change', function(){        
        var date_from = $("#date_from").val();
        var date_to = $("#date_to").val();
        if (!(date_from == "" || date_to == "")) {
            $("#submit").removeAttr('disabled');
        }
    });
        
});
</script>
@endsection
