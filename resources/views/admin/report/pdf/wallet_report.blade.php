<!doctype html>
<html>
    <head>
        <style type="text/css">
            body{
                font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                size: 30%;
            }
            #footer {
                position : absolute;
                bottom : 0;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Account Holder Report</title>
        
    </head>
    <body>
        <table width="100%">
            <tr> 
                <td width="30%"></td>
                <td width="40%" style="text-align: center; font-weight:bold;font-size: 100%" >Wallet Report From {{$data['date_from']}} - {{$data['date_to']}}</td>
                <td width="30%"></td>
            </tr>
        </table>
        <table width="100%" style="font-size: 80%">
            <thead>
                <tr>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Transaction Type</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Transaction Category</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Enrolled by</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Sender Wallet</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Receiver Wallet</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Debit (RM)</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Credit (RM)</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Balance (RM)</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Description</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Status</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Created At</th>
                </tr>
            </thead>
            <tbody>
              @foreach($data['records'] as $record)
                <tr>
                  <td>{{ $record['transaction_type']['name'] }}</td>
                  <td>{{ $record['transaction_category']['name'] }}</td>
                  <td>{{ $record['user']['name'] }}</td>
                  <td>{{ $record['sender_wallet']['user']['name'] }}</td>
                  <td>{{ $record['receiver_wallet']['user']['name'] }}</td>
                  @if($record->transaction_type_id == 67)
                      <td style="text-align: center;">{{ $record['amount'] }}</td>
                      <td></td>
                  @elseif ($record->transaction_type_id == 66)
                      <td ></td>
                      <td style="text-align: center;">{{ $record['amount'] }}</td>
                  @endif
                  <td style="text-align: center;">{{ $record['balance'] }}</td>
                  <td style="text-align: center;">{{ $record['description'] }}</td>
                  <td style="text-align: center;">{{ $record['status']['name'] }}</td>
                  <td>{{ $record->created_at }}</td>
                </tr>  
              @endforeach  
                <tr>
                    <td></td>
                </tr>
                <tr>
                  <td style="border: 1px solid #000000;font-weight: bold;"></td>
                  <td style="border: 1px solid #000000;"></td>
                  <td style="border: 1px solid #000000;"></td>
                  <td style="border: 1px solid #000000;"></td>
                  <td style="border: 1px solid #000000;font-weight: bold;"></td>
                  <td style="border: 1px solid #000000;"></td>
                  <td style="border: 1px solid #000000;"></td>
                  <td style="border: 1px solid #000000;"></td>
                  <td style="border: 1px solid #000000;"></td>
              </tr>              
            </tbody>
        </table>
    </body>
</html>