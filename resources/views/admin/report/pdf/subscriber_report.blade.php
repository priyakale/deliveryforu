<!doctype html>
<html>
    <head>
        <style type="text/css">
            body{
                font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                size: 30%;
            }
            #footer {
                position : absolute;
                bottom : 0;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Subscribers Report</title>
        
    </head>
    <body>
        <table width="100%">
            <tr> 
                <td width="30%"></td>
                <td width="40%" style="text-align: center; font-weight:bold;font-size: 100%" >Subscribers Report From {{$data['date_from']}} - {{$data['date_to']}}</td>
                <td width="30%"></td>
            </tr>
        </table>
        <table width="100%" style="font-size: 60%">
            <thead>
                <tr>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">No.</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Name</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Email</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Identification Type</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Identification Number</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Mobile Number</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Address</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Referral Name</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Status</th>
                    <th style="border: 1px solid #000000; text-align: center;height:50px;vertical-align: middle;">Created At</th>
                </tr>
            </thead>
            <tbody>
            @php $total_new_accounts = 0 @endphp
              @foreach($data['records'] as $record)
                <tr>
                  <td>{{ $record->name }}</td>
                  <td>{{ $record->email }}</td>
                  <td>{{ $record->identification_type->name }}</td>
                  <td>{{ $record->identification_number }}</td>
                  <td>{{ $record->mobile_number }}</td>
                  <td>{{ $record->address }}</td>
                  <td>{{ $record->parent_referral->name }}</td>
                  <td>{{ $record->status->name }}</td>
                  <td>{{ $record->created_at }}</td>
                </tr>      
              @endforeach              
            </tbody>
        </table>
    </body>
</html>