@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<style type="text/css" >
    .chosen-container{
        min-width: 100px !important;
    }
</style>
<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td>{!! (!empty($data['record']['status']['name']) ? $data['record']['status']['name'] : '-') !!}</td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        
                                        <button class="float-right btn btn-warning" id="back" type="button">Back</button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>TRANSACTION DETAILS</h3>
                            <hr>
                            <div class=row>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="merchant">Registered Company Name</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['merchant']['company_registered_name']) ? $data['record']['merchant']['company_registered_name'] : '-') !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="company_number">Company Registered Number</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['merchant']['business_registration_number']) ? $data['record']['merchant']['business_registration_number'] : '-') !!}</p>
                                    </div>
                                </div>
                            </div>   

                            <div class=row>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">User Name</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['user']['name']) ? $data['record']['user']['name'] : '-') !!}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">User Email</label>
                                        <br>
                                        <p>{!! (!empty($data['record']['user']['email']) ? $data['record']['user']['email'] : '-') !!}</p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h3>TRANSACTION FIELDS</h3>
                            <hr>

                            
                            @if (empty($data['record']['fields'][0]))
                                <tr>
                                    <th colspan="7" class="empty-record text-center"><label>No Result</label></th>
                                </tr>
                            @endif
                            @foreach($data['record']['fields'] as $index => $record) 
                                <div class=row>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="merchant">Field Name</label>
                                            <br>
                                            <p>{!! $record['field']['name'] !!}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="merchant">Field Type</label>
                                            <br>
                                            <p>{!! $record['field']['data_type']['name'] !!}</p>
                                        </div>
                                    </div>
                                    @if ($record['field']['data_type_id']==65)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="merchant">Field Value</label>
                                                <br>
                                                <img src="/uploads/transaction-fields/{{$record['id']}}/{{$record['value']}}"  width="40" height="40">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="merchant">Field Value</label>
                                                <br>
                                                <p>{!! $record['value'] !!}</p>
                                            </div>
                                        </div>
                                    @endif
                                        
                                </div>  
                            @endforeach
                            
                            
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {  

    $('#back').click(function(){
        window.location.href = "{{ redirect()->back()->getTargetUrl() }}" 
    });
});
</script>


@endsection