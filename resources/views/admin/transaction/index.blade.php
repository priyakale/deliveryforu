@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')

<!-- SEARCH RESULT -->
<div class="container full_width">
    <div class="row">
        <div class="col-md-12">
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    {{ Form::open(array('url'=> URL::route("index_transaction") 
                    , 'method' => 'GET', 'id'=>'searchform')) }}
                        <div class="input-group form-width-full padding-top">
                            <div class="input-group form-width-full">
                                <div class="col-md-3">
                                    <label for="name" >Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ !empty($data['search']['name']) ? $data['search']['name'] : '' }}" >
                                </div>
                                <div class="col-md-3">
                                    <label for="status_id">Status</label>
                                    <select name="status_id" id="status_id" class="form-control chosen-select">
                                        <option value="">Select Status...</option>
                                        @if(!empty($data['status_records']))
                                            @foreach($data['status_records'] as $key => $value)
                                              <option value="{{ $value['id'] }}" {{ !empty($data['search']['status_id']) && $data['search']['status_id']==$value['id'] ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-group form-width-full padding-top padding-bottom padding-right">
                            <div class="form-group float-right">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-success">
                                    <i class="fa fa-search"></i>Search</button>
                                    &nbsp;&nbsp;
                                    <button type="button" id="valuereset" class="btn btn-defult">
                                    <i class="fa fa-times"></i>Reset</button>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>                
            </div>  
        </div>
    </div>
    <!-- END SEARCH CRITERIA -->
    <!-- SEARCH RESULT -->
    <div class="row">
        <div class="col-md-12">
           <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td>
                                Search Result  
                                <span class="badge custom-badge"></span>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body progress-panel ">
                    <div class="float-left">
                         {{ $data['records']->appends(Request::except('page'))->links() }}    
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="10%">No.</th>
                                <th width="20%">User</th>
                                <th width="20%">Merchant</th>
                                <th width="15%">Amount</th>
                                <th width="15%">Status</th>
                                <th width="15%">DateTime</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (empty($data['records'][0]))
                            <tr>
                                <th colspan="7" class="empty-record text-center"><label>No Result</label></th>
                            </tr>
                        @endif
                        @foreach($data['records'] as $index => $record) 
                            <tr id="carrier{{$record->id}}">
                                <td>{{ $index+1 }}</td>
                                @if(empty($record['user_id']))
                                    <td>{{ $record['investor']['fullname'] }}</td>
                                @else
                                    @if($record['user']['role_id']==5)
                                        <td>{{ $record['user']['investor_details']['fullname'] }}</td>
                                    @else
                                        @if($record['user']['merchant_details']['type_id']==71)
                                            <td>{{ $record['user']['merchant_details']['trading_name'] }}</td>
                                        @else
                                            <td>{{ $record['user']['merchant_details']['individual_name'] }}</td>
                                        @endif                                        
                                    @endif
                                @endif

                                @if($record['merchant']['type_id']==71)
                                    <td>{{ $record['merchant']['company_registered_name'] }}</td>
                                @else
                                    <td>{{ $record['merchant']['individual_name'] }}</td>
                                @endif
                                <td>{{ $record['currency'].' '.number_format($record['amount'],2) }}</td>
                                <td>{{ $record['status']['name'] }}</td>
                                <td>{{ $record['created_at'] }}</td>
                                <td>
                                    <a href="{{ URL::route('view_transaction',$record->id) }}" class="btn btn-success btn-sm"><span class="fas fa-pencil-alt"></span></a>
                                </td>                                   
                            </tr> 
                        @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{ $data['records']->appends(Request::except('page'))->links() }}    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SEARCH RESULT -->

<!-- SCRIPT -->
<script>
$(document).ready(function(){
    
    $('#valuereset').click(function(){
        window.location.href = "{{URL::route('index_transaction')}}" 
    });
});
</script> 
<!-- END SCRIPT --> 

@endsection