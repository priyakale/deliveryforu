@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<!-- Ionicons -->
<script src="{{ asset('/assets/js/add_individual_merchant.js') }}" type="text/javascript"></script>
<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet" type="text/css" />

<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
        <form class="" role="update" name="save_merchant" id="save_individual_merchant" action="{{ URL::route('save_merchant') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="hdn_ind" id="hdn_ind" value="Individual">
            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td></td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <button type="button" class="float-right btn btn-success" id="save-merchant">
                                            <i class="fa fa-save"></i>Save
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs submission-step">
                        <li class="active"><a data-toggle="tab" href="#headquarters_details" id="headquarters_details_tab">Individual Details</a></li>
                        <li ><a data-toggle="tab" href="#bank_account" id="bank_account_tab">Bank Account</a></li>
                        <!-- <li ><a data-toggle="tab" href="#fees_section" class="out_tab">Fees & Chargers</a></li>
                        <li ><a data-toggle="tab" href="#bank_account" class="out_tab">Acknowledgement</a></li>
                        <li ><a data-toggle="tab" href="#bank_account" class="out_tab">For Office Use Only</a></li>
                        <li ><a data-toggle="tab" href="#bank_account" class="out_tab">Documents Required</a></li>  -->     
                        <li ><a data-toggle="tab" href="#fields" id="fields_tab">Input Fields</a></li>
                    </ul>
                    <!-- FORM UPDATE NOTIFICATION -->
                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="{{!empty($data['records']['id']) ? $data['records']['id']:'' }}">
                    <input type="hidden" name="user_id" id="user_id" value="{{!empty($data['records']['user_id']) ? $data['records']['user_id']:'' }}">

                    <?php //echo '<pre>';print_r($data['records']);exit;?>
                    <div class="row tab-content" >
                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in active" id="headquarters_details">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="document_upload-1">Logo <span style="font-size: 12px;font-style: italic;">(Type = png, jpeg, jpg | Max Size = 3MB | Dimensions Max Width = 432 and Max Height = 432)</span> </label><br>
                                            <div class="btn-group" role="group" aria-label="...">

                                                <label class="btn btn-default">
                                                  Upload <input name="individual_merchant_logo" id="individual_merchant_logo" type="file" style="display: none;" accept="png, .jpeg, .jpg"/>
                                                </label>
                                            </div>                                            
                                            <!-- <img id="image" width="60" height="60" src="{{ asset('assets/img/new_logo.png') }}"/> -->

                                            <img id="image" width="60" height="60" src="{{ !empty($data['records']['logo']) ? '/uploads/merchants/MerchantId-'.$data['records']['id'].'/'.$data['records']['logo']:'/assets/img/new_logo.png' }}"/>

                                            <br>
                                            <span class="logo common_class">{{ $errors->first('logo') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="sub_merchant_ref">Sub Merchant Identifier</label>
                                           <input type="text" class="form-control" id="sub_merchant_ref"  name="sub_merchant_ref" value="{{!empty($data['records']['sub_merchant_ref']) ? $data['records']['sub_merchant_ref'] : old('sub_merchant_ref') }}">
                                            <span class="sub_merchant_ref common_class">{{ $errors->first('sub_merchant_ref') }}</span>
                                        </div>
                                    </div>
                                    

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="individual_name">Name (IC)</label>
                                           <input type="text" class="form-control" id="individual_name"  name="individual_name" value="{{!empty($data['records']['individual_name']) ? $data['records']['individual_name'] : old('individual_name') }}">
                                            <span class="individual_name common_class">{{ $errors->first('individual_name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ic_number">IC  Number</label>
                                            <input type="text" class="form-control" id="ic_number"  name="ic_number" value="{{!empty($data['records']['ic_number']) ? $data['records']['ic_number'] : old('ic_number') }}">
                                            <span class="ic_number common_class">{{ $errors->first('ic_number') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="merchant_email_id">Email ID</label>
                                            <input type="text" class="form-control" id="merchant_email_id"  name="merchant_email_id" value="{{!empty($data['records']['user']['email']) ? $data['records']['user']['email'] : old('merchant_email_id') }}">
                                            <span class="merchant_email_id common_class">{{ $errors->first('merchant_email_id') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Mobile Number</label>
                                            <input type="text" class="form-control" id="phone"  name="phone" value="{{!empty($data['records']['phone']) ? $data['records']['phone'] : old('phone') }}">
                                            <span class="merchant_mobile_number common_class">{{ $errors->first('phone') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="trading_name">Trading Name <span style="font-size: 12px;font-style: italic;">(Displayed on the application)</span></label>
                                            <input type="text" class="form-control" id="trading_name"  name="trading_name" value="{{!empty($data['records']['trading_name']) ? $data['records']['trading_name'] : old('trading_name') }}">
                                            <span class="trading_name common_class">{{ $errors->first('trading_name') }}</span>
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="salesperson_name">Type Of Business</label>
                                            <select  class="form-control chosen-select" id="type_of_business"  name="type_of_business">
                                                <option value="">Select Type Of Business</option>
                                                @foreach($data['business_types'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['type_of_business']['id']) && $data['records']['type_of_business']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="type_of_business common_class">{{ $errors->first('type_of_business') }}</span>
                                        </div>  
                                    </div>

                                   
                                </div>

                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="years_in_business">Year(s) In Business</label>
                                            <input type="text" class="form-control" id="years_in_business"  name="years_in_business" value="{{!empty($data['records']['years_in_business']) ? $data['records']['years_in_business'] : old('years_in_business') }}">
                                            <span class="years_in_business common_class">{{ $errors->first('years_in_business') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('user_password') ? 'has-error' : '' }}">
                                            <label for="user_password">Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span></label>
                                            <input type="password" class="form-control" id="user_password"  name="user_password" value="{{ old('user_password')}}">
                                            @if ($errors->has('user_password'))
                                                <span class="help-block">
                                                    {{ $errors->first('user_password') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                   
                                </div>                                
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('user_password_confirmation') ? 'has-error' : '' }}">
                                            <label for="user_password_confirmation">Confirm Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span> </label>
                                            <input type="password" class="form-control" id="user_password_confirmation"  name="user_password_confirmation" value="{{ old('user_password_confirmation')}}">
                                            @if ($errors->has('user_password_confirmation'))
                                                <span class="help-block">
                                                    {{ $errors->first('user_password_confirmation') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_from">Sign Off Date</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" type="text" placeholder="Sign Off Date"  name="sign_off" id="sign_off" 
                                                           value="{!! (!empty($data['records']['sign_off']) ? $data['records']['sign_off'] : '') !!}" >
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    
                             </div>

                             <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                           <label for="date_from">End Date</label>
                                            <div class="input-group">
                                                <input class="form-control datepicker" type="text" placeholder="End Date"  name="end_date" id="end_date" 
                                                       value="{!! (!empty($data['records']['end_date']) ? $data['records']['end_date'] : '') !!}" >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_company">Agent Company</label>
                                            <input type="text" class="form-control" id="agent_company"  name="agent_company" value="{{!empty($data['records']['agent_company']) ? $data['records']['agent_company'] : old('agent_company') }}">
                                            <span class="agent_company common_class">{{ $errors->first('agent_company') }}</span>
                                        </div>
                                    </div>
                                   
                                </div>

                                   <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_type">Agent Type</label>
                                            <input type="text" class="form-control" id="agent_type"  name="agent_type" value="{{!empty($data['records']['agent_type']) ? $data['records']['agent_type'] : old('agent_type') }}">
                                            <span class="agent_type common_class">{{ $errors->first('agent_type') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_name">Agent Name</label>
                                            <input type="text" class="form-control" id="agent_name"  name="agent_name" value="{{!empty($data['records']['agent_name']) ? $data['records']['agent_name'] : old('agent_name') }}">
                                            <span class="agent_name common_class">{{ $errors->first('agent_name') }}</span>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <div class="row"> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_code">Agent Code</label>
                                            <input type="text" class="form-control" id="agent_code"  name="agent_code" value="{{!empty($data['records']['agent_code']) ? $data['records']['agent_code'] : old('agent_code') }}">
                                            <span class="agent_code common_class">{{ $errors->first('agent_code') }}</span>
                                        </div>
                                    </div>                                   
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="individual_address">Address</label>
                                            <textarea type="text" class="form-control" id="individual_address"  name="individual_address">{{!empty($data['records']['individual_address']) ? $data['records']['individual_address'] : old('individual_address') }}</textarea>
                                            <span class="individual_address common_class">{{ $errors->first('individual_address') }}</span>
                                        </div>
                                    </div>                                    
                                </div>                               

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="status_id">Status</label>
                                                <select  class="form-control chosen-select" id="status_id"  name="status_id">
                                               <!-- <option value="">Select Status</option>-->
                                                @foreach($data['status_records'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['status_id']) && $data['records']['status_id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="status_id common_class">{{ $errors->first('status_id') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->
                    
                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in" id="bank_account">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="bank_name">Bank</label>
                                            <input type="hidden" name="bank_account_id" value="{{ !empty($data['records']['bank_account']['id']) ? $data['records']['bank_account']['id'] : '' }}">
                                            <select  class="form-control" id="bank_name"  name="bank_name">
                                                <option value="">Select Bank</option>
                                                @foreach($data['bank_names'] as $key => $value )
                                                  <option value="{{ $value['id'] }}" {!! (!empty($data['records']['bank']['bank_name']['id']) && $data['records']['bank']['bank_name']['id']==$value['id'] ? 'selected' : '') !!}>{{ $value['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="branch_information">Branch Information</label>
                                            <textarea type="text" class="form-control" id="branch_information"  name="branch_information">{{!empty($data['records']['bank']['branch_information']) ? $data['records']['bank']['branch_information'] : old('branch_information') }}</textarea>
                                            <span class="branch_information common_class">{{ $errors->first('branch_information') }}</span>
                                        </div>  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_number">Account Number</label>
                                            <input type="text" class="form-control" id="acc_number"  name="acc_number" value="{{!empty($data['records']['bank']['acc_number']) ? $data['records']['bank']['acc_number'] : old('acc_number') }}">
                                        </div>  
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_holder_name">Bank Account Holder Name</label>
                                            <input type="text" class="form-control" id="acc_holder_name"  name="acc_holder_name" value="{{!empty($data['records']['bank']['acc_holder_name']) ? $data['records']['bank']['acc_holder_name'] : old('acc_holder_name') }}">
                                        </div>
                                    </div>                                    
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ind_contact_number">Contact Number</label>
                                            <input type="text" class="form-control" id="ind_contact_number"  name="ind_contact_number" value="{{!empty($data['records']['bank']['acc_contact_number']) ? $data['records']['bank']['acc_contact_number'] : old('ind_contact_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_email">Email Address</label>
                                            <input type="text" class="form-control" id="acc_email"  name="acc_email" value="{{!empty($data['records']['bank']['acc_email']) ? $data['records']['bank']['acc_email'] : old('acc_email') }}">
                                        </div>  
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->


                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in" id="fields">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">

                                <table class="table table-hover" id="table_fields"> 
                                    <thead> 
                                        <tr> 
                                            <th>Field Name</th>
                                            <th class="text-center">Display</th>
                                            <th class="text-center">Mandatory</th>
                                            <th class="text-center">Position</th>
                                        </tr> 
                                    </thead> 
                                    <tbody>
                                       <input type="hidden" name="count_fields" id="count_fields" value="<?php echo count($data['input_fields']); ?>">
                                        @foreach($data['input_fields'] as $key => $value)
                                            @if(!empty($data['records']['input_fields']))
                                                @php
                                                    $display = 0;
                                                @endphp
                                                
                                                @foreach($data['records']['input_fields'] as $key_merchant => $value_merchant)
                                                    @if($value_merchant['input_field_id']==$value['id'])
                                                        @php
                                                            $display = $key_merchant+1;
                                                        @endphp
                                                    @endif
                                                @endforeach  

                                               
                                                @if($display>0)
                                                    <tr class="tr-{{ $key }}">
                                                        <td>
                                                            {{$value['name']}}
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">
                                                            
                                                            <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                            <input type="checkbox" id="fields[{{$key}}][display]" class="display" name="fields[{{$key}}][display]" value="1" checked>
                                                        
                                                            <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" disabled>
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" {!! (((!empty($data['records']['input_fields'][$display-1]['is_mandatory']) && $data['records']['input_fields'][$display-1]['is_mandatory']==1) || old('is_mandatory')==1) ? "checked" : '') !!}>
                                                        
                                                            <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" {!! (((!empty($data['records']['input_fields'][$display-1]['is_mandatory']) && $data['records']['input_fields'][$display-1]['is_mandatory']==1) || old('is_mandatory')==1) ? "disabled" : '') !!}>
                                                        </td>
                                                        <td class="text-center">
                                                            @if($value['data_type']['name']=='Image')
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="{{ $data['records']['input_fields'][$display-1]['position'] }}" disabled>
                                                            @else
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="{{ $data['records']['input_fields'][$display-1]['position'] }}">
                                                            @endif                                                                
                                                        </td>
                                                        
                                                    </tr>
                                                @else
                                                    <tr class="tr-{{ $key }}">
                                                        <td>
                                                            {{$value['name']}}
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                            <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                            <input type="checkbox" id="fields[{{$key}}][display]" class="display" name="fields[{{$key}}][display]" value="1" >
                                                        
                                                            <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" >
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" >
                                                        
                                                            <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" >
                                                        </td>
                                                        <td class="text-center">
                                                            @if($value['data_type']['name']=='Image')
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="" disabled>
                                                            @else
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="">
                                                            @endif
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                @endif 
                                            @else
                                            <tr class="tr-{{ $key }}">
                                                <td>
                                                    {{$value['name']}}
                                                </td>
                                                <td class="text-center">
                                                    <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                    <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                    <input type="checkbox" id="fields[{{$key}}][display]" class="display" name="fields[{{$key}}][display]" value="1" >
                                                
                                                    <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" >
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" >
                                                
                                                    <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" >
                                                </td>
                                                <td class="text-center">
                                                    @if($value['data_type']['name']=='Image')
                                                        <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="" disabled>
                                                    @else
                                                        <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="">
                                                    @endif
                                                    
                                                </td>
                                                
                                            </tr>
                                            @endif
                                            
                                        @endforeach     

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->

                    </div><!-- END TAB -->
                    <!-- END FORM UPDATE NOTIFICATION -->
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" >
$(document).ready(function() {

    $("input:file").change(function (){
      var fileName = $(this).val().split('\\').pop();;
      $("label[for='selectedURL']").text(fileName);

        if (this.files && this.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#image').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(this.files[0]);
          }
        else{
            $("#image").attr("src","");
        }
    });

    $(".ass_val").on('change',function(){

        if(parseInt($(this).val())>0)
        {
            if(parseInt($(this).val()) <= parseInt($("#count_fields").val()))
            {
                var x=$(this).val();
                var z=0;
                $("input:text.ass_val").each(function(){
                    var y=$(this).val();
                    if(x==y){
                        z=z+1;
                    }
                });
                if(z>1){
                   alert("You can not enter same value again!");
                   $(this).val('');
                }
            }
            else
            {
                alert("Invalid value");
                $(this).val('');
            }
        }
        else
        {
            alert("Invalid value");
            $(this).val('');
        }
    });

    $(".out_tab").on( "click",function() {
        $('.out_name').prop('required', true);
    });

    $("#selectAll").on( "click",function() {
        var ischecked= $("#selectAll").is(':checked');
        $("input[type='checkbox']").each(function () {
            var id = $(this).attr('id');

            if (ischecked) {
                $("#"+id).prop('checked', true);
                $("#"+id+"-hidden").prop('disabled', true);
            }
            else{
                $("#"+id).prop('checked', false);
                $("#"+id+"-hidden").removeAttr("disabled");
            }
        });
    });

    $("input:checkbox.display").change(function() {
        var id = $(this).attr('id');
        if($(this).is(":checked")) {

            $(this).closest('td').next('td').find('.ass_val').val('0');
            $("#table_fields input[id='"+id+"-hidden']").prop('disabled', true);
            var tr_class = $(this).parents('tr').attr('class');
            $("."+tr_class).find('input.ass_val').prop('required', true);
        }
        else{
            var tr_class = $(this).parents('tr').attr('class');
            $("#table_fields input[id='"+id+"-hidden']").removeAttr("disabled");
            $("."+tr_class).find('input.ass_val').prop('required', false);
            $("."+tr_class).find('input.ass_val').css("border", "1px solid #D5D5D5");
        }
            
    });

    $("input:checkbox").change(function() {
        var id = $(this).attr('id');
        if($(this).is(":checked")) {
            $("#table_fields input[id='"+id+"-hidden']").prop('disabled', true);
        }
        else{
            $("#table_fields input[id='"+id+"-hidden']").removeAttr("disabled");
        }
            
    });

    /*$('#save-merchant').on('click',function(){

        var data = new FormData($('#save_merchant')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_merchant') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                window.location.href = "{{ URL::route('index_merchant') }}";
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
        //return true;
    });*/

    $('#save-merchant').on('click',function(e){
 
        //var email = $("#user_email_id").val();
        var email = 'dipak1630@gmail.com';
        if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){
          var data = new FormData($('#save_individual_merchant')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_individual_merchant') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                window.location.href = "{{ URL::route('index_merchant') }}";
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);                  
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
        //return true;
       } else { 
         event.preventDefault();
      }
    });

    $("#btn_add_outlet").click(function(){
        var total_outlet = $('#ul_outlet li').length;
        
        if (total_outlet < 3) { ///maximum 3 only file user
            cloneUL('ul_outlet');
        }
        
    });

    $("#ul_outlet").on("click", ".btn_remove_outlet", function () {
    
        var rowCount = $('#ul_outlet li').length;
        var id_array =  $(this).attr('id').split("-");
        
        if(rowCount < 2){
            $("#ul_outlet #li_outlet-"+id_array[1]+" #li_outlet_row-"+id_array[1]+" #outlet_id-"+id_array[1]).val("");
        }else{
            $('#li_outlet-'+id_array[1]).remove();
        }
    });

    $("input:checkbox").on('change',function() { 

        var tr_class=$(this).parents('tr').attr('class');
        var id_array = tr_class.split("-");

        if($("[name='fields["+id_array[1]+"][data_type]'").val()=='Image'){
            if($(this).is(":checked")) {
                $('#table_fields tbody tr').each(function(){ 
                    var tr_class_child = $(this).attr('class'); 
                    var id_array_child = tr_class_child.split("-");                       
                    if($("[name='fields["+id_array_child[1]+"][data_type]'").val()=='Image'){
                        if(id_array[1]!=id_array_child[1]){
                            $(this).find('input').prop('disabled', true); 
                        }         
                    }
                });
            }else{
                $('#table_fields tbody tr').each(function(){ 
                    var tr_class_child = $(this).attr('class'); 
                    var id_array_child = tr_class_child.split("-");
                    if($("[name='fields["+id_array_child[1]+"][data_type]'").val()=='Image'){
                        if(id_array[1]!=id_array_child[1]){
                            $(this).find('input').prop('disabled', false); 
                            $("[name='fields["+id_array_child[1]+"][position]'").prop('disabled', true); 
                        }         
                    }
                });
            }  
        }            
    }); 
});

function cloneUL(table_id){
    var rowCount = $('#'+table_id+' li').length;
    var clone_ele = $('#'+table_id+' li:last').clone();
    
    clone_ele.find("select").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('select[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('select[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find("input").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".li_condition_row").each(function() {
        $(this).attr('id', function(_, id) {

            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_outlet").each(function() {
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.attr('id', function(_, id) {
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    })

    $("#"+table_id+" li:last").after(clone_ele);
    
    clone_ele.attr('id', function(_, id) {
        var id_array = id.split("-");
       
        if(table_id == 'ul_condition'){
            $("#ul_condition #li_condition-"+id_array[1]+" #li_condition_row-"+id_array[1]+" #promotion_name-"+id_array[1]).val("");
            $("#ul_condition #li_condition-"+id_array[1]+" #li_condition_row-"+id_array[1]+" div").not('div:eq(0),div:eq(1)').remove();
        }
    })
}//End Clone Table       

$( document ).ready(function() {

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('.btn-date').click(function () {
        var ele_name = $(this).parents(".input-group-btn").parents(".input-group").first().find('.datepicker').attr('id');
        $('#' + ele_name).datepicker("show");
    });

});
</script>

<!-- role js -->
<script src="{!!asset('/js/summernote.min.js')!!}"></script>
<script src="{!!asset('/js/editor.js')!!}"></script>

@endsection
