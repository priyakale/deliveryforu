@extends('admin.layout')
 

@section('main-content')

<style>
    .align_right{
        margin-left: 700px !important;
    }
</style>

<!-- SEARCH RESULT -->
<div class="container full_width">
	<div class="row">       
         <div class="col-md-12">
            <!-- START SEARCH FILTERS -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    <form action="{{ URL::route('index_merchant') }}">
                        <div class="input-group form-width-full padding-top">
                            <div class="input-group form-width-full">                        
                                <div class="col-md-4">
                                    <label for="keyword">Name</label>
                                    <input type="text" class="form-control" placeholder="Search Name" name="company_registered_name" value="{{ !empty($data['search']['company_registered_name']) ? $data['search']['company_registered_name'] : '' }}" >
                                </div>
                                <div class="col-md-4">
                                    <label for="keyword">Sub Merchant Ref</label>
                                    <input type="text" class="form-control" placeholder="Search Sub Merchant Ref" name="sub_merchant_ref" value="{{ !empty($data['search']['sub_merchant_ref']) ? $data['search']['sub_merchant_ref'] : '' }}" >
                                </div>
                                <div class="col-md-4">
                                    <label for="status_id">Status</label>
                                    <select name="status_id" id="status_id" class="form-control chosen-select">
                                        <option value="">Select Status</option>
                                        @if(!empty($data['status_records']))
                                            @foreach($data['status_records'] as $key => $value)
                                              <option value="{{ $value->id }}" {{ !empty($data['search']['status_id']) && ($data['search']['status_id']==$value['id']) ? 'selected' : '' }}>{{ $value->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>                               
                            </div>
                        </div>
                        <div class="input-group form-width-full padding-top padding-bottom padding-right">
                            <div class="form-group float-right">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-success">
                                    <i class="fa fa-search"></i>Search</button>
                                    &nbsp;&nbsp;
                                    <button type="button" id="valuereset" class="btn btn-defult">
                                    <i class="fa fa-times"></i>Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>                            
            <!-- END SEARCH FILTERS --> 
        </div>
    </div>  
	<!-- SEARCH RESULT -->
	<div class="row">
	    <div class="col-md-12">	    	
	       <div class="panel panel-custom">			  
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td>
                                Search Result  
                                <span class="badge custom-badge"></span>
                            </td>
                            <td>
                                <div class="form-group float-left">
                                    <div class="btn-group input-group">
                                        <a href="{{ URL::route('add_merchant') }}" class="btn btn-success btn-sm float-left align_right"><span class="fa fa-plus"></span>Add Merchant</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <a href="{{ URL::route('add_individual_merchant') }}" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span>Add Individual</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body progress-panel ">                   
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="5%">Logo</th>
                                <th width="20%">Name</th>
                                <th width="20%">Sub Merchant Ref</th>
                                <th width="10%">Status</th>
                                <th width="20%">Created At</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (empty($data['record'][0]))
                            <tr>
                                <th colspan="6" class="empty-record text-center"><label>No Result</label></th>
                            </tr>
                        @endif
                        <?php $n = 0;  ?>
                        @foreach($data['record'] as $index => $record)
                                             
                          <?php  $n++; ?>
                            <tr id="carrier{{$record->id}}">
                                <td>{{ $n }}</td>
                                <td><img src="/uploads/merchants/MerchantId-{{$record['id']}}/{{$record['logo']}}"  width="30" height="30"></td>
                                <td>{{ !empty($record['company_registered_name']) ? $record['company_registered_name']:$record['individual_name'] }}</td>
                                <td>{{ !empty($record['sub_merchant_ref']) ? $record['sub_merchant_ref']:'-' }}</td>
                                <td>{{ $record['status']['name'] }}</td>
                                <td>{{ $record['created_at'] }}</td>
                                <td>
                                    <?php if($record['type_id']==71)
                                    { ?>
                                    <a href="{{ URL::route('edit_merchant',$record->id) }}" class="btn btn-success btn-sm"><span class="fa fa-pencil-alt"></span></a>
                                    <?php } 
                                    else
                                    { ?>
                                        <a href="{{ URL::route('edit_individual_merchant',$record->id) }}" class="btn btn-success btn-sm"><span class="fa fa-pencil-alt"></span></a>
                                    <?php } ?>
                                    <button type="button" class="btn btn-danger btn-sm btn-delete" 
                                    data-id="{{ $record->id }}" >
                                    <span class="fa fa-trash"></span>
                                    </button>  
                                </td>                                   
                            </tr> 
                        @endforeach
                        </tbody>
                    </table>                   
                    <div class="modal fade" id="deleteModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="deleteModalLabel">Delete</h4>
                                </div>
                                <div class="modal-body" id="deleteModal_body">
                                    <h5>Are you sure want to delete this merchant?</h5>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" id="delete_id" />
                                    <button class="btn btn-danger" id="delete">Delete</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	            
	    	</div>
		</div>
	</div>
</div>
@stop

@push('custom_script')
<script>
$(document).ready(function(){
    $('#valuereset').click(function(){
        window.location.href = "{{URL::route('index_merchant')}}" 
    });

    $('.btn-delete').on('click', function(e) {
        var id = $(this).data('id');
        $('#delete_id').val(id);
        $('#deleteModal').modal('show');
    });

    $('#delete').on('click', function(e) {
        var id = $('#delete_id').val();
        // AJAX
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'GET',
            contentType: false,
            processData: false,
            url : "{{ URL::route('delete_merchant') }}/" + id,
            dataType: 'json',
            success: function (data) {
                window.location.href = "{{ URL::route('index_merchant') }}";
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['errors']);
                } 
                else{
                    alertError(jqXHR['responseJSON']['message']);
                } 
            }
        });
        // END AJAX
    });

  /* $('#table_id').DataTable( {
        "order": [[ 2, "asc" ]]
    } );*/
});
</script>
@endpush