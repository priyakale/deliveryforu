@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<!-- Ionicons -->
<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet" type="text/css" />

<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
        <form class="" role="update" name="save_merchant" id="save_merchant" action="{{ URL::route('save_merchant') }}" method="POST" enctype="multipart/form-data">

            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td></td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <button type="button" class="float-right btn btn-success" id="save-merchant">
                                            <i class="fa fa-save"></i>Save
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs submission-step">
                        <li class="active"><a data-toggle="tab" href="#headquarters_details" id="headquarters_details_tab">Headquarter Details</a></li>
                        <li><a data-toggle="tab" href="#outlet_details" id="outlet_details_tab">Outlet/ Salesperson Details</a></li>
                        <li ><a data-toggle="tab" href="#bank_account" id="bank_account_tab">Bank Account</a></li>
                        <li ><a data-toggle="tab" href="#fields" id="fields_tab">Input Fields</a></li>
                    </ul>
                    <!-- FORM UPDATE NOTIFICATION -->
                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="{{!empty($data['records']['id']) ? $data['records']['id']:'' }}">
                    <input type="hidden" name="user_id" id="user_id" value="{{!empty($data['records']['user_id']) ? $data['records']['user_id']:'' }}">

                    <div class="row tab-content" >
                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in active" id="headquarters_details">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="document_upload-1">Logo <span style="font-size: 12px;font-style: italic;">(Type = png, jpeg, jpg | Max Size = 3MB | Dimensions Max Width = 432 and Max Height = 432)</span> </label><br>
                                            <div class="btn-group" role="group" aria-label="...">

                                                <label class="btn btn-default">
                                                  Upload <input name="logo" id="logo" type="file" style="display: none;" accept="png, .jpeg, .jpg"/>
                                                </label>
                                            </div>
                                            
                                            <img id="image" width="60" height="60" src="{{ !empty($data['records']['logo']) ? '/uploads/merchants/MerchantId-'.$data['records']['id'].'/'.$data['records']['logo']:'/assets/img/no_img.png' }}"/>
                                            <br>
                                            <span class="logo common_class">{{ $errors->first('logo') }}</span>
                                        </div>
                                    </div>
                                    

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="sub_merchant_ref">Sub Merchant Identifier</label>
                                            <input type="text" class="form-control" id="sub_merchant_ref"  name="sub_merchant_ref" value="{{!empty($data['records']['sub_merchant_ref']) ? $data['records']['sub_merchant_ref'] : old('sub_merchant_ref') }}">
                                            <span class="sub_merchant_ref common_class">{{ $errors->first('sub_merchant_ref') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="user_email_id">Email ID</label>
                                            <input type="text" class="form-control" id="user_email_id"  name="user_email_id" value="{{!empty($data['records']['user']['email']) ? $data['records']['user']['email'] : old('user_email_id') }}">
                                            <span class="user_email_id common_class">{{ $errors->first('user_email_id') }}</span> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('user_password') ? 'has-error' : '' }}">
                                            <label for="user_password">Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span></label>
                                            <input type="password" class="form-control" id="user_password"  name="user_password" value="{{ old('user_password')}}">
                                            @if ($errors->has('user_password'))
                                                <span class="help-block">
                                                    {{ $errors->first('user_password') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('user_password_confirmation') ? 'has-error' : '' }}">
                                            <label for="user_password_confirmation">Confirm Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span> </label>
                                            <input type="password" class="form-control" id="user_password_confirmation"  name="user_password_confirmation" value="{{ old('user_password_confirmation')}}">
                                            @if ($errors->has('user_password_confirmation'))
                                                <span class="help-block">
                                                    {{ $errors->first('user_password_confirmation') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="company_registered_name">Company Registered Name</label>
                                            <input type="text" class="form-control" id="company_registered_name"  name="company_registered_name" value="{{!empty($data['records']['company_registered_name']) ? $data['records']['company_registered_name'] : old('company_registered_name') }}">
                                            <span class="company_registered_name common_class">{{ $errors->first('company_registered_name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="business_registration_number">Business Registration Number</label>
                                            <input type="text" class="form-control" id="business_registration_number"  name="business_registration_number" value="{{!empty($data['records']['business_registration_number']) ? $data['records']['business_registration_number'] : old('business_registration_number') }}">
                                            <span class="business_registration_number common_class">{{ $errors->first('business_registration_number') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="trading_name">Trading Name <span style="font-size: 12px;font-style: italic;">(Displayed on the application)</span></label>
                                            <input type="text" class="form-control" id="trading_name"  name="trading_name" value="{{!empty($data['records']['trading_name']) ? $data['records']['trading_name'] : old('trading_name') }}">
                                            <span class="trading_name common_class">{{ $errors->first('trading_name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="years_in_business">Year(s) In Business</label>
                                            <input type="number" class="form-control" id="years_in_business"  name="years_in_business" value="{{!empty($data['records']['years_in_business']) ? $data['records']['years_in_business'] : old('years_in_business') }}">
                                            <span class="years_in_business common_class">{{ $errors->first('years_in_business') }}</span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="salesperson_name">Type Of Ownership</label>
                                                <select  class="form-control chosen-select" id="type_of_ownership"  name="type_of_ownership">
                                                <option value="">Select Type Of Ownership</option>
                                                @foreach($data['ownership_types'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['type_of_ownership']['id']) && $data['records']['type_of_ownership']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="type_of_ownership common_class">{{ $errors->first('type_of_ownership') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="salesperson_name">Type Of Business</label>
                                            <select  class="form-control chosen-select" id="type_of_business"  name="type_of_business">
                                                <option value="">Select Type Of Business</option>
                                                @foreach($data['business_types'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['type_of_business']['id']) && $data['records']['type_of_business']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="type_of_business common_class">{{ $errors->first('type_of_business') }}</span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="headquarters_contact_number">Headquarter Contact Number</label>
                                            <input type="text" class="form-control" id="headquarters_contact_number"  name="headquarters_contact_number" value="{{!empty($data['records']['headquarters_contact_number']) ? $data['records']['headquarters_contact_number'] : old('headquarters_contact_number') }}">
                                            <span class="headquarters_contact_number common_class">{{ $errors->first('headquarters_contact_number') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="salesperson_name">Salesperson Name</label>
                                            <input type="text" class="form-control" id="salesperson_name"  name="salesperson_name" value="{{!empty($data['records']['salesperson_name']) ? $data['records']['salesperson_name'] : old('salesperson_name') }}">
                                        <span class="salesperson_name common_class">{{ $errors->first('salesperson_name') }}</span>
                                        </div>  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="headquarters_address">Headquarter Address</label>
                                            <textarea type="text" class="form-control" id="headquarters_address"  name="headquarters_address">{{!empty($data['records']['headquarters_address']) ? $data['records']['headquarters_address'] : old('headquarters_address') }}</textarea>
                                            <span class="headquarters_address common_class">{{ $errors->first('headquarters_address') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_from">Sign Off Date</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" type="text" placeholder="Sign Off Date"  name="sign_off" id="sign_off" 
                                                           value="{!! (!empty($data['records']['sign_off']) ? $data['records']['sign_off'] : '') !!}" >
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <label for="date_from">End Date</label>
                                            <div class="input-group">
                                                <input class="form-control datepicker" type="text" placeholder="End Date"  name="end_date" id="end_date" 
                                                       value="{!! (!empty($data['records']['end_date']) ? $data['records']['end_date'] : '') !!}" >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_company">Agent Company</label>
                                            <input type="text" class="form-control" id="agent_company"  name="agent_company" value="{{!empty($data['records']['agent_company']) ? $data['records']['agent_company'] : old('agent_company') }}">
                                            <span class="agent_company common_class">{{ $errors->first('agent_company') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_type">Agent Type</label>
                                            <input type="text" class="form-control" id="agent_type"  name="agent_type" value="{{!empty($data['records']['agent_type']) ? $data['records']['agent_type'] : old('agent_type') }}">
                                            <span class="agent_type common_class">{{ $errors->first('agent_type') }}</span>
                                        </div>
                                    </div>
                                </div>

                                   <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_name">Agent Name</label>
                                            <input type="text" class="form-control" id="agent_name"  name="agent_name" value="{{!empty($data['records']['agent_name']) ? $data['records']['agent_name'] : old('agent_name') }}">
                                            <span class="agent_name common_class">{{ $errors->first('agent_name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="agent_code">Agent Code</label>
                                            <input type="text" class="form-control" id="agent_code"  name="agent_code" value="{{!empty($data['records']['agent_code']) ? $data['records']['agent_code'] : old('agent_code') }}">
                                            <span class="agent_code common_class">{{ $errors->first('agent_code') }}</span>
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="status_id">Status</label>
                                                <select  class="form-control chosen-select" id="status_id"  name="status_id">
                                              <!--  <option value="">Select Status</option>-->
                                                @foreach($data['status_records'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['status_id']) && $data['records']['status_id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="status_id common_class">{{ $errors->first('status_id') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="status_id">Charges Apply</label>
                                            <br>
                                            <input type="checkbox" onclick="$('.charges_data').toggle();" id="charges_apply" class="charges" name="charges_apply" value="1"  {!! (!empty($data['records']['charges_apply']) && $data['records']['charges_apply']=='1' ? 'checked' : '') !!}>
                                        
                                            <input type="hidden" id="charges_apply-hidden" class="charges" name="charges_apply" value="0" {!! (!empty($data['records']['charges_apply']) && $data['records']['charges_apply']=='1' ? 'disabled' : '') !!}>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                 <div class="col-md-6 charges_data" style="display:none";>
                                        <div class="form-group">
                                        <label for="credit_card">Credit Card Fee (%)</label>
                                        <input type="hidden" name="id" value="">
                                        <input type="number" class="form-control" placeholder="Credit Card Fee (%)" id="credit_card" name="credit_card" value="" required>       
                                        @if ($errors->has('credit_card'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('credit_card') }}</strong>
                                            </span>
                                        @endif                        
                                    </div>
                                </div>
                                <div class="col-md-6 charges_data" style="display:none";>
                                    <div class="form-group">
                                        <label for="debit_card">Debit Card Fee (%)</label>
                                        <input type="number" class="form-control" placeholder="Debit Card Fee (%)" id="debit_card" name="debit_card" value="" required>       
                                        @if ($errors->has('debit_card'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('debit_card') }}</strong>
                                            </span>
                                        @endif                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 charges_data" style="display:none";>
                                    <div class="form-group">
                                        <label for="fpx">FPX Fee (%)</label>
                                        <input type="number" class="form-control" placeholder="FPX Fee (%)" id="fpx" name="fpx" value="" required>       
                                        @if ($errors->has('fpx'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('fpx') }}</strong>
                                            </span>
                                        @endif                        
                                    </div>
                                </div>
                                 <div class="col-md-6 charges_data" style="display:none";>
                                    <div class="form-group">
                                        <label for="convenience_fee">Convenience Fee (RM)</label>
                                        <input type="number" class="form-control" placeholder="Convenience Fee (RM)" id="convenience_fee" name="convenience_fee" value="" required>       
                                        @if ($errors->has('convenience_fee'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('convenience_fee') }}</strong>
                                            </span>
                                        @endif                        
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                        <!-- END GENERAL TAB -->
                        <form id="form1" name="form1"> 
                        <div class="col-md-12 tab-pane fade in" id="outlet_details">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class='row float-left detaill-inner full_width'>
                                    <div class="btn-group input-group">
                                        <button type='button' class='btn btn-default' id='btn_add_outlet'><i class='fa fa-plus'></i>Add Outlet
                                        </button>
                                    </div>
                                    <hr>
                                </div>


                                <div class='row float-left detaill-inner full_width'>
                                    <ul id="ul_outlet" class="list-group">
                                         @if(!empty($data['records']['outlets']) && count($data['records']['outlets']) > 0)
                                            @php $count = 1; $cnt = 0; @endphp
                                            @foreach($data['records']['outlets'] as $record)
                                                <li id='li_outlet-{{ $count }}' class="list-group-item" style="margin-bottom: 10px;">
                                                    <div class="row li_condition_row" id='li_outlet_row-{{ $count }}' >
                                                        <div class="row" id="outlet_valid">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="document_upload-1">Logo <span style="font-size: 12px;font-style: italic;">(Type = png, jpeg, jpg | Max Size = 3MB | Dimensions Max Width = 432 and Max Height = 432)</span> </label><br>
                                                                    <div class="btn-group" role="group" aria-label="...">
                                                                        <label class="btn btn-default">
                                                                          Upload <input name="outlets[{{ $count }}][logo]" class="imgpreview" id="outlets[{{ $count }}][logo]" type="file" style="display: none;" accept="png, .jpeg, .jpg"/>
                                                                        </label>
                                                                    </div>            

                                                                    <img id="outlets-{{ $count }}" name="outlets-{{ $count }}" width="60" height="60" src="{{ !empty($record['outlet_logo']) ? '/uploads/outlets/'.$record['id'].'/'.$record['outlet_logo']:'/assets/img/no_img.png' }}"/>

                                                                    <br>
                                                                    <span class="outlet_logo common_class">{{ $errors->first('outlet_logo') }}</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input type="hidden" class="form-control out_name" id="outlets[{{ $count }}][user_id]"  name="outlets[{{ $count }}][user_id]" value="{{!empty($record['user_id']) ? $record['user_id'] : ''}}">

                                                        <input type="hidden" class="form-control out_name" id="outlets[{{ $count }}][id]"  name="outlets[{{ $count }}][id]" value="{{!empty($record['id']) ? $record['id'] : ''}}">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_name">Outlet</label>
                                                                    <input type="text" class="form-control out_name" id="outlets[{{ $count }}][outlet_name]"  name="outlets[{{ $count }}][outlet_name]" value="{{!empty($record['outlet_name']) ? $record['outlet_name'] : ''}}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_pic_name">Salesperson Name</label>
                                                                    <input type="text" class="form-control" id="outlets[{{ $count }}][outlet_pic_name]"  name="outlets[{{ $count }}][outlet_pic_name]" value="{{!empty($record['outlet_pic_name']) ? $record['outlet_pic_name'] : ''}}">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_pic_email">Email</label>
                                                                    <input type="email" class="form-control" id="outlets[{{ $count }}][outlet_pic_email]"  name="outlets[{{ $count }}][outlet_pic_email]" value="{{!empty($record['outlet_pic_email']) ? $record['outlet_pic_email'] : ''}}">
                                                                </div>  
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_pic_mobile_number">Mobile Number</label>
                                                                    <input type="text" class="form-control" id="outlets[{{ $count }}][outlet_pic_mobile_number]"  name="outlets[{{ $count }}][outlet_pic_mobile_number]" value="{{!empty($record['outlet_pic_mobile_number']) ? $record['outlet_pic_mobile_number'] : ''}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_pic_id_num">Identification Number</label>
                                                                    <input type="text" class="form-control" id="outlets[{{ $count }}][outlet_pic_id_num]"  name="outlets[{{ $count }}][outlet_pic_id_num]" value="{{!empty($record['outlet_pic_id_num']) ? $record['outlet_pic_id_num'] : ''}}">
                                                                </div>  
                                                            </div>
                                                            
                                                        </div>
                                                   
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_pic_password">Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span></label>
                                                                    <input type="password" name="outlets[{{ $count }}][outlet_pic_password]" id="outlets[{{ $count }}][outlet_pic_password]" class="form-control password" data-toggle="password" value="{{!empty($record['outlet_pic_password']) ? $record['outlet_pic_password'] : ''}}">
                                                                </div>  
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="outlet_pic_password_confirmation">Confirm Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span> </label>
                                                                    <input type="password" name="outlets[{{ $count }}][outlet_pic_password_confirmation]" id="outlets[{{ $count }}][outlet_pic_password_confirmation]" class="form-control password" data-toggle="password">
                                                                </div>  
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="outlet_address">Address</label>
                                                                    <textarea type="text" class="form-control" id="outlets[{{ $count }}][outlet_address]"  name="outlets[{{ $count }}][outlet_address]">{{!empty($record['outlet_address']) ? $record['outlet_address'] : ''}}</textarea>
                                                                </div> 
                                                            </div>  
                                                        </div>
                                                       
                                                        <div class="col-md-1 float-right">
                                                            <button type="button" class="btn btn-danger btn_remove_added_outlet" data-id="{{!empty($record['id']) ? $record['id'] : ''}}" id="btn_remove_outlet-{{ $count }}">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </li>
                                                @php $count++; $cnt++; @endphp
                                            @endforeach
                                        @else
                                            <li id='li_outlet-1' class="list-group-item" style="margin-bottom: 10px;">
                                                <div class="row li_condition_row" id='li_outlet_row-1' >
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label >Logo <span style="font-size: 12px;font-style: italic;">(Type = png, jpeg, jpg | Max Size = 3MB | Dimensions Max Width = 432 and Max Height = 432)</span> </label><br>
                                                                <div class="btn-group" role="group" aria-label="...">

                                                                    <label class="btn btn-default">
                                                                      Upload <input name="outlets[1][logo]" id="outlets[1][logo]" type="file" class="imgpreview" style="display: none;" accept="png, .jpeg, .jpg"/>
                                                                    </label>
                                                                </div>
                                                                <img id="outlets-1" name="outlets-1" width="60" height="60" src="{{ !empty($data['records']['outlets'][1]['outlet_logo']) ? '/uploads/merchants/MerchantId-'.$data['records']['id'].'/outlets'.$data['records']['outlets'][1]['outlet_logo']:'/assets/img/no_img.png' }}"/>
                                                                <br>
                                                                <span class="outlet_logo common_class">{{ $errors->first('outlet_logo') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" class="form-control out_name" id="outlets[1][user_id]"  name="outlets[1][user_id]" value="">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_name">Outlet</label>
                                                                <input type="text" class="form-control out_name" id="outlets[1][outlet_name]"  name="outlets[1][outlet_name]" value="{{!empty($outlet_record->outlet_name) ? $outlet_record->outlet_name : ''}}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_pic_name">Salesperson Name</label>
                                                                <input type="text" class="form-control" id="outlets[1][outlet_pic_name]"  name="outlets[1][outlet_pic_name]" value="{{!empty($outlet_record->outlet_pic_name) ? $outlet_record->outlet_pic_name : ''}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_pic_email">Email</label>
                                                                <input type="email" class="form-control" id="outlets[1][outlet_pic_email]"  name="outlets[1][outlet_pic_email]" value="{{!empty($outlet_record->outlet_pic_email) ? $outlet_record->outlet_pic_email : ''}}">
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_pic_mobile_number">Mobile Number</label>
                                                                <input type="text" class="form-control" id="outlets[1][outlet_pic_mobile_number]"  name="outlets[1][outlet_pic_mobile_number]" value="{{!empty($outlet_record->outlet_pic_mobile_number) ? $outlet_record->outlet_pic_mobile_number : ''}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_pic_id_num">Identification Number</label>
                                                                <input type="text" class="form-control" id="outlets[1][outlet_pic_id_num]"  name="outlets[1][outlet_pic_id_num]" value="{{!empty($outlet_record->outlet_pic_id_num) ? $outlet_record->outlet_pic_id_num : ''}}">
                                                            </div>  
                                                        </div>                                                        
                                                    </div>
                                               
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_pic_password">Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span></label>
                                                                <input type="password" name="outlets[1][outlet_pic_password]" id="outlets[1][outlet_pic_password]" class="form-control password" data-toggle="password">
                                                            </div>  
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_pic_password_confirmation">Confirm Password <span style="font-size: 12px;font-style: italic;">(Leave empty if dont want to change password)</span> </label>
                                                                <input type="password" name="outlets[1][outlet_pic_password_confirmation]" id="outlets[1][outlet_pic_password_confirmation]" class="form-control password" data-toggle="password">
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="outlet_address">Address</label>
                                                                <textarea type="text" class="form-control" id="outlets[1][outlet_address]"  name="outlets[1][outlet_address]">{{!empty($outlet_record->outlet_address) ? $outlet_record->outlet_address : ''}}</textarea>
                                                            </div> 
                                                        </div>  
                                                    </div>
                                                    
                                                    <div class="col-md-1 float-right">
                                                        <button type="button" class="btn btn-danger btn_remove_outlet" id="btn_remove_outlet-1">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in" id="bank_account">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="bank_name">Bank</label>
                                            <input type="hidden" name="bank_account_id" value="{{ !empty($data['records']['bank_account']['id']) ? $data['records']['bank_account']['id'] : '' }}">
                                            <select  class="form-control" id="bank_name"  name="bank_name">
                                                <option value="">Select Bank</option>
                                                @foreach($data['bank_names'] as $key => $value )
                                                  <option value="{{ $value['id'] }}" {!! (!empty($data['records']['bank']['bank_name']['id']) && $data['records']['bank']['bank_name']['id']==$value['id'] ? 'selected' : '') !!}>{{ $value['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_number">Bank Account Number</label>
                                            <input type="text" class="form-control" id="acc_number"  name="acc_number" value="{{ !empty($data['records']['bank']['acc_number']) ? $data['records']['bank']['acc_number'] : '' }}">
                                        </div>  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_holder_name">Bank Account Holder Name</label>
                                            <input type="text" class="form-control" id="acc_holder_name"  name="acc_holder_name" value="{{ !empty($data['records']['bank']['acc_holder_name']) ? $data['records']['bank']['acc_holder_name'] : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_finance_contact_number">Finance Contact Person</label>
                                            <input type="text" class="form-control" id="acc_finance_contact_number"  name="acc_finance_contact_number" value="{{ !empty($data['records']['bank']['acc_finance_contact_number']) ? $data['records']['bank']['acc_finance_contact_number'] : '' }}">
                                        </div>  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_contact_number">Contact Number</label>
                                            <input type="text" class="form-control" id="acc_contact_number"  name="acc_contact_number" value="{{ !empty($data['records']['bank']['acc_contact_number']) ? $data['records']['bank']['acc_contact_number'] : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="acc_email">Email Address</label>
                                            <input type="text" class="form-control" id="acc_email"  name="acc_email" value="{{ !empty($data['records']['bank']['acc_email']) ? $data['records']['bank']['acc_email'] : '' }}">
                                        </div>  
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->

                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in" id="fields">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">

                                <table class="table table-hover" id="table_fields"> 
                                    <thead> 
                                        <tr> 
                                            <th>Field Name</th>
                                            <th class="text-center">Display</th>
                                            <th class="text-center">Mandatory</th>
                                            <th class="text-center">Position</th>
                                        </tr> 
                                    </thead> 
                                    <tbody>
                                       <input type="hidden" name="count_fields" id="count_fields" value="<?php echo count($data['input_fields']); ?>">
                                        @foreach($data['input_fields'] as $key => $value)
                                            @if(!empty($data['records']['input_fields']))
                                                @php
                                                    $display = 0;
                                                @endphp
                                                
                                                @foreach($data['records']['input_fields'] as $key_merchant => $value_merchant)
                                                    @if($value_merchant['input_field_id']==$value['id'])
                                                        @php
                                                            $display = $key_merchant+1;
                                                        @endphp
                                                    @endif
                                                @endforeach  

                                               
                                                @if($display>0)
                                                    <tr class="tr-{{ $key }}">
                                                        <td>
                                                            {{$value['name']}}
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                            <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">                             
                                                            <input type="checkbox" id="fields[{{$key}}][display]" class="display dis_row" name="fields[{{$key}}][display]" value="1" checked> 

                                                            <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" disabled>
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" class="dis_row" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" {!! (((!empty($data['records']['input_fields'][$display-1]['is_mandatory']) && $data['records']['input_fields'][$display-1]['is_mandatory']==1) || old('is_mandatory')==1) ? "checked" : '') !!}>
                                                           
                                                            <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" {!! (((!empty($data['records']['input_fields'][$display-1]['is_mandatory']) && $data['records']['input_fields'][$display-1]['is_mandatory']==1) || old('is_mandatory')==1) ? "disabled" : '') !!}>
                                                        </td>
                                                        <td class="text-center">
                                                            @if($value['data_type']['name']=='Image')
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="{{ $data['records']['input_fields'][$display-1]['position'] }}" disabled>
                                                            @else
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="{{ $data['records']['input_fields'][$display-1]['position'] }}">
                                                            @endif                                                                
                                                        </td>
                                                        
                                                    </tr>
                                                @else
                                                    <tr class="tr-{{ $key }}">
                                                        <td>
                                                            {{$value['name']}}
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                            <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                            <input type="checkbox" id="fields[{{$key}}][display]" class="display dis_row" name="fields[{{$key}}][display]" value="1">
                                                        
                                                            <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" >                 
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" class="dis_row" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1">

                                                            <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" >
                                                        </td>
                                                        <td class="text-center">
                                                            @if($value['data_type']['name']=='Image')
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="" disabled>
                                                            @else
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="">
                                                            @endif
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                @endif 
                                            @else
                                            <tr class="tr-{{ $key }}">
                                                <td>
                                                    {{$value['name']}}
                                                </td>
                                                <td class="text-center">
                                                    <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                    <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                    <input type="checkbox" id="fields[{{$key}}][display]" class="display dis_row" name="fields[{{$key}}][display]" value="1" >
                                                
                                                    <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" >
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" class="dis_row" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" >
                                                
                                                    <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" >
                                                </td>
                                                <td class="text-center">
                                                    @if($value['data_type']['name']=='Image')
                                                        <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="" disabled>
                                                    @else
                                                        <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="">
                                                    @endif
                                                    
                                                </td>
                                                
                                            </tr>
                                            @endif
                                            
                                        @endforeach     

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->

                    </div><!-- END TAB -->
                    <!-- END FORM UPDATE NOTIFICATION -->
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete</h4>
            </div>
            <div class="modal-body" id="deleteModal_body">
                <h5>Are you sure want to delete this outlet?</h5>
            </div>
            <div class="modal-footer">

                <form id="delete_outlet_form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="delete_outlet_id" name="delete_outlet_id" value="" />
                    <input type="hidden" id="delete_array_id" name="delete_array_id" value="" />
                </form>


                <button class="btn btn-danger" id="delete">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" >
$(document).ready(function() {

    $("#logo").change(function (){  

      var fileNames = $(this).val().split('\\').pop();;
      $("label[for='selectedURL']").text(fileNames);

        if (this.files && this.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#image').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(this.files[0]);
          }
        else{
            $("#image").attr("src","");
        }
    });   

    $("#ul_outlet").on("change", ".imgpreview", function () { 

        var name = $(this).attr('name');       

        var startIndex = name.indexOf('[');
        var endIndex = name.indexOf(']');
        var oldNumber = parseInt(name.substr(startIndex + 1, endIndex - startIndex - 1));

        var fileName = $(this).val().split('\\').pop();
        $("label[for='selectedURL']").text(fileName);

        if (this.files && this.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#outlets-'+oldNumber).attr('src', e.target.result);
              }
              
              reader.readAsDataURL(this.files[0]);
          }
        else{
            $('#outlets-'+oldNumber).attr("src","");
        }
    });  

    $(".ass_val").on('change',function(){

        if(parseInt($(this).val())>0)
        {
            if(parseInt($(this).val()) <= parseInt($("#count_fields").val()))
            {
                var x=$(this).val();
                var z=0;
                $("input:text.ass_val").each(function(){
                    var y=$(this).val();
                    if(x==y){
                        z=z+1;
                    }
                });
                if(z>1){
                   alert("Sorry, this position is already in the list.");
                   $(this).val('');
                }
            }
            else
            {
                alert("Invalid value");
                $(this).val('');
            }
        }
        else
        {
            alert("Invalid value");
            $(this).val('');
        }
    });

    $(".out_tab").on( "click",function() {
        $('.out_name').prop('required', true);
    });

    $("#selectAll").on( "click",function() {
        var ischecked= $("#selectAll").is(':checked');
        $("input[type='checkbox']").each(function () {
            var id = $(this).attr('id');

            if (ischecked) {
                $("#"+id).prop('checked', true);
                $("#"+id+"-hidden").prop('disabled', true);
            }
            else{
                $("#"+id).prop('checked', false);
                $("#"+id+"-hidden").removeAttr("disabled");
            }
        });
    });

    $("input:checkbox.display").change(function() {
        var id = $(this).attr('id');
        if($(this).is(":checked")) {

            $(this).closest('td').next('td').find('.ass_val').val('0');
            $("#table_fields input[id='"+id+"-hidden']").prop('disabled', true);
            var tr_class = $(this).parents('tr').attr('class');
            $("."+tr_class).find('input.ass_val').prop('required', true);
        }
        else{
            var tr_class = $(this).parents('tr').attr('class');
            $("#table_fields input[id='"+id+"-hidden']").removeAttr("disabled");
            $("."+tr_class).find('input.ass_val').prop('required', false);
            $("."+tr_class).find('input.ass_val').css("border", "1px solid #D5D5D5");
        }
            
    });

    $("input:checkbox").change(function() { 
        var id = $(this).attr('id');
         if($(this).is(":checked")) {
            $("#table_fields input[id='"+id+"-hidden']").prop('disabled', true);
        }
        else{
            $("#table_fields input[id='"+id+"-hidden']").removeAttr("disabled");
        }            
    });

    $("input:checkbox.charges").change(function() { 
        
        var id = $(this).attr('id');

         if($(this).is(":checked")) {
            $("#charges_apply-hidden").prop('disabled', true);
        }
        else{
            $("#charges_apply-hidden").removeAttr("disabled");
        }            
    }); 



    /*$('#save-merchant').on('click',function(){

        var data = new FormData($('#save_merchant')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('save_merchant') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                window.location.href = "{{ URL::route('index_merchant') }}";
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
        //return true;
    });*/

    $('#save-merchant').on('click',function(e){

        var id = $("#id").val();
        if(id!="")
        {
            var email = $("#user_email_id").val();
           //  if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){
              var data = new FormData($('#save_merchant')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                contentType: false,
                processData: false,
                url : "{{ URL::route('save_merchant') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    window.location.href = "{{ URL::route('index_merchant') }}";
                },
                error: function (jqXHR) {
                    if(jqXHR.status==422){
                        showValidationError(jqXHR['responseJSON']['error']['message']);
                    } 
                    else{
                        alertError(jqXHR['responseJSON']);
                    } 
                }
            });
            //return true;
           //  } else {
            //    $('.req_email').text('Please enter email');
              
             //   e.preventDefault();
            // }
        }
       

        else
        {
            var data = new FormData($('#save_merchant')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                contentType: false,
                processData: false,
                url : "{{ URL::route('save_merchant') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    window.location.href = "{{ URL::route('index_merchant') }}";
                },
                error: function (jqXHR) {
                    if(jqXHR.status==422){
                        showValidationError(jqXHR['responseJSON']['error']['message']);
                    } 
                    else{
                        alertError(jqXHR['responseJSON']);
                    } 
                }
            });
            //return true;
        }        
    });

    $("#ul_outlet").on("click", ".btn_remove_added_outlet", function () {
        var id = $(this).data('id');
        var id_array =  $(this).attr('id').split("-");
        $('#delete_outlet_id').val(id);
        $('#delete_array_id').val(id_array[1]);
        $('#deleteModal').modal('show');
    });

    $('#deleteModal').on('click','#delete',function(){
        $('#deleteModal').modal('hide');

        var id_array = $('#delete_array_id').val();
        
        var data = new FormData($('#delete_outlet_form')[0]);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('remove_added_outlet') }}",
            data: data,
            dataType: 'json',
            success: function (data) {

                var rowCount = $('#ul_outlet li').length;
                
                if(rowCount < 2){
                    
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row").find('input').val("");
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row").find('textarea').val("");

                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #outlets-"+id_array).attr('src','/assets/img/no_img.png');
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #btn_remove_outlet-"+id_array).removeClass('btn_remove_added_outlet');
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #btn_remove_outlet-"+id_array).removeAttr("data-id");
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #btn_remove_outlet-"+id_array).addClass('btn_remove_outlet');
                }else{
                    $('#li_outlet-'+id_array).remove();
                }
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
       
    });

    $("#btn_add_outlet").click(function(){
        var total_outlet = $('#ul_outlet li').length;
        
        if (total_outlet < 40) { ///maximum 40 outlets only
            cloneUL('ul_outlet');
        }
        
    });

    $("#ul_outlet").on("click", ".btn_remove_outlet", function () {
    
        var rowCount = $('#ul_outlet li').length;
        var id_array =  $(this).attr('id').split("-");
        
        if(rowCount < 2){
            
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('input').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('textarea').val("");
            
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #outlets-"+id_array[1]).attr('src','/assets/img/no_img.png');
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeClass('btn_remove_added_outlet');
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeAttr("data-id");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).addClass('btn_remove_outlet');
        }else{
            $('#li_outlet-'+id_array[1]).remove();
        }
    });


    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('.btn-date').click(function () {
        var ele_name = $(this).parents(".input-group-btn").parents(".input-group").first().find('.datepicker').attr('id');
        $('#' + ele_name).datepicker("show");
    });

    $('.outlet_valid').click(function() { 
        if ($("#outlet_pic_id_num").valid()) {
            //activate next tab
            $('#outlet_pic_name').tabs('enable', $(this).attr("rel"));
            //switch to next tab
            $tabs.tabs('select', $(this).attr("rel"));
            return false;
        }
    });

    $("input:checkbox").on('change',function() { 

        var tr_class=$(this).parents('tr').attr('class');
        var id_array = tr_class.split("-");

        if($("[name='fields["+id_array[1]+"][data_type]'").val()=='Image'){
            if($(this).is(":checked")) {
                $('#table_fields tbody tr').each(function(){ 
                    var tr_class_child = $(this).attr('class'); 
                    var id_array_child = tr_class_child.split("-");                       
                    if($("[name='fields["+id_array_child[1]+"][data_type]'").val()=='Image'){
                        if(id_array[1]!=id_array_child[1]){
                            $(this).find('input').prop('disabled', true); 
                        }         
                    }
                });
            }else{
                $('#table_fields tbody tr').each(function(){ 
                    var tr_class_child = $(this).attr('class'); 
                    var id_array_child = tr_class_child.split("-");
                    if($("[name='fields["+id_array_child[1]+"][data_type]'").val()=='Image'){
                        if(id_array[1]!=id_array_child[1]){
                            $(this).find('input').prop('disabled', false); 
                            $("[name='fields["+id_array_child[1]+"][position]'").prop('disabled', true); 
                        }         
                    }
                });
            }  
        }            
    }); 
});

function cloneUL(table_id){ 
    var rowCount = $('#'+table_id+' li').length;
    var clone_ele = $('#'+table_id+' li:last').clone();
    
    clone_ele.find("textarea").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('textarea[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) { 
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('select[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });

    }).end();

    clone_ele.find("input").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) { 
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find("img").each(function () {
        $(this).val('').attr('id', function (_, id) {
            var id_array = id.split("-");
            if (id_array.length > 1) {
                var new_id = id_array[0] + '-' + rowCount;
                while ($('#' + table_id + ' #' + new_id).length) {
                    rowCount++;
                    new_id = id_array[0] + '-' + rowCount;
                }
                return new_id;
            } else {
                return id;
            }
        });
        $(this).attr('name', function (_, id) {
            var id_array = id.split("-");
            if (id_array.length > 1) {
                var new_id = id_array[0] + '-' + rowCount;
                while ($('#' + table_id + ' #' + new_id).length) {
                    rowCount++;
                    new_id = id_array[0] + '-' + rowCount;
                }
                return new_id;
            } else {
                return id;
            }
        });
    }).end();

    clone_ele.find(".li_condition_row").each(function() {
        $(this).attr('id', function(_, id) { 

            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_outlet").each(function() {
        $(this).attr('id', function(_, id) { 
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_added_outlet").each(function() {
        $(this).attr('id', function(_, id) { 
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.attr('id', function(_, id) { 
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    })

    $("#"+table_id+" li:last").after(clone_ele);
    
    clone_ele.attr('id', function(_, id) {
        var id_array = id.split("-");
       
        if(table_id == 'ul_outlet'){
                                                        
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('input').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('textarea').val("");

            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #outlets-"+id_array[1]).attr('src','/assets/img/no_img.png');
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeClass('btn_remove_added_outlet');
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeAttr("data-id");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).addClass('btn_remove_outlet');
        }
    })
}//End Clone Table
</script>
<script>
$(document).ready(function(){

    function toggler(divId) {
        $("#" + divId).toggle();
    }      
});
</script>
<!-- role js -->
<script src="{!!asset('/js/summernote.min.js')!!}"></script>
<script src="{!!asset('/js/editor.js')!!}"></script>

@endsection
