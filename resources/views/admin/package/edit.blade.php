@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<!-- Ionicons -->
<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    select.ppsel__blck{
        display: block !important;
        width: 100%;
    }
 .hidechosefiles .chosen-single{
    display: none !important;
 }
</style>
<div class="container full_width">
    <div class="row">
        <div class="col-lg-12">
        <form class="" role="update" name="save_package" id="save_package" action="{{ URL::route('save_package') }}" method="POST" enctype="multipart/form-data">

            <div class="panel panel-custom">
                <div class="panel-heading panel-heading-custom">
                    <table class="full_width">
                        <tr>
                            <td></td>
                            <td>
                                <div class="form-group float-right">
                                    <div class="btn-group input-group">
                                        <button type="button" class="float-right btn btn-success" id="save-package">
                                            <i class="fa fa-save"></i>Save
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs submission-step">
                        <li class="active"><a data-toggle="tab" href="#headquarters_details" id="headquarters_details_tab">Package</a></li>
                        <li><a data-toggle="tab" href="#sub_services" id="sub_services_tab">Sub Service</a></li>
                        <li ><a data-toggle="tab" href="#document_checklist" id="document_checklist_tab">Document Checklist</a></li>
                        <li ><a data-toggle="tab" href="#fields" id="fields_tab">Input Fields</a></li>
                    </ul>
                    <!-- FORM UPDATE NOTIFICATION -->
                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="{{!empty($data['records']['id']) ? $data['records']['id']:'' }}">
                    <input type="hidden" name="pkg_id" id="pkg_id" value="{{!empty($data['records']['pkg_id']) ? $data['records']['pkg_id']:'' }}">

                    <div class="row tab-content" >
                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in active" id="headquarters_details">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="document_upload-1">Logo <span style="font-size: 12px;font-style: italic;">(Type = png, jpeg, jpg | Max Size = 3MB | Dimensions Max Width = 432 and Max Height = 432)</span> </label><br>
                                            <div class="btn-group" role="group" aria-label="...">
                                                <label class="btn btn-default">
                                                  Upload <input name="logo" id="logo" type="file" style="display: none;" accept="png, .jpeg, .jpg"/>
                                                </label>
                                            </div>
                                            
                                            <img id="image" width="60" height="60" src="{{ !empty($data['records']['logo']) ? '/uploads/packages/'.$data['records']['id'].'/'.$data['records']['logo']:'/assets/img/no_img.png' }}"/>
                                            <br>
                                            <span class="logo common_class">{{ $errors->first('logo') }}</span>
                                        </div>
                                    </div> 
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="pkgcode">Package Code</label>
                                            <input type="text" class="form-control" id="pkgcode"  name="pkgcode" value="{{!empty($data['records']['pkgcode']) ? $data['records']['pkgcode'] : old('pkgcode') }}">
                                            <span class="pkgcode common_class">{{ $errors->first('pkgcode') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name"  name="name" value="{{!empty($data['records']['name']) ? $data['records']['name'] : old('name') }}">
                                            <span class="name common_class">{{ $errors->first('name') }}</span> 
                                        </div>
                                    </div>
                                </div>                               
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="description">Description</label> 
                                            <textarea type="text" class="form-control" id="description"  name="description">{{!empty($data['records']['description']) ? $data['records']['description'] : old('description') }}</textarea>
                                            <span class="description common_class">{{ $errors->first('description') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="category_id">Category</label>
                                                <select  class="form-control chosen-select" id="category_id"  name="category_id">
                                                <option value="">Select Category</option>
                                                @foreach($data['category_id'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['category_id']['id']) && $data['records']['category_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="category_id common_class">{{ $errors->first('category_id') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="date_from">Start Date</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" type="text" placeholder="Start Date"  name="start" id="start" 
                                                           value="{!! (!empty($data['records']['start']) ? $data['records']['start'] : '') !!}" >
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <label for="date_from">End Date</label>
                                            <div class="input-group">
                                                <input class="form-control datepicker" type="text" placeholder="End Date"  name="end" id="end" 
                                                       value="{!! (!empty($data['records']['end']) ? $data['records']['end'] : '') !!}" >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                </div>                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="deal_method_id">Deal Method</label>
                                                <select  class="form-control chosen-select" id="deal_method_id"  name="deal_method_id">
                                                <option value="">Select Deal Method</option>
                                                @foreach($data['deal_method_id'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['deal_method_id']['id']) && $data['records']['deal_method_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="deal_method_id common_class">{{ $errors->first('deal_method_id') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="payment_type_id">Payment Type</label>
                                            <select  class="form-control chosen-select" id="payment_type_id"  name="payment_type_id">
                                                <option value="">Select Payment Type</option>
                                                @foreach($data['payment_type_id'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['payment_type_id']['id']) && $data['records']['payment_type_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="payment_type_id common_class">{{ $errors->first('payment_type_id') }}</span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="insurance_id">Insurance</label>
                                            <select  class="form-control chosen-select" id="insurance_id"  name="insurance_id">
                                                <option value="">Select Insurance</option>
                                                @foreach($data['insurance_id'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['insurance_id']['id']) && $data['records']['insurance_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="insurance_id common_class">{{ $errors->first('insurance_id') }}</span>
                                        </div>  
                                    </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inhouse_id">In House</label>
                                            <select  class="form-control chosen-select" id="inhouse_id" name="inhouse_id">
                                                <option value="">Select In House</option>
                                                @foreach($data['inhouse_id'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['inhouse_id']['id']) && $data['records']['inhouse_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="inhouse_id common_class">{{ $errors->first('inhouse_id') }}</span>
                                        </div>  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ruleid">RuleId</label>
                                            <select  class="form-control chosen-select" id="ruleid" name="ruleid">
                                            <option value="">Select RuleId</option>
                                                @foreach($data['ruleid'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['ruleid']['id']) && $data['records']['ruleid']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="ruleid common_class">{{ $errors->first('ruleid') }}</span>
                                        </div>  
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="status_id">Status</label>
                                                <select class="form-control chosen-select" id="status_id"name="status_id">   
                                                @foreach($data['status_records'] as $key => $value)
                                                    <option value="{{$value['id']}}" {!! (!empty($data['records']['status_id']['id']) && $data['records']['status_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="status_id common_class">{{ $errors->first('status_id') }}</span>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->
                        <form id="form1" name="form1"> 
                        <div class="col-md-12 tab-pane fade in" id="sub_services">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class='row float-left detaill-inner full_width'>
                                    <div class="btn-group input-group">
                                        <button type='button' class='btn btn-default' id='btn_add_sub_services'><i class='fa fa-plus'></i>Add Sub Services
                                        </button>
                                    </div>
                                    <hr>
                                </div>
                                <div class='row float-left detaill-inner full_width'>
                                    <ul id="ul_outlet" class="list-group"> 
                                    @php $count = 1; $cnt = 0; @endphp
                                    @if(!empty($data['records']['subservice']) && count($data['records']['subservice']) > 0)
                                    
                                    @foreach($data['records']['subservice'] as $record) 
                                        <li id='li_outlet-{{ $count }}' class="list-group-item" style="margin-bottom: 10px;">
                                                <div class="row li_condition_row" id='li_outlet_row-{{ $count }}'>

                                                    <input type="hidden" class="form-control out_name" id="sub_services[{{ $count }}][id]"  name="sub_services[{{ $count }}][id]" value="{{!empty($record['id']) ? $record['id'] : ''}}">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="name">Name</label>
                                                                    <input type="text" class="form-control out_name" id="sub_services[{{ $count }}][sub_service_name]"  name="sub_services[{{ $count }}][sub_service_name]" value="{{!empty($record['name']) ? $record['name'] : ''}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="description">Description</label>
                                                                <textarea type="text" class="form-control out_description" id="sub_services[{{ $count }}][sub_service_description]"  name="sub_services[{{ $count }}][sub_service_description]">{{!empty($record['description']) ? $record['description'] : ''}}</textarea>         
                                                            </div>
                                                        </div>                 

                                                    </div>
                                                    <div class="row">    
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="date_from">Start Date</label>
                                                                <input type="text" class="form-control out_start datepicker" id="sub_services[{{ $count }}][sub_service_start]"  name="sub_services[{{ $count }}][sub_service_start]" value="{{!empty($record['start']) ? $record['start'] : ''}}">
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                <label for="date_to">End Date</label>
                                                                <input type="text" class="form-control out_end datepicker" id="sub_services[{{ $count }}][sub_service_end]"  name="sub_services[{{ $count }}][sub_service_end]" value="{{!empty($record['end']) ? $record['end'] : ''}}">
                                                                 
                                                                </div>  
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="row">          
                                                           <div class="col-md-6">
                                                            <div class="form-group hidechosefiles">
                                                                <label for="status">Status</label>
                                                                <select class="form-control ppsel__blck out_status chosen-select" id="sub_services[{{ $count }}][sub_service_status]"  name="sub_services[{{ $count }}][sub_service_status]">   
                                                                @foreach($data['status_records'] as $key => $value)
                                                                <option value="{{$value['id']}}" {!! (!empty($data['records']['status_id']['id']) && $data['records']['status_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                                @endforeach
                                                                </select>
                                                            </div>
                                                        </div>                        
                                                        </div>
                                                        <div class="col-md-1 float-right">
                                                            <button type="button" class="btn btn-danger btn_remove_added_outlet" data-id="{{!empty($record['id']) ? $record['id'] : ''}}" id="btn_remove_outlet-{{ $count }}">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </li>
                                                @php $count++; $cnt++; @endphp

                                            @endforeach
                                        @else
                                            <li id='li_outlet-1' class="list-group-item sub_services_li" style="margin-bottom: 10px;">
                                                <div class="row li_condition_row" id='li_outlet_row-1'>                         
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="name">Sub Service</label>
                                                                <input type="text" class="form-control out_name" id="sub_services[1][sub_service_name]"  name="sub_services[1][sub_service_name]" value="{{!empty($subservice->name) ? $subservice->name : ''}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="description">Description</label>
                                                                <textarea type="text" class="form-control out_description" id="sub_services[1][sub_service_description]"  name="sub_services[1][sub_service_description]">{{!empty($subservice->description) ? $subservice->description : ''}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="start_date">Start Date</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control out_start datepicker" id="sub_services[1][sub_service_start]"  name="sub_services[1][sub_service_start]" value="{{!empty($subservice['start']) ? $subservice['start'] : ''}}">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                                    </div>
                                                                </div>                     
                                                            </div>  
                                                        </div>
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="end_date">End Date</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control out_end datepicker" id="sub_services[1][sub_service_end]"  name="sub_services[1][sub_service_end]" value="{{!empty($subservice['end']) ? $subservice['end'] : ''}}">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-default btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                                                    </div>
                                                                </div>                  
                                                            </div>  
                                                        </div>
                                                    </div>                
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group hidechosefiles">
                                                                <label for="status">Status</label>
                                                                <select class="form-control ppsel__blck out_status chosen-select" id="sub_services[1][sub_service_status]"  name="sub_services[1][sub_service_status]">   
                                                        @foreach($data['status_records'] as $key => $value)
                                                        <option value="{{$value['id']}}" {!! (!empty($data['records']['status_id']['id']) && $data['records']['status_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>                      @endforeach
                                                                </select>
                                                                <span class="status common_class">{{ $errors->first('status') }}</span>
                                                            </div>  
                                                        </div>                         
                                                    </div>                
                                                    <div class="col-md-1 float-right">
                                                        <button type="button" class="btn btn-danger btn_remove_outlet" id="btn_remove_outlet-1">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                            <input type="hidden" id="sub_service_count" value="{{ $count }}">
                                        @endif
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in" id="document_checklist">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">
                                <div class="row"> 
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="hidden" name="document_checklist_id" value="{{ !empty($data['records']['servicedocs']['id']) ? $data['records']['servicedocs']['id'] : '' }}">
                                            <input type="text" class="form-control" id="doc_name"  name="doc_name" value="{{ !empty($data['records']['servicedocs']['name']) ? $data['records']['servicedocs']['name'] : '' }}">
                                        </div>
                                    </div>                                  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea type="text" class="form-control" id="doc_description" name="doc_description">{{ !empty($data['records']['servicedocs']['description']) ? $data['records']['servicedocs']['description'] : '' }}</textarea>
                                            <span class="description">{{ $errors->first('description') }}</span>
                                        </div>  
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="is_required">Status</label>   
                                            <select  class="form-control" id="is_required"  name="is_required">
                                                @foreach($data['status_records'] as $key => $value )
                                                 <option value="{{$value['id']}}" {!! (!empty($data['records']['status_id']['id']) && $data['records']['status_id']['id']==$value['id'] ? 'selected' : '') !!}>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->

                        <!-- START GENERAL TAB-->
                        <div class="col-md-12 tab-pane fade in" id="fields">
                            <div class="widget widget-default widget-item-icon padding-left padding-right padding-bottom">

                                <table class="table table-hover" id="table_fields"> 
                                    <thead> 
                                        <tr> 
                                            <th>Field Name</th>
                                            <th class="text-center">Display</th>
                                            <th class="text-center">Mandatory</th>
                                            <th class="text-center">Position</th>
                                        </tr> 
                                    </thead> 
                                    <tbody>
                                       <input type="hidden" name="count_fields" id="count_fields" value="<?php echo count($data['input_fields']); ?>">
                                        @foreach($data['input_fields'] as $key => $value)
                                            @if(!empty($data['records']['input_fields']))
                                                @php
                                                    $display = 0;
                                                @endphp
                                                
                                                @foreach($data['records']['input_fields'] as $key_merchant => $value_merchant)
                                                    @if($value_merchant['input_field_id']==$value['id'])
                                                        @php
                                                            $display = $key_merchant+1;
                                                        @endphp
                                                    @endif
                                                @endforeach  

                                               
                                                @if($display>0)
                                                    <tr class="tr-{{ $key }}">
                                                        <td>
                                                            {{$value['name']}}
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                            <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">                             
                                                            <input type="checkbox" id="fields[{{$key}}][display]" class="display dis_row" name="fields[{{$key}}][display]" value="1" checked> 

                                                            <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" disabled>
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" class="dis_row" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" {!! (((!empty($data['records']['input_fields'][$display-1]['is_mandatory']) && $data['records']['input_fields'][$display-1]['is_mandatory']==1) || old('is_mandatory')==1) ? "checked" : '') !!}>
                                                           
                                                            <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" {!! (((!empty($data['records']['input_fields'][$display-1]['is_mandatory']) && $data['records']['input_fields'][$display-1]['is_mandatory']==1) || old('is_mandatory')==1) ? "disabled" : '') !!}>
                                                        </td>
                                                        <td class="text-center">
                                                            @if($value['data_type']['name']=='Image')
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="{{ $data['records']['input_fields'][$display-1]['position'] }}" disabled>
                                                            @else
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="{{ $data['records']['input_fields'][$display-1]['position'] }}">
                                                            @endif                                                                
                                                        </td>
                                                        
                                                    </tr>
                                                @else
                                                    <tr class="tr-{{ $key }}">
                                                        <td>
                                                            {{$value['name']}}
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                            <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                            <input type="checkbox" id="fields[{{$key}}][display]" class="display dis_row" name="fields[{{$key}}][display]" value="1">
                                                        
                                                            <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" >                 
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="checkbox" class="dis_row" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1">

                                                            <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" >
                                                        </td>
                                                        <td class="text-center">
                                                            @if($value['data_type']['name']=='Image')
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="" disabled>
                                                            @else
                                                                <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="">
                                                            @endif
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                @endif 
                                            @else
                                            <tr class="tr-{{ $key }}">
                                                <td>
                                                    {{$value['name']}}
                                                </td>
                                                <td class="text-center">
                                                    <input type="hidden" id="fields[{{$key}}][data_type]" name="fields[{{$key}}][data_type]" value="{{$value['data_type']['name']}}">

                                                    <input type="hidden" id="fields[{{$key}}][id]" name="fields[{{$key}}][id]" value="{{$value['id']}}">

                                                    <input type="checkbox" id="fields[{{$key}}][display]" class="display dis_row" name="fields[{{$key}}][display]" value="1" >
                                                
                                                    <input type="hidden" id="fields[{{$key}}][display]-hidden" name="fields[{{$key}}][display]" value="0" >
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" class="dis_row" id="fields[{{$key}}][is_mandatory]" name="fields[{{$key}}][is_mandatory]" value="1" >
                                                
                                                    <input type="hidden" id="fields[{{$key}}][is_mandatory]-hidden" name="fields[{{$key}}][is_mandatory]" value="0" >
                                                </td>
                                                <td class="text-center">
                                                    @if($value['data_type']['name']=='Image')
                                                        <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="" disabled>
                                                    @else
                                                        <input type="text" class="form-control ass_val" id="fields[{{$key}}][position]"  name="fields[{{$key}}][position]" value="">
                                                    @endif
                                                    
                                                </td>
                                                
                                            </tr>
                                            @endif
                                            
                                        @endforeach     

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END GENERAL TAB -->

                    </div><!-- END TAB -->
                    <!-- END FORM UPDATE NOTIFICATION -->
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete</h4>
            </div>
            <div class="modal-body" id="deleteModal_body">
                <h5>Are you sure want to delete this sub services?</h5>
            </div>
            <div class="modal-footer">

                <form id="delete_outlet_form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="delete_sub_service_id" name="delete_sub_service_id" value="" />
                    <input type="hidden" id="delete_array_id" name="delete_array_id" value="" />
                </form>


                <button class="btn btn-danger" id="delete">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" >
$(document).ready(function() {    

    $("#logo").change(function (){  

      var fileNames = $(this).val().split('\\').pop();;
      $("label[for='selectedURL']").text(fileNames);

        if (this.files && this.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function (e) {
                  $('#image').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(this.files[0]);
          }
        else{
            $("#image").attr("src","");
        }
    }); 
    
    $(".ass_val").on('change',function(){

        if(parseInt($(this).val())>0)
        {
            if(parseInt($(this).val()) <= parseInt($("#count_fields").val()))
            {
                var x=$(this).val();
                var z=0;
                $("input:text.ass_val").each(function(){
                    var y=$(this).val();
                    if(x==y){
                        z=z+1;
                    }
                });
                if(z>1){
                   alert("Sorry, this position is already in the list.");
                   $(this).val('');
                }
            }
            else
            {
                alert("Invalid value");
                $(this).val('');
            }
        }
        else
        {
            alert("Invalid value");
            $(this).val('');
        }
    });

    $(".out_tab").on( "click",function() {
        $('.out_name').prop('required', true);
    });

    $("#selectAll").on( "click",function() {
        var ischecked= $("#selectAll").is(':checked');
        $("input[type='checkbox']").each(function () {
            var id = $(this).attr('id');

            if (ischecked) {
                $("#"+id).prop('checked', true);
                $("#"+id+"-hidden").prop('disabled', true);
            }
            else{
                $("#"+id).prop('checked', false);
                $("#"+id+"-hidden").removeAttr("disabled");
            }
        });
    });

    $("input:checkbox.display").change(function() {
        var id = $(this).attr('id');
        if($(this).is(":checked")) {

            $(this).closest('td').next('td').find('.ass_val').val('0');
            $("#table_fields input[id='"+id+"-hidden']").prop('disabled', true);
            var tr_class = $(this).parents('tr').attr('class');
            $("."+tr_class).find('input.ass_val').prop('required', true);
        }
        else{
            var tr_class = $(this).parents('tr').attr('class');
            $("#table_fields input[id='"+id+"-hidden']").removeAttr("disabled");
            $("."+tr_class).find('input.ass_val').prop('required', false);
            $("."+tr_class).find('input.ass_val').css("border", "1px solid #D5D5D5");
        }
            
    });

    $("input:checkbox").change(function() { 
        var id = $(this).attr('id');
         if($(this).is(":checked")) {
            $("#table_fields input[id='"+id+"-hidden']").prop('disabled', true);
        }
        else{
            $("#table_fields input[id='"+id+"-hidden']").removeAttr("disabled");
        }            
    });    
    
    $('#save-package').on('click',function(e){

        var id = $("#id").val();
        if(id!="")
        {
            var data = new FormData($('#save_package')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                contentType: false,
                processData: false,
                url : "{{ URL::route('save_package') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    window.location.href = "{{ URL::route('index_packageservice') }}";
                },
                error: function (jqXHR) {
                    if(jqXHR.status==422){
                        showValidationError(jqXHR['responseJSON']['error']['message']);
                    } 
                    else{
                        alertError(jqXHR['responseJSON']);
                    } 
                }
            });            
        }else{
            var data = new FormData($('#save_package')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  $('input[name="_token"]').val()
                },
                type: 'POST',
                contentType: false,
                processData: false,
                url : "{{ URL::route('save_package') }}",
                data: data,
                dataType: 'json',
                success: function (data) {
                    window.location.href = "{{ URL::route('index_packageservice') }}";
                },
                error: function (jqXHR) {
                    if(jqXHR.status==422){
                        showValidationError(jqXHR['responseJSON']['error']['message']);
                    } 
                    else{
                        alertError(jqXHR['responseJSON']);
                    } 
                }
            });
            //return true;
        }        
    });

    $("#ul_outlet").on("click", ".btn_remove_added_outlet", function () {
        var id = $(this).data('id');
        var id_array =  $(this).attr('id').split("-");
        $('#delete_sub_service_id').val(id);
        $('#delete_array_id').val(id_array[1]);
        $('#deleteModal').modal('show');
    });

    $('#deleteModal').on('click','#delete',function(){
        $('#deleteModal').modal('hide');

        var id_array = $('#delete_array_id').val();
        
        var data = new FormData($('#delete_outlet_form')[0]);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN':  $('input[name="_token"]').val()
            },
            type: 'POST',
            contentType: false,
            processData: false,
            url : "{{ URL::route('remove_added_subservices') }}",
            data: data,
            dataType: 'json',
            success: function (data) {

                var rowCount = $('#ul_outlet li').length;
                
                if(rowCount < 2){
                    
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row").find('input').val("");
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row").find('textarea').val("");
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row").find('select').val("");

                    
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #btn_remove_outlet-"+id_array).removeClass('btn_remove_added_outlet');
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #btn_remove_outlet-"+id_array).removeAttr("data-id");
                    $("#ul_outlet #li_outlet-"+id_array+" .li_condition_row #btn_remove_outlet-"+id_array).addClass('btn_remove_outlet');
                }else{
                    $('#li_outlet-'+id_array).remove();
                }
            },
            error: function (jqXHR) {
                if(jqXHR.status==422){
                    showValidationError(jqXHR['responseJSON']['error']['message']);
                } 
                else{
                    alertError(jqXHR['responseJSON']);
                } 
            }
        });
       
    });

    $("#btn_add_sub_services").click(function(){
        var total_outlet = $('#ul_outlet li').length;    
        if (total_outlet < 40) { ///maximum 40 sub_services only
            $('#ul_outlet li').last().clone().appendTo('#ul_outlet');
            var count = parseInt($("#sub_service_count").val());
            count=count+1;
            $('#ul_outlet li:last-child').find(".out_name").attr("id","sub_services["+count+"][sub_service_name]").attr("name","sub_services["+count+"][sub_service_name]");
            $('#ul_outlet li:last-child').find(".out_description").attr("id","sub_services["+count+"][sub_service_description]").attr("name","sub_services["+count+"][sub_service_description]");
            $('#ul_outlet li:last-child').find(".out_start").attr("id","sub_services["+count+"][sub_service_start]").attr("name","sub_services["+count+"][sub_service_start]");
            $('#ul_outlet li:last-child').find(".out_end").attr("id","sub_services["+count+"][sub_service_end]").attr("name","sub_services["+count+"][sub_service_end]");
            $('#ul_outlet li:last-child').find(".out_status").attr("id","sub_services["+count+"][sub_service_status]").attr("name","sub_services["+count+"][sub_service_status]");
            $("#sub_service_count").val(count);
        }

        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });
        
    });

    $("#ul_outlet").on("click", ".btn_remove_outlet", function () {
    
        var rowCount = $('#ul_outlet li').length;
        var id_array =  $(this).attr('id').split("-");
        
        if(rowCount < 2){
            
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('input').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('textarea').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('select').val("");            
            
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeClass('btn_remove_added_outlet');
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeAttr("data-id");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).addClass('btn_remove_outlet');
        }else{
            $('#li_outlet-'+id_array[1]).remove();
        }
    });


    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('.btn-date').click(function () {
        var ele_name = $(this).parents(".input-group-btn").parents(".input-group").first().find('.datepicker').attr('id');
        $('#' + ele_name).datepicker("show");
    });    

    $("input:checkbox").on('change',function() { 

        var tr_class=$(this).parents('tr').attr('class');
        var id_array = tr_class.split("-");

        if($("[name='fields["+id_array[1]+"][data_type]'").val()=='Image'){
            if($(this).is(":checked")) {
                $('#table_fields tbody tr').each(function(){ 
                    var tr_class_child = $(this).attr('class'); 
                    var id_array_child = tr_class_child.split("-");                       
                    if($("[name='fields["+id_array_child[1]+"][data_type]'").val()=='Image'){
                        if(id_array[1]!=id_array_child[1]){
                            $(this).find('input').prop('disabled', true); 
                        }         
                    }
                });
            }else{
                $('#table_fields tbody tr').each(function(){ 
                    var tr_class_child = $(this).attr('class'); 
                    var id_array_child = tr_class_child.split("-");
                    if($("[name='fields["+id_array_child[1]+"][data_type]'").val()=='Image'){
                        if(id_array[1]!=id_array_child[1]){
                            $(this).find('input').prop('disabled', false); 
                            $("[name='fields["+id_array_child[1]+"][position]'").prop('disabled', true); 
                        }         
                    }
                });
            }  
        }            
    }); 
});

function cloneUL(table_id){ 
    var rowCount = $('#'+table_id+' li').length;
    var clone_ele = $('#'+table_id+' li:last').clone();
    
    clone_ele.find("textarea").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('textarea[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });        

        $(this).attr('name', function(_, id) { 
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('select[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });

    }).end();

    clone_ele.find("input").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[id="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) { 
            var startIndex = id.indexOf('[');
            var endIndex = id.indexOf(']');
            var oldNumber = parseInt(id.substr(startIndex + 1, endIndex - startIndex - 1));
            if(Number.isInteger(oldNumber)){
                var incrementedNumber = oldNumber+1;
                var newId = id.replace(oldNumber, incrementedNumber);
                while ($('input[name="'+newId+'"]').length > 1) {
                    incrementedNumber++;
                    newId = id.replace(oldNumber, incrementedNumber);
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find("img").each(function () {
        $(this).val('').attr('id', function (_, id) {
            var id_array = id.split("-");
            if (id_array.length > 1) {
                var new_id = id_array[0] + '-' + rowCount;
                while ($('#' + table_id + ' #' + new_id).length) {
                    rowCount++;
                    new_id = id_array[0] + '-' + rowCount;
                }
                return new_id;
            } else {
                return id;
            }
        });
        $(this).attr('name', function (_, id) {
            var id_array = id.split("-");
            if (id_array.length > 1) {
                var new_id = id_array[0] + '-' + rowCount;
                while ($('#' + table_id + ' #' + new_id).length) {
                    rowCount++;
                    new_id = id_array[0] + '-' + rowCount;
                }
                return new_id;
            } else {
                return id;
            }
        });
    }).end();

    clone_ele.find(".li_condition_row").each(function() {
        $(this).attr('id', function(_, id) { 

            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_outlet").each(function() {
        $(this).attr('id', function(_, id) { 
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find(".btn_remove_added_outlet").each(function() {
        $(this).attr('id', function(_, id) { 
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.attr('id', function(_, id) { 
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var newId = id_array[0]+'-'+rowCount;
                while ($("#"+ newId).length) {
                    rowCount++;
                    newId = id_array[0]+'-'+rowCount;
                }
                return newId; 
            }else{
                return id;
            }
        });
    })

    $("#"+table_id+" li:last").after(clone_ele);
    
    clone_ele.attr('id', function(_, id) {
        var id_array = id.split("-");
       
        if(table_id == 'ul_outlet'){
                                                        
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('input').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('textarea').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row").find('select').val("");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeClass('btn_remove_added_outlet');
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).removeAttr("data-id");
            $("#ul_outlet #li_outlet-"+id_array[1]+" .li_condition_row #btn_remove_outlet-"+id_array[1]).addClass('btn_remove_outlet');
        }
    })
}//End Clone Table
</script>
<script>
$(document).ready(function(){

    function toggler(divId) {
        $("#" + divId).toggle();
    }      
});
</script>
<!-- role js -->
<script src="{!!asset('/js/summernote.min.js')!!}"></script>
<script src="{!!asset('/js/editor.js')!!}"></script>

@endsection
