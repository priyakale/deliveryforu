<tr>
    <td colspan="6" class="td_detail">
        <div id="detail-{{ $record->id }}" class="detaill-inner collapse panel-collapse">
            <table class="table table-inner table-striped">
                <thead class="th-inner"> 
                    <tr> 
                        <th style="width: 50%;">Old Value</th> 
                        <th style="width: 50%;">New Value</th> 
                    </tr> 
                </thead> 
                <tbody>
                    <tr>
                        @if($record->old_value == 'null')
                        <td class="empty-record">Empty</td>
                        @else
                        <td>
                        <pre>{{ 
                        substr( print_r((array)json_decode($record->old_values)), 0, -1) 
                        }}</pre>
                        </td>
                        @endif

                        @if($record->new_value == 'null')
                        <td class="empty-record">Empty</td>
                        @else
                        <td>
                        <pre>{{ substr( print_r((array)json_decode($record->new_values)), 0, -1) }}</pre>
                        </td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </td>
</tr>