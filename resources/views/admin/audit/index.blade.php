@extends('admin.layout')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
<style type="text/css">
    .empty-record {
        text-align: center;
        background-color: #faf9c9 !important;
        color: #9f6002 !important;
        font-weight: bolder !important;
    }
</style>
<div class="container full_width">
    <div class="row">
        <div class="col-md-3">
            <!-- START USER-->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-pencil-square-o"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{ $data['total_audit'] }}</div>
                    <div class="widget-title">Audit</div>
                    <div class="widget-subtitle">Total : </div>
                </div>                
            </div>                            
            <!-- END USER --> 
        </div>
         <div class="col-md-9">
            <!-- START SERARCH CATERIA -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-search"></span>
                </div>
                <div class="widget-data">
                    <form class="padding-right" role="search">
                        <div class="row col-lg-12">
                            <div class="col-md-4 padding-left-zero">
                                <label for="name">User</label>
                                <select name="name" id="name" class="form-control chosen-select">
                                    <option value="">Select User</option>
                                    <option value="All">All</option>
                                    @if(!empty($data['user_records']))
                                        @foreach($data['user_records'] as $key => $value )
                                          <option {{ !empty($data['input']['name'])
                                            && $data['input']['name'] == $value->id ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-4 padding-left-zero">
                                <label for="model_name">Model</label>
                                <select name="model_name" id="model_name" class="form-control chosen-select">
                                    <option value="">Select Model</option>
                                    <option value="All">All</option>
                                    @if(!empty($data['model_records']))
                                        @foreach($data['model_records'] as $model_record )
                                          <option {{ !empty($data['input']['model_name'])
                                            && $data['input']['model_name'] == $model_record->auditable_type ? 'selected' : '' }} value="{{ $model_record->auditable_type }}">{{ $model_record->auditable_type }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-4 padding-left-zero">
                                <label for="action_name">Action</label>
                                <select name="action_name" id="action_name" class="form-control chosen-select">
                                    <option value="">Select Action</option>
                                    <option value="All">All</option>
                                    @if(!empty($data['action_records']))
                                        @foreach($data['action_records'] as $action_record )
                                          <option {{ !empty($data['input']['action_name'])
                                            && $data['input']['action_name'] == $action_record->event ? 'selected' : '' }} value="{{ $action_record->event }}">{{ $action_record->event }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                         <div class="input-group form-width-full padding-top">
                            <div class="col-md-5 padding-left-zero">
                                <div class="form-group">
                                    <label for="date_from">Date From</label>
                                    <div class="input-group">
                                        <input class="form-control datepicker" type="text" name="date_from" id="date_from" 
                                        value="">
                                        <div class="input-group-btn">
                                            <button class="btn btn-empty btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>                     


                            <div class="col-md-5 padding-left-zero">
                                <div class="form-group">
                                    <label for="datepicker">Date To</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="date_to" id="date_to"
                                        value="{{ !empty($data['input']['date_to']) ? $data['input']['date_to'] : '' }}" >
                                        <div class="input-group-btn">
                                            <button class="btn btn-empty btn-date" type="button"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 padding-left-zero padding-right-zero">
                                 <div class="form-group float-right">
                                    <label for="datepicker">&nbsp</label>
                                    <div class="input-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>                            
            <!-- END SERARCH CATERIA --> 
        </div>
    </div>
     <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-custom">
                    <div class="panel-heading panel-heading-custom">
                        <table class="full_width">
                            <tr>
                                <td>
                                    Search Result  
                                    <span class="badge custom-badge">{{ $data['records']->total() }}</span>
                                </td>
                                <td>
                                    <div class="form-group float-right">
                                        <div class="btn-group input-group">
                                            <a 
                                            class="float-right btn btn-success" id="btn_collapse" 
                                            href="javascript:void(0)">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-body">
                        <div class="float-left">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                        <table class="table table-hover"> 
                            <thead> 
                                <tr> 
                                    <th>No.</th> 
                                    <th>By Who</th>
                                    <th>At Table</th>
                                    <th>Action</th>
                                    <th>IP Address</th>
                                    <th>Date</th> 
                                </tr> 
                            </thead> 
                            <tbody>
                                @php $count = 1 @endphp
                                @if (empty($data['records'][0]))
                                    <tr>
                                        <th colspan="6" class="empty-record text-center"><label>No Result</label></th>
                                    </tr>
                                @else
                                    @foreach($data['records'] as $record)
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>
                                        {{ $record->name }} @ 
                                        <a href="javascript:void(0)" data-toggle="collapse" 
                                        data-target="#detail-{{ $record->id }}" />
                                        #{{ $record->id }}
                                        </a>
                                        </td>
                                        <td>{{ $record->auditable_type }}</td>
                                        <td>{{ $record->event }}</td>
                                        <td>{{ $record->ip_address }}</td>
                                        <td>{{ $record->updated_at_value }}</td>
                                    </tr>
                                    
                                    <!-- Audit Collapse Detail-->
                                    @include('admin.audit.audit_details')
                                    <!-- End Audit Collapse Detail-->
                                    
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $data['records']->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<script src="{{ asset('/js/report.js') }}" type="text/javascript"></script>
<script type="text/javascript" >
    $( document ).ready(function() {
        var Date_from = new Date("{{ !empty($data['input']['date_from']) ? $data['input']['date_from'] : '' }}");
        $('#date_from').datepicker('setDate', Date_from);
            /*if({{ !empty($data['input']['date_from']) ? 1 : 0 }}){        
            }*/

            /*if({{ !empty($data['input']['date_to']) ? 1 : 0 }}){            
            }*/
            var Date_to = new Date("{{ !empty($data['input']['date_to']) ? $data['input']['date_to'] : '' }}");
            $('#date_to').datepicker('setDate', Date_to);

            $('.btn-date').click(function () {
                var ele_name = $(this).parents(".input-group-btn").parents(".input-group").first().find('.datepicker').attr('id');
                $('#' + ele_name).datepicker("show");
            });
    });
</script>
@endsection
