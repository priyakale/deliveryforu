@extends('auth.layouts.app')
@section('content')

@include('common.menu')

<!-- Section 1 -->
<div class="hero-wrap">
  <div class="overlay"></div>
  <div class="overlay1"></div>
  <div class="hero__content1">
    <p>Order services to your door</p>
    <p>
      A bidding platform that aims to create an ecosystem of service providers and customers.
    </p>
    <p><a href="#">GET STARTED</a></p>
  </div>
  <div class="hero__content2">
    <p>Bringing services to you at the cheapest price!</p>
    <p>Just post your service request. The next you know, vendors will be lining up at your doorstep, offering the best rates they have.  Worry no more about overpaying or not getting the best rates.  </p>
  </div>
</div>
<!-- Section 1 -->

<!-- Our Services -->

<div class="our__services">
    <span class="service__span1">
      <p>Our Services</p>
      <p>Browse potential and trusted vendor</p>
    </span>
    <span class="service__span2">
      <div class="owl-carousel owl-theme owl__theme">
          <div class="item">
            <span><img src="{{ custom_asset('images/web/delivery.png') }}" alt=""></span>
            <span>Delivery</span>
          </div>
          <div class="item">
            <span><img src="{{ custom_asset('images/web/cleaning.png') }}" alt=""></span>
            <span>Cleaning</span>
          </div>
          <div class="item">
            <span><img src="{{ custom_asset('images/web/e-hailing.png') }}" alt=""></span>
            <span>E-Hailing</span>
          </div>
          <div class="item">
            <span><img src="{{ custom_asset('images/web/delivery.png') }}" alt=""></span>
            <span>Delivery</span>
          </div>
          <div class="item">
            <span><img src="{{ custom_asset('images/web/delivery.png') }}" alt=""></span>
            <span>Delivery</span>
          </div>
          <div class="item">
            <span><img src="{{ custom_asset('images/web/delivery.png') }}" alt=""></span>
            <span>Delivery</span>
          </div>
          <div class="item">
            <span><img src="{{ custom_asset('images/web/delivery.png') }}" alt=""></span>
            <span>Delivery</span>
          </div>
      </div>
    </span>
  </div>

<!-- Our Services -->

<!-- Benefits -->

<div class="row our__row">
    <div class="col-md-6">
      <div class="our__benefits">
          <p>What is our Benefits</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget neque eget dolor imperdiet volutpat </p> 
      </div>
    </div>
    <div class="col-md-3 ">
      <div class="our__img">
          <img src="{{ custom_asset('images/web/cheap.png') }}" alt="">
          <div class="our__content">
            <p>Cheapest price in market</p>
            <p>“Vendors bidding for your service requests. Just choose the best price.”</p>
          </div>
        </div>
    </div>
    <div class="col-md-3 ">
      <div class="our__img">
          <img src="{{ custom_asset('images/web/shoes.png') }}" alt="">
          <div class="our__content">
            <p>Fast Services</p>
            <p>“Vendors bidding for your service requests. Just choose the best price.”</p>
          </div>
        </div>
    </div>
</div>
  <div class="row our__row2">
    <div class="col-md-3 ">
      <div class="our__img">
          <img src="{{ custom_asset('images/web/bank.png') }}" alt="">
          <div class="our__content">
            <p>Safe Payment</p>
            <p>“Vendors bidding for your service requests. Just choose the best price.”</p>
          </div>
        </div>
    </div>
    <div class="col-md-3 ">
      <div class="our__img">
          <img src="{{ custom_asset('images/web/mouse.png') }}" alt="">
          <div class="our__content">
            <p>Easy & Convenient</p>
            <p>“Vendors bidding for your service requests. Just choose the best price.”</p>
          </div>
        </div>
    </div>
    <div class="col-md-3 ">
      <div class="our__img">
          <img src="{{ custom_asset('images/web/menu.png') }}" alt="">
          <div class="our__content">
            <p>Variety of Services</p>
            <p>“Vendors bidding for your service requests. Just choose the best price.”</p>
          </div>
        </div>
    </div>
    <div class="col-md-3 ">
      <div class="our__img">
          <img src="{{ custom_asset('images/web/search.png') }}" alt="">
          <div class="our__content">
            <p>Anytime, Anywhere</p>
            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. ”</p>
          </div>
        </div>
    </div>
  </div>
<!-- Benefits -->

<!-- How it works< -->

<div class="row how__row">
    <div class="col-md-4">
      <div class="how__img">
        <img src="{{ custom_asset('images/web/how.png') }}" alt="">
      </div>
    </div>
    <div class="col-md-8 how-md-8">
      <span class="how__p">
        <p>How it works</p>
        <p>With a  simple steps to get the job done</p>
      </span>
      <div class="how__imgs">
        <span>
          <p><img src="{{ custom_asset('images/web/how1.png') }}" alt=""></p>
          <p>Request a service</p>
        </span>
        <span>
          <p><img src="{{ custom_asset('images/web/how2.png') }}" alt=""></p>
          <p>Wait for Vendors to bit</p>
        </span>
        <span>
          <p><img src="{{ custom_asset('images/web/how3.png') }}" alt=""></p>
          <p>Wait for Vendors to bit</p>
        </span>
        <span>
          <p><img src="{{ custom_asset('images/web/how4.png') }}" alt=""></p>
          <p>Wait for Vendors to bit</p>
        </span>
        <span>
          <p><img src="{{ custom_asset('images/web/how5.png') }}" alt=""></p>
          <p>Wait for Vendors to bit</p>
        </span>
      </div>
    </div>
</div>

<!-- How it works< -->

@include('common.footer')
@endsection

@section('script')
<script type="text/javascript">
  
$(document).ready(function(){
 $('.owl-carousel').owlCarousel({
    stagePadding: 70,
    loop:true,
    margin:50,
    nav:true,
    autoplay:true,
    dots:false,
    responsive:{
        0:{
            items:1,
            margin:80,
        },
        480:{
            items:1,
            margin:30,
        },
        568:{
            items:1,
            margin:5,
        },
        640:{
            items:2,
            margin:20,
        },
        1024:{
            items:2,
            margin:30,
        },
        1280:{
            items:3,
            margin:30,
        }
    }
})
});
</script>

@endsection