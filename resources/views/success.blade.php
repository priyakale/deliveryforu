@extends('auth.layouts.app')

@section('title', 'Sign Up')

@section('style')
<!-- ************* style css ************* -->
<link rel="stylesheet" href="{{ custom_asset('css/country_list/intlTelInput.css') }}">
@endsection
@section('content')

@include('common.menu')
<div class="hero-wrap">
  <div class="overlay__sign"></div>
  <div class="overlay1__sign"></div>

  <div class="success__block">
      <div class="success__img">
          <img src="{{ custom_asset('images/web/success.png') }}" alt="">
      </div>
        <div class="success__content">
          <p>Congratulations</p>
          <p>You have successfuly registered. An email has been sent to m*****.zarim@g*****.com. Please verify your account to sign in.</p>
          <p><a href="">Go back to sign in page</a></p>
        </div>
  </div>

</div>

@include('common.footer2')
@endsection
@section('script')
<script src="{{ custom_asset('js/country_list/intlTelInput.js') }}"></script>

@endsection