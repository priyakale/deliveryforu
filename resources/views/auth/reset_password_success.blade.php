<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ SettingHelper::getAppName() }} - @yield('htmlheader_title', 'Your title here') </title>
    <link href="{{ asset('/img/logo.ico') }}" type="image/x-icon" rel="icon">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/css/vendor.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- login-logo -->
        <div class="login-logo">
            <img src="{{ asset('/img/logo.png') }} " class="img-responsive logo-center" >
            <a href="{{ url('/home') }}"><b>{{ SettingHelper::getAppName() }}</b> {{ SettingHelper::getAppName() }}</a>
        </div>
        <!-- /.login-logo -->

    <div class="alert alert-success">
        <span>
            <i class="fa fa-check"></i>  Your password has been changed successfully. Please login to Delivery4U with the new password.
            <a href="#" class="close close-alert">&times;</a>
        </span>
    </div>    

</div><!-- /.login-box-body -->

</div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/plugins/jQuery/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

</html>