@extends('auth.layouts.app')

@section('title', 'Log In')

@section('content')
<div class="div__headsignin signmob__hide">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-x-4">
                <div class="logotxt">
                    <span class="redcircle"></span>
                    <span class="delv__txt">DELIVER4U</span>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-8">
                <div class="signinup__dets">
                    <span class="acc__txt">Do not have an account yet?</span>
                    <span>
						    <a href="{{ route('register') }}" class="btn signupbtn">SIGN UP</a>
						</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="close_mobsign">
    <a href=""><i class="material-icons">close</i></a>
</div>
<div class="sect__two">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7 col-x-12 leftimg__css">
                <div class="algnimg">
                    <div class="leftwelcm__img">
                        <div class="txtwelcm__dets">
                            <h6 class="h6txt__wel">WELCOME TO</h6>
                            <h5 class="h5txtdelv">DELIVER4U</h5>
                            <div class="mobhide__styl">
                                <p class="bus__succ">For your business to success, retain, and </p>
                                <p class="retn__custtxt"> multiply customers.</p>
                            </div>
                            <div class="desktp__hide">
                                <p class="bus__succ">Where transportation are easier in the </p>
                                <p class="retn__custtxt"> future and we live in it</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="signin__form">
                    <h1>Sign In</h1>
                    <p class="phell__txt">Hello Earthlings! Please Sign in</p>
                    <form class="form-horizontal form__iptsignin" action="{{ route('login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12">Email
                                Address</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="email" class="form-control" placeholder="Email Address" name="email"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                <span class="errorstyl">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                   for="pwd">Password</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pass__btneye">
							<span class="btn-show-passtwo">
								<i class="material-icons" onclick="togglePasswordVisibility()">remove_red_eye</i>
							</span>
                                    <input type="password" class="form-control" placeholder="Enter password"
                                           name="password" id="password">
                                    @if ($errors->has('password'))
                                    <span class="errorstyl">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="frgt__pas">
                                    <a href="{{ route('password.request') }}">Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn signinbtn">SIGN IN</button>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <p class="or__txt">OR</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.facebook.login') }}" type="submit" class="btn signinbtnfb">
                                    <img src="{{ custom_asset('images/fb.png') }}" alt="Facebook">
                                    <span>CONTINUE WITH FACEBOOK</span></a>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.google.login') }}" type="submit"
                                   class="btn signinbtngogl"><img
                                            src="{{ custom_asset('images/google.png') }}" alt="Google"><span>CONTINUE WITH GOOGLE</span></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function togglePasswordVisibility() {
        let password = $("#password");

        if (password.attr('type') === "password") {
            password.attr('type', 'text')
        } else {
            password.attr('type', 'password')
        }

    }
</script>
@endsection
