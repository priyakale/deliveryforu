@extends('auth.layouts.app')

@section('title', 'Sign Up')

@section('style')
<!-- ************* style css ************* -->
<link rel="stylesheet" href="{{ custom_asset('css/country_list/intlTelInput.css') }}">
@endsection

@section('content')
<div class="div__headsignin">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-x-12">
                <div class="logotxt">
                    <span class="redcircle"></span><span class="delv__txt">DELIVER4U</span>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="signinup__dets">
                    <span class="acc__txt">Already have an account?</span>
                    <span>
					    	<a href="{{ route('login') }}" class="btn signupbtn">SIGN IN</a>
					    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="topsect__signup">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="imgarea__signup">
                    <div class="img__signup"></div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="signup__form">
                    <form class="form-horizontal form__iptsignup" action="{{ route('register') }}" method="POST">
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.facebook.login') }}" type="submit" class="btn signinbtnfb">
                                    <img src="{{ custom_asset('images/fb.png') }}" alt="Facebook">
                                    <span>SIGN UP WITH FACEBOOK</span>
                                </a>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.google.login') }}" type="submit" class="btn signinbtngogl">
                                    <img src="{{ custom_asset('images/google.png') }}" alt="Google">
                                    <span>SIGN UP WITH GOOGLE</span>
                                </a>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <p class="or__txt">OR</p>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1>Sign Up</h1>
                                <p class="pbasic__txt">Enter basic information here</p>
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="row form-group">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padrghtleft">
                                <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="first_name">First
                                    Name</label>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                           value="{{ old('first_name') }}">

                                    @if ($errors->has('first_name'))
                                    <span class="errorstyl">{{ $errors->first('first_name') }}</span>
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padrghtleft">
                                <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="last_name">Last
                                    Name</label>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                           value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                    <span class="errorstyl">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="email">Email
                                Address</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="email" class="form-control" placeholder="Email Address" name="email"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                <span class="errorstyl">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="email">Phone
                                Number</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input name="phone_no" id="phone-no" type="tel" class="form-control"
                                       value="{{ old('phone_no') }}">
                                @if ($errors->has('phone_no'))
                                <span class="errorstyl">{{ $errors->first('phone_no') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                   for="password">Password</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="password" class="form-control" placeholder="Enter Password"
                                       name="password">
                                @if ($errors->has('password'))
                                <span class="errorstyl">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                   for="password_confirmation">Confirm Password</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="password" class="form-control" placeholder="Confirm Password"
                                       name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                <span class="errorstyl">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="chkbx__agree">
                                    <div class="checkbox">
                                        <label class="customcheckoutlet">
                                            <input type="checkbox" required>
                                            <span class="checkmark"></span>
                                            <span> I Agree with the <a href="">Terms & Conditions</a> </span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn signinbtn">SIGN UP</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ custom_asset('js/country_list/intlTelInput.js') }}"></script>
<script>
    $(document).ready(function () {
        var input = document.querySelector("#phone-no");
        window.intlTelInput(input, {
            utilsScript: "{{ custom_asset('js/country_list/utils.js') }}",
        });
    });
</script>
@endsection
