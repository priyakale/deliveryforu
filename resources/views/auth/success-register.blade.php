@extends('auth.layouts.app')

@section('title', 'Successfully Registered')

@section('content')
<div class="div__headsignin">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-x-4">
                <div class="logotxt">
                    <span class="redcircle"></span><span class="delv__txt">DELIVER4U</span>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-8">
                <div class="signinup__dets">
                    <span><a href="{{ route('login') }}" class="btn signinlogbtn">SIGN IN</a></span>
                    <span><a href="{{ route('register') }}" class="btn signupbtn">SIGN UP</a></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="success__section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="imgman__succss">
                    <img src="{{ custom_asset('images/signup/man.png') }}" alt="image">
                    <h1>Congratulations</h1>
                    <p class="p1">You have successfuly registered. An </p>
                    <p class="p2">email has been sent to </p>
                    <p class="p2">{{ mask_email($user->email) }} </p>
                    <p class="p2">Please verify your account to sign in.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="cntr__signin">
                    <a href="{{ route('login') }}" class="btn signupbtnbig">SIGN IN</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
