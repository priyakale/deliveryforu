<!DOCTYPE html>
<html>

<body class="">

<div class="page-wrapper">
    

    <div class="main">
        <div class="main-inner">

<div class="header-content">
                <div class="header-bottom">
                    
                    @if(Request::url() == URL::route('login'))
                      <li class="active">
                    @else
                      <li>
                    @endif    
                        <a href="{{ route('login') }}">
                            <span> Login</span>
                        </a>
                    </li>

                    @if(Request::url() == URL::route('register'))
                      <li class="active">
                    @else
                      <li>
                    @endif    
                        <a href="{{ route('register') }}">
                            <span> Register</span>
                        </a>
                    </li>

                </div>
                <!-- /.header-bottom -->
            </div>
            <!-- /.header-content -->
<div class="content">
    <div class="mt-150">
        <div class="hero-image">
            <div class="hero-image-inner" >
                <div class="hero-image-content">
                    <div class="container">
                        <div class="row ">
			<div class="col-md-12">
<p>		
1.GENERAL
<br><br>
1.1Yakin! is made available by Mobieco Wireless Sdn. Bhd. (Company No. 988140-M) (“Mobieco”) and is supported by its third party service provider, Asurion Technology Malaysia Sdn Bhd (“Asurion”) subject to the general terms and conditions provided under the Terms and Conditions of the Program (https://www.yakin2u.com). 
<br><br>
1.2You acknowledge that You have read and fully understood these Terms and Conditions. Your registration as a Yakin! Dealer upon the Start Date (as defined in clause 6 of the Dealer Appointment Terms and Conditions), constitutes unconditional acceptance on Your part to be bound by these Terms and Conditions as may be amended from time to time. You must ensure that You comply with these Terms and Conditions and You will be liable for any breach of the Terms and Conditions by You.
<br><br>
2.PROGRAM SUMMARY
<br><br>
2.1Yakin! allows a Subscriber to request for a device swap – this is a request for a Like Mobile Device (as defined in the General Terms and Conditions of the Program) in exchange for a Registered Device if the Registered Device is in the Subscriber’s possession (a “Device Swap”), (Device Swap being a “Service”).
<br><br>
3.APPLICATION
<br><br>
3.1To register as a Yakin! Dealer, You are required to submit a duly completed Dealer Information Form together with all the required documents as stated in Clause 3.2 and submit in person or via any other enrolment channel which shall be made available by Mobieco from time to time.
<br><br>
3.2To complete the application, You shall submit the following documents:<br>
(a)Completed Dealer Information Form;<br>
(b)Form 9 or Form 13; and<br>
(c)Form 24.<br>

3.3Eligibility<br><br>
To be eligible as a Yakin! Registered Dealer, the Dealer must:
<br><br>
(a)Be a duly organised and validly existing and in good standing under the laws of Malaysia and has all requisite power and authority to own its properties and assets and to carry on its business as now conducted and as proposed to be conducted, and to perform each of its obligations hereunder and under any agreement contemplated hereunder to which it is a party;<br><br>
(b)possess commercial premises to conduct dealership related activities;<br><br>
(c) not be in violation of any applicable law, statute, rule, regulation, order or restriction of any domestic government or any department or agency thereof in respect of the conduct of its business or the ownership of its properties; and<br><br>
(d)has all requisite power, authority and capacity to enter into a Dealer Agreement, and to perform its obligations hereunder and under any agreement contemplated hereunder to which it is a party.<br><br>
4.APPOINTMENT <br><br>
4.1Dealer shall be authorized to offer, market, promote, distribute and sell the Program to consumers subject to the terms and conditions of this Dealer Appointment Terms and Conditions.<br><br>

4.2No Partnership. Nothing in the terms and conditions of the appointment as a registered Dealer shall constitute or be deemed to constitute a partnership between the parties and the Dealer shall have no authority or power to bind Mobieco or to contract in the name of and create a liability against Mobieco in any way or for any purpose.<br>
<br>
4.3No Assignment. The Dealer cannot assign any rights in the Dealer Agreement. However, Mobieco reserves the right to assign this entire Agreement to any of its associated companies, or sell, transfer or assign to a financial institution or companies.<br><br>

5.NON-EXCLUSIVE RIGHTS. <br><br>

5.1Mobieco grants to Dealer for resale, and Dealer accepts, the non-exclusive right to sell and distribute the Mobieco ‘device swap’ program (“Program”) under the trade name “Yakin!” during the term of the Dealer Agreement.
<br><br>
5.2Availability of Program. Mobieco agrees to make available the Program as and when the Dealer place a Subscriber registration request to Mobieco at the prices and subject to the terms set forth in the Dealer Agreement.
<br><br>
6.TERM.<br><br>

6.1The initial term of the appointment as a registered Dealer is one (1) year from the date of the Dealer Agreement. Thereafter the Dealer Agreement will automatically renew for successive one (1) year terms, unless terminated under the terms and conditions of the Agreement.
<br><br>
7.DEALER OBLIGATIONS. 
<br><br>
7.1Digital Wallet System. Dealer shall open an account in the Digital Wallet System with an initial amount as mutually agreed upon in the New Dealer Information Form.
<br><br>
7.2Promotion of Program. Promote and sell the Program to consumers in accordance to the pricing structure, guidelines and instructions provided by the Mobieco. Dealer will advertise and/or promote Program and provide program information and promotional materials to its customers.
<br><br>
7.3Sales Standards. Dealer will pursue advertising or promotional activities that portray the Program according to the advertising and promotional standards of Mobieco. Dealer shall only use materials and conduct activities that has been provided or approved by Mobieco. 
<br><br>
7.4 Sales Guidelines. Dealer shall promote the Program according to the information received and agreed to (i) in this agreement (ii) terms and conditions of the Program (iii) Training Materials, and do not misrepresent the Program (including by exaggerating or changing any contents of the program) or make any material omission concerning the Program during the promotion process.
<br><br>
7.5Training. Dealer shall ensure that it will only allow its employees who have attended the training program to promote the Program to its customers. Dealers will maintain a full understanding of the Program requirements and will be fully responsible for executing according to those guidelines.
<br><br>
7.6Non-Adherence. The Dealer will be liable to Mobieco for any losses relating to any failure by any of the promoters to promote the Program in accordance with the Sales Guidelines.  
<br><br>
7.7No Re-Selling of Program. Not to appoint any other parties to resell the Program to any consumers as such appointment shall only be made by Mobieco.
<br><br>
7.8Maintain and Support.  Give information to the Subscribers on matters pertaining to the Program, teach Subscribers on how to use the Program in accordance with the procedures or guidelines as stipulated. All Subscribers registered on the Program shall be governed by the Terms and Conditions of the Program and it shall be the duty of the Dealer to highlight the Terms and Conditions of the Program to the Subscriber.
<br><br>
7.9Subscriber Information. Dealer shall obtain all the mandatory information from the potential Subscriber and key into the Dealer Application all such information in the relevant fields.
<br><br>
7.10Personal Data Protection. Dealer shall obtain from each Subscriber all necessary consent needed for the provision of the Program under the Dealer agreement including for the transfer of personal data outside of Malaysia in order to comply with the applicable data protection laws in Malaysia and shall continue to maintain such consents as may be required for the processing and transfer of data in relation to the provision of the Services under this agreement.
<br><br>
7.11Dealer outlet. Display at his premises a signage in a form to be notified by Mobieco declaring it is a registered Dealer of the Yakin! Program and prominently display the marketing and promotional materials related to the Program.
<br><br>
7.12Monthly Reporting. Dealer shall provide a monthly report to Mobieco stating the number of devices sold.<br>
8.PAYMENT OF INCENTIVES. <br><br>
8.1Incentives are calculated by Mobieco and the rate is as agreed in the Dealer Agreement. The payment of incentives are subject to Dealer strictly observing the terms and conditions of payment. In the event Dealer does not comply with the terms and conditions of payment, Mobieco reserves the right to suspend, withhold and/or forfeit any or all of the Incentives.<br><br>

8.2Right to Amend. Mobieco reserves the right to amend all types of incentives and its calculations at any time and from time to time. Mobieco will inform the Dealer (either by email, fax, sms or letter, by written circular) the effective date for amendments of incentives.<br><br>

8.3Dispute on Incentive. If the Dealer has any disputes on incentive calculation and payment, he shall inform Mobieco in writing within seven (7) working days from the receipt of the incentives, failing which the incentive payment shall be deemed correct and accepted by the Dealer in good faith.
<br><br>
9.TERMS AND CONDITIONS OF INCENTIVE PAYMENTS. 
<br><br>
9.1Dealer fully understands that Mobieco will not pay incentive to the Dealer under the following circumstances, where there is an element of fraud, cheating, misrepresentation, illegal activities and non-compliance with rules and regulations:
<br><br>
(a)Registration of fictitious subscribers, i.e. the person may be dead, missing, migrated, name does not match with identity card/passport number.<br>
(b)Registration of subscribers where more than 10 persons are sharing the same address.<br>
(c)Registration of subscribers where there are no such addresses as indicated by the National Post Office or registered with a post-box number only.<br>
(d)Registration of subscribers who has not purchased the device from their outlet and not on the same day of registering on the Program.<br>
(e)If the Dealer has not been actively registering subscribers for the past 6 months.<br>
<br><br>
9.2Incentive Payment. Dealer unconditionally agree and accept as full settlement that Mobieco will pay all types of Incentives, Sponsorship, rewards and/or reimbursement expenses to the Dealer either by:
<br><br>
(a)Direct telegraphic bank transfers to Dealer’s bank account;<br>
(b)Cheques given to Dealer for banking in;<br>
(c)Direct e-wallet credit to Dealer account;<br>
(d)Contra of accounts for Dealer’s debts owing to the Mobieco; and/or<br>
(e)A combination of the above.<br>

10.RIGHTS TO AUDIT / PERFORMANCE REVIEW.  <br><br>

10.1Mobieco and/or its Affiliates shall have the absolute right to conduct field inspections at the Dealer’s Outlet, at any time and from time to time without written notice to Dealer. The inspection will cover areas on the Dealer’s existing Products, compliance with the Mobieco’s policies, procedures and system, verifying the accuracy of information on the Program provided by the Dealer to consumers.
<br><br>
11.ENROLMENT OF SUBSCRIBER. 
<br><br>
11.1Any potential subscriber registered on the Program shall within 3 working days be confirmed accepted into the Program unless notified otherwise.
<br><br>
11.2Rejection of Subscriber. In the event of a rejection of a subscriber within 3 working days of his registration and payment has been received, any payment received shall be payable by Mobieco to the Subscriber.
<br><br>
12.DEALER PRICING AND PAYMENT. 
<br><br>
12.1Dealer shall market the Program according to the (i) Price according to the respective tiers as determined by Mobieco and (ii) Terms and Conditions of the Program. Dealer shall not have any authority to change any aspects of the Program including the pricing structure.
<br><br>
13.MARKETING SUPPORT. 
<br><br>
13.1All sales and marketing materials used for the promotion of the Program shall be provided by Mobieco to the Dealer and Dealer shall be strictly governed by these materials.
<br><br>
13.2Trademark. The Program may bear certain trade names, trademarks, trade devices, logos, codes or other symbols of Yakin! or Mobieco (“Trademarks”). Mobieco hereby grants to Dealer the non-exclusive, royalty-free right to use the Trademarks for the purpose of carrying out the activities described in the Dealer Agreement, provided that Dealer will not be entitled to conduct business under any of the Trademarks or derivatives or variations thereof. All use of such Trademarks will be to the benefit of Mobieco and will not vest in Dealer any rights in or to the Trademarks.
<br><br>
14.TERMINATION. 
<br><br>
14.1Either party may terminate the Dealer Agreement, with or without cause, by giving thirty (30) days prior written notice to the other party. 
<br><br>
14.2Immediate Termination. Either party may immediately terminate the Dealer Agreement with written notice if the other party:
<br><br>
(a)materially breaches any term of the Dealer Agreement and such breach continues for thirty (30) business days after written notification thereof; or
<br>
(b)ceases to conduct business in the normal course, becomes insolvent, makes a general assignment for the benefit of creditors, suffers or permits the appointment of a receiver for its business or assets, or avails itself of or becomes subject to any proceeding; or
<br>
(c)Dealer carried out activities where there is an element of fraud, cheating, misrepresentation, and or illegal activities; or
<br>
(d)Dealer intentionally and fraudulently enrolled Subscribers that have not purchased a new mobile phone; or
<br>
(e)Dealer has not co-operated with Mobieco in putting up the required marketing tools for display at its Outlet at all times.
<br><br>
14.3Return of Materials. Upon termination of the Dealer Agreement all materials provided by Mobieco including but not limited to marketing and promotion materials and products be it digital or otherwise shall be promptly returned to Mobieco.
<br><br>
14.4Non-Waiver. The termination or expiry of the Dealer Agreement, in whole or in part, does not operate as a waiver of any breach by a party of any provisions and is without prejudice to any rights, liabilities or obligations of any party which have accrued up to date of termination or expiry including the right of indemnity.
<br><br>
14.5The issuance of a suspension notice shall not in any way prejudice or prevent Mobieco from exercising its right to issue a termination notice, notwithstanding the above clause.
<br><br>
15.INDEMNITY BY DEALER. 
<br><br>
15.1Without prejudice to any other rights and remedies of Mobieco under the Dealer Agreement or under the law, Dealer is liable for and shall compensate, protect and defend Mobieco under its own expenses against all claims, legal actions and proceedings (including all legal fees incurred) that may be brought against Mobieco for all losses or damage arising out of Dealer performing its obligations under this Agreement, including but not limited to:
<br><br>
(a)Infringement of third party intellectual property rights under this Agreement;<br>
(b)Any violation or default by Dealer against the terms, conditions, responsibilities or statements under this Agreement;<br>
(c)Any promotion of the Program in an inappropriate and inaccurate manner, that do not represent the Program (including by exaggerating or changing any contents of the program) or any material omission concerning the Program during the promotion process resulting in a Subscriber making a claim against the Program;<br>
(d)Failure to comply or breach of any laws or rules or guidelines; and or<br>
(e)Failure to fully perform its obligations pursuant to the terms as stated in the Dealer Agreement.<br><br>
15.2Dealer agrees and accepts that the responsibility to compensate Mobieco shall not exclude any liabilities under the Dealer Agreement.<br><br>
15.3The obligation of Dealer to compensate Mobieco under the Dealer Agreement shall survive the termination of the Dealer Agreement.<br><br>

16.REPRESENTATIONS AND WARRANTIES. <br><br>
16.1.Each party represents and warrants to the other party from the Effective Date and during the Term:
<br><br>
(a)It is aware of the requirements of the Malaysian Anti-Corruption Act 2009 and any other applicable laws regarding anti-corruption or anti-bribery (collectively, “Anti-Bribery Laws”) and will not take any action that could violate the Anti-Bribery Laws or expose the other party to liability under the Anti-Bribery Laws.<br>
(b)The Dealer agreement, when executed and delivered will constitute valid and legally binding obligations, subject to bankruptcy, insolvency, moratorium, reorganisation and similar laws affecting creditors’ rights generally and general equitable principles.<br>
<br>
17.GENERAL PROVISIONS
<br><br>
17.1Notice. Any notice which either party may desire to give the other party must be in writing and may be given by (i) personal delivery to an officer of the party, (ii) by mailing the same by registered or certified mail, return receipt requested, to the party to whom the party is directed at the address of such party as set forth at the beginning of the Dealer Agreement, or such other address as the parties may hereinafter designate, and (iii) by facsimile or telex communication subsequently to be confirmed in writing pursuant to item (ii) herein.
<br><br>
17.2Delivery. Each Notice delivered in accordance with this clause shall be deemed to have been received:<br>
(a)on the date such Notice is delivered, if by personal delivery or e-mail;<br>
(b)on the date that is five Business Days after such Notice has been sent, if sent by mail;<br>
(c)on the date that is two Business Days after such Notice has been sent, if sent by an internationally recognised overnight courier;<br>
(d)on the date such notice is transmitted and a written confirmation of such transmission is received, if by facsimile, or if such date is not a Business Day, then on the next Business Day.<br>
<br>
17.3Confidentiality. Each party acknowledges that in the course of its obligations pursuant to the Dealer Agreement, it may obtain certain information specifically marked as confidential or proprietary (“Confidential Information”). Each party hereby agrees that all such Confidential Information communicated to it by the other party, its parents, affiliates, subsidiaries, or Customers, whether before or after the date of this Agreement, shall be and was received in strict confidence, shall be used only for the purposes of this Agreement, and shall not be disclosed without the prior written consent of the other party, except Confidential Information which:<br><br>
(a)is already known to the recipient of such Confidential Information (“Recipient”) at the time of its disclosure;<br>
(b)is or becomes publicly known through no wrongful act of the Recipient; <br>
(c)is received from a third party without similar restrictions and without breach of the Dealer Agreement;<br> 
(d)is independently developed by the Recipient ; or <br>
(e)is lawfully required to be disclosed to any government agency or is otherwise required to be disclosed by law.
<br><br>
18.INTERPERTATION
<br><br>
Confidential Information	Any information specifically marked as confidential or proprietary
Dealer	A registered Dealer of the Yakin! Program
Dealer Application 	Online registration and management of the Program
Digital Wallet System	A software-based system for making e-commerce transactions
Effective Date	The date as stated on this Agreement.
Outlet	The retail outlet(s) established by the Dealer
Program	Mobieco device swap program under the trade  name Yakin!
Subscriber	Customers who sign up for the Progam
Term	Initial one (1) year program and any successive one (1) year terms
Trademark	Certain trade names, trademarks, trade devices, logos, codes or other symbols of Yakin! or Mobieco
You and its variations	means the Yakin! program Dealer 
</p>
				
			</div>
		</div>
        
        </div>

                        
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.hero-image-content -->

            </div>
            <!-- /.hero-image-inner -->
        </div>
        <!-- /.hero-image -->
    </div>


</div>
<!-- /.content -->

</div><!-- /.main-inner -->
    </div><!-- /.main -->

    

</div><!-- /.page-wrapper -->

</body>
</html>
