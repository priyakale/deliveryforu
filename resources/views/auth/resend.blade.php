<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/upay_me.png')}}" />
    <title>Delivery4U</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{asset('assets/css/loginstyle.css')}}">
</head>
<body>
    <div class="Logo">
        <img class= "ImgLogo" src="{{asset('assets/img/logo.png')}}">
    </div>
    <div class="textArea">
        <p style="font-size: 45px; font-weight: 700; line-height: 0px;">SELF CARE<p>
        <p style="font-weight: 300; font-size: 18px;">Manage all your Delivery4U accounts on the go with this easy all-in-one portal</p>
    </div>

    <div style="display: block;margin: 0 auto;width: 430px;">
        @if (Session::has('message') || Session::has('success') || Session::has('warning') || Session::has('danger'))

            @if (Session::has('message'))
                <div class="alert alert-icon alert-dismissible alert-info mt-70" role="alert" style="margin-left: 15px;margin-right: 15px;">
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-icon alert-dismissible alert-success mt-70" role="alert" style="margin-left: 15px;margin-right: 15px;">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::has('warning'))
                <div style="padding-bottom: 20px; margin-top: 20px;margin-bottom:-19px;" class="alert alert-warning" >
      {{ session('warning') }}<br>
       
                <span>or click here to  </span><a href="{{ url('/resend') }}">Resend Activation Email</a>
      
      
    </div>

            @endif
            @if (Session::has('danger'))
                <div class="alert alert-icon alert-dismissible alert-danger mt-70" role="alert" style="margin-left: 15px;margin-right: 15px;">
                    {{ Session::get('danger') }}
                </div>
            @endif

        @endif

        @if (!empty($errors) && $errors->any())
            <div class="alert alert-icon alert-dismissible alert-danger mt-70" role="alert" style="margin-left: 15px;margin-right: 15px;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    
    <div class="box">
        <div id="header">
            <h1 id="logintoregister" style="color: #ec2585">Resend Activation Email !</h1>
        </div> 

        <form action="{{ route('send_activation_again') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="group">      
                <input type="email" maxlength="100" class="inputMaterial" name="email" id="email"/>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Email</label>
            </div>
            <button id="buttonlogintoregister" type="submit">Submit</button>
        </form>

    </div>
</body>

</html>
