@extends('auth.layouts.app')

@section('title', 'Forgot Password')

@section('content')
<div class="container-fluid">
    <div class="frgtemail__headr">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<span class="emailclose__icon">
					<a href="{{ route('password.request') }}"><i class="material-icons">arrow_back</i></span>
                <span class="emailtxt">Back</span></a>
            </div>
        </div>
    </div>

    <div class="midsec__frgtemail">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="chkemail__frgt">
                    <h1>Check Your Email</h1>
                    <p class="frg1">If {{ $email }} is associated with an Deliver4U, you should receive an
                        email containing instructions on how to create a new password.</p>
                    <p class="frg2">Didn't receive the email?Check spam or bulk folders for a message coming from
                        admin@d4u.com.my</p>
                    <p class="retrn__signin"><a href="{{ route('login') }}">Return To Sign In</a></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
        </div>
    </div>
</div>
@endsection
