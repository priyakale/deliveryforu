@extends('auth.layouts.app')

@section('title', 'Forgot Password')

@section('content')
<div class="container-fluid">
    <div class="frgtemail__headr">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<span class="emailclose__icon">
					<a href="{{ route('login') }}"><i class="material-icons">arrow_back</i></span>
                <span class="emailtxt">Back</span></a>
            </div>
        </div>
    </div>

    <div class="midsec__frgtpass">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="pass__frgt">
                    <h1>Forgot Password</h1>
                    <p class="frgpass1">Enter your email address so that we can identify you</p>

                    <form class="form-horizontal frgt__formstyl" action="{{ route('password.email') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="email">Email
                                Address</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="email" class="form-control" placeholder="Enter your email address here"
                                       name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                <span class="errorstyl">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn sntemail">SENT EMAIL</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
        </div>
    </div>
</div>
@endsection


