@extends('auth.layouts.app')

@section('title', 'Reset Password')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container-fluid">
    <div class="reset__headr">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
                <!--                <span class="close__icon"><a href="sign_in.html"><i class="material-icons">close</i></a></span>-->
                <!--                <span class="resettxt">Reset Password</span>-->
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-8">
                <div class="resetpasssigninup__dets">
                    <span class="resetacc__txt">Have an account?</span>
                    <span>
						    	<a href="{{ route('login') }}" class="btn restsigninlogbtn">SIGN IN</a>
						    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="midsec__resetpass">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <form class="form-horizontal form__iptsignin form__resetpassfrm"
                      action="{{ route('password.request') }}" method="POST">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="email" value="{{ $email }}">
                    <div class="row form-group">
                        <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="npass">* New
                            Password</label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="password" name="password" class="form-control"
                                   placeholder="Enter your password here">
                            @if ($errors->has('password'))
                            <span class="errorstyl">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="npass">* Confirm New
                            Password</label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="password" name="password_confirmation" class="form-control"
                                   placeholder="Confirm your password here">
                            @if ($errors->has('password_confirmation'))
                            <span class="errorstyl">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group restbtnup">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn signinbtn">RESET PASSWORD</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
        </div>
    </div>
</div>
@endsection
