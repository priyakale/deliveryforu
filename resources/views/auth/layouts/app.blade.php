<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  ************* bootstrap 3 css *************** -->
    <link rel="stylesheet" href="{{ custom_asset('css/bootstrap.min.css') }}">
    <!-- ************* font family css ************* -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <!-- ************* material design icon css ************* -->
    <link href="{{ custom_asset('css/material-icons.css') }}" rel="stylesheet">
    <!-- ************* Owl ************* -->
    <link rel="stylesheet" href="{{ custom_asset('owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ custom_asset('owlcarousel/assets/owl.theme.default.min.css') }}">
    <!-- ************* mobile number ************* -->
    <link rel="stylesheet" href="{{ custom_asset('css/style.css') }}">
    <!-- ************* responsive css ************* -->
    <link rel="stylesheet" href="{{ custom_asset('css/responsive.css') }}">
    <style>

    </style>
    @yield('style')
</head>
<body>

@yield('content')

<script src="{{ custom_asset('js/jquery.min.js') }}"></script>
<script src="{{ custom_asset('js/bootstrap.min.js') }}"></script>
<script src="{{ custom_asset('owlcarousel/owl.carousel.js') }}"></script>
<script>

</script>
@yield('script')
</body>
</html>
