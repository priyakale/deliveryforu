@extends('auth.layouts.app')

@section('title', 'Log In')

@section('content')
<nav class="navbar navbar-inverse navbar__inverse navbar__inverse__sign">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Deliver4u</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li class="seprate__menu"><a href="#">Do not have an account yet?</a></li>
        <li class="rounded__up"><a href="{{ route('register') }}">SIGN UP</a></li>
    </ul>
  </div>
</nav>
<div class="hero-wrap">
  <div class="overlay__sign"></div>
  <div class="overlay1__sign"></div>

  <div class="row">
  	<div class="col-md-6">
  		<div class="signup__img">
  			<img src="{{ custom_asset('images/web/signup.png') }}" alt="">
  		</div>
  	</div>


    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="signin__form">
                    <h1>Sign In</h1>
                    <p class="phell__txt">Hello Earthlings! Please Sign in</p>
                    <form class="form-horizontal form__iptsignin" action="{{ route('login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12">Email
                                Address</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="email" class="form-control" placeholder="Email Address" name="email"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                <span class="errorstyl">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                   for="pwd">Password</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pass__btneye">
              <span class="btn-show-passtwo">
                <i class="material-icons" onclick="togglePasswordVisibility()">remove_red_eye</i>
              </span>
                                    <input type="password" class="form-control" placeholder="Enter password"
                                           name="password" id="password">
                                    @if ($errors->has('password'))
                                    <span class="errorstyl">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="frgt__pas">
                                    <a href="{{ route('password.request') }}">Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn signinbtn">SIGN IN</button>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <p class="or__txt">OR</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.facebook.login') }}" type="submit" class="btn signinbtnfb">
                                    <img src="{{ custom_asset('images/fb.png') }}" alt="Facebook">
                                    <span>CONTINUE WITH FACEBOOK</span></a>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.google.login') }}" type="submit"
                                   class="btn signinbtngogl"><img
                                            src="{{ custom_asset('images/google.png') }}" alt="Google"><span>CONTINUE WITH GOOGLE</span></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

  
  </div>

</div>

@include('common.footer2')
@endsection

@section('script')
<script>
    function togglePasswordVisibility() {
        let password = $("#password");

        if (password.attr('type') === "password") {
            password.attr('type', 'text')
        } else {
            password.attr('type', 'password')
        }

    }
</script>
@endsection