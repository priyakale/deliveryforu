@extends('auth.layouts.app')
@section('content')

<nav class="navbar navbar-inverse navbar__inverse navbar__inverse__sign">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">Deliver4u</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li class="seprate__menu"><a href="#">Already have an account?</a></li>
        <li class="rounded__up"><a href="{{ route('login') }}">SIGN IN</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="hero-wrap">
  <div class="overlay__sign"></div>
  <div class="overlay1__sign"></div>

  <div class="row">
  	<div class="col-md-6">
  		<div class="signup__img">
  			<img src="{{ custom_asset('images/web/signup.png') }}" alt="">
  		</div>
  	</div>

  	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="signup__form">
                    <form class="form-horizontal form__iptsignup" action="{{ route('register') }}" method="POST">
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.facebook.login') }}" type="submit" class="btn signinbtnfb">
                                    <img src="{{ custom_asset('images/fb.png') }}" alt="Facebook">
                                    <span>SIGN UP WITH FACEBOOK</span>
                                </a>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ route('auth.google.login') }}" type="submit" class="btn signinbtngogl">
                                    <img src="{{ custom_asset('images/google.png') }}" alt="Google">
                                    <span>SIGN UP WITH GOOGLE</span>
                                </a>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <p class="or__txt">OR</p>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1>Sign Up</h1>
                                <p class="pbasic__txt">Enter basic information here</p>
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="row form-group">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padrghtleft">
                                <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="first_name">First
                                    Name</label>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                           value="{{ old('first_name') }}">

                                    @if ($errors->has('first_name'))
                                    <span class="errorstyl">{{ $errors->first('first_name') }}</span>
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padrghtleft">
                                <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="last_name">Last
                                    Name</label>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                           value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                    <span class="errorstyl">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="email">Email
                                Address</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="email" class="form-control" placeholder="Email Address" name="email"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                <span class="errorstyl">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12" for="email">Phone
                                Number</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input name="phone_no" id="phone-no" type="tel" class="form-control"
                                       value="{{ old('phone_no') }}">
                                @if ($errors->has('phone_no'))
                                <span class="errorstyl">{{ $errors->first('phone_no') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                   for="password">Password</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="password" class="form-control" placeholder="Enter Password"
                                       name="password">
                                @if ($errors->has('password'))
                                <span class="errorstyl">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                   for="password_confirmation">Confirm Password</label>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="password" class="form-control" placeholder="Confirm Password"
                                       name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                <span class="errorstyl">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="chkbx__agree">
                                    <div class="checkbox">
                                        <label class="customcheckoutlet">
                                            <input type="checkbox" required>
                                            <span class="checkmark"></span>
                                            <span> I Agree with the <a href="">Terms & Conditions</a> </span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn signinbtn">SIGN UP</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
  </div>

</div>

<!-- Footer -->

  <div class="footer__bg no__imgfooter">

    <div class="footer__inner">
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">DELIVER4U</a>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Home</a></li>
            <li><a href="#">Jobs</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Become a vendor</a></li>
          </ul>
        </div>
      </nav>
      <hr>
      <div class="footer__copy">
        <p>Copyright 2019 Deliver4u</p>
        <p>Powered by I-Serve</p>
      </div>
    </div>
  </div>

<!-- Footer -->
@endsection
@section('script')
<script src="{{ custom_asset('js/country_list/intlTelInput.js') }}"></script>
<script>
    $(document).ready(function () {
        var input = document.querySelector("#phone-no");
        window.intlTelInput(input, {
            utilsScript: "{{ custom_asset('js/country_list/utils.js') }}",
        });
    });
</script>
@endsection