$( document ).ready(function() {
    $('#update_user').submit( function() {
        if($.trim($('#name').val()) == ''){
            alert('Role Name empty. Please fill in role name!');
            return false;
        }else{
            return true;
        }
     });

    $("#btn_add").click(function(){
        cloneTableTr('table_access');
    });
    
    $("#btn_add_menu").click(function(){
        cloneTableTr('table_menu');
    });

    $("#table_access").on("click", ".a-select-function", function () {
        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
        $(this).parents(".dropdown").find('.hidden-value').val($(this).text());
    });

    $("#table_access").on("click", ".a-select-controller", function () {
        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
        $(this).parents(".dropdown").find('.hidden-value').val($(this).text());
        setValueController(this);
    });

    $("#table_menu").on("click", ".a-select-menu", function () {
        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
        $(this).parents(".dropdown").find('.hidden-value').val($(this).text());
    });

    $("#table_access").on("click", ".btn_remove", function () {
        var rowCount = $('#table_access tr').length;
        var id_array =  $(this).attr('id').split("-");
        if(rowCount <= 2){
            $('#select_controller-'+id_array[1]).html('Please Select Controller<span class="caret"></span>');
            $('#controller_name-'+id_array[1]).val('');
            $('#select_function-'+id_array[1]).html('Please Select Function<span class="caret"></span>');
            $('#function_name-'+id_array[1]).val('');
            $("#select_function_li-"+id_array[1]+" li").remove();
        }else{
            $('#tr_controller-'+id_array[1]).remove();
        }
    });

    $("#table_menu").on("click", ".btn_remove_menu", function () {
        var rowCount = $('#table_menu tr').length;
        var id_array =  $(this).attr('id').split("-");
        if(rowCount <= 2){
            $('#select_menu_btn-'+id_array[1]).html('Please Select Function<span class="caret"></span>');
            $('#pos-'+id_array[1]).val('');
            $('#menu_name-'+id_array[1]).val('');
        }else{
            $('#tr_menu-'+id_array[1]).remove();
        }
    });
});//End docuemnt ready

function setValueController(element){
    var element = $(element).parents(".dropdown").find('.hidden-value');
    var id_array = element.attr('id').split("-");
    var select_function_id = 'select_function-'+id_array[1];
    
    $('#'+select_function_id).html('Please Select Function <span class="caret"></span>');
    $('#'+select_function_id).next().val('');

    $("#select_function_li-"+id_array[1]+" li").remove();
    setTimeout(
    function(){
        var value_select = element.val();
        $.each(array_controller[value_select], function( index, value ) {
            $("#select_function_li-"+id_array[1])
            .append('<li><a href="javascript:void(0)" class="a-select-function"' +
            'value="'+value+'">'+ value +'</a></li>');
        });
    }, 100);
}//End set value controller

function cloneTableTr(table_id){
    var rowCount = $('#'+table_id+' tr').length;
    var clone_ele = $('#'+table_id+' tr:last').clone();

    clone_ele.find("input").each(function() {
        $(this).val('').attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var new_id = id_array[0]+'-'+rowCount;
                while ($('#'+ new_id).length) {
                    rowCount++;
                    new_id = id_array[0]+'-'+rowCount;
                }
                return new_id; 
            }else{
                return id;
            }
        });
        $(this).attr('name', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var new_id = id_array[0]+'-'+rowCount;
                while ($('#'+ new_id).length) {
                    rowCount++;
                    new_id = id_array[0]+'-'+rowCount;
                }
                return new_id; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find("ul").each(function() {
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var new_id = id_array[0]+'-'+rowCount;
                while ($('#'+ new_id).length) {
                    rowCount++;
                    new_id = id_array[0]+'-'+rowCount;
                }
                return new_id; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.find("button").each(function() {
        $(this).attr('id', function(_, id) {
            var id_array = id.split("-");
            if(id_array.length > 1){
                var new_id = id_array[0]+'-'+rowCount;
                while ($('#'+ new_id).length) {
                    rowCount++;
                    new_id = id_array[0]+'-'+rowCount;
                }
                return new_id; 
            }else{
                return id;
            }
        });
    }).end();

    clone_ele.attr('id', function(_, id) {
        var id_array = id.split("-");
        if(id_array.length > 1){
            var new_id = id_array[0]+'-'+rowCount;
            while ($('#'+ new_id).length) {
                rowCount++;
                new_id = id_array[0]+'-'+rowCount;
            }
            return new_id; 
        }else{
            return id;
        }
    })

    $("#"+table_id+" tr:first").after(clone_ele);
    
    clone_ele.attr('id', function(_, id) {
        var id_array = id.split("-");
        if(table_id == 'table_access'){
            $('#select_controller-'+id_array[1])
            .html('Please Select Controller<span class="caret"></span>');
            $('#select_function-'+id_array[1])
            .html('Please Select Function<span class="caret"></span>');
        }else{
            $('#select_menu_btn-'+id_array[1])
            .html('Please Select Menu<span class="caret"></span>');
        }
    })
}//End Clone Table