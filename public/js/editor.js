$( document ).ready(function() {
    $('.text-area-edit').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style']],
            ['fontstyle', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['insert', ['picture']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['widget', ['table','link','help']],
        ]
    });
});//end document ready