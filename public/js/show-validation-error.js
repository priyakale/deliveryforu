function showValidationError(data){
    $('div.alert.alert-icon').remove();
    $('#'+Object.keys(data)[0]).focus();

    var tab_select = $('#'+Object.keys(data)[0]).parents('.tab-pane').attr('id');

    $('.submission-step').removeClass('active');
    $('.tab-pane').removeClass('in active');

    $('#'+tab_select).addClass('in active');
    $('#'+tab_select +'_tab').addClass('active');

    $("span.help-block").remove();
    $('div .form-group').removeClass('has-error');
    $('div .form-group label').removeAttr('style');
    
    $.each( data, function( key, value ) {
    	if ($('#'+key).parents('div .form-group').find('span.help-block').length <1) {
            $('#'+key).parents('div .form-group').append("<span class='help-block'><span>"+value[0]+"</span></span>");
	        $('#'+key).parents('div .form-group').addClass('has-error'); 
	        $('#'+key).parents('div .form-group').find('label').css("color", "#a94442");
		}
    }); 
}//End show validation error

function alertError(data){
    $('div.alert.alert-icon').remove();
    $('#'+Object.keys(data)[0]).focus();

    var tab_select = $('#'+Object.keys(data)[0]).parents('.tab-pane').attr('id');

    $('.submission-step').removeClass('active');
    $('.tab-pane').removeClass('in active');

    $('#'+tab_select).addClass('in active');
    $('#'+tab_select +'_tab').addClass('active');

    $("span.help-block").remove();
    $('div .form-group').removeClass('has-error');
    $('div .form-group label').removeAttr('style');
    
    $.each( data, function( key, value ) {
        if ($('#'+key).parents('div .form-group').find('span.help-block').length <1) {
            $('#'+key).parents('div .form-group').append("<span class='help-block'><span>"+value[0]+"</span></span>");
            $('#'+key).parents('div .form-group').addClass('has-error'); 
            $('#'+key).parents('div .form-group').find('label').css("color", "#a94442");
        }
    }); 
}//End alert error