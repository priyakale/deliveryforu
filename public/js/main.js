$( document ).ready(function() {
	$('.chosen-select').chosen({
    	no_results_text: "Oops, Nothing Found!",
    	width: "98%"
  	});

  	$('.chosen-select-maxlength').chosen({
    	no_results_text: "Oops, Nothing Found!",
    	width: "200px"
  	});

	$('a[rel=popover]').popover({
        html: true,
        trigger: 'hover',
        content: function () {
            return '<img width="100%" src="' + $(this).data('img') + '" />';
        }
    });

    $(".switch-checkbox").bootstrapSwitch();
    $('.switch-checkbox').on('switchChange.bootstrapSwitch', function(event, state) {
	  	var ele_name = $(this).attr('name');
	  	if(state){
	  		$('#' + ele_name).val(1);
	  	}else{
	  		$('#' + ele_name).val(0);
	  	}
	});

    $(".a-select-value").click(function(){
	  	$(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
 		$(this).parents(".dropdown").find('.hidden-value').val($(this).text());
	});

    $(".close-alert").click(function(){
	  	$(this).parents(".alert").hide();
	});
	
	$('.datepickerFromTo').datepicker({
	    format: 'dd/mm/yyyy'
	}).on('changeDate', dateChanged);

    $('#btn_collapse').on('click',function(){
    	var icon = '';
        $('a[data-toggle="collapse"]').each(function(){
            var objectID=$(this).attr('data-target');
            if($(objectID).hasClass('in')===false){
             	$(objectID).collapse('show');
             	 icon = "minus";
            }else{
            	$(objectID).collapse('hide');
            	icon = "plus";
            }
        });
        $('#btn_collapse').html('<i class="fa fa-'+ icon +'" aria-hidden="true"></i>'); 
    });
});//end document ready

function dateChanged(ev){
	var selected_date =  $(this).datepicker('getDate');
	var selected_id = $(this).attr('id');
	var other_id = '';
	var other_date = '';

	if(selected_id == 'date_from'){
		other_id = 'date_to';
		other_date =  $('#'+other_id).datepicker('getDate');
		if(other_date !== null && selected_date !== null){
			if( (selected_date.getTime() > other_date.getTime())){
			    alert('Date From must greater then Date To');
			    $('#'+selected_id).datepicker('setDate', null);
			}
		}
	}else{
		other_id = 'date_from';
		other_date =  $('#'+other_id).datepicker('getDate');
		if(other_date !== null && selected_date !== null){
			if( (other_date.getTime() > selected_date.getTime())){
			    alert('Date To must greater then Date From');
			    $('#'+selected_id).datepicker('setDate', null);
			}
		}
	}
}//end dateChanged
