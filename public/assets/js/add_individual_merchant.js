$(document).ready(function() 
{
	$("#save_individual_merchant").validate({
      rules: {
        merchant_email_id: {
          required: true,
          email:true
        }
      },
      messages:{
        merchant_email_id: {
          required: "Please enter email id"
        }
      }
    });
});