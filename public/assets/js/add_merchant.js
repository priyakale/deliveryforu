$(document).ready(function() 
{
	$("#save_merchant").validate({
      rules: {
        user_name: {
          required: true
        },
        user_email_id: {
          required: true,
          //email:true
        },
        
        company_registered_name: {
          required: true
        },
        business_registration_number: {
          required: true
        },
        trading_name: {
          required: true
        },
        years_in_business: {
          required: true
        },
        headquarters_contact_number: {
          required: true
        },
        salesperson_name: {
          required: true
        },
        headquarters_address: {
          required: true
        },
        logo: {
          required: true
        },
        type_of_ownership: {
          required: true
        },
        type_of_business: {
          required: true
        },
        bank_name: {
          required: true
        },
        acc_number: {
          required: true
        },
        acc_holder_name: {
          required: true
        },
        acc_finance_contact_number: {
          required: true
        },
        acc_contact_number: {
          required: true
        },
        acc_email: {
          required: true,
          email:true
        }
      },
      messages:{
        user_name: {
          required: "Please enter user name"
        },
        user_email_id: {
          required: "Please enter email"
        },
        user_password: {
          required: "Please enter password"
        },
        user_confirm_password: {
          required: "Please enter password"
        },
        company_registered_name: {
          required: "Please enter company name"
        },
        business_registration_number: {
          required: "Please enter business registration number"
        },
        trading_name: {
          required: "Please trading name"
        },
        years_in_business: {
          required: "Please year of business"
        },
        headquarters_contact_number: {
          required: "Please enter headquarters contact number"
        },
        salesperson_name: {
          required: "Please enter salesperson name"
        },
        headquarters_address: {
          required: "Please enter headquarters address"
        },
        logo: {
          required: "Please select image"
        },
        type_of_ownership: {
          required: "Please select type of ownership"
        },
        type_of_business: {
          required: "Please select type of business"
        },
        bank_name: {
          required: "Please enter bank name"
        },
        acc_number: {
          required: "Please enter acc number"
        },
        acc_holder_name: {
          required: "Please enter acc holder name"
        },
        acc_finance_contact_number: {
          required: "Please enter acc finance contact number"
        },
        acc_contact_number: {
          required: "Please enter acc contact number"
        },
        acc_email: {
          required: "Please enter email"
        }
      }
    });
});