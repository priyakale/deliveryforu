function showValidationError(data){
    $('div.alert.alert-icon').remove();
    $('#'+Object.keys(data)[0]).focus();

    var id_array =  Object.keys(data)[0].split(".");

    if (id_array[1]!==undefined) {
        var tab_select = $("[name='outlets["+id_array[1]+"]["+id_array[2]+"]'").parents('.tab-pane').attr('id');
    }
    else{
        var tab_select = $('#'+Object.keys(data)[0]).parents('.tab-pane').attr('id');
    }

    $('.submission-step').find('li').removeClass('active');
    $('.tab-pane').removeClass('in active');

    $('#'+tab_select).addClass('in active');
    $('#'+tab_select +'_tab').parents('li').addClass('active');
 
    $("span.help-block").remove();
    $('div .form-group').removeClass('has-error');
    $('div .form-group label').removeAttr('style');
    
    $.each( data, function( key, value ) {

        var id_array =  key.split(".");
        
        if (id_array[1]!==undefined) {
            if ($("[name='outlets["+id_array[1]+"]["+id_array[2]+"]'").parents('div .form-group').find('span.help-block').length <1) {
                $("[name='outlets["+id_array[1]+"]["+id_array[2]+"]'").parents('div .form-group').append("<span class='help-block' style='display:block;'><span>"+value[0]+"</span></span>");
                $("[name='outlets["+id_array[1]+"]["+id_array[2]+"]'").parents('div .form-group').addClass('has-error'); 
                $("[name='outlets["+id_array[1]+"]["+id_array[2]+"]'").parents('div .form-group').find('label').css("color", "#a94442");
            }
        } else if ($('#'+key).parents('div .form-group').find('span.help-block').length <1) {
            $('#'+key).parents('div .form-group').append("<span class='help-block' style='display:block;'><span>"+value[0]+"</span></span>");
	        $('#'+key).parents('div .form-group').addClass('has-error'); 
	        $('#'+key).parents('div .form-group').find('label').css("color", "#a94442");
		}
    }); 
}

function alertError(message){
    $('div.alert.alert-icon').remove();
    $( ".main-inner" ).prepend("<div class='alert alert-icon alert-dismissible alert-danger mt-70' role='alert' style='margin-left: 15px;margin-right: 15px;'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='fa fa-times' aria-hidden='true'></i></button>"+message+"</div>");
}