<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Company Registered Name
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icon', 100);
            $table->string('name', 100);
            $table->char('code', 3);
            $table->string('symbols', 20);
            $table->integer('status_id')->nullable()->unsigned();
            $table->timestamps();

            //FK
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
