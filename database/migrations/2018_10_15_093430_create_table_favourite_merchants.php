<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFavouriteMerchants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Company Registered Name
        Schema::create('favourite_merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('merchant_id')->nullable()->unsigned();
            $table->timestamps();

            //FK
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourite_merchants');
    }
}
