<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_transaction_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rtid')->unsigned()->nullable();
            $table->integer('vid')->unsigned()->nullable();
            $table->decimal('amount',15,2);
            $table->integer('status')->nullable()->unsigned();
            $table->dateTime('accepteddatetime')->nullable(); 
            $table->string('statusreason',50)->nullable();
            $table->integer('created_by')->unsigned()->nullable();   
            //FK
            $table->foreign('rtid')->references('id')->on('request_transactions')->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('vid')->references('id')->on('vendors')->onUpdate('cascade')->onDelete('cascade');                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_transaction_logs');
    }
}
