<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorDocumentChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_document_checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vid')->unsigned()->nullable();
            $table->string('docid',20)->nullable();
            $table->string('remark',100)->nullable(); 
            $table->integer('created_by')->unsigned()->nullable();            
            //FK
            $table->foreign('vid')->references('id')->on('vendors')->onUpdate('cascade')->onDelete('cascade');                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_document_checklists');
    }
}
