<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rid')->unsigned()->nullable();
            $table->integer('vid')->unsigned()->nullable();
            $table->decimal('amount',15,2);
            $table->integer('status')->nullable()->unsigned();
            $table->dateTime('accepteddatetime')->nullable(); 
            $table->string('statusreason',50)->nullable();   
            //FK
            $table->foreign('rid')->references('id')->on('requests')->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('vid')->references('id')->on('vendors')->onUpdate('cascade')->onDelete('cascade');                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_transactions');
    }
}
