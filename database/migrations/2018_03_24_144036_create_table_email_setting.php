<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmailSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->integer('driver_id')->unsigned();
            $table->string('smtp_host', 50)->nullable();
            $table->string('smtp_username', 50)->nullable();
            $table->string('smtp_password', 50)->nullable();
            $table->integer('port')->unsigned();
            $table->integer('encryption_id')->unsigned();
            $table->string('sendmail_path', 50)->nullable();
            $table->string('mailgun_domain', 100)->nullable();
            $table->string('mailgun_secret', 100)->nullable();
            $table->timestamps();

            $table->foreign('driver_id')->references('id')
            ->on('lookups');
            $table->foreign('encryption_id')->references('id')
            ->on('lookups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_settings');
    }
}
