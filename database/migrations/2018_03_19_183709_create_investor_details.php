<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('salutation',100)->nullable();
            $table->integer('gender')->nullable();
            $table->string('fullname',100)->nullable();
            $table->integer('nationality')->nullable();
            $table->string('id_no',100)->nullable();
            $table->string('passport',100)->nullable();
            $table->date('dob')->nullable();
            $table->string('correspondence',255)->nullable();
            $table->string('zipcode',100)->nullable();
            $table->string('city',100)->nullable();
            $table->string('state')->nullable();
            $table->integer('country')->nullable();
            $table->string('phone',100)->nullable();
            $table->string('email',100)->nullable();
            $table->binary('id_upload')->nullable();
            $table->string('risk',100)->nullable();
            $table->string('ack',100)->nullable();
            $table->binary('referrer')->nullable();
            $table->binary('signature')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('total_paid')->nullable();
            $table->integer('status_payment')->default(20);//20=pending, 21=completed, 22=Rejected
            
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_details');
    }
}
