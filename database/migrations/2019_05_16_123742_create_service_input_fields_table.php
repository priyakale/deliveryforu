<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceInputFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_input_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pkgid')->unsigned()->nullable();
            $table->integer('input_field_id')->unsigned()->nullable();
            $table->integer('is_mandatory')->unsigned()->nullable(); 
            $table->integer('position')->unsigned()->nullable(); 
            //FK
            $table->foreign('pkgid')->references('id')->on('package_services')->onUpdate('cascade')->onDelete('cascade');
            //FK
            $table->foreign('input_field_id')->references('id')->on('input_fields')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_input_fields');
    }
}
