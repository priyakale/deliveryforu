<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOutletAddName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_outlets', function($table) {       
            $table->dropColumn('outlet_pic_password');       
            $table->dropColumn('outlet_pic_username');      

            $table->string('outlet_name', 100)->nullable()->after('user_id'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_outlets', function($table) {
            $table->dropColumn('outlet_name');    
            $table->string('outlet_pic_password', 100)->nullable()->after('outlet_pic_email');
            $table->string('outlet_pic_username', 100)->nullable()->after('outlet_pic_password');

        });
    }
}
