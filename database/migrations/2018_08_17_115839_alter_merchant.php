<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchants', function($table) {
            $table->string('individual_name',100)->nullable()->default(NULL)->after('sub_merchant_ref');
            $table->string('individual_address',100)->nullable()->default(NULL)->after('individual_name');
            $table->string('ic_number',100)->nullable()->default(NULL)->after('individual_address');
            $table->integer('type_id')->unsigned()->default('71')->after('ic_number');

            $table->foreign('type_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function($table) {
            $table->dropForeign(['type_id']);
            $table->dropColumn('individual_name');
            $table->dropColumn('individual_address');
            $table->dropColumn('ic_number'); 
            $table->dropColumn('type_id');       
        });
    }
}
