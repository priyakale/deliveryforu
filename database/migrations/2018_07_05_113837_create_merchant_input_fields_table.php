<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantInputFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_input_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id')->nullable()->unsigned();
            $table->integer('input_field_id')->nullable()->unsigned();
            $table->integer('is_mandatory')->nullable()->default('0');
            $table->integer('position')->nullable()->default('0');

            
            $table->foreign('input_field_id')->references('id')->on('input_fields')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_input_fields');
    }
}
