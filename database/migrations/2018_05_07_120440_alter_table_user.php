<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_identification_type_id_foreign');
            $table->dropForeign('users_language_foreign');
            $table->dropColumn('address');
            $table->dropColumn('mobile_number');
            $table->dropColumn('imei1');
            $table->dropColumn('imei2');
            $table->dropColumn('identification_type_id');
            $table->dropColumn('identification_number');
            $table->dropColumn('last_login');
            $table->dropColumn('language');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address', 250)->after('password')->nullable();
            $table->string('mobile_number', 20)->after('address')->nullable();
            $table->string('imei1', 20)->after('mobile_number')->nullable();
            $table->string('imei2', 20)->after('imei1')->nullable();
            $table->integer('identification_type_id')->after('imei2')->nullable()->unsigned();
            $table->string('identification_number', 30)->after('identification_type_id')->nullable();
            $table->timestamp('last_login')->after('identification_number')->nullable();
            $table->integer('language')->after('last_login')->default(16)->nullable()->unsigned();
        });
    }
}
