<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->binary('signature')->after('amount')->nullable();
            $table->binary('invoice_img')->after('signature')->nullable();
            $table->string('account_number')->after('invoice_img')->nullable();
            $table->string('bill_number')->after('account_number')->nullable();
            $table->string('invoice_number')->after('invoice_img')->nullable();
            $table->string('remarks',100)->after('invoice_img')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('signature');
            $table->dropColumn('invoice_img');
            $table->dropColumn('account_number');
            $table->dropColumn('bill_number');
            $table->dropColumn('invoice_number');
        });
    }
}
