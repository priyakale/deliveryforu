<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlavoriteRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flavorite_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pkgid')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('category')->unsigned()->nullable(); 
            $table->integer('status')->unsigned()->nullable(); 
            $table->integer('dealmethod')->unsigned()->nullable(); 
            $table->decimal('maxamount',15,2); 
            $table->decimal('minamount',15,2); 
            $table->string('note',255)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            //FK
            $table->foreign('pkgid')->references('id')->on('package_services')->onUpdate('cascade')->onDelete('cascade');
            //FK
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flavorite_requests');
    }
}
