<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('description',255)->nullable();
            $table->integer('status_id')->nullable()->unsigned();
            $table->timestamps();

            //FK
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });

        Schema::create('role_accessibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->string('controller',100)->nullable();
            $table->string('function', 100)->nullable();
            $table->timestamps();

            //FK
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });

        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('link_name',100)->nullable();
            $table->char('icon_type', 1)->default('N'); //F = fontawesome.io & M = material & N = none
            $table->string('icon',100)->nullable();
            $table->string('css_class',120)->nullable();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('status_id')->nullable()->unsigned();
            $table->timestamps();

            //FK
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });

        Schema::create('role_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('menu_id')->unsigned();
            $table->integer('position');
            $table->timestamps();

            //FK
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('menu_id')->references('id')->on('menus')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_menus');
        Schema::dropIfExists('menus');
        Schema::dropIfExists('role_accessibilities');
        Schema::dropIfExists('roles');
    }
}
