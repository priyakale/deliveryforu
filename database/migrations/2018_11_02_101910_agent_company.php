<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgentCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchants', function (Blueprint $table) {
             $table->string('agent_company',100)->nullable()->after('charges_apply');
             $table->string('agent_type',100)->nullable()->after('agent_company');
             $table->string('agent_name',100)->nullable()->after('agent_type');
             $table->string('agent_code',100)->nullable()->after('agent_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->dropColumn(['agent_company']);
            $table->dropColumn(['agent_type']);
            $table->dropColumn(['agent_name']);
            $table->dropColumn(['agent_code']);
        });
    }
}
