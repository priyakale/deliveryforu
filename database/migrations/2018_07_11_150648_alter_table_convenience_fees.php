<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableConvenienceFees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convenience_fees', function($table) {
            $table->char('createdby')->default('NULL');
            $table->char('modifyby')->default('NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convenience_fees', function($table) {
            $table->dropColumn('createdby');
            $table->dropColumn('modifyby');           
        });
    }
}
