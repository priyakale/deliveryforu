<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id')->unsigned();
            $table->integer('bank_name')->unsigned();
            $table->string('acc_number');
            $table->string('acc_holder_name');
            $table->string('acc_finance_contact_number');
            $table->string('acc_contact_number');
            $table->string('acc_email');


            $table->foreign('bank_name')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
            
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_bank_accounts');
    }
}
