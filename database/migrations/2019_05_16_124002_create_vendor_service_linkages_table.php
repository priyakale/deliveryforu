<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorServiceLinkagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_service_linkages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pkgid')->unsigned()->nullable();
            $table->integer('vid')->unsigned()->nullable();
            $table->integer('status')->nullable()->unsigned();
            $table->string('type',20)->nullable();
            $table->string('yrexp',20)->nullable();
            $table->string('rates', 20)->nullable();
            $table->string('email', 50)->nullable(); 
            $table->string('notes',255)->nullable();
            $table->string('vehicle_type',20)->nullable(); 
            $table->string('vehicle_brand',20)->nullable();  
            $table->string('vehicle_model',20)->nullable();
            $table->string('vehicle_color',20)->nullable();  
            $table->string('vehicle_number',20)->nullable();  
            $table->string('license',50)->nullable();  
            $table->string('addr1',255)->nullable();      
            $table->string('addr2',255)->nullable();
            $table->string('addr3',255)->nullable();
            $table->string('addr_postcode',20)->nullable();    
            $table->integer('addr_state')->nullable()->unsigned();
            $table->integer('addr_country')->nullable()->unsigned();
            $table->integer('deal_method_id')->nullable()->unsigned();
            $table->dateTime('vehicleregdate')->nullable();  
            //FK
            $table->foreign('pkgid')->references('id')->on('package_services')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('vid')->references('id')->on('vendors')->onUpdate('cascade')->onDelete('cascade');                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_service_linkages');
    }
}
