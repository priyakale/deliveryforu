<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmailNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 250)->nullable();
            $table->longText('description')->nullable();
            $table->integer('email_for_id')->unsigned();
            $table->integer('status_id')->nullable()->unsigned();

            //FK
            $table->foreign('email_for_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_notifications');
    }
}
