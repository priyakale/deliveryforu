<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code',2)->unique();
            $table->char('code_A3',3);
            $table->char('code_N3',3);
            $table->string('name')->nullable();
            $table->integer('position')->default(0);
            $table->char('region',2);
            $table->float('lat')->default(0);
            $table->float('lon')->default(0);
            $table->string('phonecode')->nullable();
            $table->integer('phonecode_position')->default(0);
            $table->integer('status_id')->nullable()->unsigned();
            $table->timestamps();

            //FK
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
