<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('address_type_id')->unsigned();
          $table->integer('user_id')->unsigned();
          $table->string('address_1',200);
          $table->string('address_2',200)->nullable();
          $table->string('city',100);
          $table->string('postal_code',10);
          $table->integer('state_id')->unsigned();
          $table->char('country_code',2)->nullable();

          $table->timestamps();

          //FK
          $table->foreign('state_id')->references('id')->on('states')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('country_code')->references('code')->on('countries')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('address_type_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

          $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
