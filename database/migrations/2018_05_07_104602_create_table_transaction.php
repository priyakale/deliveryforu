<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investor_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->decimal('amount', 11, 2)->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->default(20);//20=pending, 21=completed, 22=Rejected
            
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('investor_id')->references('id')->on('investor_details')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
