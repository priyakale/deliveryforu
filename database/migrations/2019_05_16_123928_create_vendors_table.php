<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('shortname',20)->nullable();
            $table->string('longname',50)->nullable();
            $table->integer('status')->nullable()->unsigned();
            $table->date('startservice')->nullable();
            $table->date('endservice')->nullable(); 
            $table->string('rating',20)->nullable();
            $table->integer('category')->nullable()->unsigned();
            $table->string('profile_pic', 255)->nullable();
            $table->string('email', 50)->nullable(); 
            $table->string('bankname',50)->nullable();
            $table->string('bankaccount',50)->nullable(); 
            $table->string('mykad',50)->nullable();  
            $table->string('uchatid',20)->nullable();
            $table->string('uchatpay',20)->nullable();  
            $table->string('comp_addr1',255)->nullable();  
            $table->string('comp_addr2',255)->nullable();  
            $table->string('comp_addr3',255)->nullable();      
            $table->string('compaddr_postcode',20)->nullable();  
            $table->integer('compaddr_state')->nullable()->unsigned();
            $table->integer('compaddr_country')->nullable()->unsigned();
            $table->string('bizreg',20)->nullable();   
            //FK
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
