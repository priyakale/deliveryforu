<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->integer('field_id')->unsigned()->nullable();
            $table->string('value', 255)->nullable();

            $table->foreign('transaction_id')->references('id')->on('transactions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('field_id')->references('id')->on('input_fields')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('merchant_id')->unsigned()->nullable();    

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_fields');

        Schema::table('transactions', function($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['merchant_id']);
            $table->dropColumn('user_id');      
            $table->dropColumn('merchant_id');    
        });
    }
}
