<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('package_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pkgcode',5)->nullable();
            $table->string('name',20)->nullable();
            $table->string('description',50)->nullable();
            $table->integer('category_id')->nullable()->unsigned();
            $table->integer('status_id')->nullable()->unsigned();
            $table->date('start')->nullable();
            $table->date('end')->nullable(); 
            $table->integer('deal_method_id')->nullable()->unsigned();
            $table->integer('payment_type_id')->nullable()->unsigned();
            $table->integer('screen_flow')->nullable()->unsigned();
            $table->string('logo',50)->nullable();
            $table->integer('insurance_id')->nullable()->unsigned();
            $table->integer('inhouse_id')->nullable()->unsigned();
            $table->integer('chaincode')->nullable()->unsigned();
            $table->integer('ruleid')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_services');
    }
}
