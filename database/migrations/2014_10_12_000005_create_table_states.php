<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',32);
            $table->string('name');
            $table->integer('status_id')->nullable()->unsigned();
            $table->char('country_code',2)->nullable();
            $table->timestamps();

            //FK
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('country_code')->references('code')->on('countries')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
