<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConvenienceFees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenience_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id')->unsigned();
            $table->integer('feetype')->unsigned();
            $table->integer('creditcard_rate')->default('0');
            $table->integer('debitcard_rate')->default('0');
            $table->char('fpx_rate', 1)->default('D');
            $table->char('wallet', 1)->default('D');
            $table->char('active', 1)->default('D');
            $table->char('isdeleted', 1)->default('D');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convenience_fees');
    }
}
