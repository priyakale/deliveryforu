<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInputFieldOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_field_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('field_id')->nullable()->unsigned();
            $table->timestamps();

            //FK
            $table->foreign('field_id')->references('id')->on('input_fields')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_field_options');
    }
}
