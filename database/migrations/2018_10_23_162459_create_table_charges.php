<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('convenience_fee', 11, 2)->nullable()->unsigned();
            $table->decimal('credit_card', 11, 2)->nullable()->unsigned();
            $table->decimal('debit_card', 11, 2)->nullable()->unsigned();
            $table->decimal('fpx', 11, 2)->nullable()->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
