<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->string('email', 100)->nullable()->unique();
            $table->string('password', 200);
            $table->string('address', 250)->nullable();
            $table->string('mobile_number', 20)->nullable();
            $table->string('imei1', 20)->nullable();
            $table->string('imei2', 20)->nullable();
            $table->integer('identification_type_id')->nullable()->unsigned();
            $table->string('identification_number', 30)->nullable();
            $table->boolean('activated')->default(false);
            $table->timestamp('last_login')->nullable();
            $table->integer('language')->default(16)->nullable()->unsigned();
            $table->integer('status_id')->default(1)->nullable()->unsigned();
            $table->integer('role_id')->unsigned()->default(2); //2 for Customer @ Registered
            $table->integer('region')->nullable()->unsigned();
            $table->string('referral_code',100)->nullable();
            $table->integer('referral_id')->nullable()->unsigned();
            $table->timestamps();
            $table->rememberToken();
            
            //FK
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('language')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('region')->references('id')->on('states')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('identification_type_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('referral_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });

        Schema::create('user_activations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('token');
            $table->timestamps();

            //FK
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_activations');
        Schema::dropIfExists('users');
    }
}
