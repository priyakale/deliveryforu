<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pkgid')->unsigned()->nullable();
            $table->integer('subpkgid')->nullable()->unsigned();
            $table->integer('category')->unsigned()->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->integer('deal_method_id')->nullable()->unsigned();
            $table->decimal('final_amount',15,2); 
            $table->decimal('max_amount',15,2);   
            $table->decimal('min_amount',15,2);
            $table->integer('requestor')->unsigned()->nullable();   
            $table->dateTime('requestdatetime')->nullable();
            $table->dateTime('startdealingdatetime')->nullable();
            $table->dateTime('enddealingdatetime')->nullable();
            $table->dateTime('completedatetime')->nullable(); 
            $table->integer('transaction_type')->unsigned()->nullable(); 
            $table->integer('transaction_status')->unsigned()->nullable();
            $table->dateTime('transactionrevdate')->nullable(); 
            $table->dateTime('transactionpaiddate')->nullable();
            $table->string('transactionrefno',30)->nullable();          
            $table->string('note',500)->nullable();  
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            //FK
            $table->foreign('pkgid')->references('id')->on('package_services')->onUpdate('cascade')->onDelete('cascade');  
            $table->foreign('subpkgid')->references('id')->on('sub_services')->onUpdate('cascade')->onDelete('cascade');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
