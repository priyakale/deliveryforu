<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableInvestorDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_details', function (Blueprint $table) {
             $table->integer('user_id')->after('id')->nullable();

             $table->dropColumn('project_id');
             $table->dropColumn('total_paid');
             $table->dropColumn('status_payment');


            // $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_details', function (Blueprint $table) {
            $table->integer('project_id')->after('signature')->nullable();
            $table->integer('total_paid')->after('project_id')->nullable();
            $table->integer('status_payment')->after('total_paid')->default(20);//20=pending, 21=completed, 22=Rejected


             $table->dropColumn('user_id');
        });
    }
}
