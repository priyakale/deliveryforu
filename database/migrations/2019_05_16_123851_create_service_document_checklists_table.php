<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceDocumentChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_document_checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pkgid')->unsigned()->nullable();
            $table->string('name',20)->nullable();
            $table->string('description',100)->nullable();
            $table->integer('is_required')->unsigned()->nullable();   
            //FK
            $table->foreign('pkgid')->references('id')->on('package_services')->onUpdate('cascade')->onDelete('cascade');
                              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_document_checklists');
    }
}
