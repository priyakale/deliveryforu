<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChainProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chain_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shortname',20)->nullable();
            $table->string('longname',50)->nullable();
            $table->integer('status')->unsigned()->nullable(); 
            $table->date('startservice')->nullable();
            $table->date('endservice')->nullable(); 
            $table->string('email',50)->nullable();
            $table->string('bank_name',30)->nullable();
            $table->string('bank_account_no',30)->nullable(); 
            $table->string('compaddr1',255)->nullable();
            $table->string('compaddr2',255)->nullable();
            $table->string('compaddr3',255)->nullable();
            $table->string('compaddr_postcode',5)->nullable(); 
            $table->integer('compaddr_state')->unsigned()->nullable();
            $table->integer('compaddr_country')->unsigned()->nullable();
            $table->string('bizreg',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chain_profiles');
    }
}
