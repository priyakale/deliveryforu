<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Company Registered Name
        Schema::create('merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('logo',100)->nullable();
            $table->string('company_registered_name',100)->nullable();
            $table->string('business_registration_number',100)->nullable();
            $table->string('trading_name',100)->nullable();
            $table->text('headquarters_address')->nullable();
            $table->text('headquarters_contact_number')->nullable();
            $table->integer('type_of_ownership')->nullable();
            $table->integer('type_of_business')->nullable();
            $table->integer('years_in_business')->nullable();
            $table->string('office_number',100)->nullable();
            $table->string('fax_number',100)->nullable();
            $table->string('phone',100)->nullable();
            $table->text('salesperson_name')->nullable();
            $table->integer('position')->default('0');
            $table->integer('status_id')->nullable()->unsigned();
            $table->string('sub_merchant_ref',100)->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('lookups')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
