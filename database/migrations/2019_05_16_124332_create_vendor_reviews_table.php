<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('vendor_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vid')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->date('vrdate')->nullable(); 
            $table->time('vrtime')->nullable();
            $table->string('rating',20)->nullable(); 
            $table->string('remark',100)->nullable();   
            //FK
            $table->foreign('vid')->references('id')->on('vendors')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_reviews');
    }
}
