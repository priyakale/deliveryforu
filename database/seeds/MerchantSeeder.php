<?php

use Illuminate\Database\Seeder;
use App\Models\Merchant;
use App\Models\User;
use App\Models\MerchantBankAccount;
use App\Models\MerchantOutlet;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = array(
            array('id' => '241', 'name' => 'I-SERVE', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'iserve@yahoo.com', 'status_id' => 1),
            array('id' => '242', 'name' => 'MAXIS COMMUNICATIONS', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'maxis@yahoo.com', 'status_id' => 1),
            array('id' => '243', 'name' => 'UNIFI', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'unifi@yahoo.com', 'status_id' => 1),
            array('id' => '244', 'name' => 'INDAH WATER KONSORTIUM', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'indah@yahoo.com', 'status_id' => 1),
            array('id' => '245', 'name' => 'TELEKOM MALAYSIA', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'telekom@yahoo.com', 'status_id' => 1),
            array('id' => '246', 'name' => 'SYABAS', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'syabas@yahoo.com', 'status_id' => 1),
            array('id' => '247', 'name' => 'CELCOM', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'celcom@yahoo.com', 'status_id' => 1),
            array('id' => '248', 'name' => 'DIGI TELECOMMUNICATIONS', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'digi@yahoo.com', 'status_id' => 1),
            array('id' => '249', 'name' => 'ASTRO', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'astro@yahoo.com', 'status_id' => 1),
            array('id' => '250','name' => 'TENAGA NASIONAL BERHAD', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'tenaga@yahoo.com', 'status_id' => 1),
            array('id' => '251','name' => 'UMOBILE', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'umobile@yahoo.com', 'status_id' => 1),
            array('id' => '252','name' => 'YES', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'yes@yahoo.com', 'status_id' => 1),
            array('id' => '253','name' => 'WEBE DIGITAL SDN BHD', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'webe@yahoo.com', 'status_id' => 1),
            array('id' => '254','name' => 'TOMO', 'password' => 123123, 'role_id' => 6, 'activated' => 1, 'email' => 'tomo@yahoo.com', 'status_id' => 1)          
        );

        foreach ($users as $value) {
            $data = array(
                'id'            => $value['id'],
                'name'          => $value['name'],
                'email'         => $value['email'],
                'role_id'         => $value['role_id'],
                'activated'         => $value['activated'],
                'password'      => bcrypt($value['password']),
                'status_id'     => $value['status_id']
            );

            $data_exists = User::find($value['id']);
            if (!is_null($data_exists)) {
                User::where('id', $value['id'])
                        ->update($data); //UPDATE
            } else {
                User::create($data); //CREATE
            }
        }

        $this->command->info('Merchant Users table seeded');


        $merchants = array(
            array('id' => '1','user_id' => '241','company_registered_name' => 'I-SERVE','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'blank'),
            array('id' => '2','user_id' => '242','company_registered_name' => 'MAXIS COMMUNICATIONS','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'MAXIS_UPayMe'),
            array('id' => '3','user_id' => '243','company_registered_name' => 'UNIFI','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'UNIFI_UPayMe'),
            array('id' => '4','user_id' => '244','company_registered_name' => 'INDAH WATER KONSORTIUM','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'INDAHWATER_UPayMe'),
            array('id' => '5','user_id' => '245','company_registered_name' => 'TELEKOM MALAYSIA','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'TELEKOMMALAYSIA_UPayMe'),
            array('id' => '6','user_id' => '246','company_registered_name' => 'SYABAS','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'SYABAS_UPayMe'),
            array('id' => '7','user_id' => '247','company_registered_name' => 'CELCOM','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'CELCOM_UPayMe'),
            array('id' => '8','user_id' => '248','company_registered_name' => 'DIGI TELECOMMUNICATIONS','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'DIGI_UPayMe'),
            array('id' => '9','user_id' => '249','company_registered_name' => 'ASTRO','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'ASTRO_UPayMe'),
            array('id' => '10','user_id' => '250','company_registered_name' => 'TENAGA NASIONAL BERHAD','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'TENAGANASIONAL_UPayMe'),
            array('id' => '11','user_id' => '251','company_registered_name' => 'UMOBILE','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'UMOBILE_UPayMe'),
            array('id' => '12','user_id' => '252','company_registered_name' => 'YES','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'YES_UPayMe'),
            array('id' => '13','user_id' => '253','company_registered_name' => 'WEBE DIGITAL SDN BHD','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'WEBEDIGITAL_UPayMe'),
            array('id' => '14','user_id' => '254','company_registered_name' => 'TOMO','business_registration_number' => '1','trading_name' => 'adgasdgas','headquarters_address' => 'adsgasdg','headquarters_contact_number' => 'sdgsdg','type_of_ownership' => '52','type_of_business' => '40','years_in_business' => '1','office_number' => '242341','fax_number' => '112341234','phone' => '112341234','salesperson_name' => 'asdfasdf','position' => '1','status_id' => '1','sub_merchant_ref' => 'TOMOHEATER_UPayMe'),

        );

        foreach ($merchants as $merchant) {
            $data_exists = Merchant::find($merchant['id']);
            if (!is_null($data_exists)) {
                Merchant::where('id', $merchant['id'])
                        ->update($merchant);  
            } else {
            	Merchant::create($merchant); 
            }
            
        }

        $this->command->info('merchant table seeded');


        $merchant_banks = array(
            array('id' => '1','merchant_id' => '1','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '2','merchant_id' => '2','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '3','merchant_id' => '3','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '4','merchant_id' => '4','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '5','merchant_id' => '5','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '6','merchant_id' => '6','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '7','merchant_id' => '7','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '8','merchant_id' => '8','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '9','merchant_id' => '9','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '10','merchant_id' => '10','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '11','merchant_id' => '11','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '12','merchant_id' => '12','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '13','merchant_id' => '13','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
            array('id' => '14','merchant_id' => '14','bank_name' => '60','acc_number' => '124233452345','acc_holder_name' => 'test','acc_finance_contact_number' => '4846464','acc_contact_number' => '852652684984','acc_email' => 'test@yahoo.com'),
        );

        foreach ($merchant_banks as $value) {
            $data_exists = MerchantBankAccount::find($value['id']);
            if (!is_null($data_exists)) {
                MerchantBankAccount::where('id', $value['id'])
                        ->update($value);  
            } else {
                MerchantBankAccount::create($value); 
            }
            
        }

        $this->command->info('Merchant Bank table seeded');


        $merchant_outlets = array(
            array('id' => '1','merchant_id' => '1','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '2','merchant_id' => '2','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '3','merchant_id' => '3','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '4','merchant_id' => '4','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '5','merchant_id' => '5','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '6','merchant_id' => '6','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '7','merchant_id' => '7','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '8','merchant_id' => '8','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '9','merchant_id' => '9','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '10','merchant_id' => '10','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '11','merchant_id' => '11','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '12','merchant_id' => '12','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '13','merchant_id' => '13','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
            array('id' => '14','merchant_id' => '14','outlet_pic_name' => 'test_picname','outlet_pic_id_num' => '65468544','outlet_pic_mobile_number' => '6516165','outlet_pic_email' => 'testing@yahoo.com','outlet_pic_username' => '451651651','outlet_pic_password' => '123123','outlet_address' => 'sadfasdfasdfasdf'),
        );

        foreach ($merchant_outlets as $value) {
            $data_exists = MerchantOutlet::find($value['id']);
            if (!is_null($data_exists)) {
                MerchantOutlet::where('id', $value['id'])
                        ->update($value);  
            } else {
                MerchantOutlet::create($value); 
            }
            
        }

        $this->command->info('Merchant Outlet table seeded');
    }
}
