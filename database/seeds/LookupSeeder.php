<?php

use Illuminate\Database\Seeder;
use App\Models\Lookup;

class LookupSeeder extends Seeder {
// php artisan db:seed --class=LookupSeeder
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $lookups = array(
            array('id' => '1', 'name' => 'Active', 'category' => 'General Status', 'status' => 'A', 'value' => null),
            array('id' => '2', 'name' => 'Disable', 'category' => 'General Status', 'status' => 'A', 'value' => null),
            array('id' => '3', 'name' => 'Active', 'category' => 'Register Status', 'status' => 'A', 'value' => null),
            array('id' => '4', 'name' => 'Disable', 'category' => 'Register Status', 'status' => 'A', 'value' => null),
            array('id' => '5', 'name' => 'Pending', 'category' => 'Register Status', 'status' => 'A', 'value' => null),
            array('id' => '6', 'name' => 'Complete', 'category' => 'Register Status', 'status' => 'A', 'value' => null),
            array('id' => '7', 'name' => 'Reject', 'category' => 'Register Status', 'status' => 'A', 'value' => null),
            array('id' => '8', 'name' => 'Male','category' => 'Gender', 'status' => 'A', 'value' => null),
            array('id' => '9', 'name' => 'Female','category' => 'Gender', 'status' => 'A', 'value' => null),
            array('id' => '10','name' => 'Both','category' => 'Gender', 'status' => 'D', 'value' => null),
            array('id' => '11','name' => 'smtp','category' => 'Mail Driver', 'status' => 'A', 'value' => null),
            array('id' => '12','name' => 'mailgun','category' => 'Mail Driver', 'status' => 'A', 'value' => null),
            array('id' => '13','name' => 'SSL','category' => 'Mail Encryption', 'status' => 'A', 'value' => null),
            array('id' => '14','name' => 'TLS','category' => 'Mail Encryption', 'status' => 'A', 'value' => null),
            array('id' => '15', 'name' => 'Activation', 'category' => 'Email Notification', 'status' => 'A', 'value' => null),
            array('id' => '16', 'name' => 'English', 'category' => 'Language', 'status' => 'A', 'value' => null),
            array('id' => '17', 'name' => 'Malay', 'category' => 'Language', 'status' => 'A', 'value' => null),
            array('id' => '18', 'name' => 'Home', 'category' => 'Address', 'status' => 'A', 'value' => null),
            array('id' => '19', 'name' => 'Mailing', 'category' => 'Address', 'status' => 'A', 'value' => null),
            array('id' => '20', 'name' => 'Pending', 'category' => 'Transaction Status', 'status' => 'A', 'value' => null),
            array('id' => '21', 'name' => 'Successful', 'category' => 'Transaction Status', 'status' => 'A', 'value' => null),
            array('id' => '22', 'name' => 'Rejected', 'category' => 'Transaction Status', 'status' => 'A', 'value' => null),
            array('id' => '23', 'name' => 'Cancelled', 'category' => 'Transaction Status', 'status' => 'A', 'value' => null),
            array('id' => '24', 'name' => 'Active', 'category' => 'Device Status', 'status' => 'A', 'value' => null),
            array('id' => '25', 'name' => 'Renewed', 'category' => 'Device Status', 'status' => 'A', 'value' => null),
            array('id' => '26', 'name' => 'Terminated', 'category' => 'Device Status', 'status' => 'A', 'value' => null),
            array('id' => '27', 'name' => 'Payment Status', 'category' => 'Email Notification', 'status' => 'A', 'value' => null),
            array('id' => '28', 'name' => 'Newsletter', 'category' => 'Email Notification', 'status' => 'A', 'value' => null),
            array('id' => '29', 'name' => 'Forgot Password', 'category' => 'Email Notification', 'status' => 'A', 'value' => null),

            array('id' => '30', 'name' => 'Accommodation/Hotel/B&B', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '31', 'name' => 'Apparel', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '32', 'name' => 'Automotive/Workshop', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '33', 'name' => 'Bag/Shoes/Watch/Accessories', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '34', 'name' => 'Beauty/Cosmetic/Saloon', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '35', 'name' => 'Book/Magazine/Stationery', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '36', 'name' => 'Café/Restaurant/Bar/Bakery', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '37', 'name' => 'Cleaning Service/Laundry/Tailor', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '38', 'name' => 'Convenient Store/Grocery', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '39', 'name' => 'Convenient Store/Grocery', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '40', 'name' => 'Education/University/College', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '41', 'name' => 'Electrical Appliances', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '42', 'name' => 'Electronic/Computer/Games/Software', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '43', 'name' => 'Entertainment/Cinema/Theme Park', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '44', 'name' => 'Gadget/Camera', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '45', 'name' => 'Health/Pharmacy/Clinic', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '46', 'name' => 'Music', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '47', 'name' => 'Sport/Fitness', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '48', 'name' => 'Telco', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '49', 'name' => 'Transport', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '50', 'name' => 'Travel Agency/Airline', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            array('id' => '51', 'name' => 'Others', 'category' => 'Type Of Business', 'status' => 'A', 'value' => null),
            
            array('id' => '52', 'name' => 'Sole Proprietor', 'category' => 'Type Of Ownership', 'status' => 'A', 'value' => null),
            array('id' => '53', 'name' => 'Partnership', 'category' => 'Type Of Ownership', 'status' => 'A', 'value' => null),
            array('id' => '54', 'name' => 'Corporation (Sdn Bhd/Bhd)', 'category' => 'Type Of Ownership', 'status' => 'A', 'value' => null),
            
            array('id' => '55', 'name' => 'Visa', 'category' => 'Convenience Charge Method', 'status' => 'A', 'value' => null),
            array('id' => '56', 'name' => 'MasterCard', 'category' => 'Convenience Charge Method', 'status' => 'A', 'value' => null),
            array('id' => '57', 'name' => 'FPX', 'category' => 'Convenience Charge Method', 'status' => 'A', 'value' => null),
            array('id' => '58', 'name' => 'Wallet', 'category' => 'Convenience Charge Method', 'status' => 'A', 'value' => null),
            array('id' => '59', 'name' => 'MDR', 'category' => 'Convenience Charge Method', 'status' => 'A', 'value' => null),

            array('id' => '60', 'name' => 'Maybank', 'category' => 'Bank Name', 'status' => 'A', 'value' => null),
            array('id' => '61', 'name' => 'CIMB', 'category' => 'Bank Name', 'status' => 'A', 'value' => null),
            
            array('id' => '62', 'name' => 'Text', 'category' => 'Data type', 'status' => 'A', 'value' => null),
            array('id' => '63', 'name' => 'Email', 'category' => 'Data type', 'status' => 'A', 'value' => null),
            array('id' => '64', 'name' => 'Phone', 'category' => 'Data type', 'status' => 'A', 'value' => null),
            array('id' => '65', 'name' => 'Image', 'category' => 'Data type', 'status' => 'A', 'value' => null),
            array('id' => '66', 'name' => 'Integer', 'category' => 'Data type', 'status' => 'A', 'value' => null),
            array('id' => '67', 'name' => 'Transaction Report', 'category' => 'Report Type', 'status' => 'A', 'value' => null),

            array('id' => '68', 'name' => 'Excel', 'category' => 'Download Type', 'status' => 'A', 'value' => null),
            array('id' => '69', 'name' => 'Alliance Bank', 'category' => 'Bank Name', 'status' => 'A', 'value' => null),
            array('id' => '70', 'name' => 'Public Bank', 'category' => 'Bank Name', 'status' => 'A', 'value' => null),
            array('id' => '71', 'name' => 'Business', 'category' => 'Type Id', 'status' => 'A', 'value' => null),
            array('id' => '72', 'name' => 'Individual', 'category' => 'Type Id', 'status' => 'A', 'value' => null),
            array('id' => '73', 'name' => 'Dropdown', 'category' => 'Data type', 'status' => 'A', 'value' => null),
            
        );

        foreach ($lookups as $lookup) {
            $data = array(
                'id'                => $lookup['id'],
                'name'              => $lookup['name'],
                'category'        => $lookup['category'],
                'status'            => $lookup['status'],
                'value'            => $lookup['value']
            );

            $data_exists = Lookup::find($lookup['id']);
            if (!is_null($data_exists)) {
                Lookup::where('id', $lookup['id'])
                        ->update($data); //UPDATE
            } else {
                Lookup::create($data); //CREATE
            }
        }

        $this->command->info('Lookups table seeded');
    }

}
