<?php

use Illuminate\Database\Seeder;
use App\Models\Project;

class ProjectSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

       $projects = array(
            array('id' => '1', 'name' => 'I-SERVE PROJECT', 'code' => 'ISERVE001', 'value' => '20000.00',  'profit' => 20, 'description' => 'bla bla bla bla bla bla bla bla', 'close' => '2018-09-20')
            );

        foreach ($projects as $project) {
            $data = array(
                'id'                => $project['id'],
                'name'              => $project['name'],
                'code'        => $project['code'],
                'value'            => $project['value'],
                'profit'            => $project['profit'],
                'description'            => $project['description'],
                'close'            => $project['close']
            );

            $data_exists = Project::find($project['id']);
            if (!is_null($data_exists)) {
                Project::where('id', $project['id'])
                        ->update($data); //UPDATE
            } else {
                Project::create($data); //CREATE
            }
        }

        $this->command->info('Project table seeded');
    }

}
