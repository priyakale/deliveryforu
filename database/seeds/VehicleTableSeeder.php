<?php

use Illuminate\Database\Seeder;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'type' => 'SUV',
                'Brand' => 'Honda'
            ],
            [
                'type' => 'Hatchback',
                'Brand' => 'Honda'
            ],
            [
                'type' => 'Sedan',
                'Brand' => 'Honda'
            ],
            [
                'type' => 'MPV',
                'Brand' => 'Honda'
            ],
            [
                'type' => 'Coupe',
                'Brand' => 'Honda'
            ],
            [
                'type' => 'Sedan',
                'Brand' => 'Toyota'
            ],
            [
                'type' => 'MPV',
                'Brand' => 'Toyota'
            ],
        ];

        collect($data)->each(function($datum) {
           \App\Models\Vehicle::create($datum);
        });
    }
}
