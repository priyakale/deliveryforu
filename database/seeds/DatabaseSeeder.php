<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LookupSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(StateSeeder::class);
        $this->call(NationalitySeeder::class);
        $this->call(EmailSettingSeeder::class);
        $this->call(EmailNotificationSeeder::class);
        $this->call(InpuFieldSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(VehicleTableSeeder::class);

        //$this->call(MerchantSeeder::class);
    }
}
