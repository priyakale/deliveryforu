<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Super Administrator',
            'description' => 'Super Administrator', 
            'status_id' => '1'
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Administrator',
            'description' => 'Administrator',
            'status_id' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Staff',
            'description' => 'Staff',
            'status_id' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'Agent',
            'description' => 'Agent',
            'status_id' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 5,
            'name' => 'Member',
            'description' => 'Member',
            'status_id' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 6,
            'name' => 'Merchant',
            'description' => 'Merchant',
            'status_id' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 7,
            'name' => 'Individual',
            'description' => 'Individual',
            'status_id' => 1
        ]);

        DB::table('roles')->insert([
            'id' => 8,
            'name' => 'Salesperson',
            'description' => 'Salesperson',
            'status_id' => 1
        ]);


        $this->command->info('Roles table seeded');

      	$controller_data  = array(
          'Admin\EmailSettingController',
          'Admin\AuditController',
          'Admin\MenuController',
          'Admin\NotificationController',
          'Admin\RoleController',
          'Admin\SettingController',
          'Admin\UserController',
          'Admin\ReportController',
          'Admin\TransactionController',
          'Admin\PaymentSettingController',
          'Admin\MerchantController',
          'Admin\InputFieldController',
          'Admin\ChargesController',
        );

        $roleaccessbility_data = array();
        foreach ($controller_data as $controller_value) {
          foreach (Route::getRoutes()->getRoutes() as $route){
              $prefix_value = $route->getPrefix();
              $prefix_value_sub = explode('/', $prefix_value);
              if($prefix_value_sub[0] == 'admin'){
                $action = $route->getAction();
                if (array_key_exists('controller', $action)){
                    if (strpos($action['controller'], $controller_value) !== false) {
                      $raw_data = explode('@', $action['controller']);
                      if(count($raw_data) > 1){
                          //get function
                          $roleaccessbility_data[] =array('role_id' => '1', 'controller' => $controller_value, 'function' => $raw_data[1]);
                      }
                    }
                }
              }
          }//end for route
        }
        DB::table('role_accessibilities')->insert($roleaccessbility_data);

        $this->command->info('Role access for role super admin table seeded');

        DB::table('role_accessibilities')->insert(array(
            //NOTIFICATION
            array('role_id'=>'2','controller'=>'NotificationController','function'=>'index'), 
            array('role_id'=>'2','controller'=>'NotificationController','function'=>'add'), 
            array('role_id'=>'2','controller'=>'NotificationController','function'=>'save'), 
            array('role_id'=>'2','controller'=>'NotificationController','function'=>'edit'), 
            array('role_id'=>'2','controller'=>'NotificationController','function'=>'delete'), 
            //END NOTIFICATION
        ));

		$this->command->info('Role access for role admin table seeded');

		DB::table('menus')->insert(array(
            array('name'=>'Setting','link_name'=>'','icon'=>'fas fa-cogs','parent_id'=>'0','status_id'=>1),
            array('name'=>'Menu','link_name'=>'index_menu','icon'=>'fa-bars','parent_id'=>'1','status_id'=>1), 
            array('name'=>'Role','link_name'=>'index_role','icon'=>'fa-lock','parent_id'=>'1','status_id'=>1), 
            array('name'=>'User','link_name'=>'index_user','icon'=>'fa-user','parent_id'=>'0','status_id'=>1),
            array('name'=>'Audit','link_name'=>'index_audit','icon'=>'fas fa-pencil-alt','parent_id'=>'0','status_id'=>1),
            array('name'=>'Project','link_name'=>'index_setting','icon'=>'fa-gear','parent_id'=>'1','status_id'=>1),
            array('name'=>'Email Template','link_name'=>'index_notification','icon'=>'fa-envelope','parent_id'=>'0','status_id'=>1),
            //array('name'=>'Package','link_name'=>'index_package','icon'=>'fa-list-ul','parent_id'=>'0','status_id'=>1),
            //array('name'=>'Commission','link_name'=>'index_commission','icon'=>'fa-list-ul','parent_id'=>'0','status_id'=>1),
            array('name'=>'Email','link_name'=>'index_email','icon'=>'fa-cog','parent_id'=>'1','status_id'=>1),
            //array('name'=>'Tier','link_name'=>'index_tier','icon'=>'fa-gear','parent_id'=>'0','status_id'=>1),
            array('name'=>'Transaction','link_name'=>'index_transaction','icon'=>'fa-money-bill-alt','parent_id'=>'0','status_id'=>1),
            //array('name'=>'User Device','link_name'=>'index_user_device','icon'=>'fa-mobile','parent_id'=>'0','status_id'=>1),
            array('name' => 'Report', 'link_name' => 'index_report', 'icon' => 'fa-file-alt', 'parent_id' => '0', 'status_id' => '1'),
            //array('name'=>'Blacklist','link_name'=>'','icon'=>'fa-ban','parent_id'=>'0','status_id'=>1),
            //array('name'=>'User List','link_name'=>'index_blacklist','icon'=>'fa-user','parent_id'=>'15','status_id'=>1),
            //array('name'=>'Blacklisted','link_name'=>'index_blacklist','icon'=>'fa-user','parent_id'=>'15','status_id'=>1),
            //array('name'=>'Dealer Application','link_name'=>'index_dealer','icon'=>'fa-user','parent_id'=>'0','status_id'=>1),
            
            array('name'=>'Upay Configuration','link_name'=>'index_payment_setting','icon'=>'fa-cog','parent_id'=>'1','status_id'=>1),

            array('name'=>'Merchants','link_name'=>'index_merchant','icon'=>'fa-user','parent_id'=>'0','status_id'=>1),
            
            array('name'=>'Input Fields Configuration','link_name'=>'index_input_field','icon'=>'fas fa-keyboard','parent_id'=>'0','status_id'=>1),

            array('name'=>'Charges','link_name'=>'index_charges','icon'=>'fa-money-bill-alt','parent_id'=>'0','status_id'=>1),

        ));

		$this->command->info('Menus table seeded');
        
        DB::table('role_menus')->insert(array(
            array('role_id'=>'1','menu_id'=>'4','position'=>'1'),
            array('role_id'=>'1','menu_id'=>'5','position'=>'5'),
            array('role_id'=>'1','menu_id'=>'7','position'=>'6'),
            array('role_id'=>'1','menu_id'=>'9','position'=>'10'),
            array('role_id'=>'1','menu_id'=>'10','position'=>'11'),
            array('role_id'=>'1','menu_id'=>'2','position'=>'13'),
            array('role_id'=>'1','menu_id'=>'3','position'=>'14'),
            array('role_id'=>'1','menu_id'=>'6','position'=>'15'),
            array('role_id'=>'1','menu_id'=>'8','position'=>'16'),
            array('role_id'=>'1','menu_id'=>'12','position'=>'2'),
            array('role_id'=>'1','menu_id'=>'13','position'=>'3'),
            array('role_id'=>'1','menu_id'=>'14','position'=>'3'),
        ));

        DB::table('role_menus')->insert(array(
            array('role_id'=>'2','menu_id'=>'3','position'=>'1'),
            array('role_id'=>'2','menu_id'=>'7','position'=>'2')
        ));

        $this->command->info('Role menus table seeded');

        DB::table('users')->insert([
            'name' => 'SuperAdmin',
            'email' => 'superadmin@dev.com',
            'password' => bcrypt('123123'),
            'status_id' => 1,
            'role_id' => 1,
            'activated' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@dev.com',
            'password' => bcrypt('123123'),
            'status_id' => 1,
            'role_id' => 2,
            'activated' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'Allan Goh',
            'email' => 'allangoh8@gmail.com',
            'password' => bcrypt('123123'),
            'status_id' => 1,
            'role_id' => 3,
            'activated' => 1,
            'referral_code' => 'AS12345678'
        ]);

        DB::table('users')->insert([
            'name' => 'Crystal',
            'email' => 'chingsiew@i-serve.com.my',
            'password' => bcrypt('123123'),
            'status_id' => 1,
            'role_id' => 4,
            'activated' => 1,
            'referral_code' => 'MC12345678'
        ]);

        DB::table('users')->insert([
            'name' => 'Niban',
            'email' => 'thacayaniban@i-serve.com.my',
            'password' => bcrypt('123123'),
            'status_id' => 1,
            'role_id' => 5,
            'activated' => 1,
            'referral_code' => 'MBCISR00000001',
            'referral_id' => 4
        ]);       

        $this->command->info('Users table seeded');       
    }
}
