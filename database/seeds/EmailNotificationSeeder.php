<?php

use Illuminate\Database\Seeder;
use App\Models\EmailNotification;

class EmailNotificationSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $email = array(
            array(
                'id' => 1
                , 'subject' => 'UPayMe (Payment Successful)'
                , 'description' => '<p>Dear Valued Member,</p><p><br></p><p>Thank you for your payment via UPayMe mobile application.</p>'
                . '<p>We are pleased to confirm that we have received your payment information. The details are as follows:</p>'
                . '<p><br></p>'
                . '</p><p><br></p>'
                . '<table class="table table-bordered"><tbody>'
                . '<tr><td>Order Id: </td><td>%ORDERID</td></tr>'
                . '<tr><td>Payment Type: </td><td>%PAYMENTTYPE</td></tr>'
                . '<tr><td>Service Type: </td><td>%SERVICETYPE</td></tr>'
                . '<tr><td>Name: </td><td>%NAME</td></tr>'
                . '<tr><td>Phone No.: </td><td>%PHONE</td></tr>'
                . '<tr><td>Total Paid: </td><td>%PAID</td></tr>'
                . '<tr><td>Payment Date/Time: </td><td>%DATETIME</td></tr>'
                . '<tr><td>Remarks: </td><td>%REMARKS</td></tr>'
                . '</tbody></table>'
                . '<p><br></p>'
                . '<p>Thank you,<br>I-Serve Technology & Vacations Sdn. Bhd.</p>'
                , 'email_for_id' => 27
                , 'status_id' => 1
            ), 
             array(
                'id' => 2
                , 'subject' => 'UPayMe (Payment Successful)'
                , 'description' => '<p>Dear Admin,</p><p><br></p>'
                . '<p>Payment is <b>Successful!</b> The details are as follows:</p><p><br></p>'
                . '</p><p><br></p>'
                . '<table class="table table-bordered"><tbody>'
                . '<tr><td>Order Id: </td><td>%ORDERID</td></tr>'
                . '<tr><td>Payment Type: </td><td>%PAYMENTTYPE</td></tr>'
                . '<tr><td>Service Type: </td><td>%SERVICETYPE</td></tr>'
                . '<tr><td>Name: </td><td>%NAME</td></tr>'
                . '<tr><td>Phone No.: </td><td>%PHONE</td></tr>'
                . '<tr><td>Total Paid: </td><td>%PAID</td></tr>'
                . '<tr><td>Payment Date/Time: </td><td>%DATETIME</td></tr>'
                . '<tr><td>Remarks: </td><td>%REMARKS</td></tr>'
                . '</tbody></table>'
                . '<p><br></p>'
                . '<p>Thank you,<br>I-Serve Technology & Vacations Sdn. Bhd.</p>'
                , 'email_for_id' => 28
                , 'status_id' => 1
            ), 
             array(
                'id' => 3
                , 'subject' => 'UPayMe Reset Password'
                , 'description' => '<p>Welcome %NAME,</p><p>You are receiving this email because we received a password reset request for your account.</p><p><br></p><p>Please reset password your UPayMe account by clicking the button below.</p><p><br></p><p>%BUTTON</p><p><br></p><p>Please note that this link will expire in 7 day.</p><p>If you did not request a password reset, no further action is required.</p><p><br></p><p>Thank you,</p><p>Your UPayMe Team</p>'
                , 'email_for_id' => 29
                , 'status_id' => 1
            ),  
           
             
        );

        ///SEED ENAIL NOTIFIACTIONS////
        foreach ($email as $email_seed) {

            $data = array(
                'id' => $email_seed['id'],
                'subject' => $email_seed['subject'],
                'description' => $email_seed['description'],
                'email_for_id' => $email_seed['email_for_id'],
                'status_id' => $email_seed['status_id'],
            );

            $data_exists = EmailNotification::find($email_seed['id']);
            if (!is_null($data_exists)) {
                EmailNotification::where('id', $email_seed['id'])
                        ->update($data); //UPDATE
            } else {
                EmailNotification::create($data); //CREATE
            }
        }


        $this->command->info('Email notification seeded');
    }

}
