<?php

use Illuminate\Database\Seeder;
use App\Models\InputField;

class InpuFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $inputFields = array(
            array( 'id' => '1', 'name' => 'Account Number',  'data_type_id' => '62',   'status_id' => 1 ),
            array( 'id' => '2', 'name' => 'Email',  'data_type_id' => '63',   'status_id' => 1 ),
            array( 'id' => '3', 'name' => 'Phone',  'data_type_id' => '64',   'status_id' => 1 ),
            array( 'id' => '4', 'name' => 'Password',  'data_type_id' => '65',   'status_id' => 1 ),
            array( 'id' => '5', 'name' => 'image',  'data_type_id' => '66',   'status_id' => 1 ),
        );

        foreach ($inputFields as $inputField) {



            $data_exists = InputField::find($inputField['id']);
            if (!is_null($data_exists)) {
                InputField::where('id', $inputField['id'])
                        ->update($inputField);  
            } else {
            	InputField::create($inputField); 
            }
            
        }

        $this->command->info('InputField table seeded');
    }
}