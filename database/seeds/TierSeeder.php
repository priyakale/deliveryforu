<?php

use Illuminate\Database\Seeder;
use App\Models\Tier;

class TierSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

       $tiers = array(
            array('id' => '1', 'name' => 'Tier 1', 'description' => '0-500', 'status_id' => 1, 'swap_fee' => 75.00),
            array('id' => '2', 'name' => 'Tier 2', 'description' => '501-1000', 'status_id' => 1, 'swap_fee' => 150.00),
            array('id' => '3', 'name' => 'Tier 3', 'description' => '1001-1500', 'status_id' => 1, 'swap_fee' => 210.00),
            array('id' => '4', 'name' => 'Tier 4', 'description' => '1501-3000', 'status_id' => 1, 'swap_fee' => 450.00),
            array('id' => '5', 'name' => 'Tier 5', 'description' => '>3000', 'status_id' => 1, 'swap_fee' => 700.00)
        );

        foreach ($tiers as $data) {
            $data = array(
                'id' => $data['id']
                ,'name' => $data['name']
                ,'description' => $data['description']
                ,'status_id' => $data['status_id']
                ,'swap_fee' => $data['swap_fee']
            );

            $data_exists = Tier::find($data['id']);
            if (!is_null($data_exists)) {
                Tier::where('id', $data['id'])
                        ->update($data); //UPDATE
            } else {
                Tier::create($data); //CREATE
            }
        }

        $this->command->info('Tiers table seeded');
    }

}
