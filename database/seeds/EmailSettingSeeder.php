<?php

use Illuminate\Database\Seeder;
use App\Models\EmailSetting;

class EmailSettingSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        $email_settings = array(
            array('id' => 1, 'name' => 'UPayMe', 'driver_id' => 11, 'smtp_host' => 'smtp.gmail.com', 'smtp_username' => 'noreplyupaycom@gmail.com', 'smtp_password' => 'ysejozvbnmkqgsby', 'port' => 587, 'encryption_id' => 14, 'sendmail_path' => '', 'mailgun_domain' => '', 'mailgun_secret' => ''),
            );
        
        ///SEED EMAIL SETTINGS////
        foreach ($email_settings as $email_setting) {
          
            $data = array(
                'id'                => $email_setting['id'],
                'name'              => $email_setting['name'],
                'driver_id'        => $email_setting['driver_id'],
                'smtp_host'            => $email_setting['smtp_host'],
                'smtp_username'   => $email_setting['smtp_username'],
                'smtp_password'   => $email_setting['smtp_password'],
                'port'   => $email_setting['port'],
                'encryption_id'   => $email_setting['encryption_id'],
                'sendmail_path'   => $email_setting['sendmail_path'],
                'mailgun_domain'   => $email_setting['mailgun_domain'],
                'mailgun_secret'   => $email_setting['mailgun_secret'],
            );

            $data_exists = EmailSetting::find($email_setting['id']);
            if (!is_null($data_exists)) {
                EmailSetting::where('id', $email_setting['id'])
                        ->update($data); //UPDATE
            } else {
                EmailSetting::create($data); //CREATE
            }
        }
        
        $this->command->info('Email setting seeded');

        
    }

}
