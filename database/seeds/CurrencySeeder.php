<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $currencies = array(
            array( 'id'=>'1', 'icon'=>'myr.png', 'name'=>'Malaysian Ringgit', 'code'=>'MYR', 'symbols'=>'RM', 'status_id' => 1 ),
            array( 'id'=>'2', 'icon'=>'usd.png', 'name'=>'US Dollar', 'code'=>'USD', 'symbols'=>'$', 'status_id' => 1 ),
            array( 'id'=>'3', 'icon'=>'eur.png', 'name'=>'Euro', 'code'=>'EUR', 'symbols'=>'€', 'status_id' => 1 ),
            array( 'id'=>'4', 'icon'=>'gbp.png', 'name'=>'British Pound', 'code'=>'GBP', 'symbols'=>'£', 'status_id' => 1 ),
            array( 'id'=>'5', 'icon'=>'sgd.png', 'name'=>'Singapore Dollar', 'code'=>'SGD', 'symbols'=>'$', 'status_id' => 1 ),
            array( 'id'=>'6', 'icon'=>'aud.png', 'name'=>'Australian Dollar', 'code'=>'AUD', 'symbols'=>'$', 'status_id' => 1 ),
        );

        foreach ($currencies as $data) {

            $data_exists = Currency::find($data['id']);
            if (!is_null($data_exists)) {
                Currency::where('id', $data['id'])
                        ->update($data);  
            } else {
            	Currency::create($data); 
            }            
        }

        $this->command->info('Currency table seeded');
    }
}