<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountrySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {



        $countries = array(
            array('id' => '1', 'code' => 'AF', 'name' => 'Afghanistan', 'phonecode' => '93', 'code_A3' => 'AFG', 'code_N3' => '004', 'region' => 'AS', 'lat' => '33.00', 'lon' => '65.00'),
            array('id' => '2', 'code' => 'AL', 'name' => 'Albania', 'phonecode' => '355', 'code_A3' => 'ALB', 'code_N3' => '008', 'region' => 'EU', 'lat' => '41.00', 'lon' => '20.00'),
            array('id' => '3', 'code' => 'DZ', 'name' => 'Algeria', 'phonecode' => '213', 'code_A3' => 'DZA', 'code_N3' => '012', 'region' => 'AF', 'lat' => '28.00', 'lon' => '3.00'),
            array('id' => '4', 'code' => 'AS', 'name' => 'American Samoa', 'phonecode' => '1684', 'code_A3' => 'ASM', 'code_N3' => '016', 'region' => 'AU', 'lat' => '-14.20', 'lon' => '-170.00'),
            array('id' => '5', 'code' => 'AD', 'name' => 'Andorra', 'phonecode' => '376', 'code_A3' => 'AND', 'code_N3' => '020', 'region' => 'EU', 'lat' => '42.30', 'lon' => '1.30'),
            array('id' => '6', 'code' => 'AO', 'name' => 'Angola', 'phonecode' => '244', 'code_A3' => 'AGO', 'code_N3' => '024', 'region' => 'AF', 'lat' => '-12.30', 'lon' => '18.30'),
            array('id' => '7', 'code' => 'AI', 'name' => 'Anguilla', 'phonecode' => '1264', 'code_A3' => 'AIA', 'code_N3' => '660', 'region' => 'LA', 'lat' => '18.15', 'lon' => '-63.10'),
            array('id' => '8', 'code' => 'AQ', 'name' => 'Antarctica', 'phonecode' => '0', 'code_A3' => 'ATA', 'code_N3' => '010', 'region' => 'AN', 'lat' => '-90.00', 'lon' => '0.00'),
            array('id' => '9', 'code' => 'AG', 'name' => 'Antigua And Barbuda', 'phonecode' => '1268', 'code_A3' => 'ATG', 'code_N3' => '028', 'region' => 'LA', 'lat' => '17.03', 'lon' => '-61.48'),
            array('id' => '10', 'code' => 'AR', 'name' => 'Argentina', 'phonecode' => '54', 'code_A3' => 'ARG', 'code_N3' => '032', 'region' => 'LA', 'lat' => '-34.00', 'lon' => '-64.00'),
            array('id' => '11', 'code' => 'AM', 'name' => 'Armenia', 'phonecode' => '374', 'code_A3' => 'ARM', 'code_N3' => '051', 'region' => 'AS', 'lat' => '40.00', 'lon' => '45.00'),
            array('id' => '12', 'code' => 'AW', 'name' => 'Aruba', 'phonecode' => '297', 'code_A3' => 'ABW', 'code_N3' => '533', 'region' => 'LA', 'lat' => '12.30', 'lon' => '-69.58'),
            array('id' => '13', 'code' => 'AU', 'name' => 'Australia', 'phonecode' => '61', 'code_A3' => 'AUS', 'code_N3' => '036', 'region' => 'AU', 'lat' => '-27.00', 'lon' => '133.00'),
            array('id' => '14', 'code' => 'AT', 'name' => 'Austria', 'phonecode' => '43', 'code_A3' => 'AUT', 'code_N3' => '040', 'region' => 'EU', 'lat' => '47.20', 'lon' => '13.20'),
            array('id' => '15', 'code' => 'AZ', 'name' => 'Azerbaijan', 'phonecode' => '994', 'code_A3' => 'AZE', 'code_N3' => '031', 'region' => 'AS', 'lat' => '40.30', 'lon' => '47.30'),
            array('id' => '16', 'code' => 'BS', 'name' => 'Bahamas The', 'phonecode' => '1242', 'code_A3' => 'BHS', 'code_N3' => '044', 'region' => 'LA', 'lat' => '24.15', 'lon' => '-76.00'),
            array('id' => '17', 'code' => 'BH', 'name' => 'Bahrain', 'phonecode' => '973', 'code_A3' => 'BHR', 'code_N3' => '048', 'region' => 'AS', 'lat' => '26.00', 'lon' => '50.33'),
            array('id' => '18', 'code' => 'BD', 'name' => 'Bangladesh', 'phonecode' => '880', 'code_A3' => 'BGD', 'code_N3' => '050', 'region' => 'AS', 'lat' => '24.00', 'lon' => '90.00'),
            array('id' => '19', 'code' => 'BB', 'name' => 'Barbados', 'phonecode' => '1246', 'code_A3' => 'BRB', 'code_N3' => '052', 'region' => 'LA', 'lat' => '13.10', 'lon' => '-59.32'),
            array('id' => '20', 'code' => 'BY', 'name' => 'Belarus', 'phonecode' => '375', 'code_A3' => 'BLR', 'code_N3' => '112', 'region' => 'EU', 'lat' => '53.00', 'lon' => '28.00'),
            array('id' => '21', 'code' => 'BE', 'name' => 'Belgium', 'phonecode' => '32', 'code_A3' => 'BEL', 'code_N3' => '056', 'region' => 'EU', 'lat' => '50.50', 'lon' => '4.00'),
            array('id' => '22', 'code' => 'BZ', 'name' => 'Belize', 'phonecode' => '501', 'code_A3' => 'BLZ', 'code_N3' => '084', 'region' => 'LA', 'lat' => '17.15', 'lon' => '-88.45'),
            array('id' => '23', 'code' => 'BJ', 'name' => 'Benin', 'phonecode' => '229', 'code_A3' => 'BEN', 'code_N3' => '204', 'region' => 'AF', 'lat' => '9.30', 'lon' => '2.15'),
            array('id' => '24', 'code' => 'BM', 'name' => 'Bermuda', 'phonecode' => '1441', 'code_A3' => 'BMU', 'code_N3' => '060', 'region' => 'LA', 'lat' => '32.20', 'lon' => '-64.45'),
            array('id' => '25', 'code' => 'BT', 'name' => 'Bhutan', 'phonecode' => '975', 'code_A3' => 'BTN', 'code_N3' => '064', 'region' => 'AS', 'lat' => '27.30', 'lon' => '90.30'),
            array('id' => '26', 'code' => 'BO', 'name' => 'Bolivia', 'phonecode' => '591', 'code_A3' => 'BOL', 'code_N3' => '068', 'region' => 'LA', 'lat' => '-17.00', 'lon' => '-65.00'),
            array('id' => '27', 'code' => 'BA', 'name' => 'Bosnia and Herzegovina', 'phonecode' => '387', 'code_A3' => 'BIH', 'code_N3' => '070', 'region' => 'EU', 'lat' => '44.00', 'lon' => '18.00'),
            array('id' => '28', 'code' => 'BW', 'name' => 'Botswana', 'phonecode' => '267', 'code_A3' => 'BWA', 'code_N3' => '072', 'region' => 'AF', 'lat' => '-22.00', 'lon' => '24.00'),
            array('id' => '29', 'code' => 'BV', 'name' => 'Bouvet Island', 'phonecode' => '0', 'code_A3' => 'BVT', 'code_N3' => '074', 'region' => 'AN', 'lat' => '-54.26', 'lon' => '3.24'),
            array('id' => '30', 'code' => 'BR', 'name' => 'Brazil', 'phonecode' => '55', 'code_A3' => 'BRA', 'code_N3' => '076', 'region' => 'LA', 'lat' => '-10.00', 'lon' => '-55.00'),
            array('id' => '31', 'code' => 'IO', 'name' => 'British Indian Ocean Territory', 'phonecode' => '246', 'code_A3' => 'IOT', 'code_N3' => '086', 'region' => 'AS', 'lat' => '-6.00', 'lon' => '71.30'),
            array('id' => '32', 'code' => 'BN', 'name' => 'Brunei', 'phonecode' => '673', 'code_A3' => 'BRN', 'code_N3' => '096', 'region' => 'AS', 'lat' => '4.30', 'lon' => '114.40'),
            array('id' => '33', 'code' => 'BG', 'name' => 'Bulgaria', 'phonecode' => '359', 'code_A3' => 'BGR', 'code_N3' => '100', 'region' => 'EU', 'lat' => '43.00', 'lon' => '25.00'),
            array('id' => '34', 'code' => 'BF', 'name' => 'Burkina Faso', 'phonecode' => '226', 'code_A3' => 'BFA', 'code_N3' => '854', 'region' => 'AF', 'lat' => '13.00', 'lon' => '-2.00'),
            array('id' => '35', 'code' => 'BI', 'name' => 'Burundi', 'phonecode' => '257', 'code_A3' => 'BDI', 'code_N3' => '108', 'region' => 'AF', 'lat' => '-3.30', 'lon' => '30.00'),
            array('id' => '36', 'code' => 'KH', 'name' => 'Cambodia', 'phonecode' => '855', 'code_A3' => 'KHM', 'code_N3' => '116', 'region' => 'AS', 'lat' => '13.00', 'lon' => '105.00'),
            array('id' => '37', 'code' => 'CM', 'name' => 'Cameroon', 'phonecode' => '237', 'code_A3' => 'CMR', 'code_N3' => '120', 'region' => 'AF', 'lat' => '6.00', 'lon' => '12.00'),
            array('id' => '38', 'code' => 'CA', 'name' => 'Canada', 'phonecode' => '1', 'code_A3' => 'CAN', 'code_N3' => '124', 'region' => 'NA', 'lat' => '60.00', 'lon' => '-95.00'),
            array('id' => '39', 'code' => 'CV', 'name' => 'Cape Verde', 'phonecode' => '238', 'code_A3' => 'CPV', 'code_N3' => '132', 'region' => 'AF', 'lat' => '16.00', 'lon' => '-24.00'),
            array('id' => '40', 'code' => 'KY', 'name' => 'Cayman Islands', 'phonecode' => '1345', 'code_A3' => 'CYM', 'code_N3' => '136', 'region' => 'LA', 'lat' => '19.30', 'lon' => '-80.30'),
            array('id' => '41', 'code' => 'CF', 'name' => 'Central African Republic', 'phonecode' => '236', 'code_A3' => 'CAF', 'code_N3' => '140', 'region' => 'AF', 'lat' => '7.00', 'lon' => '21.00'),
            array('id' => '42', 'code' => 'TD', 'name' => 'Chad', 'phonecode' => '235', 'code_A3' => 'TCD', 'code_N3' => '148', 'region' => 'AF', 'lat' => '15.00', 'lon' => '19.00'),
            array('id' => '43', 'code' => 'CL', 'name' => 'Chile', 'phonecode' => '56', 'code_A3' => 'CHL', 'code_N3' => '152', 'region' => 'LA', 'lat' => '-30.00', 'lon' => '-71.00'),
            array('id' => '44', 'code' => 'CN', 'name' => 'China', 'phonecode' => '86', 'code_A3' => 'CHN', 'code_N3' => '156', 'region' => 'AS', 'lat' => '35.00', 'lon' => '105.00'),
            array('id' => '45', 'code' => 'CX', 'name' => 'Christmas Island', 'phonecode' => '61', 'code_A3' => 'CXR', 'code_N3' => '162', 'region' => 'AU', 'lat' => '-10.30', 'lon' => '105.40'),
            array('id' => '46', 'code' => 'CC', 'name' => 'Cocos (Keeling) Islands', 'phonecode' => '672', 'code_A3' => 'CCK', 'code_N3' => '166', 'region' => 'AU', 'lat' => '-12.30', 'lon' => '96.50'),
            array('id' => '47', 'code' => 'CO', 'name' => 'Colombia', 'phonecode' => '57', 'code_A3' => 'COL', 'code_N3' => '170', 'region' => 'LA', 'lat' => '4.00', 'lon' => '-72.00'),
            array('id' => '48', 'code' => 'KM', 'name' => 'Comoros', 'phonecode' => '269', 'code_A3' => 'COM', 'code_N3' => '174', 'region' => 'AF', 'lat' => '-12.10', 'lon' => '44.15'),
            array('id' => '49', 'code' => 'CG', 'name' => 'Republic Of The Congo', 'phonecode' => '242', 'code_A3' => 'COG', 'code_N3' => '178', 'region' => 'AF', 'lat' => '0.00', 'lon' => '25.00'),
            array('id' => '50', 'code' => 'CD', 'name' => 'Democratic Republic Of The Congo', 'phonecode' => '242', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '51', 'code' => 'CK', 'name' => 'Cook Islands', 'phonecode' => '682', 'code_A3' => 'COK', 'code_N3' => '184', 'region' => 'AU', 'lat' => '-21.14', 'lon' => '-159.46'),
            array('id' => '52', 'code' => 'CR', 'name' => 'Costa Rica', 'phonecode' => '506', 'code_A3' => 'CRI', 'code_N3' => '188', 'region' => 'LA', 'lat' => '10.00', 'lon' => '-84.00'),
            array('id' => '53', 'code' => 'CI', 'name' => 'Cote D\'Ivoire (Ivory Coast)', 'phonecode' => '225', 'code_A3' => 'CIV', 'code_N3' => '384', 'region' => 'AF', 'lat' => '7.64', 'lon' => '-4.93'),
            array('id' => '54', 'code' => 'HR', 'name' => 'Croatia (Hrvatska)', 'phonecode' => '385', 'code_A3' => 'HRV', 'code_N3' => '191', 'region' => 'EU', 'lat' => '45.10', 'lon' => '15.30'),
            array('id' => '55', 'code' => 'CU', 'name' => 'Cuba', 'phonecode' => '53', 'code_A3' => 'CUB', 'code_N3' => '192', 'region' => 'LA', 'lat' => '21.30', 'lon' => '-80.00'),
            array('id' => '56', 'code' => 'CY', 'name' => 'Cyprus', 'phonecode' => '357', 'code_A3' => 'CYP', 'code_N3' => '196', 'region' => 'EU', 'lat' => '35.00', 'lon' => '33.00'),
            array('id' => '57', 'code' => 'CZ', 'name' => 'Czech Republic', 'phonecode' => '420', 'code_A3' => 'CZE', 'code_N3' => '203', 'region' => 'EU', 'lat' => '49.45', 'lon' => '15.30'),
            array('id' => '58', 'code' => 'DK', 'name' => 'Denmark', 'phonecode' => '45', 'code_A3' => 'DNK', 'code_N3' => '208', 'region' => 'EU', 'lat' => '56.00', 'lon' => '10.00'),
            array('id' => '59', 'code' => 'DJ', 'name' => 'Djibouti', 'phonecode' => '253', 'code_A3' => 'DJI', 'code_N3' => '262', 'region' => 'AF', 'lat' => '11.30', 'lon' => '43.00'),
            array('id' => '60', 'code' => 'DM', 'name' => 'Dominica', 'phonecode' => '1767', 'code_A3' => 'DMA', 'code_N3' => '212', 'region' => 'LA', 'lat' => '15.25', 'lon' => '-61.20'),
            array('id' => '61', 'code' => 'DO', 'name' => 'Dominican Republic', 'phonecode' => '1809', 'code_A3' => 'DOM', 'code_N3' => '214', 'region' => 'LA', 'lat' => '19.00', 'lon' => '-70.40'),
            array('id' => '62', 'code' => 'TP', 'name' => 'East Timor', 'phonecode' => '670', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '63', 'code' => 'EC', 'name' => 'Ecuador', 'phonecode' => '593', 'code_A3' => 'ECU', 'code_N3' => '218', 'region' => 'LA', 'lat' => '-2.00', 'lon' => '-77.30'),
            array('id' => '64', 'code' => 'EG', 'name' => 'Egypt', 'phonecode' => '20', 'code_A3' => 'EGY', 'code_N3' => '818', 'region' => 'AF', 'lat' => '27.00', 'lon' => '30.00'),
            array('id' => '65', 'code' => 'SV', 'name' => 'El Salvador', 'phonecode' => '503', 'code_A3' => 'SLV', 'code_N3' => '222', 'region' => 'LA', 'lat' => '13.50', 'lon' => '-88.55'),
            array('id' => '66', 'code' => 'GQ', 'name' => 'Equatorial Guinea', 'phonecode' => '240', 'code_A3' => 'GNQ', 'code_N3' => '226', 'region' => 'AF', 'lat' => '2.00', 'lon' => '10.00'),
            array('id' => '67', 'code' => 'ER', 'name' => 'Eritrea', 'phonecode' => '291', 'code_A3' => 'ERI', 'code_N3' => '232', 'region' => 'AF', 'lat' => '15.00', 'lon' => '39.00'),
            array('id' => '68', 'code' => 'EE', 'name' => 'Estonia', 'phonecode' => '372', 'code_A3' => 'EST', 'code_N3' => '233', 'region' => 'EU', 'lat' => '59.00', 'lon' => '26.00'),
            array('id' => '69', 'code' => 'ET', 'name' => 'Ethiopia', 'phonecode' => '251', 'code_A3' => 'ETH', 'code_N3' => '210', 'region' => 'AF', 'lat' => '8.00', 'lon' => '38.00'),
            array('id' => '70', 'code' => 'XA', 'name' => 'External Territories of Australia', 'phonecode' => '61', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '71', 'code' => 'FK', 'name' => 'Falkland Islands', 'phonecode' => '500', 'code_A3' => 'FLK', 'code_N3' => '238', 'region' => 'LA', 'lat' => '-51.45', 'lon' => '-59.00'),
            array('id' => '72', 'code' => 'FO', 'name' => 'Faroe Islands', 'phonecode' => '298', 'code_A3' => 'FRO', 'code_N3' => '234', 'region' => 'EU', 'lat' => '62.00', 'lon' => '-7.00'),
            array('id' => '73', 'code' => 'FJ', 'name' => 'Fiji Islands', 'phonecode' => '679', 'code_A3' => 'FJI', 'code_N3' => '242', 'region' => 'AU', 'lat' => '-18.00', 'lon' => '175.00'),
            array('id' => '74', 'code' => 'FI', 'name' => 'Finland', 'phonecode' => '358', 'code_A3' => 'FIN', 'code_N3' => '246', 'region' => 'EU', 'lat' => '64.00', 'lon' => '26.00'),
            array('id' => '75', 'code' => 'FR', 'name' => 'France', 'phonecode' => '33', 'code_A3' => 'FRA', 'code_N3' => '250', 'region' => 'EU', 'lat' => '46.00', 'lon' => '2.00'),
            array('id' => '76', 'code' => 'GF', 'name' => 'French Guiana', 'phonecode' => '594', 'code_A3' => 'GUF', 'code_N3' => '254', 'region' => 'LA', 'lat' => '4.00', 'lon' => '-53.00'),
            array('id' => '77', 'code' => 'PF', 'name' => 'French Polynesia', 'phonecode' => '689', 'code_A3' => 'PYF', 'code_N3' => '258', 'region' => 'AU', 'lat' => '-15.00', 'lon' => '-140.00'),
            array('id' => '78', 'code' => 'TF', 'name' => 'French Southern Territories', 'phonecode' => '0', 'code_A3' => 'ATF', 'code_N3' => '260', 'region' => 'AN', 'lat' => '-43.00', 'lon' => '67.00'),
            array('id' => '79', 'code' => 'GA', 'name' => 'Gabon', 'phonecode' => '241', 'code_A3' => 'GAB', 'code_N3' => '266', 'region' => 'AF', 'lat' => '-1.00', 'lon' => '11.45'),
            array('id' => '80', 'code' => 'GM', 'name' => 'Gambia The', 'phonecode' => '220', 'code_A3' => 'GMB', 'code_N3' => '270', 'region' => 'AF', 'lat' => '13.28', 'lon' => '-16.34'),
            array('id' => '81', 'code' => 'GE', 'name' => 'Georgia', 'phonecode' => '995', 'code_A3' => 'GEO', 'code_N3' => '268', 'region' => 'AS', 'lat' => '42.00', 'lon' => '43.30'),
            array('id' => '82', 'code' => 'DE', 'name' => 'Germany', 'phonecode' => '49', 'code_A3' => 'DEU', 'code_N3' => '276', 'region' => 'EU', 'lat' => '51.00', 'lon' => '9.00'),
            array('id' => '83', 'code' => 'GH', 'name' => 'Ghana', 'phonecode' => '233', 'code_A3' => 'GHA', 'code_N3' => '288', 'region' => 'AF', 'lat' => '8.00', 'lon' => '-2.00'),
            array('id' => '84', 'code' => 'GI', 'name' => 'Gibraltar', 'phonecode' => '350', 'code_A3' => 'GIB', 'code_N3' => '292', 'region' => 'EU', 'lat' => '36.80', 'lon' => '-5.21'),
            array('id' => '85', 'code' => 'GR', 'name' => 'Greece', 'phonecode' => '30', 'code_A3' => 'GRC', 'code_N3' => '300', 'region' => 'EU', 'lat' => '39.00', 'lon' => '22.00'),
            array('id' => '86', 'code' => 'GL', 'name' => 'Greenland', 'phonecode' => '299', 'code_A3' => 'GRL', 'code_N3' => '304', 'region' => 'NA', 'lat' => '72.00', 'lon' => '-40.00'),
            array('id' => '87', 'code' => 'GD', 'name' => 'Grenada', 'phonecode' => '1473', 'code_A3' => 'GRD', 'code_N3' => '308', 'region' => 'LA', 'lat' => '12.07', 'lon' => '-61.40'),
            array('id' => '88', 'code' => 'GP', 'name' => 'Guadeloupe', 'phonecode' => '590', 'code_A3' => 'GLP', 'code_N3' => '312', 'region' => 'LA', 'lat' => '16.15', 'lon' => '-61.35'),
            array('id' => '89', 'code' => 'GU', 'name' => 'Guam', 'phonecode' => '1671', 'code_A3' => 'GUM', 'code_N3' => '316', 'region' => 'AU', 'lat' => '13.28', 'lon' => '144.47'),
            array('id' => '90', 'code' => 'GT', 'name' => 'Guatemala', 'phonecode' => '502', 'code_A3' => 'GTM', 'code_N3' => '320', 'region' => 'LA', 'lat' => '15.30', 'lon' => '-90.15'),
            array('id' => '91', 'code' => 'XU', 'name' => 'Guernsey and Alderney', 'phonecode' => '44', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '92', 'code' => 'GN', 'name' => 'Guinea', 'phonecode' => '224', 'code_A3' => 'GIN', 'code_N3' => '324', 'region' => 'AF', 'lat' => '11.00', 'lon' => '-10.00'),
            array('id' => '93', 'code' => 'GW', 'name' => 'Guinea-Bissau', 'phonecode' => '245', 'code_A3' => 'GNB', 'code_N3' => '624', 'region' => 'AF', 'lat' => '12.00', 'lon' => '-15.00'),
            array('id' => '94', 'code' => 'GY', 'name' => 'Guyana', 'phonecode' => '592', 'code_A3' => 'GUY', 'code_N3' => '328', 'region' => 'LA', 'lat' => '5.00', 'lon' => '-59.00'),
            array('id' => '95', 'code' => 'HT', 'name' => 'Haiti', 'phonecode' => '509', 'code_A3' => 'HTI', 'code_N3' => '332', 'region' => 'LA', 'lat' => '19.00', 'lon' => '-72.25'),
            array('id' => '96', 'code' => 'HM', 'name' => 'Heard and McDonald Islands', 'phonecode' => '0', 'code_A3' => 'HMD', 'code_N3' => '334', 'region' => 'AU', 'lat' => '-53.06', 'lon' => '72.31'),
            array('id' => '97', 'code' => 'HN', 'name' => 'Honduras', 'phonecode' => '504', 'code_A3' => 'HND', 'code_N3' => '340', 'region' => 'LA', 'lat' => '15.00', 'lon' => '-86.30'),
            array('id' => '98', 'code' => 'HK', 'name' => 'Hong Kong S.A.R.', 'phonecode' => '852', 'code_A3' => 'HKG', 'code_N3' => '344', 'region' => 'AS', 'lat' => '22.15', 'lon' => '114.10'),
            array('id' => '99', 'code' => 'HU', 'name' => 'Hungary', 'phonecode' => '36', 'code_A3' => 'HUN', 'code_N3' => '348', 'region' => 'EU', 'lat' => '47.00', 'lon' => '20.00'),
            array('id' => '100', 'code' => 'IS', 'name' => 'Iceland', 'phonecode' => '354', 'code_A3' => 'ISL', 'code_N3' => '352', 'region' => 'EU', 'lat' => '65.00', 'lon' => '-18.00'),
            array('id' => '101', 'code' => 'IN', 'name' => 'India', 'phonecode' => '91', 'code_A3' => 'IND', 'code_N3' => '356', 'region' => 'AS', 'lat' => '20.00', 'lon' => '77.00'),
            array('id' => '102', 'code' => 'ID', 'name' => 'Indonesia', 'phonecode' => '62', 'code_A3' => 'IDN', 'code_N3' => '360', 'region' => 'AS', 'lat' => '-5.00', 'lon' => '120.00'),
            array('id' => '103', 'code' => 'IR', 'name' => 'Iran', 'phonecode' => '98', 'code_A3' => 'IRN', 'code_N3' => '364', 'region' => 'AS', 'lat' => '32.00', 'lon' => '53.00'),
            array('id' => '104', 'code' => 'IQ', 'name' => 'Iraq', 'phonecode' => '964', 'code_A3' => 'IRQ', 'code_N3' => '368', 'region' => 'AS', 'lat' => '33.00', 'lon' => '44.00'),
            array('id' => '105', 'code' => 'IE', 'name' => 'Ireland', 'phonecode' => '353', 'code_A3' => 'IRL', 'code_N3' => '372', 'region' => 'EU', 'lat' => '53.00', 'lon' => '-8.00'),
            array('id' => '106', 'code' => 'IL', 'name' => 'Israel', 'phonecode' => '972', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '107', 'code' => 'IT', 'name' => 'Italy', 'phonecode' => '39', 'code_A3' => 'ITA', 'code_N3' => '380', 'region' => 'EU', 'lat' => '42.50', 'lon' => '12.50'),
            array('id' => '108', 'code' => 'JM', 'name' => 'Jamaica', 'phonecode' => '1876', 'code_A3' => 'JAM', 'code_N3' => '388', 'region' => 'LA', 'lat' => '18.15', 'lon' => '-77.30'),
            array('id' => '109', 'code' => 'JP', 'name' => 'Japan', 'phonecode' => '81', 'code_A3' => 'JPN', 'code_N3' => '392', 'region' => 'AS', 'lat' => '36.00', 'lon' => '138.00'),
            array('id' => '110', 'code' => 'XJ', 'name' => 'Jersey', 'phonecode' => '44', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '111', 'code' => 'JO', 'name' => 'Jordan', 'phonecode' => '962', 'code_A3' => 'JOR', 'code_N3' => '400', 'region' => 'AS', 'lat' => '31.00', 'lon' => '36.00'),
            array('id' => '112', 'code' => 'KZ', 'name' => 'Kazakhstan', 'phonecode' => '7', 'code_A3' => 'KAZ', 'code_N3' => '398', 'region' => 'AS', 'lat' => '48.00', 'lon' => '68.00'),
            array('id' => '113', 'code' => 'KE', 'name' => 'Kenya', 'phonecode' => '254', 'code_A3' => 'KEN', 'code_N3' => '404', 'region' => 'AF', 'lat' => '1.00', 'lon' => '38.00'),
            array('id' => '114', 'code' => 'KI', 'name' => 'Kiribati', 'phonecode' => '686', 'code_A3' => 'KIR', 'code_N3' => '296', 'region' => 'AU', 'lat' => '1.25', 'lon' => '173.00'),
            array('id' => '115', 'code' => 'KP', 'name' => 'Korea North', 'phonecode' => '850', 'code_A3' => 'PRK', 'code_N3' => '408', 'region' => 'AS', 'lat' => '40.00', 'lon' => '127.00'),
            array('id' => '116', 'code' => 'KR', 'name' => 'Korea South', 'phonecode' => '82', 'code_A3' => 'KOR', 'code_N3' => '410', 'region' => 'AS', 'lat' => '37.00', 'lon' => '127.30'),
            array('id' => '117', 'code' => 'KW', 'name' => 'Kuwait', 'phonecode' => '965', 'code_A3' => 'KWT', 'code_N3' => '414', 'region' => 'AS', 'lat' => '29.30', 'lon' => '45.45'),
            array('id' => '118', 'code' => 'KG', 'name' => 'Kyrgyzstan', 'phonecode' => '996', 'code_A3' => 'KGZ', 'code_N3' => '417', 'region' => 'AS', 'lat' => '41.00', 'lon' => '75.00'),
            array('id' => '119', 'code' => 'LA', 'name' => 'Laos', 'phonecode' => '856', 'code_A3' => 'LAO', 'code_N3' => '418', 'region' => 'AS', 'lat' => '18.00', 'lon' => '105.00'),
            array('id' => '120', 'code' => 'LV', 'name' => 'Latvia', 'phonecode' => '371', 'code_A3' => 'LVA', 'code_N3' => '428', 'region' => 'EU', 'lat' => '57.00', 'lon' => '25.00'),
            array('id' => '121', 'code' => 'LB', 'name' => 'Lebanon', 'phonecode' => '961', 'code_A3' => 'LBN', 'code_N3' => '422', 'region' => 'AS', 'lat' => '33.50', 'lon' => '35.50'),
            array('id' => '122', 'code' => 'LS', 'name' => 'Lesotho', 'phonecode' => '266', 'code_A3' => 'LSO', 'code_N3' => '426', 'region' => 'AF', 'lat' => '-29.30', 'lon' => '28.30'),
            array('id' => '123', 'code' => 'LR', 'name' => 'Liberia', 'phonecode' => '231', 'code_A3' => 'LBR', 'code_N3' => '430', 'region' => 'AF', 'lat' => '6.30', 'lon' => '-9.30'),
            array('id' => '124', 'code' => 'LY', 'name' => 'Libya', 'phonecode' => '218', 'code_A3' => 'LBY', 'code_N3' => '434', 'region' => 'AF', 'lat' => '25.00', 'lon' => '17.00'),
            array('id' => '125', 'code' => 'LI', 'name' => 'Liechtenstein', 'phonecode' => '423', 'code_A3' => 'LIE', 'code_N3' => '438', 'region' => 'EU', 'lat' => '47.16', 'lon' => '9.32'),
            array('id' => '126', 'code' => 'LT', 'name' => 'Lithuania', 'phonecode' => '370', 'code_A3' => 'LTU', 'code_N3' => '440', 'region' => 'EU', 'lat' => '56.00', 'lon' => '24.00'),
            array('id' => '127', 'code' => 'LU', 'name' => 'Luxembourg', 'phonecode' => '352', 'code_A3' => 'LUX', 'code_N3' => '442', 'region' => 'EU', 'lat' => '49.45', 'lon' => '6.10'),
            array('id' => '128', 'code' => 'MO', 'name' => 'Macau S.A.R.', 'phonecode' => '853', 'code_A3' => 'MAC', 'code_N3' => '446', 'region' => 'AS', 'lat' => '22.10', 'lon' => '113.33'),
            array('id' => '129', 'code' => 'MK', 'name' => 'Macedonia', 'phonecode' => '389', 'code_A3' => 'MKD', 'code_N3' => '807', 'region' => 'EU', 'lat' => '41.50', 'lon' => '22.00'),
            array('id' => '130', 'code' => 'MG', 'name' => 'Madagascar', 'phonecode' => '261', 'code_A3' => 'MDG', 'code_N3' => '450', 'region' => 'AF', 'lat' => '-20.00', 'lon' => '47.00'),
            array('id' => '131', 'code' => 'MW', 'name' => 'Malawi', 'phonecode' => '265', 'code_A3' => 'MWI', 'code_N3' => '454', 'region' => 'AF', 'lat' => '-13.30', 'lon' => '34.00'),
            array('id' => '132', 'code' => 'MY', 'name' => 'Malaysia', 'phonecode' => '60', 'code_A3' => 'MYS', 'code_N3' => '458', 'region' => 'AS', 'lat' => '2.30', 'lon' => '112.30'),
            array('id' => '133', 'code' => 'MV', 'name' => 'Maldives', 'phonecode' => '960', 'code_A3' => 'MDV', 'code_N3' => '462', 'region' => 'AS', 'lat' => '3.15', 'lon' => '73.00'),
            array('id' => '134', 'code' => 'ML', 'name' => 'Mali', 'phonecode' => '223', 'code_A3' => 'MLI', 'code_N3' => '466', 'region' => 'AF', 'lat' => '17.00', 'lon' => '-4.00'),
            array('id' => '135', 'code' => 'MT', 'name' => 'Malta', 'phonecode' => '356', 'code_A3' => 'MLT', 'code_N3' => '470', 'region' => 'EU', 'lat' => '35.50', 'lon' => '14.35'),
            array('id' => '136', 'code' => 'XM', 'name' => 'Man (Isle of)', 'phonecode' => '44', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '137', 'code' => 'MH', 'name' => 'Marshall Islands', 'phonecode' => '692', 'code_A3' => 'MHL', 'code_N3' => '584', 'region' => 'AU', 'lat' => '9.00', 'lon' => '168.00'),
            array('id' => '138', 'code' => 'MQ', 'name' => 'Martinique', 'phonecode' => '596', 'code_A3' => 'MTQ', 'code_N3' => '474', 'region' => 'LA', 'lat' => '14.40', 'lon' => '-61.00'),
            array('id' => '139', 'code' => 'MR', 'name' => 'Mauritania', 'phonecode' => '222', 'code_A3' => 'MRT', 'code_N3' => '478', 'region' => 'AF', 'lat' => '20.00', 'lon' => '-12.00'),
            array('id' => '140', 'code' => 'MU', 'name' => 'Mauritius', 'phonecode' => '230', 'code_A3' => 'MUS', 'code_N3' => '480', 'region' => 'AF', 'lat' => '-20.17', 'lon' => '57.33'),
            array('id' => '141', 'code' => 'YT', 'name' => 'Mayotte', 'phonecode' => '269', 'code_A3' => 'MYT', 'code_N3' => '175', 'region' => 'AF', 'lat' => '-12.50', 'lon' => '45.10'),
            array('id' => '142', 'code' => 'MX', 'name' => 'Mexico', 'phonecode' => '52', 'code_A3' => 'MEX', 'code_N3' => '484', 'region' => 'LA', 'lat' => '23.00', 'lon' => '-102.00'),
            array('id' => '143', 'code' => 'FM', 'name' => 'Micronesia', 'phonecode' => '691', 'code_A3' => 'FSM', 'code_N3' => '583', 'region' => 'AU', 'lat' => '6.55', 'lon' => '158.15'),
            array('id' => '144', 'code' => 'MD', 'name' => 'Moldova', 'phonecode' => '373', 'code_A3' => 'MDA', 'code_N3' => '498', 'region' => 'EU', 'lat' => '47.00', 'lon' => '29.00'),
            array('id' => '145', 'code' => 'MC', 'name' => 'Monaco', 'phonecode' => '377', 'code_A3' => 'MCO', 'code_N3' => '492', 'region' => 'EU', 'lat' => '43.44', 'lon' => '7.24'),
            array('id' => '146', 'code' => 'MN', 'name' => 'Mongolia', 'phonecode' => '976', 'code_A3' => 'MNG', 'code_N3' => '496', 'region' => 'AS', 'lat' => '46.00', 'lon' => '105.00'),
            array('id' => '147', 'code' => 'MS', 'name' => 'Montserrat', 'phonecode' => '1664', 'code_A3' => 'MSR', 'code_N3' => '500', 'region' => 'LA', 'lat' => '16.45', 'lon' => '-62.12'),
            array('id' => '148', 'code' => 'MA', 'name' => 'Morocco', 'phonecode' => '212', 'code_A3' => 'MAR', 'code_N3' => '504', 'region' => 'AF', 'lat' => '32.00', 'lon' => '-5.00'),
            array('id' => '149', 'code' => 'MZ', 'name' => 'Mozambique', 'phonecode' => '258', 'code_A3' => 'MOZ', 'code_N3' => '508', 'region' => 'AF', 'lat' => '-18.15', 'lon' => '35.00'),
            array('id' => '150', 'code' => 'MM', 'name' => 'Myanmar', 'phonecode' => '95', 'code_A3' => 'MMR', 'code_N3' => '104', 'region' => 'AS', 'lat' => '22.00', 'lon' => '98.00'),
            array('id' => '151', 'code' => 'NA', 'name' => 'Namibia', 'phonecode' => '264', 'code_A3' => 'NAM', 'code_N3' => '516', 'region' => 'AF', 'lat' => '-22.00', 'lon' => '17.00'),
            array('id' => '152', 'code' => 'NR', 'name' => 'Nauru', 'phonecode' => '674', 'code_A3' => 'NRU', 'code_N3' => '520', 'region' => 'AU', 'lat' => '-0.32', 'lon' => '166.55'),
            array('id' => '153', 'code' => 'NP', 'name' => 'Nepal', 'phonecode' => '977', 'code_A3' => 'NPL', 'code_N3' => '524', 'region' => 'AS', 'lat' => '28.00', 'lon' => '84.00'),
            array('id' => '154', 'code' => 'AN', 'name' => 'Netherlands Antilles', 'phonecode' => '599', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '155', 'code' => 'NL', 'name' => 'Netherlands The', 'phonecode' => '31', 'code_A3' => 'NLD', 'code_N3' => '528', 'region' => 'EU', 'lat' => '52.30', 'lon' => '5.45'),
            array('id' => '156', 'code' => 'NC', 'name' => 'New Caledonia', 'phonecode' => '687', 'code_A3' => 'NCL', 'code_N3' => '540', 'region' => 'AU', 'lat' => '-21.30', 'lon' => '165.30'),
            array('id' => '157', 'code' => 'NZ', 'name' => 'New Zealand', 'phonecode' => '64', 'code_A3' => 'NZL', 'code_N3' => '554', 'region' => 'AU', 'lat' => '-41.00', 'lon' => '174.00'),
            array('id' => '158', 'code' => 'NI', 'name' => 'Nicaragua', 'phonecode' => '505', 'code_A3' => 'NIC', 'code_N3' => '558', 'region' => 'LA', 'lat' => '13.00', 'lon' => '-85.00'),
            array('id' => '159', 'code' => 'NE', 'name' => 'Niger', 'phonecode' => '227', 'code_A3' => 'NER', 'code_N3' => '562', 'region' => 'AF', 'lat' => '16.00', 'lon' => '8.00'),
            array('id' => '160', 'code' => 'NG', 'name' => 'Nigeria', 'phonecode' => '234', 'code_A3' => 'NGA', 'code_N3' => '566', 'region' => 'AF', 'lat' => '10.00', 'lon' => '8.00'),
            array('id' => '161', 'code' => 'NU', 'name' => 'Niue', 'phonecode' => '683', 'code_A3' => 'NIU', 'code_N3' => '570', 'region' => 'AU', 'lat' => '-19.02', 'lon' => '-169.52'),
            array('id' => '162', 'code' => 'NF', 'name' => 'Norfolk Island', 'phonecode' => '672', 'code_A3' => 'NFK', 'code_N3' => '574', 'region' => 'AU', 'lat' => '-29.02', 'lon' => '167.57'),
            array('id' => '163', 'code' => 'MP', 'name' => 'Northern Mariana Islands', 'phonecode' => '1670', 'code_A3' => 'MNP', 'code_N3' => '580', 'region' => 'AU', 'lat' => '15.12', 'lon' => '145.45'),
            array('id' => '164', 'code' => 'NO', 'name' => 'Norway', 'phonecode' => '47', 'code_A3' => 'NOR', 'code_N3' => '578', 'region' => 'EU', 'lat' => '62.00', 'lon' => '10.00'),
            array('id' => '165', 'code' => 'OM', 'name' => 'Oman', 'phonecode' => '968', 'code_A3' => 'OMN', 'code_N3' => '512', 'region' => 'AS', 'lat' => '21.00', 'lon' => '57.00'),
            array('id' => '166', 'code' => 'PK', 'name' => 'Pakistan', 'phonecode' => '92', 'code_A3' => 'PAK', 'code_N3' => '586', 'region' => 'AS', 'lat' => '30.00', 'lon' => '70.00'),
            array('id' => '167', 'code' => 'PW', 'name' => 'Palau', 'phonecode' => '680', 'code_A3' => 'PLW', 'code_N3' => '585', 'region' => 'AU', 'lat' => '7.30', 'lon' => '134.30'),
            array('id' => '168', 'code' => 'PS', 'name' => 'Palestinian Territory Occupied', 'phonecode' => '970', 'code_A3' => 'PSE', 'code_N3' => '275', 'region' => 'AS', 'lat' => '31.89', 'lon' => '34.90'),
            array('id' => '169', 'code' => 'PA', 'name' => 'Panama', 'phonecode' => '507', 'code_A3' => 'PAN', 'code_N3' => '591', 'region' => 'LA', 'lat' => '9.00', 'lon' => '-80.00'),
            array('id' => '170', 'code' => 'PG', 'name' => 'Papua new Guinea', 'phonecode' => '675', 'code_A3' => 'PNG', 'code_N3' => '598', 'region' => 'AS', 'lat' => '-6.00', 'lon' => '147.00'),
            array('id' => '171', 'code' => 'PY', 'name' => 'Paraguay', 'phonecode' => '595', 'code_A3' => 'PRY', 'code_N3' => '600', 'region' => 'LA', 'lat' => '-23.00', 'lon' => '-58.00'),
            array('id' => '172', 'code' => 'PE', 'name' => 'Peru', 'phonecode' => '51', 'code_A3' => 'PER', 'code_N3' => '604', 'region' => 'LA', 'lat' => '-10.00', 'lon' => '-76.00'),
            array('id' => '173', 'code' => 'PH', 'name' => 'Philippines', 'phonecode' => '63', 'code_A3' => 'PHL', 'code_N3' => '608', 'region' => 'AS', 'lat' => '13.00', 'lon' => '122.00'),
            array('id' => '174', 'code' => 'PN', 'name' => 'Pitcairn Island', 'phonecode' => '0', 'code_A3' => 'PCN', 'code_N3' => '612', 'region' => 'AU', 'lat' => '-25.04', 'lon' => '-130.06'),
            array('id' => '175', 'code' => 'PL', 'name' => 'Poland', 'phonecode' => '48', 'code_A3' => 'POL', 'code_N3' => '616', 'region' => 'EU', 'lat' => '52.00', 'lon' => '20.00'),
            array('id' => '176', 'code' => 'PT', 'name' => 'Portugal', 'phonecode' => '351', 'code_A3' => 'PRT', 'code_N3' => '620', 'region' => 'EU', 'lat' => '39.30', 'lon' => '-8.00'),
            array('id' => '177', 'code' => 'PR', 'name' => 'Puerto Rico', 'phonecode' => '1787', 'code_A3' => 'PRI', 'code_N3' => '630', 'region' => 'LA', 'lat' => '18.15', 'lon' => '-66.30'),
            array('id' => '178', 'code' => 'QA', 'name' => 'Qatar', 'phonecode' => '974', 'code_A3' => 'QAT', 'code_N3' => '634', 'region' => 'AS', 'lat' => '25.30', 'lon' => '51.15'),
            array('id' => '179', 'code' => 'RE', 'name' => 'Reunion', 'phonecode' => '262', 'code_A3' => 'REU', 'code_N3' => '638', 'region' => 'AF', 'lat' => '-21.06', 'lon' => '55.36'),
            array('id' => '180', 'code' => 'RO', 'name' => 'Romania', 'phonecode' => '40', 'code_A3' => 'ROU', 'code_N3' => '642', 'region' => 'EU', 'lat' => '46.00', 'lon' => '25.00'),
            array('id' => '181', 'code' => 'RU', 'name' => 'Russia', 'phonecode' => '70', 'code_A3' => 'RUS', 'code_N3' => '643', 'region' => 'EU', 'lat' => '60.00', 'lon' => '100.00'),
            array('id' => '182', 'code' => 'RW', 'name' => 'Rwanda', 'phonecode' => '250', 'code_A3' => 'RWA', 'code_N3' => '646', 'region' => 'AF', 'lat' => '-2.00', 'lon' => '30.00'),
            array('id' => '183', 'code' => 'SH', 'name' => 'Saint Helena', 'phonecode' => '290', 'code_A3' => 'SHN', 'code_N3' => '654', 'region' => 'AF', 'lat' => '-15.56', 'lon' => '-5.42'),
            array('id' => '184', 'code' => 'KN', 'name' => 'Saint Kitts And Nevis', 'phonecode' => '1869', 'code_A3' => 'KNA', 'code_N3' => '659', 'region' => 'LA', 'lat' => '17.20', 'lon' => '-62.45'),
            array('id' => '185', 'code' => 'LC', 'name' => 'Saint Lucia', 'phonecode' => '1758', 'code_A3' => 'LCA', 'code_N3' => '662', 'region' => 'LA', 'lat' => '13.53', 'lon' => '-60.68'),
            array('id' => '186', 'code' => 'PM', 'name' => 'Saint Pierre and Miquelon', 'phonecode' => '508', 'code_A3' => 'SPM', 'code_N3' => '666', 'region' => 'NA', 'lat' => '46.50', 'lon' => '-56.20'),
            array('id' => '187', 'code' => 'VC', 'name' => 'Saint Vincent And The Grenadines', 'phonecode' => '1784', 'code_A3' => 'VCT', 'code_N3' => '670', 'region' => 'LA', 'lat' => '13.15', 'lon' => '-61.12'),
            array('id' => '188', 'code' => 'WS', 'name' => 'Samoa', 'phonecode' => '684', 'code_A3' => 'WSM', 'code_N3' => '685', 'region' => 'AU', 'lat' => '-13.35', 'lon' => '-172.20'),
            array('id' => '189', 'code' => 'SM', 'name' => 'San Marino', 'phonecode' => '378', 'code_A3' => 'SMR', 'code_N3' => '674', 'region' => 'EU', 'lat' => '43.46', 'lon' => '12.25'),
            array('id' => '190', 'code' => 'ST', 'name' => 'Sao Tome and Principe', 'phonecode' => '239', 'code_A3' => 'STP', 'code_N3' => '678', 'region' => 'AF', 'lat' => '1.00', 'lon' => '7.00'),
            array('id' => '191', 'code' => 'SA', 'name' => 'Saudi Arabia', 'phonecode' => '966', 'code_A3' => 'SAU', 'code_N3' => '682', 'region' => 'AS', 'lat' => '25.00', 'lon' => '45.00'),
            array('id' => '192', 'code' => 'SN', 'name' => 'Senegal', 'phonecode' => '221', 'code_A3' => 'SEN', 'code_N3' => '686', 'region' => 'AF', 'lat' => '14.00', 'lon' => '-14.00'),
            array('id' => '193', 'code' => 'RS', 'name' => 'Serbia', 'phonecode' => '381', 'code_A3' => 'SRB', 'code_N3' => '688', 'region' => 'EU', 'lat' => '44.02', 'lon' => '21.01'),
            array('id' => '194', 'code' => 'SC', 'name' => 'Seychelles', 'phonecode' => '248', 'code_A3' => 'SYC', 'code_N3' => '690', 'region' => 'AF', 'lat' => '-4.35', 'lon' => '55.40'),
            array('id' => '195', 'code' => 'SL', 'name' => 'Sierra Leone', 'phonecode' => '232', 'code_A3' => 'SLE', 'code_N3' => '694', 'region' => 'AF', 'lat' => '8.30', 'lon' => '-11.30'),
            array('id' => '196', 'code' => 'SG', 'name' => 'Singapore', 'phonecode' => '65', 'code_A3' => 'SGP', 'code_N3' => '702', 'region' => 'AS', 'lat' => '1.22', 'lon' => '103.48'),
            array('id' => '197', 'code' => 'SK', 'name' => 'Slovakia', 'phonecode' => '421', 'code_A3' => 'SVK', 'code_N3' => '703', 'region' => 'EU', 'lat' => '48.40', 'lon' => '19.30'),
            array('id' => '198', 'code' => 'SI', 'name' => 'Slovenia', 'phonecode' => '386', 'code_A3' => 'SVN', 'code_N3' => '705', 'region' => 'EU', 'lat' => '46.07', 'lon' => '14.49'),
            array('id' => '199', 'code' => 'XG', 'name' => 'Smaller Territories of the UK', 'phonecode' => '44', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '200', 'code' => 'SB', 'name' => 'Solomon Islands', 'phonecode' => '677', 'code_A3' => 'SLB', 'code_N3' => '090', 'region' => 'AU', 'lat' => '-8.00', 'lon' => '159.00'),
            array('id' => '201', 'code' => 'SO', 'name' => 'Somalia', 'phonecode' => '252', 'code_A3' => 'SOM', 'code_N3' => '706', 'region' => 'AF', 'lat' => '10.00', 'lon' => '49.00'),
            array('id' => '202', 'code' => 'ZA', 'name' => 'South Africa', 'phonecode' => '27', 'code_A3' => 'ZAF', 'code_N3' => '710', 'region' => 'AF', 'lat' => '-29.00', 'lon' => '24.00'),
            array('id' => '203', 'code' => 'GS', 'name' => 'South Georgia', 'phonecode' => '0', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '204', 'code' => 'SS', 'name' => 'South Sudan', 'phonecode' => '211', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '205', 'code' => 'ES', 'name' => 'Spain', 'phonecode' => '34', 'code_A3' => 'ESP', 'code_N3' => '724', 'region' => 'EU', 'lat' => '40.00', 'lon' => '-4.00'),
            array('id' => '206', 'code' => 'LK', 'name' => 'Sri Lanka', 'phonecode' => '94', 'code_A3' => 'LKA', 'code_N3' => '144', 'region' => 'AS', 'lat' => '7.00', 'lon' => '81.00'),
            array('id' => '207', 'code' => 'SD', 'name' => 'Sudan', 'phonecode' => '249', 'code_A3' => 'SDN', 'code_N3' => '736', 'region' => 'AF', 'lat' => '15.00', 'lon' => '30.00'),
            array('id' => '208', 'code' => 'SR', 'name' => 'Suriname', 'phonecode' => '597', 'code_A3' => 'SUR', 'code_N3' => '740', 'region' => 'LA', 'lat' => '4.00', 'lon' => '-56.00'),
            array('id' => '209', 'code' => 'SJ', 'name' => 'Svalbard And Jan Mayen Islands', 'phonecode' => '47', 'code_A3' => 'SJM', 'code_N3' => '744', 'region' => 'EU', 'lat' => '78.00', 'lon' => '20.00'),
            array('id' => '210', 'code' => 'SZ', 'name' => 'Swaziland', 'phonecode' => '268', 'code_A3' => 'SWZ', 'code_N3' => '748', 'region' => 'AF', 'lat' => '-26.30', 'lon' => '31.30'),
            array('id' => '211', 'code' => 'SE', 'name' => 'Sweden', 'phonecode' => '46', 'code_A3' => 'SWE', 'code_N3' => '752', 'region' => 'EU', 'lat' => '62.00', 'lon' => '15.00'),
            array('id' => '212', 'code' => 'CH', 'name' => 'Switzerland', 'phonecode' => '41', 'code_A3' => 'CHE', 'code_N3' => '756', 'region' => 'EU', 'lat' => '47.00', 'lon' => '8.00'),
            array('id' => '213', 'code' => 'SY', 'name' => 'Syria', 'phonecode' => '963', 'code_A3' => 'SYR', 'code_N3' => '760', 'region' => 'AS', 'lat' => '35.00', 'lon' => '38.00'),
            array('id' => '214', 'code' => 'TW', 'name' => 'Taiwan', 'phonecode' => '886', 'code_A3' => 'TWN', 'code_N3' => '158', 'region' => 'AS', 'lat' => '23.30', 'lon' => '121.00'),
            array('id' => '215', 'code' => 'TJ', 'name' => 'Tajikistan', 'phonecode' => '992', 'code_A3' => 'TJK', 'code_N3' => '762', 'region' => 'AS', 'lat' => '39.00', 'lon' => '71.00'),
            array('id' => '216', 'code' => 'TZ', 'name' => 'Tanzania', 'phonecode' => '255', 'code_A3' => 'TZA', 'code_N3' => '834', 'region' => 'AF', 'lat' => '-6.00', 'lon' => '35.00'),
            array('id' => '217', 'code' => 'TH', 'name' => 'Thailand', 'phonecode' => '66', 'code_A3' => 'THA', 'code_N3' => '764', 'region' => 'AS', 'lat' => '15.00', 'lon' => '100.00'),
            array('id' => '218', 'code' => 'TG', 'name' => 'Togo', 'phonecode' => '228', 'code_A3' => 'TGO', 'code_N3' => '768', 'region' => 'AF', 'lat' => '8.00', 'lon' => '1.10'),
            array('id' => '219', 'code' => 'TK', 'name' => 'Tokelau', 'phonecode' => '690', 'code_A3' => 'TKL', 'code_N3' => '772', 'region' => 'AU', 'lat' => '-9.00', 'lon' => '-172.00'),
            array('id' => '220', 'code' => 'TO', 'name' => 'Tonga', 'phonecode' => '676', 'code_A3' => 'TON', 'code_N3' => '776', 'region' => 'AU', 'lat' => '-20.00', 'lon' => '-175.00'),
            array('id' => '221', 'code' => 'TT', 'name' => 'Trinidad And Tobago', 'phonecode' => '1868', 'code_A3' => 'TTO', 'code_N3' => '780', 'region' => 'LA', 'lat' => '11.00', 'lon' => '-61.00'),
            array('id' => '222', 'code' => 'TN', 'name' => 'Tunisia', 'phonecode' => '216', 'code_A3' => 'TUN', 'code_N3' => '788', 'region' => 'AF', 'lat' => '34.00', 'lon' => '9.00'),
            array('id' => '223', 'code' => 'TR', 'name' => 'Turkey', 'phonecode' => '90', 'code_A3' => 'TUR', 'code_N3' => '792', 'region' => 'EU', 'lat' => '39.00', 'lon' => '35.00'),
            array('id' => '224', 'code' => 'TM', 'name' => 'Turkmenistan', 'phonecode' => '7370', 'code_A3' => 'TKM', 'code_N3' => '795', 'region' => 'AS', 'lat' => '40.00', 'lon' => '60.00'),
            array('id' => '225', 'code' => 'TC', 'name' => 'Turks And Caicos Islands', 'phonecode' => '1649', 'code_A3' => 'TCA', 'code_N3' => '796', 'region' => 'LA', 'lat' => '21.45', 'lon' => '-71.35'),
            array('id' => '226', 'code' => 'TV', 'name' => 'Tuvalu', 'phonecode' => '688', 'code_A3' => 'TUV', 'code_N3' => '798', 'region' => 'AU', 'lat' => '-8.00', 'lon' => '178.00'),
            array('id' => '227', 'code' => 'UG', 'name' => 'Uganda', 'phonecode' => '256', 'code_A3' => 'UGA', 'code_N3' => '800', 'region' => 'AF', 'lat' => '1.00', 'lon' => '32.00'),
            array('id' => '228', 'code' => 'UA', 'name' => 'Ukraine', 'phonecode' => '380', 'code_A3' => 'UKR', 'code_N3' => '804', 'region' => 'EU', 'lat' => '49.00', 'lon' => '32.00'),
            array('id' => '229', 'code' => 'AE', 'name' => 'United Arab Emirates', 'phonecode' => '971', 'code_A3' => 'ARE', 'code_N3' => '784', 'region' => 'AS', 'lat' => '24.00', 'lon' => '54.00'),
            array('id' => '230', 'code' => 'GB', 'name' => 'United Kingdom', 'phonecode' => '44', 'code_A3' => 'GBR', 'code_N3' => '826', 'region' => 'EU', 'lat' => '54.00', 'lon' => '-2.00'),
            array('id' => '231', 'code' => 'US', 'name' => 'United States', 'phonecode' => '1', 'code_A3' => 'USA', 'code_N3' => '840', 'region' => 'NA', 'lat' => '38.00', 'lon' => '-97.00'),
            array('id' => '232', 'code' => 'UM', 'name' => 'United States Minor Outlying Islands', 'phonecode' => '1', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '233', 'code' => 'UY', 'name' => 'Uruguay', 'phonecode' => '598', 'code_A3' => 'URY', 'code_N3' => '858', 'region' => 'LA', 'lat' => '-33.00', 'lon' => '-56.00'),
            array('id' => '234', 'code' => 'UZ', 'name' => 'Uzbekistan', 'phonecode' => '998', 'code_A3' => 'UZB', 'code_N3' => '860', 'region' => 'AS', 'lat' => '41.00', 'lon' => '64.00'),
            array('id' => '235', 'code' => 'VU', 'name' => 'Vanuatu', 'phonecode' => '678', 'code_A3' => 'VUT', 'code_N3' => '548', 'region' => 'AU', 'lat' => '-16.00', 'lon' => '167.00'),
            array('id' => '236', 'code' => 'VA', 'name' => 'Vatican City State (Holy See)', 'phonecode' => '39', 'code_A3' => 'VAT', 'code_N3' => '336', 'region' => 'EU', 'lat' => '41.54', 'lon' => '12.27'),
            array('id' => '237', 'code' => 'VE', 'name' => 'Venezuela', 'phonecode' => '58', 'code_A3' => 'VEN', 'code_N3' => '862', 'region' => 'LA', 'lat' => '8.00', 'lon' => '-66.00'),
            array('id' => '238', 'code' => 'VN', 'name' => 'Vietnam', 'phonecode' => '84', 'code_A3' => 'VNM', 'code_N3' => '704', 'region' => 'AS', 'lat' => '16.00', 'lon' => '106.00'),
            array('id' => '239', 'code' => 'VG', 'name' => 'Virgin Islands (British)', 'phonecode' => '1284', 'code_A3' => 'VGB', 'code_N3' => '092', 'region' => 'LA', 'lat' => '18.20', 'lon' => '-64.50'),
            array('id' => '240', 'code' => 'VI', 'name' => 'Virgin Islands (US)', 'phonecode' => '1340', 'code_A3' => 'VIR', 'code_N3' => '850', 'region' => 'LA', 'lat' => '18.20', 'lon' => '-64.50'),
            array('id' => '241', 'code' => 'WF', 'name' => 'Wallis And Futuna Islands', 'phonecode' => '681', 'code_A3' => 'WLF', 'code_N3' => '876', 'region' => 'AU', 'lat' => '-13.18', 'lon' => '-176.12'),
            array('id' => '242', 'code' => 'EH', 'name' => 'Western Sahara', 'phonecode' => '212', 'code_A3' => 'ESH', 'code_N3' => '732', 'region' => 'AF', 'lat' => '24.30', 'lon' => '-13.00'),
            array('id' => '243', 'code' => 'YE', 'name' => 'Yemen', 'phonecode' => '967', 'code_A3' => 'YEM', 'code_N3' => '887', 'region' => 'AS', 'lat' => '15.00', 'lon' => '48.00'),
            array('id' => '244', 'code' => 'YU', 'name' => 'Yugoslavia', 'phonecode' => '38', 'code_A3' => '', 'code_N3' => '', 'region' => '', 'lat' => '0.00', 'lon' => '0.00'),
            array('id' => '245', 'code' => 'ZM', 'name' => 'Zambia', 'phonecode' => '260', 'code_A3' => 'ZMB', 'code_N3' => '894', 'region' => 'AF', 'lat' => '-15.00', 'lon' => '30.00'),
            array('id' => '246', 'code' => 'ZW', 'name' => 'Zimbabwe', 'phonecode' => '263', 'code_A3' => 'ZWE', 'code_N3' => '716', 'region' => 'AF', 'lat' => '-20.00', 'lon' => '30.00')
        );



        ///SEED COUNTRIES////
        $this->command->info('Start Countries Seed..');
        foreach ($countries as $value) {


            $data = array(
                'id' => $value['id'],
                'code' => $value['code'],
                'name' => $value['name'],
                'phonecode' => $value['phonecode'],
                'code_A3' => $value['code_A3'],
                'code_N3' => $value['code_N3'],
                'region' => $value['region'],
                'lat' => $value['lat'],
                'lon' => $value['lon'],
                'status_id' => 1 //1 is active, 2 is disable
            );


            $data_exists = Country::find($value['id']);
            if (!is_null($data_exists)) {
                $data['updated_at'] = date("Y-m-d H:i:s");
                Country::where('id', $value['id'])
                        ->update($data); //UPDATE
            } else {
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['updated_at'] = date("Y-m-d H:i:s");
                Country::create($data); //CREATE
            }
        }

        $this->command->info('Update Countries Success!');

        ///Update Position
        $countries_positions = array(
            array('id' => '132', 'position' => 1), ///Malaysia
            array('id' => '196', 'position' => 2), ///Singapura
            array('id' => '32', 'position' => 3), ///Brunei
            array('id' => '102', 'position' => 4), ///Indonesia
            array('id' => '98', 'position' => 5), ///Hong Kong
            array('id' => '44', 'position' => 6), ///China
            array('id' => '214', 'position' => 7), ///Taiwan
        );
        foreach ($countries_positions as $value) {

            $data = array(
                'id' => $value['id'],
                'position' => $value['position'],
            );

            $data_exists = Country::find($value['id']);
            if (!is_null($data_exists)) {
                $data['updated_at'] = date("Y-m-d H:i:s");
                Country::where('id', $value['id'])
                        ->update($data); //UPDATE position
            }
        }
        $this->command->info('Update Countries Position Success!');
        
        
         ///Update Phone Code Position
        $countries_positions = array(
            array('id' => '132', 'phonecode_position' => 1), ///malaysia
        );
        foreach ($countries_positions as $value) {

            $data = array(
                'id' => $value['id'],
                'phonecode_position' => $value['phonecode_position'],
            );

            $data_exists = Country::find($value['id']);
            if (!is_null($data_exists)) {
                $data['updated_at'] = date("Y-m-d H:i:s");
                Country::where('id', $value['id'])
                        ->update($data); //UPDATE position
            }
        }
        $this->command->info('Update Countries PhoneCode Position Success!');
    }

}
