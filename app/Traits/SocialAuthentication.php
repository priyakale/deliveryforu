<?php

namespace App\Traits;

use App\Models\User;
use App\Services\GoogleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

Trait SocialAuthentication
{
    /**
     * This will generate the Auth URL for google login
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function googleAuthLogin()
    {
        $service = new GoogleService();

        $url = $service->GPlusScope()->createAuthUrl();

        return redirect($url);
    }

    /**
     * Handling Callback URL from Google Side
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function googleAuthCallback(Request $request)
    {
        //Check if the user authorize or cancel when login with social
        try {
            $service = new GoogleService();

            $serviceAuth = $service->getUserInfo($request->input('code'));
        } catch (\Exception $exception) {

            //TODO set session authorization rejected by the user
            //Redirect to intended
            return redirect($this->redirectTo);
        }


        //Run if authentication success
        //Check if the user already exist
        $user = User::where('email', $serviceAuth->email)->first();

        if ($user) {
            Auth::login($user);
        } else {
            $user = User::create([
                'first_name' => $serviceAuth->givenName,
                'last_name'  => $serviceAuth->familyName,
                'email'      => $serviceAuth->email,
                'phone_no'   => null,
                'google_id'  => $serviceAuth->id,
                'activated'  => User::ACTIVATE,
                'password'   => bcrypt(Str::random(40)),
            ]);

            Auth::login($user);
        }

        //Redirect to intended
        return redirect($this->redirectTo);
    }

    /**
     * Generate Redirect URL and redirect action to facebook login
     *
     * @return mixed
     */
    public function facebookAuthLogin()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Handling Callback URL from Google Side
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function facebookCallback(Request $request)
    {
        //Check if the user authorize or cancel when login with social
        try {
            //Field depends on graph from facebook
            //@reference https://developers.facebook.com/docs/graph-api/reference/v3.0/user
            $user = Socialite::driver('facebook')->fields([
                'name',
                'first_name',
                'last_name',
                'email',
                'gender'
            ])->user();

        } catch (\Exception $exception) {

            //TODO set session authorization rejected by the user
            return redirect()->route('login');
        }

        //Run if authentication success
        //Check if the user already exist
        $checkUser = User::where('email', $user->user['email'])->first();

        if ($checkUser) {
            Auth::login($checkUser);
        } else {

            $user = User::create([
                'first_name'  => $user->user['first_name'],
                'last_name'   => $user->user['last_name'],
                'email'       => $user->user['email'],
                'phone_no'    => null,
                'facebook_id' => $user->id,
                'activated'   => User::ACTIVATE,
                'password'    => bcrypt(Str::random(40)),
            ]);

            Auth::login($user);
        }

        //Redirect to intended
        return redirect($this->redirectTo);
    }
}
