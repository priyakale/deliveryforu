<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Response;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Session\TokenMismatchException;
use App\Helpers\ErrorLogs;
use Illuminate\Validation\ValidationException;
use App\Helpers\ApiLogs;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof TokenMismatchException)
        {
            if ($request->expectsJson())
            {
                $error = [
                    "error" => [
                        "code" => 400,
                        "message" => ["err_msg" => "Oops! Seems you couldn't submit form for a long time. Please try again"]
                    ]
                ];
                return response()->json($error, 400);
            }
            // Redirect to a form
            return redirect($request->fullUrl())->with('danger',"Oops! Seems you couldn't submit form for a long time. Please try again.");
        }
        else if($exception instanceof NotFoundHttpException)
        {
            if ($request->expectsJson()) 
            {
                $error = [
                    "error" => [
                        "code" => 404,
                        "message" => ["err_msg" => "HTTP Not Found"]
                    ]
                ];
                return response()->json($error, $exception->getStatusCode());
            }
        }
        else if($exception instanceof AuthenticationException)
        {
            if ($request->expectsJson()) 
            {
                $error = [
                    "error" => [
                        "code" => 401,
                        "message" => ["err_msg" => "Unauthenticated"]
                    ]
                ];
                return response()->json($error, 401);
            }
        }
        else if($exception instanceof ValidationException)
        {
            if ($request->expectsJson()) 
            {
                $error = [
                    "error" => [
                        "code" => 422,
                        "message" => $exception->validator->errors()->getMessages()
                    ]
                ];
                return response()->json($error, 422);
            }
                
        }
        else
        {
            if ($request->expectsJson()) 
            {
                $error = [
                    "error" => [
                        "code" => 500,
                        "message" => ["err_msg" => $exception->getMessage()]
                    ]
                ];

                ApiLogs::store(500,"EXCEPTION");

                return response()->json($error, 500);
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) 
        {
            $error = [
                    "error" => [
                        "code" => 401,
                        "message" => "Unauthenticated"
                    ]
                ];
            return response()->json($error);
        }

        return redirect()->guest(route('login'));
    }
}
