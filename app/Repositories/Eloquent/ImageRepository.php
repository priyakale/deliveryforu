<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Validator;
use Session;
use Auth;

//MODEL
use App\Models\InvestorDetail;

class ImageRepository extends BaseRepository 
{
    protected $model;
    
    public function __construct(InvestorDetail $model)
    {
        $this->model = $model;
    }

    public function validator(array $data, $method = null) 
    {
        $attributeNames = [
            //'user_id' => 'user'
        ];
        
        if($method ==='show')
        {
            $rule = ['id' => 'exists:images'];
        }
        elseif($method ==='destroy')
        {
            $rule = ['id' => 'exists:images'];
        }
        else if($method==='exists_id')
        {
            $rule = ['id' => 'exists:images'];
        }
        elseif($method === 'store')
        {
            $rule = [
                'image' => 'required|mimes:jpeg,jpg|max:8000'
                ,'user_id' => 'required|exists:users,id'
                ,'type_id' => 'required|exists:lookups,id'
            ];
        }
        elseif($method === 'userApiStore')
        {
            $rule = ['user_id' => 'required'];
        }
        elseif($method === 'jobApiStore')
        {
            $rule = ['job_id' => 'required'];
        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);
    }

    public function dynamicQuery($model_query, $data)
    {
        //TODO : Filter users by specific field.
        return $model_query;
    }

    public function save($data = [], $parent_folder, $id)
    {        
        //If have file
        if (!empty($data))
        {
            $image_names = $this->saveFile($data, $parent_folder, $id);
            dd($image_names);
            foreach ($image_names as $key => $value) 
            {   
               
                $image_data = array(
                    'type_id' => $value['type_id']
                    ,'user_id' => $id
                    ,'name' => $value['name']
                );

                $result = $this->create($image_data);

                if(!$result)
                {
                    return $this->errorSave('Error saving the image!');
                }
            }
                
            $result = $this->findBy(['user_id' => $id]);
        }
        else{
            $result = $this->errorEmptyData('No image to save');
        }
        //End if have file

        return $result;
    }

}