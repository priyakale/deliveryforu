<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Notifications\Email;
use App\Notifications\EmailAttach;
use App\Notifications\EmailAttachmentPDF;
use Notification;
use Validator;
use Session;
use Auth;
use DateTime;
use Log;

use UserRepo;
use WalletRepo;
use EmailNotiRepo;
use AddressRepo;
use CommissionRepo;

//MODEL
use App\Models\DealerApplication;

class DealerRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(DealerApplication $model, UserRepo $userRepo, WalletRepo $walletRepo, EmailNotiRepo $emailNotiRepo, AddressRepo $addressRepo, CommissionRepo $commissionRepo)
  	{
    	$this->model = $model;
        $this->userRepo = $userRepo;
        $this->walletRepo = $walletRepo;
        $this->emailNotiRepo = $emailNotiRepo;
        $this->addressRepo = $addressRepo;
        $this->commissionRepo = $commissionRepo;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
            ,'state_id' => 'state'
            ,'file_1' => 'Form 9 / Form 13'
            ,'file_2' => 'Business License'
          ];

        if($method =='save'){
            $rule = [
                "user_id" => "required"
                ,"company_name" => "required"
                ,"company_number" => "required"
                ,"date_incorporation" => "required"
                ,"num_of_employees" => "required|numeric"
                ,"phones_sold_monthly" => "required|numeric"
                ,"file_1" => "required|mimes:doc,pdf,jpg,jpeg|max:5000"
                ,"file_2" => "required|mimes:doc,pdf,jpg,jpeg|max:5000"
                ,"contact_designation" => "required"
                ,"contact_mobile_number" => "required"
                ,"contact_office_number" => "required"
                ,"contact_identification_type_id" => "required|in:51,52|numeric"
                ,"contact_identification_number" => "required"
                ,"authorised_name" => "required_unless:same_person,same_as_above"
                ,"authorised_designation" => "required_unless:same_person,same_as_above"
                ,"authorised_email" => "required_unless:same_person,same_as_above"
                ,"authorised_mobile_number" => "required_unless:same_person,same_as_above"
                ,"authorised_office_number" => "required_unless:same_person,same_as_above"
                ,"authorised_identification_type_id" => "required_unless:same_person,same_as_above"
                ,"authorised_identification_number" => "required_unless:same_person,same_as_above"
                ,"b_street_1" => "required"
                ,"b_postal_code" => "required"
                ,"b_city" => "required"
                ,"b_state_id" => "required"
                ,"c_street_1" => "required_unless:same_address,same_as_above"
                ,"c_postal_code" => "required_unless:same_address,same_as_above"
                ,"c_city" => "required_unless:same_address,same_as_above"
                ,"c_state_id" => "required_unless:same_address,same_as_above"
                ,"tnc" => "required"
                ,"privacy_policy" => "required"
                ,"declaration" => "required"
            ];

            foreach ($data as $key => $value) {
                $exp_key = explode('-', $key);

                if ($exp_key[0] == 'staff_name') {

                    if (!empty($data['staff_name-'.$exp_key[1]]) || !empty($data['staff_email-'.$exp_key[1]]) || !empty($data['staff_identification_number-'.$exp_key[1]]) || !empty($data['staff_identification_type_id-'.$exp_key[1]]) || !empty($data['staff_mobile_number-'.$exp_key[1]])) {
                        $rule['staff_name-'.$exp_key[1]] = 'required|between:4,60';
                        $rule['staff_email-'.$exp_key[1]] = 'required|email';
                        $rule['staff_identification_type_id-'.$exp_key[1]] = 'required|in:51,52|numeric';
                        $rule['staff_identification_number-'.$exp_key[1]] = 'required|unique:users,identification_number';
                        $rule['staff_mobile_number-'.$exp_key[1]] = 'required';

                        $attributeNames['staff_name-'.$exp_key[1]] = 'staff name';
                        $attributeNames['staff_email-'.$exp_key[1]] = 'staff email';
                        $attributeNames['staff_identification_type_id-'.$exp_key[1]] = 'staff identification type';
                        $attributeNames['staff_identification_number-'.$exp_key[1]] = 'staff identification number';
                        $attributeNames['staff_mobile_number-'.$exp_key[1]] = 'staff mobile number';
                        $attributeNames['staff_bank_name-'.$exp_key[1]] = 'staff bank name';
                        $attributeNames['staff_account_number-'.$exp_key[1]] = 'staff account number';
                    }
                }
            }

        }
        else if($method==='update')
        {
            $rule = ['id' => 'required|exists:dealer_applications,id'];
        }
        else if($method==='assign_salesperson')
        {
            $rule = [
                'id' => 'required|exists:users,id',
                'application_id' => 'required|exists:dealer_applications,id',
            ];
        }
        

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {

        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['date_from']) && $data['date_from'] != ''){
          $model_query->where('created_at', '>=',$data['date_from']);
        }

        if(!empty($data['date_to']) && $data['date_to'] != ''){
          $model_query->where('created_at', '<=',$data['date_to']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id', $data['status_id']);
        }

        if(!empty($data['saleskit_handover']) && $data['saleskit_handover'] != ''){
          $model_query->where('saleskit_handover', $data['saleskit_handover']);
        }

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
       
        if(empty($data)){
            return $this->errorEmptyData();
        }else{

            $validator = $this->validator($data, 'save')->validate();
            
            $data['tnc'] = 1;
            $data['privacy_policy'] = 1;
            $data['declaration'] = 1;
            
            foreach ($data as $key => $value) {
                $exp_key = explode('-', $key);

                if ($exp_key[0] == 'staff_name') {
                    if(!empty($data['staff_name-'.$exp_key[1]])){
                        $staff[$exp_key[1]]['name'] = $data['staff_name-'.$exp_key[1]];
                        $staff[$exp_key[1]]['email'] = $data['staff_email-'.$exp_key[1]];
                        $staff[$exp_key[1]]['identification_type_id'] = $data['staff_identification_type_id-'.$exp_key[1]];
                        $staff[$exp_key[1]]['identification_number'] = $data['staff_identification_number-'.$exp_key[1]];
                        $staff[$exp_key[1]]['mobile_number'] = $data['staff_mobile_number-'.$exp_key[1]];

                        if(!empty($data['staff_bank_name-'.$exp_key[1]])){
                            $staff[$exp_key[1]]['bank_name'] = $data['staff_bank_name-'.$exp_key[1]];
                        }

                        if(!empty($data['staff_account_number-'.$exp_key[1]])){
                            $staff[$exp_key[1]]['account_number'] = $data['staff_account_number-'.$exp_key[1]];
                        }   
                    } 
                }
            }

            DB::beginTransaction();

            if(Auth::user()->role_id < 5){
                if(empty($data['same_person'])){

                    $dealer_user['role_id'] = 6;
                    $dealer_user['name'] = $data['authorised_name'];
                    $dealer_user['email'] = $data['authorised_email'];
                    $dealer_user['mobile_number'] = $data['authorised_mobile_number'];
                    $dealer_user['identification_type_id'] = $data['authorised_identification_type_id'];
                    $dealer_user['identification_number'] = $data['authorised_identification_number'];
                    $dealer_user['bank_name'] = $data['authorised_bank_name'];
                    $dealer_user['account_number'] = $data['authorised_account_number'];
                    $dealer_user['office_number'] = $data['authorised_office_number'];
                    $dealer_user['designation'] = $data['authorised_designation'];

                    $user = $this->userRepo->save($dealer_user);
                    $data['user_id'] = $user['id'];

                    $dealer_user['role_id'] = 8;
                    $dealer_user['name'] = $data['contact_name'];
                    $dealer_user['email'] = $data['contact_email'];
                    $dealer_user['mobile_number'] = $data['contact_mobile_number'];
                    $dealer_user['identification_type_id'] = $data['contact_identification_type_id'];
                    $dealer_user['identification_number'] = $data['contact_identification_number'];
                    $dealer_user['bank_name'] = $data['contact_bank_name'];
                    $dealer_user['account_number'] = $data['contact_account_number'];
                    $dealer_user['office_number'] = $data['contact_office_number'];
                    $dealer_user['designation'] = $data['contact_designation'];
                    $dealer_user['referral_id'] = $data['user_id'];

                    $submitter_user = $this->userRepo->save($dealer_user);
                    $this->sendDealerMail($submitter_user, 125);

                    $data['contact_user_id'] = $submitter_user['id'];
                }
                else{
                    $dealer_user['role_id'] = 6;
                    $dealer_user['name'] = $data['contact_name'];
                    $dealer_user['email'] = $data['contact_email'];
                    $dealer_user['mobile_number'] = $data['contact_mobile_number'];
                    $dealer_user['identification_type_id'] = $data['contact_identification_type_id'];
                    $dealer_user['identification_number'] = $data['contact_identification_number'];
                    $dealer_user['bank_name'] = $data['contact_bank_name'];
                    $dealer_user['account_number'] = $data['contact_account_number'];
                    $dealer_user['office_number'] = $data['contact_office_number'];
                    $dealer_user['designation'] = $data['contact_designation'];

                    $user = $this->userRepo->save($dealer_user);
                    $data['same_authorised_person'] = true;
                    $data['contact_user_id'] = $user['id'];
                }
            }
            else{
                if(empty($data['same_person'])){
                    $dealer_user['role_id'] = 6;
                    $dealer_user['name'] = $data['authorised_name'];
                    $dealer_user['email'] = $data['authorised_email'];
                    $dealer_user['mobile_number'] = $data['authorised_mobile_number'];
                    $dealer_user['identification_type_id'] = $data['authorised_identification_type_id'];
                    $dealer_user['identification_number'] = $data['authorised_identification_number'];
                    $dealer_user['bank_name'] = $data['authorised_bank_name'];
                    $dealer_user['account_number'] = $data['authorised_account_number'];
                    $dealer_user['office_number'] = $data['authorised_office_number'];
                    $dealer_user['designation'] = $data['authorised_designation'];

                    $user = $this->userRepo->save($dealer_user);

                    $data['user_id'] = $user['id'];
                    $this->userRepo->update(['name'=>$data['contact_name'],'referral_id'=>$user['id'], 'role_id'=>8, 'mobile_number'=>$data['contact_mobile_number'], 'identification_type_id'=>$data['contact_identification_type_id'], 'identification_number'=>$data['contact_identification_number'], 'designation'=>$data['contact_designation'], 'office_number'=>$data['contact_office_number'], 'bank_name'=>$data['contact_bank_name'], 'account_number'=>$data['contact_account_number']], Auth::id());
                    $submitter_user = $this->userRepo->find(Auth::id());                    
                }
                else{
                    $user = Auth::user();
                    $data['user_id'] = $user['id'];
                    $this->userRepo->update(['name'=>$data['contact_name'],'mobile_number'=>$data['contact_mobile_number'], 'identification_type_id'=>$data['contact_identification_type_id'], 'identification_number'=>$data['contact_identification_number'], 'designation'=>$data['contact_designation'], 'office_number'=>$data['contact_office_number'], 'bank_name'=>$data['contact_bank_name'], 'account_number'=>$data['contact_account_number']], Auth::id());
                    $data['same_authorised_person'] = true;
                }

                $data['contact_user_id'] = Auth::id();
            }

            if(!empty($staff)){
                foreach ($staff as $key => $value) {
                    $value['role_id'] = 8;
                    $value['status_id'] = 2;
                    $value['user'] = $user;
                    $result = $this->userRepo->save($value, [], 'staff');
                    if(!$result)
                    {
                        DB::rollback();
                        return $this->errorSave('Error saving the address!');
                    }
                }
            }

            $address_array = $this->createAddressArray($data);
            
            if(!empty($address_array)){
                
                foreach ($address_array as $key => $value) {
                    $value['user_id'] = $user['id'];

                    $result = $this->addressRepo->save($value);
                    
                    if(!$result)
                    {
                        DB::rollback();
                        return $this->errorSave('Error saving the address!');
                    }
                }
            }

            $result = $this->create($data);
            
            if(!$result){
                DB::rollback();
                return $this->errorSave();
            }else{

                $user['address'] = $this->addressRepo->findBy(['user_id'=>$user['id'], 'type_id'=>84],['state']);
                
                $referral_code = $this->generateReferralCode($user);
                $this->userRepo->update(['referral_code'=>$referral_code], $user['id']);
                $wallet_data['user_id'] = $user['id'];
                $wallet = $this->walletRepo->save($wallet_data);

                if(empty($wallet)){
                    DB::rollback();
                    return $this->errorSave('Error create wallet');
                }

                // try {
                //     $this->sendDealerMail($record['user'], 79);
                // } 
                // catch (\Exception $exception) {

                // }

                $commission_result = $this->commissionRepo->save(['user_id'=>$user['id'], 'status_id'=>1, 'rate'=>50, 'ra_rate'=>50]);

                if(!$commission_result){
                    DB::rollback();
                    return $this->errorSave();
                }
                //If request has file save it    
                if (!empty($file_upload)) {

                    $folder_type = "user";
                    $file_name = $this->saveFile($file_upload, $folder_type, $data['user_id']);
                    
                    $file_data = array(
                        'form_9_or_13' => $file_name[0]
                        ,'business_license' => $file_name[1]
                    );

                    $update = $this->update($file_data, $result['id']);

                    if(!$update)
                    {
                        DB::rollback();
                        return $this->errorSave('Error saving the image!');
                    }
                }

                try {
                    if(Auth::user()->role_id >= 5){
                        if(empty($data['same_person'])){
                            $this->sendDealerMail($submitter_user, 123);
                        }
                        else{
                            $this->sendDealerMail($user, 79);
                        }
                    }
                } 
                catch (\Exception $exception) {

                }

                DB::commit();
                $result = $this->NullToEmpty($this->find($result['id'])->toArray());
                //End if request has file
                Session::flash('success', 'Save dealers form success'); 
            }
        }//End else
        return $result;
    }

    public function changeYakinSalesman($id, $salesman_id)
    {     

        $validator = $this->validator(['id'=>$salesman_id, 'application_id'=>$id], 'assign_salesperson')->validate();

        $record = $this->find($id, ['status','user']);

        if(empty($record))
        {
            return $this->errorDynamic('Application not found.');
        }
        else
        {
            DB::beginTransaction();
            //update application status to 12 (Rejected) or 13 (Accepted) or 14 (Cancelled)
            if ($this->update(['salesman_id' => $salesman_id], $id))
            {
                $record = $this->find($id, ['status','user','salesman']);
                $user = $record['salesman'];
                $user['dealer'] = $record['user'];
                try {
                    $this->sendDealerMail($user, 129);
                } 
                catch (\Exception $exception) {

                }
                Session::flash('success', 'Salesman updated successfully');
                
                $result = $this->success();
            }
            else
            {
                DB::rollback();
                $result = $this->errorUpdate();
            }
            DB::commit();
        }
            
        return $result;
    } 

    public function changeStatus($id, $status)
    {     
        $record = $this->find($id, ['status','user']);

        if(empty($record))
        {
            return $this->errorDynamic('Application not found.');
        }
        else
        {
            DB::beginTransaction();
            //update application status to 72 (Rejected) or 54 (Approved) or 56 (Pre-Approved)
            if ($this->update(['status_id' => $status], $id))
            {
                $record = $this->find($id, ['status','user']);

                $record['user']['address'] = $this->addressRepo->findBy(['user_id'=>$record['user']['id'], 'type_id'=>84],['state']);
                
                if($status==56){
                    // $referral_code = $this->generateReferralCode($record['user']);
                    // $this->userRepo->update(['referral_code'=>$referral_code], $record['user']['id']);
                    // $wallet_data['user_id'] = $record['user']['id'];
                    // $wallet = $this->walletRepo->save($wallet_data);

                    // if(empty($wallet)){
                    //     DB::rollback();
                    //     return $this->errorSave('Error create wallet');
                    // }

                    // try {
                    //     $this->sendDealerMail($record['user'], 79);
                    // } 
                    // catch (\Exception $exception) {

                    // }

                    // $staff = $this->userRepo->all(['role_id'=>8, 'referral_id'=>$record['user']['id']]);

                    // foreach ($staff as $key => $value) {
                    //     $result = $this->userRepo->changeStatus($value['id'], 1);
                    //     if(!$result){
                    //         DB::rollback();
                    //         return $this->errorSave('Error create wallet');
                    //     }
                    // }

                    Session::flash('success', 'Application accepted successfully');
                }
                else{
                    try {
                        $this->sendDealerMail($record['user'], 80);

                        $finance_users = $this->userRepo->all(['role_id'=>11]);

                        foreach ($finance_users as $key => $value) {
                            $value['dealer'] = $record['user'];
                            $this->sendDealerMail($value, 131);
                        }  
                    } 
                    catch (\Exception $exception) {

                    }
                    Session::flash('success', 'Application rejected successfully');
                }
                
                $result = $this->success();
            }
            else
            {
                DB::rollback();
                $result = $this->errorUpdate();
            }
            DB::commit();
        }
            
        return $result;
    } 

    public function qrCodeRelease($id)
    {     
        $record = $this->find($id, ['status','user']);

        if(empty($record))
        {
            return $this->errorDynamic('Application not found.');
        }
        else
        {
            DB::beginTransaction();
            if ($this->update(['qr_release' => 1], $id))
            {
                $record = $this->find($id, ['status','user']);

                try {
                    $this->sendDealerQRMail($record['user'], 90);
                } 
                catch (\Exception $exception) {
                    
                    Log::useDailyFiles(storage_path().'/logs/Exception/QRReleaseMail/'.carbon::now()->format('Ym').'/exception.log');
                    Log::notice('Exception'. print_r($exception->getMessage(),true));
                    DB::rollback();
                    return $this->errorUpdate();
                }

                $staff = $this->userRepo->all(['role_id'=>8, 'referral_id'=>$record['user']['id']]);

                foreach ($staff as $key => $value) {

                    $value['password'] = rand(100000,999999);
                    $password = $value['password'];
                    $value['password'] = bcrypt($value['password']);

                    $this->userRepo->update(['password'=>$value['password'], 'status_id'=>1], $value['id']);

                    $value['password'] = $password;

                    try {
                        $this->sendDealerQRMail($value, 93);
                    } 
                    catch (\Exception $exception) {
                        
                        Log::useDailyFiles(storage_path().'/logs/error/QRReleaseMail/'.carbon::now()->format('Ym').'/exception.log');
                        Log::notice('Exception'. print_r($exception->getMessage(),true));
                        DB::rollback();
                        return $this->errorUpdate();
                    }
                }

                Session::flash('success', 'Qr code released successfully');
                
                $result = $this->success();
            }
            else
            {
                DB::rollback();
                $result = $this->errorUpdate();
            }
            DB::commit();
        }
            
        return $result;
    }

    public function activateWeb($id)
    {     
        $record = $this->find($id, ['status','user']);

        if(empty($record))
        {
            return $this->errorDynamic('Application not found.');
        }
        else
        {
            DB::beginTransaction();
            if ($this->update(['saleskit_handover' => 1], $id))
            {
                $record = $this->find($id, ['status','user']);

                try {
                    $this->sendDealerMail($record['user'], 94);
                } 
                catch (\Exception $exception) {
                    
                    Log::useDailyFiles(storage_path().'/logs/Exception/ActivateWeb/'.carbon::now()->format('Ym').'/exception.log');
                    Log::notice('Exception'. print_r($exception->getMessage(),true));
                }

                $staff = $this->userRepo->all(['role_id'=>8, 'referral_id'=>$record['user']['id']]);

                foreach ($staff as $key => $value) {
                    try {
                        $this->sendDealerMail($value, 130);
                    } 
                    catch (\Exception $exception) {
                        
                        Log::useDailyFiles(storage_path().'/logs/error/ActivateWeb/'.carbon::now()->format('Ym').'/exception.log');
                        Log::notice('Exception'. print_r($exception->getMessage(),true));
                    }
                }

                Session::flash('success', 'Web activated successfully');
                
                $result = $this->success();
            }
            else
            {
                DB::rollback();
                $result = $this->errorUpdate();
            }
            DB::commit();
        }
            
        return $result;
    }

    public function createAddressArray($data)
    {   
        $address['business']['type_id'] = 84;  
        $address['business']['street_1'] = $data['b_street_1'];
        $address['business']['street_2'] = $data['b_street_2'];
        $address['business']['postal_code'] = $data['b_postal_code'];
        $address['business']['city'] = $data['b_city'];
        $address['business']['state_id'] = $data['b_state_id'];
        $address['business']['country_code'] = 'MY';
        if(empty($data['same_address'])){
            $address['correspondence']['type_id'] = 85; 
            $address['correspondence']['street_1'] = $data['c_street_1'];
            $address['correspondence']['street_2'] = $data['c_street_2'];
            $address['correspondence']['postal_code'] = $data['c_postal_code'];
            $address['correspondence']['city'] = $data['c_city'];
            $address['correspondence']['state_id'] = $data['c_state_id'];
            $address['correspondence']['country_code'] = 'MY';
        }
            
        return $address;
    } 

    public function generateReferralCode($user = null){
        
        if(!empty($user['role_id'])){
            $formatted_user_id = sprintf("%05d", $user['id']);

            $time = time(); 
            $substr_time = substr($time, -4);

            $split_time = str_split($substr_time,2);

            if(!empty($user['address'])){
                $region_code = $user['address']['state']['code'];
            }
            else{
                $region_code = 'XXX';
            }

            if($user['role_id'] == 8){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
            else if($user['role_id'] == 7){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
            else if($user['role_id'] == 6){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
            else if($user['role_id'] == 5){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
            else if($user['role_id'] == 4){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
            else if($user['role_id'] == 3){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
            else if($user['role_id'] == 9){
                $referral_code = $region_code.'ZZZYYYDDD'.$formatted_user_id;
            }
        }
        else{
            $referral_code = null;
        }
        
        return $referral_code;
    }

    public function sendDealerMail($user, $email_for_id)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);//staff 80 for dealer
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            $message['description'] = str_replace("%EMAIL",$user['email'],$message['description']);
            $message['description'] = str_replace("%PASSWORD",$user['password'],$message['description']);
            $message['description'] = str_replace("%BUTTON","<table class='action' align='center' width='100%' cellpadding='0' cellspacing='0'><tr><td align='center'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td align='center'><table border='0' cellpadding='0' cellspacing='0'><tr><td><a href='$link' class='button' style='background-color: #df1174;border-top: 10px solid #df1174;border-right: 18px solid #df1174;border-bottom: 10px solid #df1174;border-left: 18px solid #df1174;' target='_blank'>".$message['actionText']."</a></td></tr></table></td></tr></table></td></tr></table>",$message['description']);
            $message['description'] = str_replace("%DEALERNAME",$user['dealer']['name'],$message['description']);
            $message['description'] = str_replace("%DEALEREMAIL",$user['dealer']['email'],$message['description']);
            $message['description'] = str_replace("%DEALERADDRESS",$user['dealer_address'],$message['description']);

            if(!empty($user['transaction'])){

                $user['transaction']['date'] = (DateTime::createFromFormat('d/m/Y H:i:s', $user['transaction']['created_at']))->format('d/m/Y');

                $user['transaction']['time'] = (DateTime::createFromFormat('d/m/Y H:i:s', $user['transaction']['created_at']))->format('H:i:s');

                $message['description'] = str_replace("%AMOUNT",$user['transaction']['amount'],$message['description']);
                $message['description'] = str_replace("%DATETRANSACTION",$user['transaction']['date'],$message['description']);
                $message['description'] = str_replace("%TIMETRANSACTION",$user['transaction']['time'],$message['description']);
            }
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new Email($message));
            Notification::route('mail', 'emails@yakin2u.com')->notify(new Email($message));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/Errors/Send_Registration_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }

    public function sendDealerQRMail($user, $email_for_id)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);//staff 80 for dealer
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            $message['description'] = str_replace("%EMAIL",$user['email'],$message['description']);
            $message['description'] = str_replace("%PASSWORD",$user['password'],$message['description']);
            $message['description'] = str_replace("%BUTTON","<table class='action' align='center' width='100%' cellpadding='0' cellspacing='0'><tr><td align='center'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td align='center'><table border='0' cellpadding='0' cellspacing='0'><tr><td><a href='$link' class='button' style='background-color: #df1174;border-top: 10px solid #df1174;border-right: 18px solid #df1174;border-bottom: 10px solid #df1174;border-left: 18px solid #df1174;' target='_blank'>".$message['actionText']."</a></td></tr></table></td></tr></table></td></tr></table>",$message['description']);
            $message['description'] = str_replace("%DEALERNAME",$user['name'],$message['description']);

            if(!empty($user['transaction'])){

                $user['transaction']['date'] = (DateTime::createFromFormat('d/m/Y H:i:s', $user['transaction']['created_at']))->format('d/m/Y');

                $user['transaction']['time'] = (DateTime::createFromFormat('d/m/Y H:i:s', $user['transaction']['created_at']))->format('H:i:s');

                $message['description'] = str_replace("%AMOUNT",$user['transaction']['amount'],$message['description']);
                $message['description'] = str_replace("%DATETRANSACTION",$user['transaction']['date'],$message['description']);
                $message['description'] = str_replace("%TIMETRANSACTION",$user['transaction']['time'],$message['description']);
            }

            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new EmailAttach($message));
            Notification::route('mail', 'emails@yakin2u.com')->notify(new EmailAttach($message));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/Errors/Send_Registration_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }

    public function sendDealerAttachment($user, $email_for_id, $pdf_output)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;
            $message['file_name'] = "Receipt";

            $tier = 'Tier '.$user['device']['sku']['tier'];

            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new EmailAttachmentPDF($message, $pdf_output));
            Notification::route('mail', 'emails@yakin2u.com')->notify(new EmailAttachmentPDF($message, $pdf_output));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/error/Send_Enrollment_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }
    
  	
}