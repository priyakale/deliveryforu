<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\PaymentSetting;

class PaymentSettingRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(PaymentSetting $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
        
        return $model_query;
    }

    public function getPaymentSettingCache() { 

      $record = collect(Cache::remember('PaymentSetting', $this->expiry_time, function() {
          return $this->find(1)->toArray();
      }));

      return $record;
    }  
}