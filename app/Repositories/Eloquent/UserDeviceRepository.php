<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Notifications\Email;
use Notification;
use Validator;
use Session;
use Log;

use SkuRepo;
use UserRepo;
use EmailNotiRepo;

//MODEL
use App\Models\UserDevice;
use App\Models\Transaction;

//OTHER
use Carbon\Carbon;

class UserDeviceRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(UserDevice $model, SkuRepo $skuRepo, UserRepo $userRepo, EmailNotiRepo $emailNotiRepo)
  	{
    	$this->model = $model;
        $this->skuRepo = $skuRepo;
        $this->userRepo = $userRepo;
        $this->emailNotiRepo = $emailNotiRepo;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'customer_id' => 'user'
            ,'id' => 'device id'
          ];

        if($method ==='create'){
            $rule = [
                'customer_id' => 'required|exists:users,id'
                ,'imei' => 'required'
                ,'make' => 'required'
                ,'model' => 'required'
                ,'sku_id' => 'required'
                ,'mobile_number' => 'required'
            ];
        }
        else if($method ==='create_swap'){
            $rule = [
                'old_device_id' => 'required|exists:user_devices,id'
                ,'imei' => 'required'
                ,'make' => 'required'
                ,'model' => 'required'
                ,'color' => 'required'
            ];
        }
        else if($method ==='update'){
            $rule = [
                'id' => 'required|exists:user_devices,id'
            ];
        }
        else if($method ==='check_imei'){
            $rule = [
                'imei' => 'required'
            ];
        }
        else if($method ==='delete'){
            $rule = ['id' => 'exists:user_devices'];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        if(!empty($data['user_id']) && $data['user_id'] != ''){
          $model_query->where('user_id', $data['user_id']);
        }
        
        if(!empty($data['date_from']) && $data['date_from'] != ''){
          $model_query->where('created_at', '>=',$data['date_from']);
        }

        if(!empty($data['date_to']) && $data['date_to'] != ''){
          $model_query->where('created_at', '<=',$data['date_to']);
        }

        if(!empty($data['imei']) && $data['imei'] != ''){
          $model_query->where('imei', $data['imei']);
          $model_query->orWhere('imei_2', $data['imei']);
        }

        if(!empty($data['mobile_number']) && $data['mobile_number'] != ''){
          $model_query->where('mobile_number', $data['mobile_number']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id', $data['status_id']);
        }

        return $model_query;
    }

    public function save(array $data){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{            
            if(!empty($data['id'])){
                DB::beginTransaction();
                $validator = $this->validator($data, 'update')->validate();
                if(!$this->update($data, $data['id'])){
                    DB::rollback();
                    return $this->errorSave();
                }
                else{
                    DB::commit();
                    $record = $this->find($data['id'], ['package_fee']);
                    try {
                        $user = $this->userRepo->find($record['user_id']);
                        
                        $user['device'] = $this->find($record['id'],['sku','package_fee.package','package_fee.tier']);
                        $this->sendMail($user, 101);
                    } catch (\Exception $exception) {
                        Log::useDailyFiles(storage_path().'/logs/error/ChangeMail/'.carbon::now()->format('Ym').'/exception.log');
                        Log::notice('Exception'. print_r($exception->getMessage(),true));
                    }

                    
                }   
            }else{
                $validator = $this->validator($data, 'create')->validate();

                $mobile_number = $this->checkMDN($data['mobile_number']);

                $sku = $this->skuRepo->find($data['sku_id']);

                $device_data = array(
                    'device_purchased_date' => date('d/m/Y')
                    ,'user_id' => $data['customer_id']
                    ,'status_id' => 54
                    ,'imei' => $data['imei']
                    ,'imei_2' => $data['imei_2']
                    ,'sku_id' => $data['sku_id']
                    ,'mobile_number' => $mobile_number
                );

                $result = $this->create($device_data);
                if(!$result){
                    return $this->errorSave();
                }else{
                    $record = $this->find($result['id'], ['package_fee','sku']);
                }
            }
            
            Session::flash('success', 'Add / update User Device success'); 
        }//End else
        return $record;
    }

    public function saveSwap(array $data){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{            
            
            $validator = $this->validator($data, 'create_swap')->validate();

            DB::beginTransaction();
            $result = $this->update(['status_id'=>113],$data['old_device_id']);

            if(!$result){
                DB::rollback();
                return $this->errorSave();
            }
            else{
                $record = $this->find($data['old_device_id']);

                $start_date = Carbon::now()->format('d/m/Y');
                $addMonth = 6;
                $end_date = Carbon::now()->addMonths($addMonth)->format('d/m/Y');

                $device_data = array(
                    'device_purchased_date' => $start_date
                    ,'user_id' => $record['user_id']
                    ,'status_id' => 46
                    ,'imei' => $data['imei']
                    ,'imei_2' => $data['imei_2']
                    ,'sku_id' => $data['color']
                    ,'mobile_number' => $record['mobile_number']
                    ,'package_fee_id' => $record['package_fee_id']
                    ,'swapped_old_device_id' => $record['id']
                    ,'swap_warranty_end' => $end_date
                );

                $result = $this->create($device_data);
                if(!$result){
                    DB::rollback();
                    return $this->errorSave();
                }else{
                    Transaction::where('device_id', $result['swapped_old_device_id'])->update(['device_id' => $result['id']]);

                    DB::commit();
                    try {
                        $user = $this->userRepo->find($record['user_id']);

                        $user['device'] = $this->find($result['id'],['sku','package_fee.package','package_fee.tier']);
                        $this->sendMail($user, 103);
                    } catch (\Exception $exception) {
                        Log::useDailyFiles(storage_path().'/logs/error/SwapMail/'.carbon::now()->format('Ym').'/exception.log');
                        Log::notice('Exception'. print_r($exception->getMessage(),true));
                    }

                    $record = $this->find($result['id'], ['package_fee','sku']);
                }
            }
            
            Session::flash('success', 'Add Swap User Device success'); 
        }//End else
        return $record;
    }

    public function checkMDN($mobile_number){
        
        $record = $this->findBy(['mobile_number'=>$mobile_number]);

        $i = 1;

        while(!empty($record)){
            $mobile_number = (explode("_",$mobile_number))[0];
            $num_padded = sprintf("%02d", $i);
            $mobile_number = $mobile_number.'_'.$num_padded;

            $record = $this->findBy(['mobile_number'=>$mobile_number]);

            $i++;
        }
        
        return $mobile_number;
    }

    public function sendMail($user, $email_for_id)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;

            $tier = 'Tier '.$user['device']['sku']['tier'];

            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);

            $message['description'] = str_replace("%PLAN",strtoupper($user['device']['package_fee']['package']['name']),$message['description']);
            $message['description'] = str_replace("%PHONEMAKE",strtoupper($user['device']['sku']['make']),$message['description']);
            $message['description'] = str_replace("%PHONEMODEL",strtoupper($user['device']['sku']['model']),$message['description']);
            $message['description'] = str_replace("%TIER",strtoupper($user['device']['sku']['tier']),$message['description']);
            $message['description'] = str_replace("%IMEI",strtoupper($user['device']['imei']),$message['description']);
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new Email($message));
            Notification::route('mail', 'emails@yakin2u.com')->notify(new Email($message));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/error/Send_Enrollment_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }
}