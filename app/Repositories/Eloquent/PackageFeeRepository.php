<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\PackageFee;

class PackageFeeRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(PackageFee $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
          ];

        if($method =='show'){
            $rule = [
              'tier_id' => 'required'
            ];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {

        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['tier_id']) && $data['tier_id'] != ''){
          $model_query->where('tier_id',$data['tier_id']);
        }

        $model_query->whereHas("package" , function($q) use($data)
        {
            if(!empty($data['status_id']))
            {
                $q->where("status_id",$data['status_id']);
            }
        });

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
       
        if(empty($data)){
            return $this->errorEmptyData();
        }else{

            $validator = $this->validator($data, 'save')->validate();
            
            $this->model->where('package_id', $data['package_id'])->delete();
            
            foreach ($data['tiers'] as $key => $value) {
                $value['package_id'] = $data['package_id'];
                $result = $this->create($value);
            }
            
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update Tier success'); 
            }
        }//End else
        return $this->success();
    }
    
  	
}