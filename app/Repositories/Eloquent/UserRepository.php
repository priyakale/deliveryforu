<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Validation\Rule;
use App\Notifications\Email;
use App\Notifications\EmailAttach;
use Illuminate\Support\Facades\DB;
use Notification;
use Validator;
use Session;
use Auth;
use ActivationRepo;
use Log;

//MODEL
use App\Models\User;
use App\Models\DealerApplication;

class UserRepository extends BaseRepository 
{
    protected $model;
    
    public function __construct(User $model, ActivationRepo $activationRepo)
    {
        $this->model = $model;
        $this->activationRepo = $activationRepo;
    }

    public function validator(array $data, $method = null) 
    {
        $attributeNames = [
        ];

        $messages = [
            'unique' => 'The :attribute is already in our database.',
            'required_if' => 'The :attribute field is required.',
        ];
        
        if($method ==='show')
        {
            $rule = ['id' => 'exists:users'];
        }
        else if($method ==='delete'){
            $rule = ['id' => 'exists:users'];
        }
        else if($method === 'create')
        {
            $rule = [
                'name' => 'required|string|between:4,60'
                ,'email' => 'required_if:role_id,9,8,6|string|email|max:50|unique:users'
                ,'role_id' => 'required|in:6,7,8,9'
                ,'identification_type_id' => 'required_if:role_id,8,9|in:51,52|numeric'
                ,'identification_number' => 'required_if:role_id,8,9|string|unique:users'
                ,'mobile_number' => 'required_if:role_id,8,9|numeric'
                ,'address' => 'required_if:role_id,9|string|max:250'
            ];
        }
        else if($method === 'password_check'){
            $rule = [
                'password' => 'required|between:6,12'
            ];
        }
        else if($method === 'exist_role')
        {
            $rule = [
                'role_id' => 'required|exists:roles'
            ];
        }
        else if($method ==='check_customer'){
            $rule = [
                'identification_number' => 'required'
                ,'email' => 'required'
                ,'mobile_number' => 'required'
            ];
        }
        else if($method === 'customer_details')
        {
            $rule = [
                'identification_number' => [
                    'required',
                    Rule::exists('users')->where(function ($query) {
                        $query->where('role_id', 9);
                    }),
                ]
            ];
        }
        else if ($method === 'update')
        {
            $rule = [
                'name' => 'required|string|between:4,25'
                ,'email' => [
                    'required_if:role_id,9',
                    'email',
                    'max:50',
                    Rule::unique('users')->ignore($data['id']),
                ]
            ];

            if(!empty($data['password'])){
                $rule['password'] = 'between:6,12|confirmed';
            }
        }
        elseif($method ==='delete')
        {
            $rule = ['id' => 'exists:users'];
        }

        return Validator::make($data, $rule, $messages)->setAttributeNames($attributeNames);
    }

    public function dynamicQuery($model_query, $data)
    {
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['user_id']) && $data['user_id'] != ''){
          $model_query->where('user_id',$data['user_id']);
        }

        if(!empty($data['referral_id']) && $data['referral_id'] != ''){
          $model_query->where('referral_id',$data['referral_id']);
        }

        if(!empty($data['role_id']) && $data['role_id'] != ''){
          $model_query->where('role_id',$data['role_id']);
        }

        if(!empty($data['role_in']) && $data['role_in'] != ''){
            $model_query->whereIn('role_id', $data['role_in']);
        }

        if(!empty($data['date_from']) && $data['date_from'] != ''){
          $model_query->where('created_at', '>=',$data['date_from']);
        }

        if(!empty($data['date_to']) && $data['date_to'] != ''){
          $model_query->where('created_at','<=',$data['date_to']);
        }

        if(!empty($data['identification_number']) && $data['identification_number'] != ''){
          $model_query->where('identification_number', $data['identification_number']);
        }

        if(!empty($data['email']) && $data['email'] != ''){
          $model_query->where('email', $data['email']);
        }

        $model_query->orderBy('created_at', 'desc');

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{

            if(!empty($data['id'])){
                $this->validator($data,'update')->validate();

                if(!empty($data['password'])){
                    $data['password'] = bcrypt($data['password']);
                }
                else{
                    unset($data['password']);
                    unset($data['password_confirmation']);
                }

                $this->update($data, $data['id']);
                $result = $this->find($data['id'], ['wallet']);
            }else{

                $this->validator($data,'create')->validate();

                if($data['role_id']=="9" || $data['role_id']=="8"){ //9 customer //8 staff
                    if($data['user']['role_id']==8){//if staff referral id will go to parent referral
                        $data['referral_id'] = $data['user']['parent_referral']['id'];
                    }
                    else{
                        $data['referral_id'] = $data['user']['id'];
                    }
                    
                    if($data['role_id']=="9"){ //9 customer
                        $data['password'] = rand(100000,999999);
                        //$password = $data['password'];
                        $data['temp_password'] = $data['password']; //store in db temporarily to send in email later
                        $data['activated'] = 1;
                    }

                    if($data['role_id']=="8"){ // staff
                        $data['password'] = rand(100000,999999);
                        $password = $data['password'];
                        $data['activated'] = 1;
                    }
                }
                else if($data['role_id']=="6"){ // dealer
                    $data['referral_id'] = 4;
                    $data['password'] = rand(100000,999999);
                    $password = $data['password'];
                    $data['activated'] = 1;
                }
                
                $data['password'] = bcrypt($data['password']);
                
                $user = $this->create($data);

                if(!$user){
                    return $this->errorSave();
                }

                if($user->role_id>5 && $user->role_id<7){
                    $user['password'] = $password;
                    try {
                        $this->sendUserMail($user, 124);
                    } catch (\Exception $exception) {
                        Log::useDailyFiles(storage_path().'/logs/Exception/Send_Registration_Mail.log');
                        Log::notice('Exception : '.$exception->getMessage());
                    }
                }   
                else if($user->role_id==8 && $user->status_id==1){
                    $user['password'] = $password;
                    try {
                        $this->sendUserMail($user, 81);
                    } catch (\Exception $exception) {
                        Log::useDailyFiles(storage_path().'/logs/Exception/Send_Registration_Mail.log');
                        Log::notice('Exception : '.$exception->getMessage());
                    }
                    $dealer = $this->find($user['referral_id']);
                    $application_record = DealerApplication::where('user_id', $user['referral_id'])->with(['salesman'])->first()->toArray();
                    if(!empty($application_record['salesman'])){
                        try {
                            $application_record['salesman']['dealer'] = $dealer;
                            $application_record['salesman']['staff']['name'] = $user['name'];
                            $this->sendUserMail($application_record['salesman'], 133);
                        } 
                        catch (\Exception $exception) {

                            Log::useDailyFiles(storage_path().'/logs/Exception/Send_Staff_Active_Salesperson_Mail.log');
                            Log::notice('Exception : '.$exception->getMessage());
                        }
                    }
                        
                } 
                // else if($user->role_id=="9"){
                //     $user['password'] = $password;
                //     try {
                //         $this->sendUserMail($user, 95);
                //     } catch (\Exception $exception) {
                //         Log::useDailyFiles(storage_path().'/logs/Exception/Send_Registration_Mail.log');
                //         Log::notice('Exception : '.$exception->getMessage());
                //     }
                // }      

                $result = $this->find($user['id'], ['wallet']);
                    
            }
           
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update User success'); 
            }
        }//End else
        return $result;
    }

    public function changeStatus($id, $status)
    {     

        $record = $this->find($id, ['status']);

        if(empty($record))
        {
            return $this->errorDynamic('User not found.');
        }
        else
        {
            DB::beginTransaction();
            //update application status to 12 (Rejected) or 13 (Accepted) or 14 (Cancelled)
            if ($this->update(['status_id' => $status], $id))
            {
                $record = $this->find($id, ['status']);
                
                
                if($status==1){

                    if($record['role_id']=="8"){ // staff
                        $record['password'] = rand(100000,999999);
                        $password = $record['password'];
                        $record['password'] = bcrypt($record['password']);
                        $this->update(['password'=>$record['password']], $record['id']);

                        $record['password'] = $password;

                        try {
                            $this->sendUserMail($record, 81);
                        } 
                        catch (\Exception $exception) {
                            Log::useDailyFiles(storage_path().'/logs/Exception/Send_Staff_Active_Mail.log');
                            Log::notice('Exception : '.$exception->getMessage());
                        }

                        $dealer = $this->find($record['referral_id']);
                        $application_record = DealerApplication::where('user_id', $record['referral_id'])->with(['salesman'])->first()->toArray();
                        if(!empty($application_record['salesman'])){
                            try {
                                $user = $application_record['salesman'];
                                $user['dealer'] = $dealer;
                                $user['staff']['name'] = $record['name'];
                                $this->sendUserMail($user, 134);
                            } 
                            catch (\Exception $exception) {

                                Log::useDailyFiles(storage_path().'/logs/Exception/Send_Staff_Active_Salesperson_Mail.log');
                                Log::notice('Exception : '.$exception->getMessage());
                            }
                        }
                    }
                }
                else{
                    try {
                        $this->sendUserMail($record, 82);
                    } 
                    catch (\Exception $exception) {

                    }
                    $dealer = $this->find($record['referral_id']);
                    $application_record = DealerApplication::where('user_id', $record['referral_id'])->with(['salesman'])->first()->toArray();
                    if(!empty($application_record['salesman'])){
                        try {
                            $user = $application_record['salesman'];
                            $user['dealer'] = $dealer;
                            $user['staff']['name'] = $record['name'];
                            $this->sendUserMail($user, 134);
                        } 
                        catch (\Exception $exception) {
                            Log::useDailyFiles(storage_path().'/logs/Exception/Send_Staff_Disable_Salesperson_Mail.log');
                            Log::notice('Exception : '.$exception->getMessage());
                        }
                    }
                        
                }

                $result = $this->success();
            }
            else
            {
                DB::rollback();
                $result = $this->errorUpdate();
            }
            DB::commit();
        }
            
        return $result;
    } 

    public function getReferralId($user_id = null){
        return $this->find($user_id, ['wallet']);
    }

    public function sendUserMail($user, $email_for_id)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            $message['description'] = str_replace("%EMAIL",$user['email'],$message['description']);
            $message['description'] = str_replace("%PASSWORD",$user['password'],$message['description']);
            $message['description'] = str_replace("%BUTTON","<table class='action' align='center' width='100%' cellpadding='0' cellspacing='0'><tr><td align='center'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td align='center'><table border='0' cellpadding='0' cellspacing='0'><tr><td><a href='$link' class='button' style='background-color: #df1174;border-top: 10px solid #df1174;border-right: 18px solid #df1174;border-bottom: 10px solid #df1174;border-left: 18px solid #df1174;' target='_blank'>".$message['actionText']."</a></td></tr></table></td></tr></table></td></tr></table>",$message['description']);
            $message['description'] = str_replace("%DEALERNAME",$user['dealer']['name'],$message['description']);
            $message['description'] = str_replace("%USERNAME",$user['staff']['name'],$message['description']);
            $message['description'] = str_replace("%DATETIME",date('d/m/Y H:i:s'),$message['description']);
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new Email($message));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/Errors/Send_Registration_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }

    public function sendStaffQRMail($user, $email_for_id)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            $message['description'] = str_replace("%EMAIL",$user['email'],$message['description']);
            $message['description'] = str_replace("%PASSWORD",$user['password'],$message['description']);
            $message['description'] = str_replace("%BUTTON","<table class='action' align='center' width='100%' cellpadding='0' cellspacing='0'><tr><td align='center'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td align='center'><table border='0' cellpadding='0' cellspacing='0'><tr><td><a href='$link' class='button' style='background-color: #df1174;border-top: 10px solid #df1174;border-right: 18px solid #df1174;border-bottom: 10px solid #df1174;border-left: 18px solid #df1174;' target='_blank'>".$message['actionText']."</a></td></tr></table></td></tr></table></td></tr></table>",$message['description']);
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new EmailAttach($message));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/Errors/Send_Staff_Active_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }

    public function checkPassword($data = null){

        $this->validator($data,'password_check')->validate();

        if (Auth::guard('web')->attempt(['email' => $data['user']['email'], 'password' => $data['password']]) == false) {
            return array('status' => 'false');
        }
        Auth::guard('web')->logout();
        return array('status' => 'true');
    }
    
    public function updateDevice($user_id,$register_id)
    {
        $record = User::where('id',$user_id)
                        ->update(['register_id' => $register_id]);
        return $record;
    }  
}