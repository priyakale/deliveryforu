<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\Country;

class CountryRepository extends BaseRepository 
{
    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(Country $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['country_code']) && $data['country_code'] != ''){
          $model_query->where('country_code',$data['country_code']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }
        
        $model_query->orderByRaw('position = 0, position asc');
       // $model_query->orderBy('position', 'asc');

        return $model_query;
    }

    public function findCountryCache(int $id) { 
      if (!is_int($id)) { 
        return null; 
      } 

      $country = collect(Cache::remember('country', $this->expiry_time, function() {
          return $this->all()->toArray();
      }));

      return $country->where('id', $id)->first();
    } 

    public function getCountryCache(string $category = "") { 
      
      $country = Cache::remember('country', $this->expiry_time, function() 
      {
          return $this->all();
      });
      
      return $country;
    }
  	
}