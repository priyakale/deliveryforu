<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Notifications\Email;
use App\Helpers\SmsHelper;
use Validator;
use Session;
use Notification;
use Log;

//REPO
use UserRepo;
use EmailNotiRepo;
use PaymentSettingRepo;

//MODEL
use App\Models\Transaction;

//OTHER
use Carbon\Carbon;

class TransactionRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(Transaction $model, UserRepo $userRepo, EmailNotiRepo $emailNotiRepo, PaymentSettingRepo $paymentSettingRepo)
  	{
    	$this->model = $model;
        $this->userRepo = $userRepo;
        $this->emailNotiRepo = $emailNotiRepo;
        $this->paymentSettingRepo = $paymentSettingRepo;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
          ];

        if($method ==='show'){
            $rule = ['id' => 'exists:transactions'];
        }
        else if($method ==='enrollment'){
            $rule = array(
                'package_fee_id' => 'required|integer',
                'device_id' => 'required|integer',
                'payment_method' => 'required|string',
            );
        } 
        else if($method ==='deposit'){
            $rule = array(
                'amount' => 'required|numeric|min:1',
                'description' => 'required',
            );
        }
        else if($method ==='renewal'){
            $rule = array(
                'package_fee_id' => 'required|integer',
                'device_id' => 'required|integer',
                'payment_method' => 'required|string',
            );
        }
        else if($method ==='termination'){
            $rule = array(
                'device_id' => 'required|integer'
            );
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {

        // if(!empty($data['user_id']) && $data['user_id'] != ''){
        //   $model_query->whereMonth('created_at', '=', $month);
        // }

        if(!empty($data['user_id']) && $data['user_id'] != ''){
            $model_query->where('user_id', $data['user_id']);
        }

        if(!empty($data['merchant_id']) && $data['merchant_id'] != ''){
            $model_query->orWhere('merchant_id', $data['merchant_id']);
        }

        if(!empty($data['salesperson_id']) && $data['salesperson_id'] != ''){
            $model_query->orWhere('salesperson_id', $data['salesperson_id']);
        }

        if(!empty($data['month']) && !empty($data['year'])){
          $model_query->where('created_at', '>=',$data['year'].':'.$data['month'].':01 00:00:00');
          $model_query->where('created_at', '<=',$data['year'].':'.$data['month'].':31 23:59:59');
        }

        if(!empty($data['date_from']) && $data['date_from'] != ''){
          $model_query->where('created_at', '>=',$data['date_from']);
        }

        if(!empty($data['date_to']) && $data['date_to'] != ''){
          $model_query->where('created_at', '<=',$data['date_to']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
            $model_query->where('status_id', $data['status_id']);
        }

        $model_query->orderBy('id', 'desc');

        return $model_query;
    }

    public static function GenerateReferenceNumber($transaction_id = null){

        if(!empty($transaction_id)){
            $formatted_transaction_id = sprintf("%08d", $transaction_id);

            $time = time(); 
            $substr_time = substr($time, -4);

            $split_time = str_split($substr_time,2);

            $reference_number = $split_time[0].$formatted_transaction_id.$split_time[1];
        }
            
        
        return $reference_number;

    }

    public function getMonth(){
        $date_array[] = '1-'.date('m-Y');

        $first_trans_date[] = '1-'.date('m-Y', strtotime("-2 months"));

        for ($i=1; $i < 6 ; $i++) { 
            $exp_key = explode('-', $date_array[$i-1]);
            $exp_key_2 = explode('-', $first_trans_date[0]);

            if($exp_key[1].'-'.$exp_key[2]!=$exp_key_2[1].'-'.$exp_key_2[2]){
                if($exp_key[1]<02){
                    $exp_key[1] = 12;  
                    $exp_key[2] = $exp_key[2] - 1;    
                }
                else{
                    $exp_key[1] = $exp_key[1] - 1;    
                }
                $exp_key[1] = str_pad($exp_key[1], 2, "0", STR_PAD_LEFT);
                $date_array[$i] = $exp_key[0].'-'.$exp_key[1].'-'.$exp_key[2];
            }
            else{
                $i=6;
            }                
        }        

        foreach ($date_array as $key => $value) {
            $time = strtotime($value);
            $output_array[$key]['month_number'] = date('m',$time);
            $output_array[$key]['month_name'] = date('F',$time);
            $output_array[$key]['year'] = date('Y',$time);
        }

        return $output_array;
    }

    public function postUpay($transaction){

        $transaction_id = $transaction['id'];
        $trxdescs = $transaction['description'].' Top Up #'.$transaction_id.' , amount RM '.$transaction['amount'];
        //Upay Setting
        $record_setting = $this->paymentSettingRepo->find(1);
        $is_test = ($record_setting->mode != 'L') ? 'Y' : 'N';
        
        $upay_merchantID = $record_setting->merchant_identify;
        $upay_verifykey = $record_setting->verify_key; 

        $signature = $upay_verifykey . $upay_merchantID . $transaction['amount'] . $transaction['receiver_wallet']['user_id'] . $transaction_id;
        $signature = hash('SHA512', $signature);
        $signature = str_pad($signature, 128, '0', STR_PAD_LEFT);
        // $signature = 'ffc20e07d492f46d7745a555d4a697208f618d7132a9c1d9c2a7f191da7dba97bac749ea5d91b5ea5000c73e71df040348cab72de1888e086d65f0d78a9d56db';

        if($record_setting->url_type == 'L'){///LIVE URL
            $upay_url = "https://www.upay2us.com/iServeGateway/transaction_window";
        }else{///UAT URL
            $upay_url = "https://www.upay2us.com/iServeGateway_UAT/transaction_window";
            //$upay_url = "http://101.99.65.112:91/iServeGateway_UAT/transaction_window";
        }

        //Return URL
        $http=(isset($_SERVER['HTTPS']) ? 'https://' : 'http://');
          $return_url=$http.$_SERVER['HTTP_HOST']."/wallet/status";
       
        $form_data = array(
                'MERCHANT_IDENTIFIER' => $upay_merchantID,
                'AMOUNT' => $transaction['amount'],
                'TXN_DESC' => $trxdescs , 
                'CUSTOMER_ID' => $transaction['receiver_wallet']['user_id'],
                'ORDER_ID' => $transaction_id,
                'INSTALLMENT' => 0, //installment not ready yet
                'CUSTOMER_NAME' => $transaction['receiver_wallet']['user']['name'],
                'CUSTOMER_EMAIL' =>  $transaction['receiver_wallet']['user']['email'], 
                'CUSTOMER_COUNTRY' =>  '',
                'CUSTOMER_BILLING_ADDRESS' => '',
                'CUSTOMER_SHIPPING_COST' => '',
                'CUSTOMER_IP' => $transaction['ip'], //
                    'CALLBACK_URL' => $return_url,
                    'TXN_SIGNATURE' => $signature,
                    'IS_TEST' => $is_test,
                    'SOURCE_FROM' => 'WEB'
            );

        //dd($form_data);

        Log::useDailyFiles(storage_path().'/logs/success/WalletReload/'.carbon::now()->format('Ym').'/payment.log');
        Log::notice('Payment form data'. print_r($form_data,true));

echo <<< EOT
<html>
<body onLoad="document.process.submit();">
    <form action="{$upay_url}" method="post" name="process">
EOT;
foreach ($form_data as $key_form => $value_form) {
    echo <<< EOT
        <input type=hidden name="{$key_form}" value="{$value_form}">
EOT;
}
echo <<< EOT
    </form>
</body>
</html>
EOT;
    }
  	
}