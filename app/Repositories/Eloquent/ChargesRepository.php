<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\Charges;

class ChargesRepository extends BaseRepository 
{
    protected $model;

    public function __construct(Charges $model)
    {
        $this->model = $model;
    }

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
            ,'role_id' => 'role'
          ];

        if($method =='save'){
            $rule = [
              'debit_card' => 'required'
              ,'credit_card' => 'required'
              ,'fpx' => 'required'
              ,'convenience_fee' => 'required'
            ];
        }
        elseif($method ==='delete'){
            $rule = ['id' => 'exists:charges'];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['user_id']) && $data['user_id'] != ''){
          $model_query->where('user_id',$data['user_id']);
        }

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            $validator = $this->validator($data, 'save')->validate();
            
            if(!empty($data['id'])){
                $result = $this->update($data, $data['id']);
            }else{
                $result = $this->create($data);
                $data['id'] = $result['id'];
            }
           
            if(!$result){
                return $this->errorSave();
            }else{
                $result = $this->find($data['id']);
                Session::flash('success', 'Add / update Charges success'); 
            }
        }//End else
        return $result;
    }
    
}