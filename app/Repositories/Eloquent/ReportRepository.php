<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\PackageFee;

class ReportRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(PackageFee $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [];

        $rule = [];

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {

        return $model_query;
    }

    
    
  	
}