<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\FavouriteMerchant;

class FavouriteMerchantRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(FavouriteMerchant $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
          ];

        if($method =='save'){
            $rule = [
              'user_id' => 'required|exists:users,id'
              ,'merchant_id' => 'required|exists:merchants,id'
              ,'salesperson_id' => 'sometimes|nullable|exists:users,id'
            ];
        }
        elseif($method ==='delete'){
            $rule = [
              'user_id' => 'required|exists:users,id'
              ,'merchant_id' => 'required|exists:merchants,id'
            ];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['user_id']) && $data['user_id'] != ''){
          $model_query->where('user_id',$data['user_id']);
        }

        if(!empty($data['merchant_id']) && $data['merchant_id'] != ''){
          $model_query->where('merchant_id',$data['merchant_id']);
        }

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            $validator = $this->validator($data, 'save')->validate();
            
            if(!empty($data['id'])){
                $result = $this->update($data, $data['id']);
                $result = $this->find($data['id']);
            }else{
                $result = $this->create($data);
            }
            
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update Favourite Merchant success'); 
            }
        }//End else
        return $result;
    }
    
  	
}