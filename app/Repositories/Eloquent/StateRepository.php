<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\State;

class StateRepository extends BaseRepository 
{
    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(State $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['country_code']) && $data['country_code'] != ''){
          $model_query->where('country_code',$data['country_code']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        return $model_query;
    }

    public function findStateCache(int $id) { 
      if (!is_int($id)) { 
        return null; 
      } 

      $state = collect(Cache::remember('state', $this->expiry_time, function() {
          return $this->all()->toArray();
      }));

      return $state->where('id', $id)->first();
    } 

    public function getStateCache(string $category = "") { 
      
      $state = Cache::remember('state', $this->expiry_time, function() 
      {
          return $this->all();
      });
      
      return $state;
    }

    public function getRegionMY(){
        return Cache::remember('RegionMY', $this->expiry_time, function() {
          return $this->all(['status_id' => '1', 'country_code' => 'MY'])->toArray();
        });
    }
  	
}