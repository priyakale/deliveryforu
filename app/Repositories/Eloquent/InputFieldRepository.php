<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\InputField;
use App\Models\InputFieldOption;

class InputFieldRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(InputField $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {       
          $attributeNames = [
            'status_id' => 'status'
            ,'data_type_id' => 'data type'
          ];

        if($method =='show'){
            $rule = [
              'name' => 'required'
              ,'status_id' => 'required'
              ,'data_type_id' => 'required'
            ];

            $rule['options.*.name'] = 'required_if:data_type_id,73';
        }
        elseif($method ==='delete'){
            $rule = ['id' => 'exists:input_fields'];
        }

          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['field_name']) && $data['field_name'] != ''){
          $model_query->where('field_name',$data['field_name']);
        }

        if(!empty($data['data_type_id']) && $data['data_type_id'] != ''){
          $model_query->where('data_type_id',$data['data_type_id']);
        }

        if(!empty($data['merchant_id']) && $data['merchant_id'] != ''){
          $model_query->where('merchant_id',$data['merchant_id']);
        }

        if(!empty($data['exclude_image']) && $data['exclude_image'] != ''){
          $model_query->where('data_type_id','!=',$data['exclude_image']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
       
        if(empty($data)){
            return $this->errorEmptyData();
        }else{

            $validator = $this->validator($data, 'save')->validate();
            
            if(!empty($data['id'])){
              
                $this->update($data, $data['id']);
                $result = $this->find($data['id']);

            }else{
                $result = $this->create($data);
            }

            if(!empty($data['options'])){
                foreach ($data['options'] as $key => $value) {
                  
                    $data['name'] = $value['name'];
                    $data['field_id'] = $result['id'];

                    $model = new InputFieldOption;

                    if(!empty($value['id'])){
                        $model = $model->find($value['id']);
                    }
                    
                    $model->fill($data);
                    $model->save();
                }
            }
            
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update Input Field success'); 
            }
        }//End else
        return $result;
    }
    
    
}