<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\Package;

class PackageRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(Package $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'customer_type_id' => 'customer type'
            ,'package_type_id' => 'package type'
            ,'payment_month_id' => 'payment month'
            ,'status_id' => 'status'
          ];

        if($method ==='save'){
            $rule = [
              'name' => 'required'
              ,'customer_type_id' => 'required'
              ,'package_type_id' => 'required'
              ,'payment_month_id' => 'required'
              ,'status_id' => 'required'
            ];
        }
        elseif($method ==='delete'){
            $rule = ['id' => 'exists:packages'];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['name']) && trim($data['name']) != ''){
          $model_query->where('name', 'LIKE', '%'. trim($data['name']).'%');
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            $validator = $this->validator($data, 'save')->validate();
            
            if(!empty($data['id'])){
                $this->update($data, $data['id']);
                $result = $this->find($data['id']);
            }else{
                $result = $this->create($data);
            }
            
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update Package success'); 
            }
        }//End else
        return $result;
    }
    
  	
}