<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Validator;
use Session;
use Auth;

//MODEL
use App\Models\Address;

class AddressRepository extends BaseRepository 
{
    protected $model;
    
    public function __construct(Address $model)
    {
        $this->model = $model;
    }

    public function validator(array $data, $method = null) 
    {
        $attributeNames = [
            //'user_id' => 'user'
        ];
        
        $rule = [];

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);
    }

    public function dynamicQuery($model_query, $data)
    {
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['user_id']) && $data['user_id'] != ''){
          $model_query->where('user_id', $data['user_id']);
        }

        if(!empty($data['type_id']) && $data['type_id'] != ''){
          $model_query->where('type_id', $data['type_id']);
        }

        return $model_query;
    }

    public function save($data = [])
    {        
        //If have file
        if (!empty($data))
        {
            if(!empty($data['id'])){
                $record = $this->update($data, $data['id']);
            }else{
                $record = $this->create($data);
                $data['id'] = $record['id'];
            }

            if(!$record)
            {
                return $this->errorSave('Error saving the address!');
            }
                
            $result = $this->find($data['id']);
        }
        else{
            $result = $this->errorEmptyData('No address to save');
        }
        //End if have file

        return $result;
    }

}