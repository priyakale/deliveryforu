<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\TrainingMaterial;

class MaterialRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(TrainingMaterial $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
          ];

        if($method =='save_pdf'){
            $rule = [
              'upload-1' => 'required|mimes:pdf|max:3000'
              ,'description-1' => 'required'
            ];

            if(!empty($data['upload-1']))
            {
                $rule['upload-1'] = 'mimes:pdf|max:3000';
                $rule['description-1'] = 'required';
            }

            if(!empty($data['upload-2']))
            {
                $rule['upload-2'] = 'mimes:pdf|max:3000';
                $rule['description-2'] = 'required';
            }

            if(!empty($data['upload-3']))
            {
                $rule['upload-3'] = 'mimes:pdf|max:3000';
                $rule['description-3'] = 'required';
            }
        }
        else if($method =='save_video'){
            $rule = [
              'upload-1' => 'required|mimetypes:video/avi,video/mpeg,video/mp4|max:20000'
              ,'description-1' => 'required'
            ];

            if(!empty($data['upload-1']))
            {
                $rule['upload-1'] = 'mimetypes:video/avi,video/mpeg,video/mp4|max:20000';
                $rule['description-1'] = 'required';
            }

            if(!empty($data['upload-2']))
            {
                $rule['upload-2'] = 'mimetypes:video/avi,video/mpeg,video/mp4|max:20000';
                $rule['description-2'] = 'required';
            }

            if(!empty($data['upload-3']))
            {
                $rule['upload-3'] = 'mimetypes:video/avi,video/mpeg,video/mp4|max:20000';
                $rule['description-3'] = 'required';
            }
        }
        else if($method ==='delete'){
            $rule = ['id' => 'exists:training_materials'];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['name']) && trim($data['name']) != ''){
          $model_query->where('name', 'LIKE', '%'. trim($data['name']).'%');
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        return $model_query;
    }

    public function save(array $data = [], $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            if($data['type']==50){
                $validator = $this->validator($data, 'save_pdf')->validate();
            }
            else if($data['type']==49){
                $validator = $this->validator($data, 'save_video')->validate();   
            }

            //If request has file save it    
            if (!empty($file_upload)) {

                $folder_type = "training-material";
                $resultSave = $this->saveFile($file_upload, $folder_type, $data['user_id']);
                
                foreach ($resultSave as $key => $value) {

                    $material_data = array(
                        'user_id' => $data['user_id']
                        ,'filename' => $value['image_name']
                        ,'material_type_id' => $data['type']
                        ,'description' => $data['description-'.$key]
                        ,'status_id' => 1
                    );

                    $result = $this->create($material_data);

                    if(!$result){
                        return $this->errorSave('Error saving the image!');
                    }
                }  
            }
            //End if request has file
            
            Session::flash('success', 'Add / update Training Material success'); 
        }//End else
        return $this->success();
    }
    
  	
}