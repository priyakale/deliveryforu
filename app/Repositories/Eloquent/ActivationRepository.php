<?php
namespace App\Repositories\Eloquent;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Email;
use Carbon\Carbon;
use Notification;
use Log;

//MODEL
use App\Models\UserActivation;
use App\Models\User;
use App\Models\EmailNotification;

class ActivationRepository {

    use Notifiable;

    /**
    * @var Model
    */
    protected $model;

    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 5; //5 in minutes

    public function __construct(UserActivation $model)
    {
        $this->model = $model;
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function createActivation($user)
    {

        $activation = $this->getActivation($user);

        if (!$activation) {
            return $this->createToken($user);
        }
        return $this->regenerateToken($user);

    }

    private function regenerateToken($user)
    {

        $token = $this->getToken();
        $this->model->where('user_id', $user->id)->update([
            'token' => $token,
            'created_at' => new Carbon()
        ]);
        return $token;
    }

    private function createToken($user)
    {
        $token = $this->getToken();
        $this->model->insert([
            'user_id' => $user->id,
            'token' => $token,
            'created_at' => new Carbon()
        ]);
        return $token;
    }

    public function getActivation($user)
    {
        return $this->model->where('user_id', $user->id)->first();
    }


    public function getActivationByToken($token)
    {
        return $this->model->where('token', $token)->first();
    }

    public function deleteActivation($token)
    {
        $this->model->where('token', $token)->delete();
    }

    public function activateUser($token)
    {
        $activation = $this->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        if (strtotime($activation->created_at) + 60 * $this->resendAfter < time()) {
            return 'expired';
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->deleteActivation($token);

        return 'activated';

    }

    private function shouldSend($user)
    {
        $activation = $this->getActivation($user);
        
        return $activation === null || strtotime($activation->created_at) + 60 * $this->resendAfter < time();
    }

    public function sendActivationMail($user)
    {   
        $response['code'] = 1; //Failed

        if ($user->activated == 1) {
            $response['code'] = 0; //Success
            $response['message'] = 'This account is already activated.'; 

            return $response;
        }

        if (!$this->shouldSend($user)) {

            $response['message'] = 'We have already sent you an activation email earlier. To avoid multiple activation email requests, please wait 5 minutes before making another request.'; 
            return $response;
        }
        $token = $this->createActivation($user);

        $link = route('user.activate', $token);
        
        // $message = array("Activation Mail", sprintf('Activate account <a href="%s">%s</a>', $link, $link),$link);

        $email_notification = EmailNotification::where('name','Activation')
            ->where('category','Email Notification')
            ->first();
      
        if(!empty($email_notification)){
            $message['actionText'] = "Click to Activate";
            $message['subject'] = $email_notification['subject'];
            $message['actionUrl'] = $link;
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            $message['description'] = str_replace("%EMAIL",$user['email'],$message['description']);
            $message['description'] = str_replace("%BUTTON","<table class='action' align='center' width='100%' cellpadding='0' cellspacing='0'><tr><td align='center'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td align='center'><table border='0' cellpadding='0' cellspacing='0'><tr><td><a href='$link' class='button button-blue' target='_blank'>".$message['actionText']."</a></td></tr></table></td></tr></table></td></tr></table>",$message['description']);
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            Notification::send($user, new Email($message));
            $response['code'] = 0; //Success
            $response['message'] = 'We have sent you an activation email.Please check your email.';
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/Errors/Send_Activation_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $response;
    }

}