<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use Validator;
use Session;
use File;

//MODEL
use App\Models\Merchant;
use App\Models\User;
use App\Models\MerchantInputField;
use App\Models\MerchantOutlet;
use App\Models\InputField;
use App\Models\InvestorDetail;
use App\Models\MerchantBankAccount;

class MerchantRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(Merchant $model)
  	{
    	$this->model = $model;
  	}

     
    public function validator(array $data, $method = null) 
    {        
        
        $attributeNames = [
          'status_id' => 'status',
          'sub_merchant_ref'=>'sub merchant identifier'
        ];

        $customMessages = [
                'sub_merchant_ref.required'=>'Please enter the Sub Merchant Identifier',
                'user_email_id.required'=>'Please enter your email address (An email address must contain a single @)',
                'user_password.required'=>'Please enter your password',
                'user_password_confirmation.required'=>'Please confirm the password',
                'company_registered_name.required'=>'Please enter your company registered name',
                'business_registration_number.required'=>'Please enter your business registration number',
                'trading_name.required'=>'Please enter your trading name',
                'years_in_business.required'=>'Please enter your year(s) in business',
                'headquarters_contact_number.required'=>'Please enter your headquater contact number',
                'salesperson_name.required'=>'Please enter your salesperson name',
                'headquarters_address.required'=>'Please enter your headquarter address',
                'bank_name.required'=>'Please select your bank',
                'acc_number.required'=>'Please enter your bank account number ',
                'acc_holder_name.required'=>'Please enter your bank account holder name',
                'acc_finance_contact_number.required'=>'Please enter your finance contact person',
                'acc_contact_number.required'=>'Please enter your contact number',
                'acc_email.required'=>'Please enter your email address' ,
                'individual_name.required'=>'Please enter your name',
                'ic_number.required'=>'Please enter your identification card number',
                'merchant_email_id.required'=>'Please enter your email ID',
                'phone.required'=>'Please enter your mobile number',
                'individual_address.required'=>'Please enter your address',
                'branch_information.required'=>'Please enter your branch information',
                'ind_contact_number.required'=>'Please enter your contact number',
               
            ];

        if($method =='save'){
            $rule = [
              'user_email_id' => 'required|unique:users,email',
              'user_password' => 'required|confirmed|min:6|max:12',
              'user_password_confirmation' => 'required|min:6|max:12',
              'company_registered_name' => 'required',
              'business_registration_number' => 'required',
              'trading_name' => 'required',
              'years_in_business' => 'required',
              'headquarters_contact_number' => 'required',
              'salesperson_name' => 'required',
              'headquarters_address' => 'required',
              'logo' => 'required|mimes:png,jpg,jpeg|max:3000|dimensions:max_width=432,max_height=432',
              'type_of_ownership' => 'required',
              'type_of_business' => 'required',
              'bank_name' => 'required',
              'acc_number' => 'required',
              'acc_holder_name' => 'required',
              'acc_finance_contact_number' => 'required',
              'acc_contact_number' => 'required',
              'acc_email' => 'required|email',
              //'outlets.*.outlet_pic_name'=>'required',
             // 'outlets.*.outlet_pic_password'=>'required|min:6|max:12',
             // 'outlets.*.outlet_pic_email'=>'required|unique:users,email',
              'sub_merchant_ref'=>'required',
              'status_id'=>'required',
             // 'sign_off'=>'required',
             // 'end_date'=>'required',
            ];

            foreach ($data['outlets'] as $key => $value) {
              if (!empty($value['logo']) || !empty($value['outlet_name']) || !empty($value['outlet_pic_name']) || !empty($value['outlet_pic_mobile_number']) || !empty($value['outlet_pic_id_num']) || !empty($value['outlet_pic_password']) || !empty($value['outlet_pic_email'])) {
                    $rule['outlets.'.$key.'.logo'] = 'required';
                    $rule['outlets.'.$key.'.outlet_name'] = 'required';
                    $rule['outlets.'.$key.'.outlet_pic_name'] = 'required';
                    $rule['outlets.'.$key.'.outlet_pic_mobile_number'] = 'required';
                    $rule['outlets.'.$key.'.outlet_pic_id_num'] = 'required';
                    $rule['outlets.'.$key.'.outlet_pic_password'] = 'required|confirmed|min:6|max:12';
                    $rule['outlets.'.$key.'.outlet_pic_email'] = 'required|email|unique:users,email';
                    $rule['outlets.'.$key.'.outlet_address'] = 'required';
                }
            }            
        }
        else if($method =='update'){
            $rule = [
              'logo' => 'sometimes|nullable|mimes:png,jpg,jpeg|max:3000|dimensions:max_width=432,max_height=432',
              'user_email_id' => 'required|email',
              'user_password' => 'sometimes|nullable|confirmed|min:6|max:12',
              'user_password_confirmation' => 'sometimes|nullable|min:6|max:12',
              'company_registered_name' => 'required',
              'business_registration_number' => 'required',
              'trading_name' => 'required',
              'years_in_business' => 'required',
              'headquarters_contact_number' => 'required',
              'salesperson_name'=>'required',
              'headquarters_address' => 'required',
              'type_of_ownership' => 'required',
              'type_of_business' => 'required',
              'bank_name' => 'required',
              'acc_number' => 'required',
              'acc_holder_name' => 'required',
              'acc_finance_contact_number' => 'required',
              'acc_contact_number' => 'required',
              'acc_email' => 'required|email',
              //'outlets.*.outlet_pic_name'=>'required',
              //'outlets.*.outlet_pic_password'=>'required|min:6|max:12',
              //'outlets.*.outlet_pic_email'=>'required|unique:users,email',
              'sub_merchant_ref'=>'required',
              'status_id'=>'required',
             // 'sign_off'=>'required',
             // 'end_date'=>'required',
            ];

            if (!empty($input['id'])) {
                
                $rule['user_email_id'][] = 'required|email';
                $rule['user_email_id'][] = Rule::unique('users.email')->ignore($input['user_id'], 'id');
            }

            foreach ($data['outlets'] as $key => $value) {
                if(!empty($value['id'])){
                      $rule['outlets.'.$key.'.outlet_name'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_name'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_mobile_number'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_id_num'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_password'] = 'sometimes|nullable|confirmed|min:6|max:12';
                      $rule['outlets.'.$key.'.outlet_address'] = 'required';

                      $rule['outlets.'.$key.'.outlet_pic_email'][] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_email'][] = 'email';
                      // $rule['outlets.'.$key.'.outlet_pic_email'][] = Rule::unique('users.email')->ignore($value['user_id'], 'id');
                }
                else if (!empty($value['outlet_name']) || !empty($value['outlet_pic_name']) || !empty($value['outlet_pic_mobile_number']) || !empty($value['outlet_pic_id_num']) || !empty($value['outlet_pic_password']) || !empty($value['outlet_pic_email'])) {
                      $rule['outlets.'.$key.'.outlet_name'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_name'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_email'] = 'required|email|unique:users,email';
                      $rule['outlets.'.$key.'.outlet_pic_mobile_number'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_id_num'] = 'required';
                      $rule['outlets.'.$key.'.outlet_pic_password'] = 'required|confirmed|min:6|max:12';
                      $rule['outlets.'.$key.'.outlet_address'] = 'required';
                }

               /* $customMessage= [     
                  $value['outlet_name'].'required'=>'Please enter name',
                  $value['outlet_pic_name'].'required'=>'Please enter salesperson name',
                  $value['outlet_pic_email'].'required'=>'Please enter email address',
                  $value['outlet_pic_mobile_number'].'required'=>'Please enter mobile number',
                  $value['outlet_pic_id_num'].'required'=>'Please enter identification number',
                  $value['outlet_pic_password'].'required'=>'Please enter password',                  
                  $value['outlet_address'].'required'=>'Please enter address'
               ];
              // dd($customMessage);
               return Validator::make($data, $rule, $customMessage)->setAttributeNames($attributeNames);
                */
            } 

        }
        else if($method =='save_individual'){
          $rule = [
            'individual_merchant_logo' => 'required|mimes:png,jpg,jpeg|max:3000|dimensions:max_width=432,max_height=432',
            'merchant_email_id' => 'required|unique:users,email',
            'user_password' => 'required|confirmed|min:6|max:12',
            'user_password_confirmation' => 'required|min:6|max:12',
            'individual_name' => 'required',
            'ic_number'=>'required',
            'phone'=>'required',
            'trading_name'=>'required',
            'type_of_business'=>'required',
            'years_in_business'=>'required|numeric',
            'individual_address'=>'required',
            'status_id'=>'required',
            'bank_name'=>'required',
            'branch_information'=>'required',
            'acc_number'=>'required',
            'acc_holder_name'=>'required',
            'ind_contact_number'=>'required',
            'acc_email'=>'required|email',
            'sub_merchant_ref'=>'required',
           // 'sign_off'=>'required',
          //  'end_date'=>'required',
          ];

        }
        else if($method =='update_individual'){
            $rule = [
              'logo' => 'sometimes|nullable|mimes:png,jpg,jpeg|max:3000|dimensions:max_width=432,max_height=432',
              'user_password' => 'sometimes|nullable|confirmed|min:6|max:12',
              'user_password_confirmation' => 'sometimes|nullable|min:6|max:12',
              'individual_name' => 'required',
              'ic_number'=>'required',
              'merchant_email_id'=>'required|email',
              'phone'=>'required',
              'trading_name'=>'required',
              'type_of_business'=>'required',
              'years_in_business'=>'required',
              'individual_address'=>'required',
              'status_id'=>'required',
              'bank_name'=>'required',
              'branch_information'=>'required',
              'acc_number'=>'required',
              'acc_holder_name'=>'required',
              'ind_contact_number'=>'required',
              'acc_email'=>'required|email',
              'sub_merchant_ref'=>'required',
             // 'sign_off'=>'required',
             // 'end_date'=>'required',
            ];

            if (!empty($input['id'])) {
                
                $rule['user_email_id'][] = 'required';
                $rule['user_email_id'][] = Rule::unique('users.email')->ignore($input['user_id'], 'id');
            }

        }
        // else if($method ==='outlet_save'){

        //     $rule = [];
        //     foreach ($data['outlets'] as $key => $value) {
        //       if (!empty($value['outlet_name']) || !empty($value['outlet_pic_name']) || !empty($value['outlet_pic_mobile_number']) || !empty($value['outlet_pic_id_num']) || !empty($value['outlet_pic_password']) || !empty($value['outlet_pic_email'])) {
        //             $rule['outlets.'.$key.'.outlet_name'] = 'required';
        //             $rule['outlets.'.$key.'.outlet_pic_name'] = 'required';
        //             $rule['outlets.'.$key.'.outlet_pic_mobile_number'] = 'required';
        //             $rule['outlets.'.$key.'.outlet_pic_id_num'] = 'required';
        //             $rule['outlets.'.$key.'.outlet_pic_password'] = 'required|confirmed|min:6|max:12';
        //             $rule['outlets.'.$key.'.outlet_pic_email'] = 'required|email';
        //             $rule['outlets.'.$key.'.outlet_address'] = 'required';
        //         }
        //     }
                


        //     // $rule = [
        //     //   'outlets.*.outlet_name'=>'required',
        //     //   'outlets.*.outlet_pic_name'=>'required',
        //     //   'outlets.*.outlet_pic_mobile_number'=>'required',
        //     //   'outlets.*.outlet_pic_id_num'=>'required',
        //     //   'outlets.*.outlet_pic_password'=>'sometimes|nullable|confirmed|min:6|max:12',
        //     //   'outlets.*.outlet_pic_email'=>'required|email',                
        //     // ];
        // } 
        else if($method ==='delete'){
            $rule = ['id' => 'exists:merchants'];
        }
        else{

        }
        //dd($customMessages);
        return Validator::make($data, $rule, $customMessages)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['company_registered_name']) && $data['company_registered_name'] != ''){
          $model_query->where('company_registered_name',$data['company_registered_name']);
        }

        if(!empty($data['business_registration_number']) && $data['business_registration_number'] != ''){
          $model_query->where('business_registration_number',$data['business_registration_number']);
        }

        if(!empty($data['trading_name']) && $data['trading_name'] != ''){
          $model_query->where('trading_name',$data['trading_name']);
        }

        if(!empty($data['headquarters_address']) && $data['headquarters_address'] != ''){
          $model_query->where('headquarters_address',$data['headquarters_address']);
        }
        
        if(!empty($data['headquarters_contact_number']) && $data['headquarters_contact_number'] != ''){
          $model_query->where('headquarters_contact_number',$data['headquarters_contact_number']);
        }
        
        if(!empty($data['type_of_ownership']) && $data['type_of_ownership'] != ''){
          $model_query->where('type_of_ownership',$data['type_of_ownership']);
        }
        
        if(!empty($data['type_of_business']) && $data['type_of_business'] != ''){
          $model_query->where('type_of_business',$data['type_of_business']);
        }
        
        if(!empty($data['years_in_business']) && $data['years_in_business'] != ''){
          $model_query->where('years_in_business',$data['years_in_business']);
        }

        if(!empty($data['salesperson_name']) && $data['salesperson_name'] != ''){
          $model_query->where('salesperson_name',$data['salesperson_name']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        if(!empty($data['merchant_id'])){
          $model_query->where('merchant_id',$data['merchant_id']);
        }   

        if(!empty($data['type_id']) && $data['type_id'] == 71){
          $model_query->where('type_id',$data['type_id']);
          $model_query->orderBy('trading_name', 'asc');
          $model_query->orderBy('position', 'asc');          
        }else if(!empty($data['type_id']) && $data['type_id'] == 72){
          $model_query->where('type_id',$data['type_id']);
          $model_query->orderBy('trading_name', 'asc');
          $model_query->orderBy('position', 'asc');           
        }
        else{
          $model_query->orderBy('position', 'asc'); 
        }
        
          
        return $model_query;
    }

    public function save(array $data, $file_upload = []){
      
        if(empty($data)){ 
            return $this->errorEmptyData();
        }else{
            
            DB::beginTransaction(); 
            if(!empty($data['id'])){
                $validator = $this->validator($data, 'update')->validate();

                $this->update($data, $data['id']);
                $result = $this->find($data['id']);

                $user = User::find($result['user_id']);
                if(!empty($data['user_password'])){
                    $user->password = bcrypt($data['user_password']);
                }

                $user->email = $data['user_email_id'];
                $user->name = $data['trading_name'];                

                if(!$user->save()){
                    DB::rollback();
                    return $this->errorSave('Error saving the password!');
                }
                  
            }else{
               
                $validator = $this->validator($data, 'save')->validate();
                //if (!empty($data['outlets'])) {
                  // $validator = $this->validator($data, 'outlet_save')->validate();
                //}
                
                $user = User::create([
                    'name' => $data['trading_name'],
                    'email' => $data['user_email_id'],
                    'password' => bcrypt($data['user_password']),
                    'role_id' => 6,
                ]);
 
                $data['user_id'] = $user['id'];
                $data['position'] = 10;
                $result = $this->create($data);
                //dd($result);
            }

            if(!$result){
                DB::rollback();
                return $this->errorSave();
            }else{
                //If request has file save it    
                if (!empty($file_upload['logo'])) {
                    
                    $image = ['logo'=>$file_upload['logo']];
                    $folder_type = "merchants";
                    $resultSave = $this->saveFile($image, $folder_type, $result['id']);
                    
                    $result->logo = $resultSave[0];

                    if(!$result->save()){
                        DB::rollback();
                        return $this->errorSave('Error saving the image!');
                    }  
                }
                //End if request has file               

                if (!empty($data['fields'])) {
                  
                  $position = 0;

                  foreach ($data['fields'] as $key => $value) {
                    if($value['display']==1){
                      if(!empty($value['position'])){
                        if($value['position']>$position){
                          $position = $value['position'];
                        }
                      }
                    }
                  }  

                  MerchantInputField::where('merchant_id', $result['id'])->delete();

                  foreach ($data['fields'] as $key => $value) {

                    if($value['display']==1){

                      $field_record = InputField::find($value['id']);

                      if($field_record['data_type_id']==65){
                          $value['position'] = $position+1;
                          $position = $position+1;
                      }

                      $field_data = array(
                          'merchant_id'=>$result['id']
                          ,'input_field_id'=>$value['id']
                          ,'is_mandatory'=>$value['is_mandatory']
                          ,'position'=>$value['position']
                      );

                      $model_input_field = new MerchantInputField();
                      $model_input_field->fill($field_data);
                      if (!$model_input_field->save()) {
                          DB::rollback();
                          return $this->errorSave();
                      }
                    }
                  }     
                }

                if (!empty($data['outlets'])) {

                    foreach ($data['outlets'] as $key => $value) {
                        if(!empty($value['outlet_name']) && !empty($value['outlet_pic_name']) && !empty($value['outlet_pic_email']) && !empty($value['outlet_pic_mobile_number']) && !empty($value['outlet_pic_id_num']) && !empty($value['outlet_address'])){

                          ////Save / Update Outlet User
                          $model_user = new User();
                          if(!empty($value['user_id'])){
                            $model_user = $model_user->find($value['user_id']);
                          }

                          $outlet_user = [
                              'name' => $value['outlet_pic_name'],
                              'email' => $value['outlet_pic_email'],
                              'role_id' => 8,
                              'referral_id' => $user['id']
                          ];  

                          if(!empty($value['outlet_pic_password'])){
                              $outlet_user['password'] = bcrypt($value['outlet_pic_password']);
                          }

                          $model_user->fill($outlet_user);
                          if(!$model_user->save()){
                              $resultJson['result'] = 0;
                              $resultJson['errMsg'] = 'Error : Cant save Outlet User data!';
                          }  
                          ////End Save / Update Outlet User
                          
                          ////Save / Update Outlet User
                          $model_outlet = new MerchantOutlet();
                          if(!empty($value['id'])){
                            $model_outlet = $model_outlet->find($value['id']);
                          }

                          $outlet_data = array(
                                'merchant_id'=>$result['id']
                                ,'user_id'=>$model_user['id']
                                ,'outlet_name'=>$value['outlet_name']
                                ,'outlet_pic_email'=>$value['outlet_pic_email']
                                ,'outlet_pic_name'=>$value['outlet_pic_name']
                                ,'outlet_pic_id_num'=>$value['outlet_pic_id_num']
                                ,'outlet_pic_mobile_number'=>$value['outlet_pic_mobile_number']
                                ,'outlet_address'=>$value['outlet_address']        
                          );  

                          $model_outlet->fill($outlet_data);
                          if(!$model_outlet->save()){
                              $resultJson['result'] = 0;
                              $resultJson['errMsg'] = 'Error : Cant save Outlet User data!';
                          } 
                          else{
                              //If request has file save it    
                              if (!empty($value['logo'])) {

                                  $image = ['logo'=>$value['logo']];

                                  $folder_type = "outlets";
                                  $resultSave = $this->saveFile($image, $folder_type, $model_outlet['id']);
                                  
                                  $model_outlet->outlet_logo = $resultSave[0];

                                  if(!$model_outlet->save()){
                                      DB::rollback();
                                      return $this->errorSave('Error saving the image!');
                                  }  
                              }
                              //End if request has file     
                          }
                        }
                    } 
                }

                $bank_data = array(
                    'merchant_id'=>$result['id']
                    ,'bank_name'=>$data['bank_name']
                    ,'acc_number'=>$data['acc_number']
                    ,'acc_holder_name'=>$data['acc_holder_name']
                    ,'acc_finance_contact_number'=>$data['acc_finance_contact_number']
                    ,'acc_contact_number'=>$data['acc_contact_number']
                    ,'acc_email'=>$data['acc_email']
                );

                $model_bank_account = MerchantBankAccount::where('merchant_id',$result['id'])->first();

                if(empty($model_bank_account)){
                  $model_bank_account = new MerchantBankAccount();
                }                

                $model_bank_account->fill($bank_data);
                $model_bank_account->save();
                //dd(DB::getQueryLog());
                if (!$model_bank_account->save()) {
                    DB::rollback();
                    return $this->errorSave();
                }                    
         
                DB::commit();
                Session::flash('success', 'Add / update Merchant success'); 
            }
        }//End else

        return $result;
      
    }

    public function save_individual(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
           
            DB::beginTransaction();
            if(!empty($data['id'])){
                $validator = $this->validator($data, 'update_individual')->validate();

                $this->update($data, $data['id']);
                $result = $this->find($data['id']);

                $user = User::find($result['user_id']);
                if(!empty($data['user_password'])){
                    $user->password = bcrypt($data['user_password']);
                }

                $user->email = $data['merchant_email_id'];
                $user->name = $data['individual_name'];
                if(!$user->save()){
                    DB::rollback();
                    return $this->errorSave('Error saving the password!');
                } 
                  
            }else{
                $validator = $this->validator($data, 'save_individual')->validate();

                $user = User::create([
                    'name' => $data['individual_name'],
                    'email' => $data['merchant_email_id'],
                    'password' => bcrypt($data['user_password']),
                    'role_id' =>7,
                ]);

                $data['user_id'] = $user['id'];
                $data['position'] = 10;
                $result = $this->create($data);
            }

            if(!$result){
                DB::rollback();
                return $this->errorSave();
            }else{
                //If request has file save it    
                if (!empty($file_upload)) {

                    $folder_type = "merchants";
                    $resultSave = $this->saveFile($file_upload, $folder_type, $result['id']);
                    
                    $result->logo = $resultSave[0];

                    if(!$result->save()){
                        DB::rollback();
                        return $this->errorSave('Error saving the image!');
                    }  
                }
                //End if request has file

                if (!empty($data['fields'])) {
                  
                  $position = 0;

                  foreach ($data['fields'] as $key => $value) {
                    if($value['display']==1){
                      if(!empty($value['position'])){
                        if($value['position']>$position){
                          $position = $value['position'];
                        }
                      }
                    }
                  }  

                  MerchantInputField::where('merchant_id', $result['id'])->delete();

                  foreach ($data['fields'] as $key => $value) {

                    if($value['display']==1){

                      $field_record = InputField::find($value['id']);

                      if($field_record['data_type_id']==65){
                          $value['position'] = $position+1;
                          $position = $position+1;
                      }

                      $field_data = array(
                          'merchant_id'=>$result['id']
                          ,'input_field_id'=>$value['id']
                          ,'is_mandatory'=>$value['is_mandatory']
                          ,'position'=>$value['position']
                      );

                      $model_input_field = new MerchantInputField();
                      $model_input_field->fill($field_data);
                      if (!$model_input_field->save()) {
                          DB::rollback();
                          return $this->errorSave();
                      }
                    }
                  }     
                }

                $bank_data = array(
                    'merchant_id'=>$result['id']
                    ,'bank_name'=>$data['bank_name']
                    ,'acc_number'=>$data['acc_number']
                    ,'acc_holder_name'=>$data['acc_holder_name']
                    ,'acc_finance_contact_number'=>''
                    ,'acc_contact_number'=>$data['ind_contact_number']
                    ,'acc_email'=>$data['acc_email']
                    ,'branch_information'=>$data['branch_information']
                );

                $model_bank_account = MerchantBankAccount::where('merchant_id',$result['id'])->first();

                if(empty($model_bank_account)){
                  $model_bank_account = new MerchantBankAccount();
                }                

                $model_bank_account->fill($bank_data);
                $model_bank_account->save();
                //dd(DB::getQueryLog());
                if (!$model_bank_account->save()) {
                    DB::rollback();
                    return $this->errorSave();
                }                    
         
                DB::commit();
                Session::flash('success', 'Add / update Merchant success'); 
            }
        }//End else

        return $result;
      
    }

    public function removeOutlet($outlet_id){
        
        if(empty($outlet_id)){ 
            return $this->errorEmptyData();
        }else{
           
            DB::beginTransaction(); 

            $merchant_outlet = MerchantOutlet::find($outlet_id);
            $user_id = $merchant_outlet['user_id'];

            $result = MerchantOutlet::destroy($outlet_id);

            if(!$result){
                DB::rollback();
                return $this->errorDelete();
            }    
            else{
                $result = User::destroy($user_id);

                if(!$result){
                    DB::rollback();
                    return $this->errorDelete();
                }    
            }
     
            DB::commit();
            Session::flash('success', 'Delete Merchant Outlet success'); 
            
        }//End else

        return $this->success();
      
    }


    public function get_user_details($user_id)
    {
      $users = DB::table('users')
               ->select('name','email','password')
               ->where('id', $user_id)
               ->first();
      return $users;
    }  
    public function getOwnershipDetails($type_of_ownership)
    {
      $ownership = DB::table('lookups')
               ->select('name','id')
               ->where('id', $type_of_ownership)
               ->first();
      return $ownership;
    }
    public function getBusiness($type_of_business)
    {
      $business = DB::table('lookups')
               ->select('name','id')
               ->where('id', $type_of_business)
               ->first();
      return $business;
    }
    public function getOutlets($merchant_id)
    {
      $outlets = DB::table('merchant_outlets')
               ->select('*')
               ->where('merchant_id', $merchant_id)
               ->get();
      return $outlets;
    }
    public function getBanks($merchant_id)
    {
      $banks = DB::table('merchant_bank_accounts')
               ->select('lookups.*','merchant_bank_accounts.*')
               ->join('lookups', 'lookups.id','=','merchant_bank_accounts.bank_name')
               ->where('merchant_bank_accounts.merchant_id', $merchant_id)
               ->first();
      return $banks;
    }

    public function getMerchantInput($merchant_id)
    {
      $merchant_input = DB::table('merchant_input_fields')
               ->select('merchant_input_fields.*','input_fields.*')
               ->join('input_fields', 'input_fields.id','=','merchant_input_fields.input_field_id')
               ->where('merchant_input_fields.merchant_id', $merchant_id)
               ->get();
      return $merchant_input;
    }

    public function get_outlet_count($merchant_id)
    {
      $outlet_count = DB::table('merchant_outlets')
               ->select(DB::raw("COUNT(*) as count_row"))
               ->where('merchant_id', $merchant_id)
               ->first();               
      return $outlet_count->count_row;
    }

    public function get_merchant_outlets($merchant_id)
    {
      $merchant_outlets = DB::table('merchant_outlets')
               ->select('*')
               ->where('merchant_outlets.merchant_id', $merchant_id)
               ->orderBy('outlet_pic_name', 'asc')
               ->get();
      return $merchant_outlets->toArray();
    }

    public function get_salesperson($outlet_pic_email)
    {
      $salesperson = DB::table('users')
               ->select('merchant_outlets.outlet_pic_name','merchant_outlets.outlet_logo','merchant_outlets.id','merchant_outlets.outlet_pic_mobile_number','merchant_outlets.outlet_address','merchant_outlets.user_id')
               ->join('merchant_outlets','merchant_outlets.user_id','=','users.id')
               ->where('users.email', $outlet_pic_email)
               ->first();
      return $salesperson;
    }

    public function getPhoneNumber($user_id)
    {
        $record = InvestorDetail::select('investor_details.phone')
                       ->where('investor_details.user_id', $user_id)
                       ->first();

        return $record;
    }

    public function updateMobile($user_id,$phone)
    {
        $record = InvestorDetail::where('user_id',$user_id)
                        ->update(['phone' => $phone]);
        return $record;
    }    
    
}