<?php
namespace App\Repositories\Eloquent;

use App\Models\Vehicle;

class VehicleRepository extends BaseRepository
{
    /**
     * @var Vehicle
     */
    protected $model;

    /**
     * VehicleRepository constructor.
     *
     * @param Vehicle $model
     */
    public function __construct(Vehicle $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $array
     * @param null $method
     */
    public function validator(array $array, $method = null)
    {

    }

    /**
     * @param $model_query
     * @param $data
     *
     * @return mixed
     */
    public function dynamicQuery($model_query, $data)
    {
        return $model_query;
    }
}
