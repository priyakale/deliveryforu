<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\Currency;

class CurrencyRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(Currency $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {

        return $model_query;
    }

    public function getCurrencyCache() { 
      
      $lookup = Cache::remember('Currency', $this->expiry_time, function() 
      {
          $records = $this->all([],['status']);

          foreach ($records as $key => $value) {
            $value['icon_url'] = asset('/uploads/currencies/' . $value['id'].'/'.$value['icon']);
          }

          return $records;
      });

      return array_values($lookup->where('status_id', 1)->toArray());
    }

    
}