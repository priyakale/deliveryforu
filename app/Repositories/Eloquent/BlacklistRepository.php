<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Validation\Rule;
use App\Notifications\Email;
use Illuminate\Support\Facades\DB;
use Notification;
use Validator;
use Session;
use Auth;
use ActivationRepo;
use WalletRepo;
use EmailNotiRepo;
use Log;

//MODEL
use App\Models\User;

class BlacklistRepository extends BaseRepository 
{
    protected $model;
    
    public function __construct(User $model, ActivationRepo $activationRepo, WalletRepo $walletRepo, EmailNotiRepo $emailNotiRepo)
    {
        $this->model = $model;
        $this->activationRepo = $activationRepo;
        $this->walletRepo = $walletRepo;
        $this->emailNotiRepo = $emailNotiRepo;
    }

    public function validator(array $data, $method = null) 
    {
        $attributeNames = [
        ];

        $messages = [
            'unique' => 'The :attribute is already in our database.',
            'required_if' => 'The :attribute field is required.',
        ];
        
        if($method ==='delete')
        {
            $rule = ['id' => 'exists:users'];
        }

        return Validator::make($data, $rule, $messages)->setAttributeNames($attributeNames);
    }

    public function dynamicQuery($model_query, $data)
    {
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['user_id']) && $data['user_id'] != ''){
          $model_query->where('user_id',$data['user_id']);
        }

        $model_query->orderBy('created_at', 'desc');

        return $model_query;
    }

    public function subscriberRejection(array $data, $file_upload = [], $create_type = ''){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            
            
        }//End else
        return $result;
    }

    
    
}