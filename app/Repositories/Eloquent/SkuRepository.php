<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\Sku;

class SkuRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(Sku $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
      
        return $model_query;
    }

    public function getSkGroupCache(string $make = "") { 
      
      return $lookup = Cache::remember('skuGroupArray', $this->expiry_time, function() 
      {
          $lookup['companies'] = $this->model->select('make as name')->groupBy('make')->get()->toArray();

          foreach ($lookup['companies'] as $key => $value) {
              
              $lookup['companies'][$key]['models'] = $this->model->select('model as name')->where('make', $value['name'])->groupBy('model')->get()->toArray();

              foreach ($lookup['companies'][$key]['models'] as $key_child => $value_child) {
                  
                  $lookup['companies'][$key]['models'][$key_child]['colors'] = $this->model->select('id', 'color as name', 'tier')->where('make', $value['name'])->where('model', $value_child['name'])->get()->toArray();
              }
          }

          return $lookup;
      });
    }

    public function getSkuMakeCache(string $make = "") { 
      
      return $lookup = Cache::remember('skuMakeArray', $this->expiry_time, function() 
      {
          return $this->model->select('make as name')->groupBy('make')->get()->toArray();;
      });
    }
  	
}