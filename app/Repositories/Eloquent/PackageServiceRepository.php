<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use Validator;
use Session;
use File;

//MODEL
use App\Models\PackageService;
use App\Models\User;
use App\Models\ServiceInputField;
use App\Models\SubService;
use App\Models\InputField;
use App\Models\InvestorDetail;
use App\Models\ServiceDocumentChecklist;

class PackageServiceRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(PackageService $model)
  	{
    	$this->model = $model;
  	}

     
    public function validator(array $data, $method = null) 
    {        
        
        $attributeNames = ['status_id' => 'status_id'];

        $customMessages = [
                'pkgcode.required'=>'Please enter the package code',
                'name.required'=>'Please enter package name',
                'description.required'=>'Please enter package description',
                'category_id.required'=>'Please select category',
                'start.required'=>'Please select start date',
                'end.required'=>'Please select end date',
                'deal_method_id.required'=>'Please select deal method',
                'payment_type_id.required'=>'Please select payment type',
                'insurance_id.required'=>'Please select insurance',
                'inhouse_id.required'=>'Please select inhouse ',
                'ruleid.required'=>'Please select ruleid',
               // 'status_id.required'=>'Please select status',              
            ];

        if($method =='save'){
            $rule = [
              'pkgcode' => 'required',
              'name' => 'required',
              'description' => 'required',
              'category_id' => 'required',
              'start' => 'required',
              'end' => 'required',
              'deal_method_id' => 'required',
              'payment_type_id' => 'required',
              'insurance_id' => 'required',
              'inhouse_id' => 'required',
              'ruleid' => 'required',
              //'status_id' => 'required',                         
            ];

            foreach ($data['sub_services'] as $key => $value) {
              if (!empty($value['sub_service_name']) || !empty($value['sub_service_description']) || !empty($value['sub_service_start']) || !empty($value['sub_service_end']) || !empty($value['sub_service_status'])) {
                    $rule['sub_services.'.$key.'.sub_service_name'] = 'required';
                    $rule['sub_services.'.$key.'.sub_service_description'] = 'required';
                    $rule['sub_services.'.$key.'.sub_service_start'] = 'required';
                    $rule['sub_services.'.$key.'.sub_service_end'] = 'required';
                   // $rule['sub_services.'.$key.'.sub_service_status'] = 'required';
                }
            }            
        }
        else if($method =='update'){
            $rule = [
              'logo' => 'sometimes|nullable|mimes:png,jpg,jpeg|max:3000|dimensions:max_width=432,max_height=432',
              'pkgcode' => 'required',
              'name' => 'required',
              'description' => 'required',
              'category_id' => 'required',
              'start' => 'required',
              'end' => 'required',
              'deal_method_id'=>'required',
              'payment_type_id' => 'required',
              'insurance_id' => 'required',
              'inhouse_id' => 'required',
              'ruleid' => 'required',
              //'status_id' => 'required',              
            ];
            
            foreach ($data['sub_services'] as $key => $value) {
                if(!empty($value['id'])){
                      $rule['sub_services.'.$key.'.sub_service_name'] = 'required';
                      $rule['sub_services.'.$key.'.sub_service_description'] = 'required';
                      $rule['sub_services.'.$key.'.sub_service_start'] = 'required';
                      $rule['sub_services.'.$key.'.sub_service_end'] = 'required';
                     // $rule['sub_services.'.$key.'.sub_service_status'] = 'required';
                }
                else if (!empty($value['sub_service_name']) || !empty($value['sub_service_description']) || !empty($value['sub_service_start']) || !empty($value['sub_service_end']) || !empty($value['sub_service_status'])) {
                      $rule['sub_services.'.$key.'.sub_service_name'] = 'required';
                      $rule['sub_services.'.$key.'.sub_service_description'] = 'required';
                      $rule['sub_services.'.$key.'.sub_service_start'] = 'required';
                      $rule['sub_services.'.$key.'.sub_service_end'] = 'required';
                    //  $rule['sub_services.'.$key.'.sub_service_status'] = 'required';
                }               
            } 

        }        
       
        else if($method ==='delete'){
            $rule = ['id' => 'exists:package_services'];
        }
        else{

        }
        //dd($customMessages);
        return Validator::make($data, $rule, $customMessages)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['name']) && $data['name'] != ''){
          $model_query->where('name',$data['name']);
        }

        if(!empty($data['description']) && $data['description'] != ''){
          $model_query->where('description',$data['description']);
        }

        if(!empty($data['category_id']) && $data['category_id'] != ''){
          $model_query->where('category_id',$data['category_id']);
        }

        if(!empty($data['start']) && $data['start'] != ''){
          $model_query->where('start',$data['start']);
        }        
        
        if(!empty($data['end']) && $data['end'] != ''){
          $model_query->where('end',$data['end']);
        }
        
        if(!empty($data['deal_method_id']) && $data['deal_method_id'] != ''){
          $model_query->where('deal_method_id',$data['deal_method_id']);
        }
        
        if(!empty($data['payment_type_id']) && $data['payment_type_id'] != ''){
          $model_query->where('payment_type_id',$data['payment_type_id']);
        }

        if(!empty($data['insurance_id']) && $data['insurance_id'] != ''){
          $model_query->where('insurance_id',$data['insurance_id']);
        }

        if(!empty($data['inhouse_id']) && $data['inhouse_id'] != ''){
          $model_query->where('inhouse_id',$data['inhouse_id']);
        }

        if(!empty($data['ruleid']) && $data['ruleid'] != ''){
          $model_query->where('ruleid',$data['ruleid']);
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        if(!empty($data['pkgid'])){
          $model_query->where('pkgid',$data['pkgid']);
        } 
        else{
          $model_query->orderBy('position', 'asc'); 
        }       
          
        return $model_query;
    }

    public function save(array $data, $file_upload = []){
       
        if(empty($data)){ 
            return $this->errorEmptyData();
        }else{
            
            DB::beginTransaction(); 
            if(!empty($data['id'])){ 
                $validator = $this->validator($data, 'update')->validate();

                $this->update($data, $data['id']);
                $result = $this->find($data['id']);                
                $package_services = PackageService::find($result['id']);               

            }else{
                $validator = $this->validator($data, 'save')->validate();

                $data['position'] = 10;
                $result = $this->create($data);
            }
            
            if(!$result){
                DB::rollback();
                return $this->errorSave();
            }else{
                //If request has file save it    
                if (!empty($file_upload['logo'])) {
                    
                    $image = ['logo'=>$file_upload['logo']];
                    $folder_type = "packages";
                    $resultSave = $this->saveFile($image, $folder_type, $result['id']);
                    
                    $result->logo = $resultSave[0];
                     
                    if(!$result->save()){
                        DB::rollback();
                        return $this->errorSave('Error saving the image!');
                    }  
                }
                //End if request has file               

                if (!empty($data['fields'])) {
                  
                  $position = 0;

                  foreach ($data['fields'] as $key => $value) {
                    if($value['display']==1){
                      if(!empty($value['position'])){
                        if($value['position']>$position){
                          $position = $value['position'];
                        }
                      }
                    }
                  }  

                  ServiceInputField::where('pkgid', $result['id'])->delete();

                  foreach ($data['fields'] as $key => $value) {

                    if($value['display']==1){

                      $field_record = InputField::find($value['id']);

                      if($field_record['data_type_id']==65){
                          $value['position'] = $position+1;
                          $position = $position+1;
                      }

                      $field_data = array(
                          'pkgid'=>$result['id']
                          ,'input_field_id'=>$value['id']
                          ,'is_mandatory'=>$value['is_mandatory']
                          ,'position'=>$value['position']
                      );

                      $model_input_field = new ServiceInputField();
                      $model_input_field->fill($field_data);
                      if (!$model_input_field->save()) {
                          DB::rollback();
                          return $this->errorSave();
                      }
                    }
                  }     
                }

                if (!empty($data['sub_services'])) {
                 
                    foreach ($data['sub_services'] as $key => $value) {
                        if(!empty($value['sub_service_name']) && !empty($value['sub_service_description']) && !empty($value['sub_service_start']) && !empty($value['sub_service_end']) && !empty($value['sub_service_status'])){
                          
                          ////Save / Update Sub Services
                          $sub_services = new SubService();
                          if(!empty($value['id'])){
                            $sub_services = $sub_services->find($value['id']);
                          }                          
                          $sub_services_data = array(
                                'pkgid'=>$result['id']
                                ,'name'=>$value['sub_service_name']
                                ,'description'=>$value['sub_service_description']
                                ,'start'=>$value['sub_service_start']
                                ,'end'=>$value['sub_service_end']
                                ,'status'=>$value['sub_service_status']          
                          );  
                          
                          $sub_serv = $sub_services->fill($sub_services_data);
                         
                          if(!$sub_services->save()){
                              $resultJson['result'] = 0;
                              $resultJson['errMsg'] = 'Error : Cant save sub services data!';
                          }                           
                        }
                    } 
                }
               
                 $docu_checklist_data = array(
                    'pkgid'=>$result['id']
                    ,'name'=>$data['doc_name']
                    ,'description'=>$data['doc_description']
                    ,'is_required'=>$data['is_required']                    
                );
                $docu_checklist = ServiceDocumentChecklist::where('pkgid',$result['id'])->first();

                if(empty($docu_checklist)){
                  $docu_checklist = new ServiceDocumentChecklist();
                }                

                $docu_checklist->fill($docu_checklist_data);
                $docu_checklist->save();
                //dd(DB::getQueryLog());
                if (!$docu_checklist->save()) {
                    DB::rollback();
                    return $this->errorSave();
                }                    
         
                DB::commit();
                Session::flash('success', 'Add / update package success'); 
            }
        }//End else

        return $result;
      
    }

    public function removeSubServices($pkg_id){
        
        if(empty($pkg_id)){ 
            return $this->errorEmptyData();
        }else{
           
            DB::beginTransaction(); 

            $sub_services = SubService::find($pkg_id);           
            $result = SubService::destroy($pkg_id);

            if(!$result){
                DB::rollback();
                return $this->errorDelete();
            } 
     
            DB::commit();
            Session::flash('success', 'Delete sub services success'); 
            
        }//End else

        return $this->success();
      
    }

    public function getCategoryDetails($category)
    {
      $category = DB::table('lookups')
               ->select('name','id')
               ->where('id', $category)
               ->first();
      return $category;
    }
    public function getDealMethod($deal_method)
    {
      $deal_method = DB::table('lookups')
               ->select('name','id')
               ->where('id', $deal_method)
               ->first();
      return $deal_method;
    }

    public function getInsurance($insurance)
    {
      $insurance = DB::table('lookups')
               ->select('name','id')
               ->where('id', $insurance)
               ->first();
      return $insurance;
    }

    public function getInhouse($inhouse)
    {
      $inhouse = DB::table('lookups')
               ->select('name','id')
               ->where('id', $inhouse)
               ->first();
      return $inhouse;
    }

    public function getRuleId($ruleid)
    {
      $ruleid = DB::table('lookups')
               ->select('name','id')
               ->where('id', $ruleid)
               ->first();
      return $ruleid;
    }

    public function getPaymentType($payment_type)
    {
      $payment_type = DB::table('lookups')
               ->select('name','id')
               ->where('id', $payment_type)
               ->first();
      return $payment_type;
    }

    public function getSubServices($pkgid)
    {
      $sub_services = DB::table('sub_services')
               ->select('*')
               ->where('pkgid', $pkgid)
               ->get();
      return $sub_services;
    }
    public function getservicedocs($pkgid)
    {
      $servicedocs = DB::table('service_document_checklists')
               ->select('*')
               ->where('pkgid', $pkgid)
               ->first();
      return $servicedocs;
    }

    public function getServiceInput($pkgid)
    {
      $service_input = DB::table('service_input_fields')
               ->select('service_input_fields.*','input_fields.*')
               ->join('input_fields', 'input_fields.id','=','service_input_fields.input_field_id')
               ->where('service_input_fields.pkgid', $pkgid)
               ->get();
      return $service_input;
    }

    public function get_subservice_count($pkgid)
    {
      $subservice_count = DB::table('sub_services')
               ->select(DB::raw("COUNT(*) as count_row"))
               ->where('pkgid', $pkgid)
               ->first();               
      return $subservice_count->count_row;
    }

    public function get_subservice($pkgid)
    {
      $subservices = DB::table('sub_services')
               ->select('*')
               ->where('sub_services.pkgid', $pkgid)
               ->get();
      return $subservices->toArray();
    }
     
    
}