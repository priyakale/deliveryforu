<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use App\Notifications\EmailAttachmentPDF;
use Log;
use Notification;
use Session;
use DateTime;
use Response;
use PDF;
use Auth;
use Validator;
use Excel;

//REPOSITORY
use EmailNotiRepo;

//OTHER
use Carbon\Carbon;

//MODEL
use App\Models\Receipt;

class ReceiptRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(Receipt $model, EmailNotiRepo $emailNotiRepo)
  	{
    	$this->model = $model;
        $this->emailNotiRepo = $emailNotiRepo;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
          ];

        if($method =='save'){
            $rule = [
              'transaction_id' => 'required'
            ];
        }
        elseif($method ==='delete'){
            $rule = ['id' => 'exists:tiers'];
        }
        else{

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['name']) && trim($data['name']) != ''){
          $model_query->where('name', 'LIKE', '%'. trim($data['name']).'%');
        }

        if(!empty($data['status_id']) && $data['status_id'] != ''){
          $model_query->where('status_id',$data['status_id']);
        }

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            $validator = $this->validator($data, 'save')->validate();
            
            if(!empty($data['id'])){
                $result = $this->update($data, $data['id']);
                $result = $this->find($data['id'], ['wallet']);
            }else{
                $result = $this->create($data);
            }
            
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update Tier success'); 
            }
            $result = $this->NullToEmpty($this->find($result['id'])->toArray());
        }//End else
        return $result;
    }

    public function sendReceiptDealer($user, $email_for_id, $transaction, $payment_for){            
        
        if(!empty($transaction)){

            $receipt_record = $this->save(['transaction_id'=>$transaction['id']]);

            $receipt_num = $this->GenerateReceiptNumber($receipt_record['id']);

            $transaction['amount_word'] = 'RINGGIT MALAYSIA '. $this->convert_number_to_words($transaction->amount);

            $values['data'] = array(
                'user' => $user
                ,'datetime' => Carbon::parse(Carbon::now())->format('j F Y - h i a')
                ,'records' => $transaction
                ,'receipt_num' => $receipt_num
                ,'payment_for' => $payment_for
            );

            $date_report = date('dmY');

            //return View('account.statement.statement', $values);
            $pdf = PDF::loadView('admin.receipt.deposit_receipt_dealer', $values)
                ->setPaper('A4', 'potrait');
                
            return $this->sendMail($user, $email_for_id, $pdf->output());
        }
        else{
            return $this->errorSave();
        }   
    }

    public function sendReceiptSubscriber($user, $email_for_id, $transaction, $payment_for){            
        
        if(!empty($transaction)){

            $receipt_record = $this->save(['transaction_id'=>$transaction['id']]);

            $receipt_num = $this->GenerateReceiptNumber($receipt_record['id']);

            $transaction['amount_word'] = 'RINGGIT MALAYSIA '. $this->convert_number_to_words($transaction->amount);

            $values['data'] = array(
                'user' => $user
                ,'datetime' => Carbon::parse(Carbon::now())->format('j F Y - h i a')
                ,'records' => $transaction
                ,'receipt_num' => $receipt_num
                ,'payment_for' => $payment_for
            );

            $date_report = date('dmY');

            //return View('account.statement.statement', $values);
            $pdf = PDF::loadView('admin.receipt.receipt_subscriber', $values)
                ->setPaper('A4', 'potrait');
                
            return $this->sendMail($user, $email_for_id, $pdf->output());
        }
        else{
            return $this->errorSave();
        }   
    }


    public function sendMail($user, $email_for_id, $pdf_output)
    {   
        $result['status'] = 1; //Failed

        $email_notification = $this->emailNotiRepo->findBy(['email_for_id'=>$email_for_id]);
        $link = route('login');

        if(!empty($email_notification)){
            $message['subject'] = $email_notification['subject'];
            $message['actionText'] = "Click to Login";
            $message['actionUrl'] = $link;
            $message['file_name'] = "Receipt";

            $tier = 'Tier '.$user['device']['sku']['tier'];

            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%DEALERNAME",strtoupper($user['name']),$email_notification['description']);

            if(!empty($user['transaction'])){

                $user['transaction']['date'] = (DateTime::createFromFormat('d/m/Y H:i:s', $user['transaction']['created_at']))->format('d/m/Y');

                $user['transaction']['time'] = (DateTime::createFromFormat('d/m/Y H:i:s', $user['transaction']['created_at']))->format('H:i:s');

                $message['description'] = str_replace("%AMOUNT",$user['transaction']['amount'],$message['description']);
                $message['description'] = str_replace("%DATETRANSACTION",$user['transaction']['date'],$message['description']);
                $message['description'] = str_replace("%TIMETRANSACTION",$user['transaction']['time'],$message['description']);
            }
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA




            $message['description'] = str_replace("%NAME",strtoupper($user['name']),$email_notification['description']);
            $message['description'] = str_replace("%PLAN",strtoupper($user['device']['package_fee']['package']['name']),$message['description']);
            $message['description'] = str_replace("%PHONEMAKE",strtoupper($user['device']['sku']['make']),$message['description']);
            $message['description'] = str_replace("%PHONEMODEL",strtoupper($user['device']['sku']['model']),$message['description']);
            $message['description'] = str_replace("%TIER",strtoupper($tier),$message['description']);
            $message['description'] = str_replace("%IMEI",strtoupper($user['device']['imei']),$message['description']);
            $message['description'] = str_replace("%VALIDFROM",strtoupper($user['transaction']['start_date']),$message['description']);
            $message['description'] = str_replace("%VALIDTILL",strtoupper($user['transaction']['end_date']),$message['description']);


            
            Notification::send($user, new EmailAttachmentPDF($message, $pdf_output));
            Notification::route('mail', 'emails@yakin2u.com')->notify(new EmailAttachmentPDF($message, $pdf_output));
            $result['status'] = 0; //Success
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/error/Send_Enrollment_Mail.log');
            Log::notice('Error Empty Email Record : Email Notification is empty');
        }

        return $result;
    }

    public static function GenerateReceiptNumber($receipt_id = null){

        if(!empty($receipt_id)){
            $receipt_number = sprintf("%09d", $receipt_id);
        } 
        
        return $receipt_number;
    }

    public function convert_number_to_words($number) {
      $hyphen      = '-';
      $conjunction = ' and ';
      $separator   = ', ';
      $negative    = 'negative ';
      $decimal     = '';
      $dictionary  = array(
          0                   => '',
          1                   => 'one',
          2                   => 'two',
          3                   => 'three',
          4                   => 'four',
          5                   => 'five',
          6                   => 'six',
          7                   => 'seven',
          8                   => 'eight',
          9                   => 'nine',
          10                  => 'ten',
          11                  => 'eleven',
          12                  => 'twelve',
          13                  => 'thirteen',
          14                  => 'fourteen',
          15                  => 'fifteen',
          16                  => 'sixteen',
          17                  => 'seventeen',
          18                  => 'eighteen',
          19                  => 'nineteen',
          20                  => 'twenty',
          30                  => 'thirty',
          40                  => 'fourty',
          50                  => 'fifty',
          60                  => 'sixty',
          70                  => 'seventy',
          80                  => 'eighty',
          90                  => 'ninety',
          100                 => 'hundred',
          1000                => 'thousand',
          1000000             => 'million',
          1000000000          => 'billion',
          1000000000000       => 'trillion',
          1000000000000000    => 'quadrillion',
          1000000000000000000 => 'quintillion'
      );

      if (!is_numeric($number)) {
          return false;
      }

      if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
          // overflow
          trigger_error(
              'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
              E_USER_WARNING
          );
          return false;
      }

      if ($number < 0) {
          return $negative . convert_number_to_words(abs($number));
      }

      $string = $fraction = null;

      if (strpos($number, '.') !== false) {
          list($number, $fraction) = explode('.', $number);
      }

      switch (true) {
          case $number < 21:
              $string = $dictionary[$number];
              break;
          case $number < 100:
              $tens   = ((int) ($number / 10)) * 10;
              $units  = $number % 10;
              $string = $dictionary[$tens];
              if ($units) {
                  $string .= $hyphen . $dictionary[$units];
              }
              break;
          case $number < 1000:
              $hundreds  = $number / 100;
              $remainder = $number % 100;
              $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
              if ($remainder) {
                  $string .= $conjunction . $this->convert_number_to_words($remainder);
              }
              break;
          default:
              $baseUnit = pow(1000, floor(log($number, 1000)));
              $numBaseUnits = (int) ($number / $baseUnit);
              $remainder = $number % $baseUnit;
              $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
              if ($remainder) {
                  $string .= $remainder < 100 ? $conjunction : $separator;
                  $string .= $this->convert_number_to_words($remainder);
              }
              break;
      }

      if (null !== $fraction && is_numeric($fraction)) {
          $string .= $decimal;
          $words = array();
          foreach (str_split((string) $fraction) as $number) {
              $words[] = $dictionary[$number];
          }
          $string .= implode(' ', $words);
      }

      return strtoupper($string);
    }
    
  	
}