<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Validator;
use Session;

//MODEL
use App\Models\MerchantInputField;
use App\Models\InputField;

use Yajra\Datatables\Datatables;

class ServiceInputFieldRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
    protected $model;
  	protected $inputFieldModel;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(MerchantInputField $model, InputField $inputFieldModel)
  	{
      $this->model = $model;
    	$this->inputFieldModel = $inputFieldModel;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['merchant_id']) && $data['merchant_id'] != ''){
          $model_query->where('merchant_id',$data['merchant_id']);
        }
        
        if(!empty($data['input_field_id']) && $data['input_field_id'] != ''){
          $model_query->where('input_field_id',$data['input_field_id']);
        }

        if(!empty($data['is_mandatory']) && $data['is_mandatory'] != ''){
          $model_query->where('is_mandatory',$data['is_mandatory']);
        }

        if(!empty($data['position']) && $data['position'] != ''){
          $model_query->where('position',$data['position']);
        }
         

        return $model_query->orderBy('position');
    }
 

   public function getMerchantFields($id , $data = null){
        
        

          $query = $this->model
          ->select('merchant_input_fields.id','field_name','lookups.name as data_type','is_mandatory')
          ->join('input_fields', 'input_fields.id', '=', 'merchant_input_fields.input_field_id')
          ->join('merchants', 'merchants.id', '=', 'merchant_input_fields.merchant_id')
          ->join('lookups', 'lookups.id', '=', 'input_fields.data_type_id')
          ->where('merchants.status','!=','D')
          ->where('input_fields.status','!=','D');


          if(!empty($data['merchant_id']) && $data['merchant_id'] != ''){
            dd($data['merchant_id']);
            $query->where('merchant_id',$data['merchant_id']);
          }else{
            $query->where('user_id',$id);
          }

          $result = $query->orderBy('merchant_input_fields.position')
          ->get()
          ->toArray();

          return $result;
    } 
 

    public function save(array $data){

      if(empty($data)){
            return $this->errorEmptyData();
        }else{


          DB::beginTransaction();
          try {
              $input =  array();
              $position  = $data['position']; 
              $merchant  = $data['merchant']; 
              $available  = $data['check_list']; 
              $mandatory  = $data['mandatory_list']; 
              
              $this->model->where('merchant_id',$merchant)->delete();
              
              $imageFiledId =  $this->inputFieldModel
                        ->where('data_type_id','66') // must change later
                        ->where('status','A')->first()->id;

              foreach ($position as $key => $value) {
                $info['merchant_id'] = $merchant;
                $info['position'] = ($key+1);
                $info['input_field_id'] = $value;

                if (in_array($value,$available)){
                  $info['status'] = 'A';
                }else{
                  $info['status'] = 'D';
                }

                if (in_array($value,$mandatory)){
                  $info['is_mandatory'] = '1';
                }else{
                  $info['is_mandatory'] = '0';
                }
                $this->create($info);
              }

                $info['merchant_id'] = $merchant;
                $info['position'] = ($key+2);
                $info['input_field_id'] = $imageFiledId;
                $info['status'] = 'A';
                $info['is_mandatory'] = '1';
                

                $this->create($info);

              
              $result = $this->success();

              DB::commit();
          } catch (\Exception $e) {
              DB::rollback();
              $result = $this->errorUpdate();
          }

          return $result;
          
        }
    }


      public function getMerchantFieldInfoRowData(){
          $data = $this->model
                        ->select('merchants.id','company_registered_name')
                        ->join('input_fields', 'input_fields.id', '=', 'merchant_input_fields.input_field_id')
                        ->join('merchants', 'merchants.id', '=', 'merchant_input_fields.merchant_id')
                        ->join('lookups', 'lookups.id', '=', 'input_fields.data_type_id')
                        ->where('merchants.status','!=','D')
                        ->where('input_fields.status','!=','D')
                        ->distinct();

          return Datatables::of($data)

            ->addColumn('action', function ($data) {
                return '<a href="'. route("edit-merchant-field", $data->id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
      }
    
    
}