<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\Lookup;

class LookupRepository extends BaseRepository 
{

    /**
   	* @var Model
   	*/
  	protected $model;

    protected $expiry_time = 1440; // 1440 in minutes (24 hours)

  	public function __construct(Lookup $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null) 
    {        
          return Validator::make($data, []);          
    }

    public function dynamicQuery($model_query, $data) {
        
        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($data['category']) && $data['category'] != ''){
          $model_query->where('category',$data['category']);
        }

        if(!empty($data['status']) && $data['status'] != ''){
          $model_query->where('status',$data['status']);
        }

        return $model_query;
    }

    public function findLookupCache(int $id) { 
      if (!is_int($id)) { 
        return null; 
      } 

      $lookup = collect(Cache::remember('lookup', $this->expiry_time, function() {
          return $this->all()->toArray();
      }));

      return $lookup->where('id', $id)->first();
    } 

    public function getLookupCache(string $category = "") { 
      
      $lookup = Cache::remember('lookup', $this->expiry_time, function() 
      {
          return $this->all();
      });

      if(!empty($category))
      {
        $lookup = $lookup->where('category', $category);
      }

      return array_values($lookup->where('status', 'A')->toArray());
    }

    public function getGeneralStatus(){
        return Cache::remember('GeneralStatus', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'General Status'])->toArray();
        });
    }
    
    public function getPaymentStatus(){
        return Cache::remember('TransactionStatus', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Transaction Status'])->toArray();
        });
    }

    public function getGender(){
        return Cache::remember('Gender', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Gender'])->toArray();
        });
    }

    public function getAddressType(){
        return Cache::remember('Address', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Address'])->toArray();
        });
    }

    public function getRace(){
        return Cache::remember('Race', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Race'])->toArray();
        });
    }

    public function getRegisterStatus(){
        return Cache::remember('RegisterStatus', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Register Status'])->toArray();
        });
    }

    public function getMailDriver(){
        return Cache::remember('MailDriver', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Mail Driver'])->toArray();
        });
    }

    public function getMailEncryption(){
        return Cache::remember('MailEncryption', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Mail Encryption'])->toArray();
        });
    } 

    public function getPaymentMonth(){
        return Cache::remember('PaymentType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Payment Month'])->toArray();
        });
    } 

    public function getTransactionType(){
        return Cache::remember('TransactionType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Transaction Type'])->toArray();
        });
    }

    public function getTransactionCategory(){
        return Cache::remember('TransactionCategory', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Transaction Category'])->toArray();
        });
    }

    public function getCustomerType(){
        return Cache::remember('CustomerType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Customer Type'])->toArray();
        });
    } 

    public function getPackageType(){
        return Cache::remember('PackageType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Package Type'])->toArray();
        });
    }  

    public function getLanguage(){
        return Cache::remember('PackageType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Language'])->toArray();
        });
    }    
  	
    public function getReportType(){
        return Cache::remember('ReportType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Report Type'])->toArray();
        });
    }   

    public function getDownloadType(){
        return Cache::remember('DownloadType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Download Type'])->toArray();
        });
    }  

    public function getImageType(){
        return Cache::remember('ImageType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Image Type'])->toArray();
        });
    }   

    public function getIdentificationType(){
        return Cache::remember('IdentificationType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Identification Type'])->toArray();
        });
    }

    public function getCategory(){
        return Cache::remember('CategoryId', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Category'])->toArray();
        });
    }   

    public function getDealMethod(){
        return Cache::remember('DealMethodId', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Deal Method'])->toArray();
        });
    } 

    public function getInsurance(){
        return Cache::remember('InsuranceId', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Insurance'])->toArray();
        });
    }

    public function getInhouse(){
        return Cache::remember('InhouseId', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Inhouse'])->toArray();
        });
    }

    public function getRuleId(){
        return Cache::remember('Ruleid', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Rule Id'])->toArray();
        });
    }

    public function getPaymentType(){
        return Cache::remember('PaymentTypeId', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Payment Type'])->toArray();
        });
    }

    public function getOwnershipType(){
        return Cache::remember('OwnershipTypes', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Type Of Ownership'])->toArray();
        });
    }
    public function getBusinessTypes(){
        return Cache::remember('BusinessTypes', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Type Of Business'])->toArray();
        });
    }
    public function getBankNames(){
        return Cache::remember('BankNames', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Bank Name'])->toArray();
        });
    }
    public function getConvenienceChargeMethod(){
        return Cache::remember('ConvenienceMethods', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Convenience Charge Method'])->toArray();
        });
    }

    public function getDataType(){
        return Cache::remember('DataType', $this->expiry_time, function() {
          return $this->all(['status' => 'A', 'category' => 'Data Type'])->toArray();
        });
    }
    
}