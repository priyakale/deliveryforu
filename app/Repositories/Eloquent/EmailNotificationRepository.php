<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

//MODEL
use App\Models\EmailNotification;

class EmailNotificationRepository extends BaseRepository 
{
    protected $model;

  	public function __construct(EmailNotification $model)
  	{
    	$this->model = $model;
  	}

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'user_id' => 'user'
          ];

        $rule = [
            // 'user_id' => 'exists:users,id'
        ];

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function dynamicQuery($model_query, $data) {

        return $model_query;
    }

    public function save(array $data, $file_upload = []){
        
        if(empty($data)){
            return $this->errorEmptyData();
        }else{
            $validator = $this->validator($data, 'save')->validate();
            
            if(!empty($data['id'])){
                $this->update($data, $data['id']);
                $result = $this->find($data['id']);
            }else{
                $data['balance'] = 0.00;
                $result = $this->create($data);
            }
            
            if(!$result){
                return $this->errorSave();
            }else{
                Session::flash('success', 'Add / update Email Notification success'); 
            }
        }//End else
        return $result;
    }
    
  	
}