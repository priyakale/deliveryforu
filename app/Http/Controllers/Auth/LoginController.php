<?php

namespace App\Http\Controllers\Auth;

use App\Models\UserActivation;
use App\Traits\SocialAuthentication;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers, SocialAuthentication;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * The user has been authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     *
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //Redirect back if user is not activate yet
        if (!$user->activated) {

            auth()->logout();

            //TODO set flash msg
            return redirect()->back();
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Logout method
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        $user                = auth()->user();
        $user->last_login_at = date('Y-m-d H:i:s');
        $user->save();

        auth()->logout();

        return redirect()->route('login');
    }

    /**
     * Handle user activation route
     *
     * @param $token
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function activation($token)
    {
        $activation = UserActivation::where('token', $token)->first();

        if ($activation) {
            //Set to activate user
            $activation->user->activated = 1;
            $activation->user->save();

            Auth::login($activation->user);

            $activation->delete();

            return redirect($this->redirectTo);
        }

        //TODO Put flash message invalid token
        return redirect()->route('login');
    }
}
