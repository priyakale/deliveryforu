<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Response;

//HELPER
use App\Helpers\SettingHelper;
use App\Helpers\RoleHelper;

//Models
use App\Models\Merchant;
use App\Models\InputField;

//Repositories
use MerchantRepo;
use MerchantInputFieldRepo;
use InputFieldRepo;
use LookupRepo;


class InputFieldController extends Controller
{

    public function __construct(MerchantRepo $merchantRepo, InputFieldRepo $inputFieldRepo, MerchantInputFieldRepo $merchantInputFieldRepo, LookupRepo $lookupRepo )
    {
        $this->middleware('auth');
        $this->merchantRepo = $merchantRepo;
        $this->inputFieldRepo = $inputFieldRepo;
        $this->lookupRepo = $lookupRepo;
    }

    public function index(Request $request)
    {
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();
        $values['data'] = array(
            'page_title' => 'List of Input Fields' 
            ,'records' => $this->inputFieldRepo->paginate($input)
        );

        return view('admin.input_field.index',$values);
    }

    public function add(){   
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $values['data'] = array(
            'page_title' => "Add Input Field"
            ,'records' => array()
            ,'status_records' => $this->lookupRepo->getGeneralStatus()
            ,'data_types' => $this->lookupRepo->getDataType()
        );

        return view('admin.input_field.edit', $values);
            
    }

    public function edit($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $record = $this->inputFieldRepo->findBy([['id', $id]],['options'])->toArray();
        //dd($record);
        
        if(!empty($record)){

            $values['data'] = array(
                'page_title' => "Edit Input Field"
                ,'records' => $record
                ,'status_records' => $this->lookupRepo->getGeneralStatus()
                ,'data_types' => $this->lookupRepo->getDataType()
            );

            return view('admin.input_field.edit', $values);           
        }else{
            Session::flash('danger', 'Input Field not found!');
            return redirect()->route('index_merchant');
        }
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $data = $request->all();

        if(empty($data['id'])){
            $user_count_record = InputField::query();
            $user_count = $user_count_record->where('name', trim($data['name']))->count();

            if($user_count>0)
            {
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Sorry, this input field is already in the list';
                return Response::json($resultJson);
            }
        }
        
        $resultJson['result'] = 1;
        $input = $request->except(['_token']);
        $this->inputFieldRepo->save($input);
        return Response::json($resultJson);
        
    }

    public function delete($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        
        return $this->inputFieldRepo->delete($id);
    } 
}
