<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

//HELPER
use App\Helpers\SettingHelper;
use App\Helpers\RoleHelper;
use Session;

//Models
use App\Models\User;
use App\Models\Merchant;
use App\Models\Lookup;

//Repositories
use LookupRepo;
use UserRepo;
use MerchantRepo;
use InputFieldRepo;
use MerchantInputFieldRepo;

class MerchantController extends Controller
{
    public function __construct(MerchantRepo $repo, LookupRepo $lookupRepo, InputFieldRepo $inputFieldRepo, MerchantInputFieldRepo $merchantInputFieldRepo, UserRepo $userRepo )
    {
        $this->middleware('auth');
        $this->repo = $repo;
        $this->lookupRepo = $lookupRepo;
        $this->inputFieldRepo = $inputFieldRepo;
        $this->userRepo = $userRepo;
        $this->merchantInputFieldRepo = $merchantInputFieldRepo;
    }

    public function index(Request $request)
    {
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $values = array();
        $input = $request->all();
        if(!empty($input)){
            if(empty($input['company_registered_name']) || strtoupper($input['company_registered_name']) == 'ALL'){
                unset($input['company_registered_name']);
            }else{
                $input['company_registered_name'] = trim($input['company_registered_name']);
            }

            if(empty($input['sub_merchant_ref']) || strtoupper($input['sub_merchant_ref']) == 'ALL'){
                unset($input['sub_merchant_ref']);
            }else{
                $input['sub_merchant_ref'] = trim($input['sub_merchant_ref']);
            }

            if(empty($input['status']) || strtoupper($input['status']) == 'ALL'){
                unset($input['status']);
            }else{ 
                if(in_array($input['status'], Lookup::getGeneralStatus())){
                    $input['status'] = array_search($input['status'], Lookup::getGeneralStatus());    
                }else{
                    unset($input['status']);
                }
            }           
        }

        //condition
        $merchants = Merchant::query();

        if(!empty($input['sub_merchant_ref']) && trim($input['sub_merchant_ref']) != ''){
          $merchants->where('sub_merchant_ref', 'LIKE', '%'. trim($input['sub_merchant_ref']).'%');
        }                

        if(!empty($input['status_id']) && $input['status_id'] != ''){ 
          $merchants->where('status_id',$input['status_id']);
        }

        if(!empty($input['company_registered_name']) && trim($input['company_registered_name']) != ''){
          $merchants->where('company_registered_name', 'LIKE', '%'. trim($input['company_registered_name']).'%')
                    ->orWhere('individual_name', 'LIKE', '%'. trim($input['company_registered_name']).'%');
        }

        $data_merch = $merchants->paginate(SettingHelper::getPerPage());
        for($i=0;$i<count($data_merch);$i++)
        {
            if($data_merch[$i]->type_id==71)
            {
                $data_merch[$i]->new_key = $data_merch[$i]->company_registered_name;
            }
            if($data_merch[$i]->type_id==72)
            {
                $data_merch[$i]->new_key = $data_merch[$i]->individual_name;
            }              
        }
        $sorted = $data_merch->sortBy('new_key',SORT_NATURAL);
       
        $values['data'] = array(
            'page_title' => 'List of Merchants ' 
            ,'record' => $sorted
            ,'search' => $input
            ,'status_records' => Lookup::getGeneralStatus()
        );
         return view('admin.merchant.index',$values);
    }

    public function add(){  
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();
        $values['data'] = array(
            'page_title' => "Add Merchant"
            ,'records' => array()
            ,'status_records' => $this->lookupRepo->getGeneralStatus()
            ,'ownership_types' => $this->lookupRepo->getOwnershipType()
            ,'business_types' => $this->lookupRepo->getBusinessTypes()
            ,'bank_names' => $this->lookupRepo->getBankNames()
            ,'convenience_charge_methods' => $this->lookupRepo->getConvenienceChargeMethod()
            ,'input_fields' => $this->inputFieldRepo->all(['status_id'=>1])
            
        );
        return view('admin.merchant.edit', $values);
            
    }

    public function add_individual_merchant(){   
        //Check access 
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $values['data'] = array(
            'page_title' => "Add Individual Merchant"
            ,'records' => array()
            ,'status_records' => $this->lookupRepo->getGeneralStatus()
            ,'ownership_types' => $this->lookupRepo->getOwnershipType()
            ,'business_types' => $this->lookupRepo->getBusinessTypes()
            ,'bank_names' => $this->lookupRepo->getBankNames()
            ,'convenience_charge_methods' => $this->lookupRepo->getConvenienceChargeMethod()
            ,'input_fields' => $this->inputFieldRepo->all(['status_id'=>1])
            
        );
        //dd($values['data']);
        return view('admin.merchant.individual_edit', $values);
            
    }

    public function edit($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $record = $this->repo->find($id,['bank.bank_name','input_fields','user','outlets','type_of_ownership','type_of_business'])->toArray();

        if(!empty($record)){

            $values['data'] = array(
                'page_title' => "Edit Merchant"
                ,'records' => $record
                ,'status_records' => $this->lookupRepo->getGeneralStatus()
                ,'ownership_types' => $this->lookupRepo->getOwnershipType()
                ,'business_types' => $this->lookupRepo->getBusinessTypes()
                ,'bank_names' => $this->lookupRepo->getBankNames()
                ,'convenience_charge_methods' => $this->lookupRepo->getConvenienceChargeMethod()
                ,'input_fields' => $this->inputFieldRepo->all(['status_id'=>1],['data_type'])
            );
            
            //echo '<pre>';print_r($ownership);exit;
            return view('admin.merchant.edit', $values);           
        }else{
            Session::flash('danger', 'Merchant not found!');
            return redirect()->route('index_merchant');
        }
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $resultJson['result'] = 0;

        $input = $request->except(['_token']);
        
        $file_upload = $request->file();

        return $this->repo->save($input,$file_upload);
    }

    public function save_individual(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $resultJson['result'] = 0;

        $input = $request->except(['_token']);
        
        $file_upload = $request->file();
        $input['type_id'] = 72;
        return $this->repo->save_individual($input,$file_upload);
    }

    public function edit_individual_merchant($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $record = $this->repo->find($id,['bank.bank_name','input_fields','user','outlets','type_of_ownership','type_of_business'])->toArray();

       // dd($record);
        if(!empty($record)){

            $values['data'] = array(
                'page_title' => "Edit Merchant"
                ,'records' => $record
                ,'status_records' => $this->lookupRepo->getGeneralStatus()
                ,'ownership_types' => $this->lookupRepo->getOwnershipType()
                ,'business_types' => $this->lookupRepo->getBusinessTypes()
                ,'bank_names' => $this->lookupRepo->getBankNames()
                ,'convenience_charge_methods' => $this->lookupRepo->getConvenienceChargeMethod()
                ,'input_fields' => $this->inputFieldRepo->all(['status_id'=>1],['data_type'])
            );
            
            //echo '<pre>';print_r($ownership);exit;
            return view('admin.merchant.individual_edit', $values);           
        }else{
            Session::flash('danger', 'Merchant not found!');
            return redirect()->route('index_merchant');
        }
    }

    public function delete($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $record = $this->repo->find($id);
        return $this->userRepo->delete($record['user_id']);    
    }

    public function removeOutlet(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();

        return $this->repo->removeOutlet($input['delete_outlet_id']);
    } 
}
