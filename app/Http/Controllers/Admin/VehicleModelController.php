<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VehicleModel\CreateRequest;
use App\Http\Requests\VehicleModel\UpdateRequest;
use App\Models\VehicleModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use VehicleModelRepo;
use VehicleRepo;

class VehicleModelController extends Controller
{
    /**
     * @var VehicleModelRepo
     */
    private $repo;

    /**
     * VehicleModelController constructor.
     *
     * @param VehicleModelRepo $repo
     * @param VehicleRepo $vehicleRepo
     */
    public function __construct(VehicleModelRepo $repo, VehicleRepo $vehicleRepo)
    {
        $this->repo = $repo;
        $this->vehicleRepo = $vehicleRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repo->all();

        return view('admin.vehicle_model.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View\
     */
    public function create()
    {
        $vehicles = $this->vehicleRepo->all();

        return view('admin.vehicle_model.edit', compact('vehicles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $model = $this->repo->create($request->all());

        return redirect()->route('admin.vehicle-model.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param VehicleModel $vehicleModel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(VehicleModel $vehicleModel)
    {
        $vehicles = $this->vehicleRepo->all();
        $model = $vehicleModel;

        return view('admin.vehicle_model.edit', compact('vehicles', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param VehicleModel $vehicleModel
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, VehicleModel $vehicleModel)
    {
        $model = $this->repo->update($request->all(), $vehicleModel->id);

        return redirect()->route('admin.vehicle-model.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param VehicleModel $vehicleModel
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(VehicleModel $vehicleModel)
    {
        $vehicleModel->delete();

        return redirect()->back();
    }
}
