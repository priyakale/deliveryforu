<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Route;
use DateTime;
use Response;

//HELPER
use App\Helpers\RoleHelper;

//MODEL
use App\Models\Menu;

class MenuController extends Controller
{   
    /**** GENERAL FUNCTION ****/
    public function __construct(){
        $this->middleware('auth');
    }

    public static function getRouteLink(){
        $link_records = array();

        foreach (Route::getRoutes()->getRoutes() as $route){
            //get the prefix
            $prefix_value = $route->getPrefix();
            $prefix_value_sub = explode('/', $prefix_value);
            if($prefix_value_sub[0] == 'admin'){
              $action_link = $route->getName();
              $action_controller = $route->getActionName();
              $action_method = $route->methods();
              $action_parameter = $route->parameterNames();
              $raw_data = explode('@', $action_controller);

              $controller_name = "";
              if(count($raw_data) > 1){
                  $raw_controller = explode('\\', $raw_data[0]);
                  $controller_name = $raw_controller[count($raw_controller) - 1];
              }
              if($action_link == ""){
                  //break;
              }else{
                  if($controller_name != ""){
                      if(count($action_method) > 1 && empty($action_parameter)){
                          if($action_method[0] == 'GET'){
                              $link_records[$controller_name][] = trim($action_link);
                          }
                      }
                  }
              }
            }
        }
        
        return $link_records;
    }
    /**** END GENERAL FUNCTION ****/

    public function index(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

    	$values = array();

        //Search base on request (GET) value
        $input = $request->all();
        if(!empty($input)){
            if(empty($input['keyword']) || strtoupper($input['keyword']) == 'ALL'){
                unset($input['keyword']);
            }else{
                $input['keyword'] = trim($input['keyword']);
            }

        }

        //get total menu
        $total_menu = Menu::count();

        //condition
        $menu_records = Menu::query();  
        if (array_key_exists('keyword', $input)) 
        	$menu_records->where('name', 'LIKE', '%'. trim($input['keyword']).'%');                    
       
        
        //set piginate to 10 
        $records = $menu_records->orderBy('parent_id')->paginate(10);
         foreach ($records as $key => &$value) {
            $date = new DateTime( $value['updated_at'] );
           // dd($date);
            $value['updated_at_value'] = $date->format('d/m/Y H:i:s');

            if(!empty($value['parent_id'])){
                $menu_record_tmp= Menu::select('name')
                ->where('id', '=', trim($value['parent_id']))
                ->first();
                if(!empty($menu_record_tmp['name'])){
                    $value['parent_name'] = $menu_record_tmp['name'];
                }
            }
        }

        $values['data'] = array(
            'page_title' => 'List of Menu'
            , 'total_menu' => $total_menu
            , 'records' => $records
            , 'input' => $input
        );
        //dd($values['data']['records']);       
        return view('admin.menu.index', $values);
    }

    public function add(){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $menu_record = Menu::select('id','name', 'parent_id')->orderBy('name','asc')->get();
        
            foreach ($menu_record as $key => &$value) {
                if(!empty($value['parent_id'])){
                    $menu_record_tmp= Menu::select('name')
                    ->where('id', '=', trim($value['parent_id']))
                    ->first();
                    if(!empty($menu_record_tmp['name'])){
                        $value['name'] = $menu_record_tmp['name'].' -> '.$value['name'];
                    }
                }
            }            
        // dd($menu_record->toArray());
        $link_records = $this->getRouteLink();

        $resultJson = array();
        $value = array();
        $values['data'] = array(
              'page_title' => 'Add Menu'
            , 'record' => array()
            , 'menu_record' => $menu_record->sortBy('name')
            , 'link_records' => $link_records
        );

        $resultJson['result'] = 1;
        $resultJson['returnHTML'] = view('admin.menu.edit',$values)->render();

        return Response::json($resultJson);
    }

    public function edit($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $record = Menu::find($id);
        $resultJson['result'] = 0;

        if(!empty($record)){
            $menu_record = Menu::select('id','name', 'parent_id')->orderBy('parent_id')->get();
            foreach ($menu_record as $key => &$value) {
                if(!empty($value['parent_id'])){
                    $menu_record_tmp= Menu::select('name')
                    ->where('id', '=', trim($value['parent_id']))
                    ->first();
                    if(!empty($menu_record_tmp['name'])){
                        $value['name'] = $menu_record_tmp['name'].' -> '.$value['name'];
                    }
                }
            }

            $link_records = $this->getRouteLink();

            foreach ($link_records as $key => $value) {
                if(in_array($record['link_name'],$value)){
                    $record['controller_name'] = $key;
                    break;
                }
            }
            
            if(!empty($record['parent_id'])){
                $menu_record_tmp= Menu::select('name')
                ->where('id', '=', trim($record['parent_id']))
                ->first();
                if(!empty($menu_record_tmp['name'])){
                    $record['parent_name'] = $menu_record_tmp['name'];
                }
            }
            
            $resultJson = array();
            $value = array();
            $values['data'] = array(
                'page_title' => 'Menu Edit'
                , 'record' => $record
                , 'menu_record' => $menu_record
                , 'link_records' => $link_records
            );

            $resultJson['result'] = 1;
            $resultJson['returnHTML'] = view('admin.menu.edit',$values)->render();
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'There are no record for this menu!';
        }

        return Response::json($resultJson);
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        try{
            $input = $request->all();
            $model_menu = new Menu();

            if(empty($input)){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Empty to save data!';
            }else{
                $requried_data = array('name','link');
                foreach ($input as $key => $value) {
                    if(in_array($key, $requried_data)) {
                        if(empty($value)){
                            $resultJson['result'] = 0;
                            $resultJson['errMsg'] = 'Missing requried data : '.$key;
                        }
                    }
                }

                if(!empty($input['parent_name'])){
                    if($input['parent_name'] == 'Select Parent'){
                        $input['parent_id'] = null;
                    }else{
                        $menu_records_tmp= Menu::select('id')
                        ->where('name', '=', trim($input['parent_name']))
                        ->first();
                        if(!empty($menu_records_tmp['id'])){
                            $input['parent_id'] = $menu_records_tmp['id'];
                        }
                    }
                }

                if (!empty($input['id'])) { //edit
                    $model_menu = $model_menu::find($input['id']);
                }

                $model_menu->fill($input);

                if(!$model_menu->save()){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Error : Cant save menu data!';
                }else{
                    $resultJson['result'] = 1;
                }
            }
        }catch(Exception $e){ // do task when error
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = $e->getMessage();
        }   
        return Response::json($resultJson);
    }

    public function delete($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        
       $menu_record = Menu::find($id);
        if(!empty($menu_record)){
            
        }else{
            
        }
        return Response::json($resultJson); 
    }
}
