<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;
use Response;

//HELPER
use App\Helpers\RoleHelper;
use App\Helpers\SettingHelper;

//MODEL
use App\Models\PaymentSetting;
use App\Models\Currency;

//REPOSITORY
use PaymentSettingRepo;
use CurrencyRepo;

class PaymentSettingController extends Controller
{
    public function __construct(PaymentSettingRepo $paymentSettingRepo, CurrencyRepo $currencyRepo){
        $this->middleware('auth');
        $this->paymentSettingRepo = $paymentSettingRepo;
        $this->currencyRepo = $currencyRepo;
    }
    /**** END GENERAL FUNCTION ****/

    public function index(Request $request) {
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $values['data'] = array(
            'page_title' => 'Upay Configuration'
            ,'record' => $this->paymentSettingRepo->getPaymentSettingCache()
            ,'currency_record' => $this->currencyRepo->getCurrencyCache()
        );

        return view('admin.payment.index', $values);
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        try{
            $input = $request->all();
            $model_currency = new Currency();
            $model_PaymentSetting = new PaymentSetting();

            if(empty($input)){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Empty to save data!';
            }else{
                $requried_data = array('merchant_identify','verify_key','rate', 'currency_id');
                foreach ($input as $key => $value) {
                    if(in_array($key, $requried_data)) {
                        if(empty($value)){
                            $resultJson['result'] = 0;
                            $resultJson['errMsg'] = 'Missing requried data : '.$key;
                            return Response::json($resultJson);
                        }else{
                            $input[$key] = trim($value);
                        }
                    }
                }

                if (!empty($input['id'])) { //edit
                    $model_PaymentSetting = $model_PaymentSetting::find($input['id']);
                }

                if(!empty($input['mode_value'])){
                    $input['mode'] = $input['mode_value'];
                }
                //dd($input);
                $model_PaymentSetting->fill($input);

                if(!$model_PaymentSetting->save()){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Error : Cant save payment setting!';
                }else{
                    Cache::forget('PaymentSetting');
                    $resultJson['result'] = 1;
                    Session::flash('success', 'Add / update payment setting success'); 
                }
            }
        }catch(Exception $e){ // do task when error
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = $e->getMessage();
        }   
        return Response::json($resultJson);
        
    } 

    
}
