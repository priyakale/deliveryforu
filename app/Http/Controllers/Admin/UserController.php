<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Notifications\Email;
use Notification;
use Session;
use DateTime;
use Response;

//HELPER
use App\Helpers\RoleHelper;
use App\Helpers\SettingHelper;

//MODEL 
use App\Models\User;
use App\Models\Role;
use App\Models\Lookup;
use App\Models\EmailNotification;

class UserController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $values = array();
        //Search base on request (GET) value
        $input = $request->all();
        if(!empty($input)){
            if(empty($input['keyword']) || strtoupper($input['keyword']) == 'ALL'){
                unset($input['keyword']);
            }else{
                $input['keyword'] = trim($input['keyword']);
            }

            if(empty($input['status']) || strtoupper($input['status']) == 'ALL'){
                unset($input['status']);
            }else{
                if(in_array($input['status'], Lookup::getGeneralStatus())){
                    $input['status'] = array_search($input['status'], Lookup::getGeneralStatus());    
                }else{
                    unset($input['status']);
                }
            }

            if(empty($input['role_name']) || strtoupper($input['role_name']) == 'ALL'){
                unset($input['role_name']);
            }else{
                $input['role_name'] = trim($input['role_name']);
                $role_record = Role::select('id')->where('name', '=', $input['role_name'])->first();
                if(!empty($role_record)){
                    $input['role_id'] = $role_record->id;
                }else{
                    unset($input['role_name']); 
                }
            }
        }

        //get total user
        $total_user = User::count();
        $role_records = Role::select('name', 'id')->get();
        //condition
        $user_records = User::query();
        
        if(!empty($input['keyword']) && trim($input['keyword']) != ''){
          $user_records->where('name', 'LIKE', '%'. trim($input['keyword']).'%');
        }

        if(!empty($input['status_id']) && $input['status_id'] != ''){
          $user_records->where('status_id',$input['status_id']);
        }

        if(!empty($input['role_id']) && $input['role_id'] != ''){
          $user_records->where('role_id',$input['role_id']);
        }

        $records = $user_records->paginate(SettingHelper::getPerPage());
        foreach ($records as $key => &$value) {
            $date = new DateTime( $value['updated_at'] );
            $value['updated_at_value'] = $date->format('d/m/Y H:i:s');

            $value['role_desc'] = 'Undefind role';
            if(!empty($value['role_id'])){
                $role_record = Role::where('id', '=', $value['role_id'])->first();
                if(!empty($role_record)){
                    $value['role_desc'] = $role_record->name;
                }
            }

            $value['status_id'] = Lookup::find($value['status_id']);
        }

        $values['data'] = array(
            'page_title' => 'List of User'
            , 'total_user' => $total_user
            , 'records' => $records
            , 'role_records' => $role_records
            , 'search' => $input
            , 'status_records' => Lookup::getGeneralStatus()
        );
        
       // print_r($values['data']);exit;
        return view('admin.user.index', $values);
    }

    public function view($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $record = User::find($id);
        
        if(!empty($record)){
        
            $values['data'] = array(
                'page_title' => 'View User'
                ,'record' => $record
            );

            return view('admin.user.view', $values);
        }else{
            Session::flash('danger', 'User not found!');
            return redirect()->route('index_user');
        }
    }

    public function update_user(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();
        $form_array = array();
        $value_array = array();
        $form_array['username'] = $input['name'];
        
        //check password
        if(!empty($input['password'])){
            if(!empty($input['confirm'])){
                if($input['password'] != $input['confirm']){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Password & Confirm password not match!';
                    return Response::json($resultJson);
                }
            }else{
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Please confirm your password';
                return Response::json($resultJson);
            }
        }
        //end check password

        foreach ($input as $key => $value) {
            if($key == "_token" || $key == 'status_id' || $key == 'role_name' || $key == 'confirm'){
                if($key == 'status_id'){
                    if(!empty($value)){
                        $status_record = Lookup::select('id', 'name')->where([['category', "General Status"],['name',$value]])->first();
                        if(!empty($status_record)){
                            $form_array['status_id'] = $status_record->id;
                        }else{
                            $form_array['status_id'] = '2';
                        }
                    }
                }
                if($key == 'role_name'){
                    $role_records = Role::select('name', 'id')->where('name', trim($value))->get()->first();
                    if(!empty($role_records)){
                        $form_array['role_id'] = $role_records->id;
                    }else{
                        $form_array['role_id'] = '0';
                    }
                }
            }else{
                if(!empty(trim($value))){
                    $user_count = 0;
                    //check email duplicate or not
                    if($key == 'email'){
                        $user_count_record = User::query();
                        if(!empty($form_array['id'])){
                            $user_count = $user_count_record->where('id', '!=' , $form_array['id'])
                                        ->where('email', trim($value))->count();
                        }else{
                            $user_count = $user_count_record->where('email', trim($value))->count();
                        }

                        if($user_count > 0){
                            $resultJson['result'] = 0;
                            $resultJson['errMsg'] = 'Sorry, this email is already in the list';
                            
                            return Response::json($resultJson);
                        }
                    }
                    $form_array[$key] =  $value;
                }
            }

            if($key == 'password' && !empty($value)){
               $form_array['password'] = bcrypt($value);
            }

        }//End for checking validation
        // dd($form_array);
        if(!empty($form_array)){
            $model = new User();

            if(!empty($form_array['id'])){ //update
                $model = $model::find($form_array['id']);
            }

            $model->fill($form_array);
            
            try{
                if(!$model->save()){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Error saving user detail!';
                }else{
                    //upload image
                    if ($file = $request->file('profile_pic')) {
                        $fileName = $model->id;
                        $extension = $file->getClientOriginalExtension() ? : 'png';
                        $destinationPath = public_path() . '/uploads/profile_pic/';
                        $safeName = $fileName . '.' . $extension;
                        $file->move($destinationPath, $safeName);
                        $model->profile_pic = $safeName;
                        $model->save();
                    }

                    $resultJson['result'] = 1;
                    Session::flash('success', 'Success update latest user details'); 
                }
            }catch(Exception $e){ // do task when error
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = $e->getMessage();
            }  
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'Error saving user detail.. Detail Empty';
        }

        return Response::json($resultJson);
    }

    public function user_detail(Request $request, $id){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $role_records = Role::select('name', 'id')->where('status_id', 1)->get();
        
        try{
            $user_obj =  User::find($id);
            if(!empty($user_obj) && !empty($id)){
                
                $user_obj['status_id'] = Lookup::select('id', 'name')->find($user_obj['status_id']);
                foreach ($role_records as $key => $role_record) {
                    if($role_record->id == $user_obj->role_id){
                        $user_obj->role_name = $role_record->name;
                        break;
                    }
                }
                
                $values['data'] = array(
                    'page_title' => 'Update User Detail'
                    , 'records' => $user_obj
                    , 'role_records' => $role_records
                    , 'status_records' => Lookup::getGeneralStatus() 
                );

                $resultJson['result'] = 1;
                $resultJson['returnHTML'] = view('admin.user.user_detail',$values)->render();
            }else{
                $values['data'] = array(
                    'page_title' => 'Create New User Detail'
                    , 'records' =>  collect()
                    , 'role_records' => $role_records
                    , 'status_records' => Lookup::getGeneralStatus()
                );

                $resultJson['result'] = 1;
                $resultJson['returnHTML'] = view('admin.user.user_detail',$values)->render();
            }
        }catch(Exception $e){ // do task when error
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = $e->getMessage();
        }
        
        return Response::json($resultJson);
    }

    public function showProfile($id)
    {
        $user = Cache::get('user:'.$id);

        return view('profile', ['user' => $user]);
    }

}
