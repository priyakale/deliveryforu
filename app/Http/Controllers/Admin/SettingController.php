<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Session;

//HELPER
use App\Helpers\RoleHelper;
use App\Helpers\SettingHelper;

//MODEL
use App\Models\Layout;
use App\Models\GeneralSetting;

class SettingController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**** END GENERAL FUNCTION ****/

    public function index(Request $request) {
    	//Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

    	$skin_colours = Layout::where('attribute','skin')->get();
    	$setting = GeneralSetting::find(1);

    	$values['data'] = array(
	    	'page_title' => 'List of Application'
	    	,'skin_colours' => $skin_colours
	    	,'setting' => $setting
	    );

    	return view('admin.setting.index', $values);
    }

    public function save(Request $request){
    	//Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

	    $input = $request->all();

	    $this->validate($request,[
			'app_name'=>'required',
			'skin'=>'required',
			'owner'=>'required',
			'version'=>'required',
			'per_page'=>'required',
           // 'per_page_api'=>'required',
			'datetime_format'=>'required'
		]); 

        if( $request->hasFile('icon') ) {
            $file = $request->file('icon');

            $destination = public_path().'/img';

            //Define the name
            $filename= $file->getClientOriginalName();

            if (file_exists($destination.'/'.$filename)){
                $filename = date('Y-m-d H-i-s').'-'.$file->getClientOriginalName();
            }

            //after that move the file to the directory
            $file->move($destination, $filename);

            //Now you have your file in a variable that you can do things with
            $path = $destination.'/'. $filename ;   
            $input['icon_name']=$filename;
        }
	 
	    $model = New GeneralSetting();

		if(!empty($input['id'])){
			$model = $model->find($input['id']);
        }

		$model->fill($input);

		if($model->save()){
			SettingHelper::removeGeneral();
			Session::flash('success', 'Add Setting complete!');
			return redirect()->action('Admin\SettingController@index'); 
		}
		else{
			Session::flash('error', 'Sorry Add Setting could not complete!');
			return redirect()->action('Admin\SettingController@index'); 
		}
		
  	} 
}
