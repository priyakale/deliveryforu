<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use DateTime;
use Response;
use PDF;
use Validator;
use Excel;
use App\Exports\RawDataExport;

//REPOSITORY
use LookupRepo;
use UserRepo;
use TransactionRepo;

//HELPER
use App\Helpers\RoleHelper;

class ReportController extends Controller
{
	/**** GENERAL FUNCTION ****/
    public function __construct(LookupRepo $lookupRepo, UserRepo $userRepo, TransactionRepo $transactionRepo){

        $this->middleware('auth');
        $this->lookupRepo = $lookupRepo;
        $this->userRepo = $userRepo;
        $this->transactionRepo = $transactionRepo;
    }
    /**** END GENERAL FUNCTION ****/

	public function index(Request $request){
    	$access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }

    	$values['data'] = array(
	    	'page_title' => 'Download Report'
            , 'download_type' => $this->lookupRepo->getDownloadType()
            , 'report' => $this->lookupRepo->getReportType()
	    );

        return view('admin.report.index',$values);
    }

    public function download_report(Request $request){
    	$access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }

        $input = $request->all();
        
        $this->validate($request, [
            'date_from' => 'required',
            'date_to' => 'required',
            'download_type' => 'required',
            'report' => 'required'
        ]);
        
        if(empty($input['date_from'])){
          	unset($input['date_from']);
      	}else{
          	$date = DateTime::createFromFormat('d/m/Y', $input['date_from']);

          	$input['date_from'] = $date->format('Y-m-d').' 00:00:00';
      	}

      	if(empty($input['date_to'])){
          	unset($input['date_to']);
      	}else{
          	$date = DateTime::createFromFormat('d/m/Y', $input['date_to']);
          	$input['date_to'] = $date->format('Y-m-d').' 23:59:59';
      	}
        
        if($input['report']==67){
      		return redirect()->route('transaction_report', ['date_from' => $input['date_from'], 'date_to' => $input['date_to'], 'type' => $input['download_type']]);
      	}
    }

  	public function transaction_report($date_from, $date_to, $type){
	    //Check access
     	$access = RoleHelper::checkRoleAccess();
	    if(!$access['result']){
	        return $access['data'];
	    }
	    //end check access

	    $transaction_records = $this->transactionRepo->all(['date_from' => $date_from, 'date_to' => $date_to],['fields.field.data_type','merchant','status','user','merchant']);	 

        if(!$transaction_records->isEmpty()){
        	$date_report = date('Ymdhis');
        	$file_name = 'Raw_data'.$date_report.'.txt';
   //      	$total_records = 0;

   //      	$file_contents = "01|".$file_name."\r\n";
		 //    foreach ($transaction_record as $key => $value) {
		 //    	if(!empty($value['sku'])){
		 //    		$brand_model_color = $value['sku']['client_brand_model_color'];
		 //    		$tier_name = "Tier ".$value['sku']['tier'];
		 //    	}
		 //    	else{
		 //    		$brand_model_color = $value['make']." ".$value['model']." ".$value['color']." ".$value['capacity'];
		 //    		$tier_name = $value['tier']['name'];
		 //    	}

		 //    	$file_contents .= "02|".strtoupper($value['transactions'][0]['transaction_type']['name']);
		 //    	$file_contents .= "|".$value['user']['mobile_number'];
		 //    	$file_contents .= "|".$value['user']['email'];
		 //    	$file_contents .= "|".$value['user']['identification_type']['name'];
		 //    	$file_contents .= "|".$value['user']['identification_number'];
		 //    	$file_contents .= "|".$brand_model_color;	
		 //    	$file_contents .= "|".$value['imei'];
		 //    	$file_contents .= "|".$value['package_fee']['package']['name']." (".$tier_name.")";
		 //    	$file_contents .= "|".$value['device_purchased_date'];
		 //    	$file_contents .= "|".$value['transactions'][0]['start_date'];
		 //    	$file_contents .= "|".$value['enrollment_termination_date'];
		 //    	$file_contents .= "|".$value['user']['parent_referral']['name'];
			//     $file_contents .= "\r\n";

			//     $total_records++;
		 //    }
			// $file_contents .= "09|".$total_records;

		    // if($type='Text File'){
		    	
		    //     $headers = [
		    // 		'Content-type'=>'text/plain',
		    // 		'Content-Disposition'=>sprintf('attachment; filename="%s"', $file_name),
		    // 		'Content-Length'=>sizeof($file_contents)
		    // 	];

		    //     return Response::make($file_contents, 200, $headers);
		    // }
		    $values['data'] = array(
	            'records' => $transaction_records
	            ,'date_from' => $date_from
	            ,'date_to' => $date_to
	            ,'type' => $type
	        );


		    if($type=='PDF'){
	        	$pdf = PDF::loadView('admin.report.pdf.transaction_report', $values)
	              	->setPaper('A4', 'landscape');
	        	return $pdf->download('Transactions_Report_'.$date_report.'.pdf');

	      	}else if($type=='Excel'){

		        //return Excel::download(new RawDataExport($values), 'Raw_Data-'.$date_report.'.xlsx');


	        	Excel::create('Transactions_Report_'.$date_report, function($excel) use($values) {
		        	$excel->sheet('New sheet', function($sheet) use($values) {
		            	$sheet->loadView('admin.report.excel.transaction_report', $values);

			            $sheet->setColumnFormat(array(
			                'E' => '#,##0.00'
			            ));
			        });
			        $lastrow= $excel->getActiveSheet()->getHighestRow();    
					$excel->getActiveSheet()->getStyle('A1:J'.$lastrow)->getAlignment()->setWrapText(true); 
			    })->export('xls');
	      	}
		    //End Generate Report  
		}
		else{
			Session::flash('danger', 'No record found on the selected date');
			return redirect()->action('Admin\ReportController@index');
		}  
  	}

  	

  	
}
