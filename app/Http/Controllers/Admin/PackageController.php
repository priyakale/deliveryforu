<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

//HELPER
use App\Helpers\SettingHelper;
use App\Helpers\RoleHelper;
use Session;

//Models
use App\Models\User;
use App\Models\PackageService;
use App\Models\Lookup;

//Repositories
use LookupRepo;
use UserRepo;
use App\Repositories\Eloquent\PackageServiceRepository;
use InputFieldRepo;
use App\Repositories\Eloquent\ServiceInputFieldRepository;

class PackageController extends Controller
{
    public function __construct(PackageServiceRepository $repo, LookupRepo $lookupRepo, InputFieldRepo $inputFieldRepo, ServiceInputFieldRepository $serviceInputFieldRepo)
    { 
        $this->middleware('auth');
        $this->repo = $repo;
        $this->lookupRepo = $lookupRepo;
        $this->inputFieldRepo = $inputFieldRepo;
        $this->serviceInputFieldRepo = $serviceInputFieldRepo;
    }

    public function index(Request $request)
    {
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $values = array();
        $input = $request->all();        
        if(!empty($input)){           

            if(empty($input['name']) || strtoupper($input['name']) == 'ALL'){
                unset($input['name']);
            }else{
                $input['name'] = trim($input['name']);
            }

            if(empty($input['status']) || strtoupper($input['status']) == 'ALL'){
                unset($input['status']);
            }else{ dd($input['status']);
                if(in_array($input['status'], Lookup::getGeneralStatus())){
                    $input['status'] = array_search($input['status'], Lookup::getGeneralStatus());

                }else{
                    unset($input['status']);
                }
            }           
        }
        //condition
        $packages = PackageService::query(); 

        if(!empty($input['status_id']) && $input['status_id'] != ''){ 
          $packages->where('status_id',$input['status_id']);
        }

        if(!empty($input['name']) && trim($input['name']) != ''){
          $packages->where('name', 'LIKE', '%'. trim($input['name']).'%');
        }

        $data_pkg = $packages->paginate(SettingHelper::getPerPage());
        for($i=0;$i<count($data_pkg);$i++)
        {
            $data_pkg[$i]->new_key = $data_pkg[$i]->name;
        }
        $sorted = $data_pkg->sortBy('new_key',SORT_NATURAL);
        
        $values['data'] = array(
            'page_title' => 'Package List' 
            ,'record' => $sorted
            ,'search' => $input
            ,'status_records' => Lookup::getGeneralStatus()            
        );
        return view('admin.package.index',$values);
    }

    public function add(){  
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access       
        $value = array();
        $values['data'] = array(
            'page_title' => "Add Package"
            ,'records' => array()
            ,'status_records' => $this->lookupRepo->getGeneralStatus()
            ,'category_id' => $this->lookupRepo->getCategory()            
            ,'deal_method_id' => $this->lookupRepo->getDealMethod()
            ,'insurance_id' => $this->lookupRepo->getInsurance()
            ,'inhouse_id' => $this->lookupRepo->getInhouse()
            ,'ruleid' => $this->lookupRepo->getRuleId()
            ,'payment_type_id' => $this->lookupRepo->getPaymentType()
            ,'input_fields' => $this->inputFieldRepo->all(['status_id'=>1])
            
        );       
        return view('admin.package.edit', $values);            
    }

    public function edit($id){ 
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $record = $this->repo->find($id,['servicedocs','input_fields','subservice','status_id','category_id','deal_method_id','insurance_id','inhouse_id','ruleid','payment_type_id'])->toArray();
        //dd($record);
        if(!empty($record)){

            $values['data'] = array(
                'page_title' => "Edit Package"
                ,'records' => $record
                ,'status_records' => $this->lookupRepo->getGeneralStatus()
                ,'category_id' => $this->lookupRepo->getCategory()
                ,'deal_method_id' => $this->lookupRepo->getDealMethod()
                ,'insurance_id' => $this->lookupRepo->getInsurance()
                ,'inhouse_id' => $this->lookupRepo->getInhouse()
                ,'ruleid' => $this->lookupRepo->getRuleId()
                ,'payment_type_id' => $this->lookupRepo->getPaymentType()
                ,'input_fields' => $this->inputFieldRepo->all(['status_id'=>1],['data_type'])
            );            
            //echo '<pre>';print_r($values['data']);exit;

            return view('admin.package.edit', $values);           
        }else{
            Session::flash('danger', 'Package not found!');
            return redirect()->route('index_packageservice');
        }
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $resultJson['result'] = 0;

        $input = $request->except(['_token']);    
        $file_upload = $request->file();

        return $this->repo->save($input,$file_upload);
    }     

    public function delete($id){       
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access 
        $record = $this->repo->find($id);        
        return $this->repo->delete($record['id']);    
    }

    public function removeSubServices(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);

        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();
       
        return $this->repo->removeSubServices($input['delete_sub_service_id']);
    } 
}
