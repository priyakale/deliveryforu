<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

//HELPER
use App\Helpers\RoleHelper;

//REPOSITORY
use LookupRepo;
use TransactionRepo;

class TransactionController extends Controller
{
    /**** GENERAL FUNCTION ****/
    public function __construct(TransactionRepo $transactionRepo, LookupRepo $lookupRepo){
        $this->middleware('auth');
        $this->lookupRepo = $lookupRepo;
        $this->transactionRepo = $transactionRepo;
    }
    /**** END GENERAL FUNCTION ****/

    public function index(Request $request) {
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();
        //$input = 21;

        $values['data'] = array(
            'records' => $this->transactionRepo->paginate($input,['fields','merchant','status','user.investor_details','user.merchant_details','investor'])
            ,'search' => $input
            ,'status_records' => $this->lookupRepo->getGeneralStatus()
        );

        return view('admin.transaction.index', $values);
           
    }

    public function view($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $value = array();

        $record = $this->transactionRepo->findBy([['id', $id]],['fields.field.data_type','merchant','status','user','merchant']);
        
        if(!empty($record)){

            $values['data'] = array(
                'page_title' => "View Transaction"
                ,'record' => $record
                ,'status_records' => $this->lookupRepo->getGeneralStatus()
            );

            return view('admin.transaction.view', $values);           
        }else{
            Session::flash('danger', 'Transaction not found!');
            return redirect()->route('index_package');
        }
    }
}
