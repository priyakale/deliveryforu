<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Response;

//HELPER
use App\Helpers\RoleHelper;

//MODEL
use App\Models\EmailSetting;
use App\Models\Lookup;

class EmailSettingController extends Controller
{
    /**** GENERAL FUNCTION ****/
    public function __construct(){
        $this->middleware('auth');
    }
    /**** END GENERAL FUNCTION ****/

    public function index(Request $request){
    	//Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

    	$email_setting = EmailSetting::first();

    	$values['data'] = array(
	    	'page_title' => 'Email Setting'
	    	,'records' => $email_setting
            ,'email_driver' => Lookup::select('id','name')->where('category','Mail Driver')->get()
            ,'email_encryption' => Lookup::select('id','name')->where('category','Mail Encryption')->get()
	    );

        return view('admin.email.index',$values);
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

    	$resultJson['result'] = 0;
        $input = $request->all();

        $model_email_setting = new EmailSetting();

        if(empty($input)){
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'Empty to save data!';
        }else{
            $required_data = array(
                'name' => 'Name cannot be empty'
                ,'driver' => 'Email Driver cannot be empty'
            );
            
            if(!empty($input['driver'])){
                $driver_name = Lookup::find($input['driver']);
                if($driver_name['name']=='smtp'){
                    $required_data['smtp_host'] = 'SMTP Host cannot be empty';
                    $required_data['smtp_username'] = 'SMTP Username cannot be empty';
                    $required_data['smtp_password'] = 'SMTP Password cannot be empty';
                    $required_data['encryption'] = 'SMTP Encryption cannot be empty';
                }
                else if($driver_name['name']=='sendmail'){
                    $required_data['sendmail_path'] = 'SMTP Sendmail Path cannot be empty';
                }
            }

            foreach ($input as $key => $value) {
                if(array_key_exists($key, $required_data)) {
                    if(empty($value)){
                        $resultJson['result'] = 0;
                        $resultJson['errMsg'] = 'Missing required data : '.$required_data[$key];
                        $resultJson['error_field'] = $key;
                        return Response::json($resultJson);
                    }
                }
            }
            
            if(!empty($input['id'])){
            	$model_email_setting = $model_email_setting->find($input['id']);
            }

            $model_email_setting->fill($input);
            
            if(!$model_email_setting->save()){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Error : Cant save Email Setting!';
            }else{
                $resultJson['result'] = 1;
                Session::flash('success', 'Add / update Email Setting success'); 
            }
        }//END ELSE
        return response()->json($resultJson);
    }
}
