<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DateTime;

//HELPER
use App\Helpers\RoleHelper;
use App\Helpers\SettingHelper;

//MODEL
use App\Models\Audit;
use App\Models\User;

class AuditController extends Controller
{
	//GENERAL FUNCTION
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
    	//Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $values = array();

        //Search base on request (GET) value
        $input = $request->all();
        if(!empty($input)){
            if(empty($input['name']) || $input['name'] == 'All'){
                unset($input['name']);
            }else{
                $user_data = User::select('id')->find($input['name']);
                if(!empty($user_data)){
                	$input['user_id'] = $user_data->id;
                }
            }

            if(empty($input['model_name']) || $input['model_name'] == 'All'){
                unset($input['model_name']);
            }else{
                $model_data = Audit::select('auditable_type')
                ->where('auditable_type','App\\Models\\'.trim($input['model_name']))
                ->first();
                if(!empty($model_data)){
                	$input['auditable_type'] = $model_data->auditable_type;
                }
            }

            if(empty($input['action_name']) || $input['action_name'] == 'All'){
                unset($input['action_name']);
            }else{
            	$input['event'] = trim($input['action_name']);
            }

            if(empty($input['date_from'])){
                unset($input['date_from']);
            }else{
                $date = DateTime::createFromFormat('d/m/Y', $input['date_from']);
                $input['date_from'] = $date->format('Y-m-d').' 00:00:00';;
            }

            if(empty($input['date_to'])){
                unset($input['date_to']);
            }else{
                $date = DateTime::createFromFormat('d/m/Y', $input['date_to']);
                $input['date_to'] = $date->format('Y-m-d').' 23:59:59';
            }
        }

        //get total
        $total_audit = Audit::count();
        
        //Model @ table record
        $model_records = Audit::distinct()->select('auditable_type')->groupBy('auditable_type')->get();
        foreach ($model_records as $key => &$value) {
        	$table_array = explode("\\", $value['auditable_type']);
        	if(count($table_array) > 1){
        		$value['auditable_type'] = $table_array[2];
        	}
        }

        //action data
        $action_records = Audit::distinct()->select('event')->groupBy('event')->get();

        //user data
        $user_records = User::distinct()->select('id','name')->get();
        
        //condition
        $audit_records = Audit::query();
        
        //Date range 
        if (array_key_exists('date_from', $input)) $audit_records->where('updated_at', '>=', $input['date_from']);
        if (array_key_exists('date_to', $input)) $audit_records->where('updated_at','<=', $input['date_to']);

        if (array_key_exists('user_id', $input)) $audit_records->where('user_id',$input['user_id']);
        if (array_key_exists('auditable_type', $input)) $audit_records->where('auditable_type', $input['auditable_type']);
        if (array_key_exists('event', $input)) $audit_records->where('event', $input['event']);

        $records = $audit_records->orderBy('updated_at', 'desc')->paginate(SettingHelper::getPerPage());
        foreach ($records as $key => &$value) {
        	$table_array = explode("\\", $value['auditable_type']);
        	if(count($table_array) > 1){
        		$value['auditable_type'] = $table_array[2];
        	}

        	if(!empty($value['user_id'])){
        		$user_record = User::find($value['user_id']);
        		if(!empty($user_record)){
        			$value['name'] = $user_record->name;
        		}
        	}

            $date = new DateTime( $value['updated_at'] );
            $value['updated_at_value'] = $date->format(SettingHelper::getDatetimeFormat());
        }
        
        $values['data'] = array(
            'page_title' => 'List of Audit'
            , 'total_audit' => $total_audit
            , 'records' => $records
            , 'model_records' => $model_records
            , 'user_records' => $user_records
            , 'action_records' => $action_records
            , 'input' => $input

        );
        
        return view('admin.audit.index', $values);
    }
}
