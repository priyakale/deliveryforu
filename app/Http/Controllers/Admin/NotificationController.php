<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Response;

//HELPER
use App\Helpers\SettingHelper;
use App\Helpers\RoleHelper;

//MODEL
use App\Models\EmailNotification;
use App\Models\User;
use App\Models\Role;
use App\Models\Lookup;

class NotificationController extends Controller
{
    /**** GENERAL FUNCTION ****/
    public function __construct(){
        $this->middleware('auth');
    }
    /**** END GENERAL FUNCTION ****/

    public function index(Request $request){
    	//Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();

        $email_notification_records = EmailNotification::query();

        //SEARCH BASE ON REQUEST (GET) VALUE
        if(!empty($input['subject']) && trim($input['subject']) != ''){
          $email_notification_records->where('subject', 'LIKE', '%'. trim($input['subject']).'%');
        }

        if(!empty($input['status']) && trim($input['status']) != ''){
          $email_notification_records->where('status', 'LIKE', '%'. trim($input['status']).'%');
        }
        //END SEARCH BASE ON REQUEST (GET) VALUE

        $records = $email_notification_records->paginate(SettingHelper::getPerPage());

        foreach ($records as $key => $value) {
            $value['email_for_id'] = Lookup::find($value['email_for_id']);
            $value['status_id'] = Lookup::find($value['status_id']);
        }

    	$values['data'] = array(
	    	'page_title' => 'List of Notification'
	    	,'records' => $records
	    );

        return view('admin.notification.index',$values);
    }

    public function add(){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access       

        $value = array();

        $values['data'] = array(
            'page_title' => 'Add Notification'
            , 'records' => array()
            , 'status_records' => Lookup::getGeneralStatus()
            , 'email_for' => Lookup::getEmailType()
        );

        return view('admin.notification.edit', $values);
    }

    public function edit($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $record = EmailNotification::find($id);

        if(!empty($record)){

            $record['role_ids'] = json_decode($record['role_ids']);

            $record['email_for_id'] = Lookup::select('id','name')->find($record['email_for_id']);
            $record['status_id'] = Lookup::find($record['status_id']);

            $values['data'] = array(
                'page_title' => 'Edit Notification'
                , 'records' => $record
            	, 'status_records' => Lookup::getGeneralStatus()
                , 'email_for' => Lookup::select('id','name')->where('category','Email Notification')->get()
            );

            return view('admin.notification.edit', $values);
        }else{
            Session::flash('danger', 'Notification not found!');
            return redirect()->action('Admin\NotificationController@index');
        }
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access 

    	$resultJson['result'] = 0;
        $input = $request->all();

        $model_email_notification = new EmailNotification();

        if(empty($input)){
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'Empty to save data!';
        }else{

            $required_data = array(
                'subject' => 'Subject'
                , 'status' => 'Status'
                , 'description' => 'description'
                , 'email_for_id' => 'Email for'
            );

            foreach ($input as $key => $value) {
                if(array_key_exists($key, $required_data)) {
                    if(empty($value)){
                        $resultJson['result'] = 0;
                        $resultJson['errMsg'] = 'Missing required data : '.$required_data[$key];
                        return Response::json($resultJson);
                    }else{
                    	$input[$key] = trim($value);
                    }
                }
            }
            
            if(empty($input['status']) || strtoupper($input['status']) == 'ALL'){
                unset($input['status']);
            }else{
                if(in_array($input['status'], Lookup::getGeneralStatus())){
                    $input['status'] = array_search($input['status'], Lookup::getGeneralStatus());    
                }else{
                    unset($input['status']);
                }
            }

            if(!empty($input['id'])){
            	$model_email_notification = $model_email_notification->find($input['id']);
            }

            $model_email_notification->fill($input);
            
            if(!$model_email_notification->save()){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Error : Cant save Notification data!';
            }else{
                $resultJson['result'] = 1;
                Session::flash('success', 'Add / update Notification success'); 
            }
        }//END ELSE
        return Response::json($resultJson);
    }

    public function delete($id){
        //Check access
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $email_notification_record = EmailNotification::find($id);
        
        $jsonResult['result'] = 0;
        if(!empty($email_notification_record)){
            try{
                $email_notification_record->delete();
                $jsonResult['result'] = 1;
                Session::flash('success', 'Delete success!');
            } 
            catch(\Exception $e){
                $jsonResult['result'] = 0;
                $jsonResult['errMsg'] = 'Delete fail , reason: ' . $e;
            }
        }
        else{
            $jsonResult['result'] = 0;
            $jsonResult['errMsg'] = 'No records';
        }

        return Response::json($jsonResult);
    }
}
