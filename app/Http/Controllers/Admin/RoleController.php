<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Route;
use Session;
use DateTime;
use Response;

//HELPER
use App\Helpers\RoleHelper;
use App\Helpers\SettingHelper;

//MODEL
use App\Models\Role;
use App\Models\Menu;
use App\Models\User;
use App\Models\RoleAccessibility;
use App\Models\RoleMenu;
use App\Models\Lookup;
use DB;

class RoleController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }

        $values = array();
        //Search base on request (GET) value
        $input = $request->all();
        if(!empty($input)){
            if(empty($input['keyword']) || strtoupper($input['keyword']) == 'ALL'){
                unset($input['keyword']);
            }else{
                $input['keyword'] = trim($input['keyword']);
            }

            if(empty($input['status_id']) || strtoupper($input['status_id']) == 'ALL'){
                unset($input['status_id']);
            }
        }

        //get total user
        $total_role = Role::count();

        //condition
        $role_records = Role::query();
        if (array_key_exists('keyword', $input)) $role_records->where('name', 'LIKE', '%'. trim($input['keyword']).'%');
        if (array_key_exists('status_id', $input))  $role_records->where('status_id', $input['status_id']);

        $status_record_collections = Lookup::getGeneralStatus();
        $status_records = array();
        foreach ($status_record_collections as $key => $value) {
            $tmp = array();
            $tmp['id'] = $value->id;
            $tmp['name'] = $value->name;
            $status_records[$value->id] = $tmp;
        }

        $records = $role_records->paginate(SettingHelper::getPerPage());
        foreach ($records as $key => &$value) {
            $date = new DateTime( $value['updated_at'] );
            $value['updated_at_value'] = $date->format('d/m/Y H:i:s');
            $total_user = 0;    
            $value['total_user'] = User::where('role_id', '=', $value['id'])->count();
            $value['status_id'] = Lookup::find($value['status_id']);
        }
        
        $values['data'] = array(
            'page_title' => 'Role Management'
            , 'total_role' => $total_role
            , 'records' => $records
            , 'input' => $input
            , 'status_records' => Lookup::getGeneralStatus()
        );

        return view('admin.role.index', $values);
    }

    public function add(){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        $controller_records = [];
        foreach (Route::getRoutes()->getRoutes() as $route){
            $action = $route->getAction();
            if (array_key_exists('controller', $action)){
                $raw_data = explode('@', $action['controller']);
                if(count($raw_data) > 1){
                    $raw_controller = explode('\\', $raw_data[0]);
                    $controller_records[$raw_controller[count($raw_controller) - 1]][] = $raw_data[1];
                }
            }
        }

        $menu_records = RoleHelper::getMenuList();

        $values['data'] = array(
            'page_title' => 'Add Role Accessibility'
            , 'controller_records' => $controller_records
            , 'menu_records' => $menu_records
            , 'record' => array()
            , 'general_status' => Lookup::getGeneralStatus()
        );

        return view('admin.role.edit', $values);
    }

    public function edit($id = null){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        if(!empty($id)){
            $record = Role::find($id);

            $menu_records = RoleHelper::getMenuList();
            $controller_role_detail_records = array();
            $controller_list_records = RoleAccessibility::select('id','controller','function')
            ->where('role_id', $id)->orderBy('controller','function')->get();
            foreach ($controller_list_records as $key => $value) {
                $controller_role_detail_records[$value['controller']][] =  array('id' => $value['id'], 'function' => $value['function']);
            }

            $menu_list_records = RoleMenu::select('menu_id','position','id')
            ->where('role_id', $id)->orderBy('position', 'Asc')->get();
           // dd( $menu_list_records);
            foreach ($menu_list_records as $key => &$value) {
                if($value['menu_id'] == '0'){
                    $value['menu'] = 'Dashboard';
                }else{
                    $menu_record_tmp= Menu::select('name','parent_id')
                    ->where('id', '=', trim($value['menu_id']))
                    ->first();
                    if(!empty($menu_record_tmp)){
                        $value['menu_value'] = $menu_record_tmp['name'];
                        if(!empty($menu_record_tmp['parent_id'])){
                            $menu_record_tmp_parent = Menu::select('name')
                            ->where('id', '=', trim($menu_record_tmp['parent_id']))
                            ->first();
                            if(!empty($menu_record_tmp_parent)){
                                $value['menu_value'] = $menu_record_tmp_parent['name'].'->'.$menu_record_tmp['name'];
                            }
                        }
                    }
                }
            }

            if(!empty($record)){
                $record->total_user = User::where('role_id', '=', $record->id)->count();
                $values['data'] = array(
                    'page_title' => 'Edit Role Accessibility'
                    , 'controller_role_detail_records' => $controller_role_detail_records
                    , 'menu_records' => $menu_records
                    , 'menu_list_records' => $menu_list_records
                    , 'record' => $record
                    , 'general_status' => Lookup::getGeneralStatus()
                );
                //dd($menu_list_records);
                return view('admin.role.edit', $values);
            }else{
                Session::flash('danger', 'Error: Record for that role not found');
                return redirect()->action('Admin\RoleController@index');
            }
        }else{
            Session::flash('danger', 'Error: Empty detail for that role!');
            return redirect()->action('Admin\RoleController@index');
        }
    }

    public function save(Request $request){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        //Search base on request (POST) value
        $input = $request->all();

        //check valid data before save_role
        if(!empty($input['status_id'])){
            $found = Lookup::find($input['status_id'])->count();
            if($found < 1){
                $input['status_id'] = 2;
            }
        }

        $model_role = new Role();

        if(!empty($input['role_id'])){ //update
            $model_role = $model_role::find($input['role_id']);
        }

        $model_role->fill($input);
        if(!$model_role->save()){
            Session::flash('danger', 'Error: Save role fail, Please contact Technial Team!');
        }else{
            if(empty($input['id'])){
                Session::flash('success', 'Create role success');
            }else{
                Session::flash('success', 'Save role success');
            }
        }

        if(empty($input['id'])){
            return redirect()->action(
                'Admin\RoleController@edit', ['id' => $model_role->id]
            );
        }else{
            return redirect()->action('Admin\RoleController@index');
        }
    }

    public function delete(Request $request){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        $input = $request->all();
        $required_data = array('id');
        foreach ($input as $key => $value) {
            if(in_array($key, $required_data)) {
                if(empty($value)){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Missing requried data : '.$key;
                }
            }
        }

        $role_delete = Role::find($input['id'])->delete();

        if($role_delete > 0){
            $resultJson['result'] = 1;
            Session::flash('success', 'Delete role success');
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'Delete role error, Please Inform Technical Team';
        }

        return Response::json($resultJson);
    }

    /**** FUNCTION ****/
    public function addFunction($id = null){ 
        $controller_records = [];
        $new_array = array();
        foreach (Route::getRoutes()->getRoutes() as $route){
            $prefix_value = $route->getPrefix(); 
            $prefix_value_sub = explode('/', $prefix_value);
            if($prefix_value_sub[0] == 'admin'){
                $action = $route->getAction(); 
                if (array_key_exists('controller', $action)){  
                $raw_data = explode('@', $action['controller']);
                   if(count($raw_data) > 1){
                        $raw_controller = explode('\\', $raw_data[0]); 
                       // array_push($new_array, $raw_controller[4]);
                        $controller_records[$raw_controller[count($raw_controller) - 1]][] = $raw_data[1];
                       
                    }
                }
            }
        } 
        /*
        $d = array();
        $d = array_unique($new_array);
        $myarray = array_values($d);
        // dd($myarray);
           
           for($i=0; $i< count($controller_records);$i++){
            // dd($myarray[$i]);
                $controller_list = DB::table('role_accessibilities')
                    ->select('role_id','controller','function')
                    ->where('controller', 'like', '%' . $myarray[$i] . '%')->get();
              // dd($controller_list);              
                $arr_index=$myarray[$i];
                // dd($myarray);exit;

                // foreach($controller_list as $key => $val){  
                 // $data_controller = explode('\\', $myarray[$i]);
                  //  dd($data_controller);exit;                               
                    if($controller_list){ 
                        dd($controller_records,$myarray[$i]);
                     unset($controller_records[$arr_index]);                    
                     }
                //}
            }*/
           
            $value = array();
            $values['data'] = array(
                'page_title' => 'Add Functionality'
                , 'link_records' => $controller_records
            ); 

            $resultJson = array();
            $resultJson['result'] = 1;
            $resultJson['returnHTML'] = view('admin.role.add_function',$values)->render();
            return Response::json($resultJson);
        } 

    public function saveRoleFunction(Request $request){
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }

        $values = array();
        $input = $request->all();

        $required_data = array('controller','function', 'role_id');
        foreach ($input as $key => $value) {
            if(in_array($key, $required_data)) {
                if(empty($value)){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Missing requried data : '.$key;
                }
            }
        }
        $input['controller'] = "Admin\\".$input['controller'];

        $model_access = new RoleAccessibility();
        if (!empty($input['id'])) { //edit
            $model_access = $model_access::find($input['id']);
        }else{ //new record
            //All FUNCTION
            if(strtoupper($input['function']) == 'ALL'){
                //get controller first
                $function_data = [];
                foreach (Route::getRoutes()->getRoutes() as $route){
                    $prefix_value = $route->getPrefix();
                    $prefix_value_sub = explode('/', $prefix_value);
                    if($prefix_value_sub[0] == 'admin'){
                        $action = $route->getAction();
                        if (array_key_exists('controller', $action)){
                            if (strpos($action['controller'], $input['controller']) !== false) {
                                $raw_data = explode('@', $action['controller']);
                                if(count($raw_data) > 1){
                                    //get function
                                    $function_data[] = $raw_data[1];
                                }
                            }
                        }
                    }
                }//end for route

                //Remove Duplicate in function data
                $function_data = array_unique($function_data);

                //Check if function and controller base on role id
                foreach ($function_data as $value) {
                    $function_found = 0;
                    $function_found = RoleAccessibility::select('id')->where('role_id', $input['role_id'])
                        ->where('controller', $input['controller'])
                        ->where('function', $value)->count();
                    if($function_found == 0){
                        $model_access_all = new RoleAccessibility();
                        $access_data = array();
                        $access_data['role_id'] = $input['role_id'];
                        $access_data['controller'] = $input['controller'];
                        $access_data['function'] = $value;

                        $model_access_all->fill($access_data);
                        if(!$model_access_all->save()){
                            $resultJson['result'] = 0;
                            $resultJson['errMsg'] = 'Error : Cant save role all function data!';
                            return Response::json($resultJson);
                        }
                    }
                }
                $resultJson['result'] = 1;
                Session::flash('success', 'Save role all function success');
                return Response::json($resultJson);

            }
            //END ALL FUNCTION
        

            //Check function action existed base on this role or not
            $record_duplicate = RoleAccessibility::where('role_id',$input['role_id'])
                ->where('controller', $input['controller'])
                ->where('function', $input['function'])
                ->count();

            if($record_duplicate > 0){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Controller '. $input['controller'] .' & Function '. $input['function'] .' already exist for this role';
                return Response::json($resultJson);
            }

            $model_access->fill($input);
            if(!$model_access->save()){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Error : Cant save role function data!';
                return Response::json($resultJson);
            }else{
                $resultJson['result'] = 1;
                Session::flash('success', 'Save role function success');
            }
        }

        return Response::json($resultJson);
    }

    public function deleteRoleFunction(Request $request){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        $input = $request->all();
        $required_data = array('function_name', 'role_id');
        foreach ($input as $key => $value) {
            if(in_array($key, $required_data)) {
                if(empty($value)){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Missing requried data : '.$key;
                }
            }
        }

        $role_access_record_delete = RoleAccessibility::where('controller', $input['function_name'])
            ->where('role_id', $input['role_id'])
            ->delete();

        if($role_access_record_delete > 0){
            $resultJson['result'] = 1;
            Session::flash('success', 'Delete role function success');
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'Delete role function error, Please Inform Technical Team';
        }

        return Response::json($resultJson);
    }

    public function deleteRoleFunctionSelected(Request $request){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        $input = $request->all();
        $required_data = array('id');
        foreach ($input as $key => $value) {
            if(in_array($key, $required_data)) {
                if(empty($value)){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Missing requried data : '.$key;
                }
            }
        }

        $role_access_record = RoleAccessibility::find($input['id']);
        if(!empty($role_access_record)){
            if($role_access_record->delete()){
                $resultJson['result'] = 1;
                Session::flash('success', 'Delete role selected function success');
            }else{
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Delete role selected function error, Please Inform Technical Team';
            }
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'There are no record for this role selected function!';
        }
        return Response::json($resultJson);
    }
    /**** END FUNCTION ****/

    /**** MENU ****/
    public function addMenuModel($id = null){
        if(!empty($id)){
            $title = 'Edit Role Menu';
            //Find Role Menu base on id
            $record = RoleMenu::find($id);
        }else{
            $title = 'Add Role Menu';
            $record = array();
        }

        $value = array();       
        $values['data'] = array(
            'page_title' => $title
            , 'record' => $record
            , 'menu_records' => Menu::select('id','name')->get()
        );
        
        $resultJson = array();
        $resultJson['result'] = 1;
        $resultJson['returnHTML'] = view('admin.role.add_menu',$values)->render();
        return Response::json($resultJson);
    }

    public function saveRoleMenu(Request $request){
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }

        $values = array();
        $input = $request->all();
        $required_data = array('menu_id','position', 'role_id');        
        foreach ($input as $key => $value) {
            if(in_array($key, $required_data)) {
                if(empty($value)){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Missing requried data : '.$key;
                }
            }
        }
        //Check valid data before save
        $found_role = Role::select('id')->where('id', $input['role_id'])->count();
        $found_menu = Menu::select('id')->where('id', $input['menu_id'])->count();

        if($found_menu > 0 && $found_role > 0){ //Valid data
            $model_role_menu = new RoleMenu();
            if (!empty($input['id'])) { //edit
                $model_role_menu = $model_role_menu::find($input['id']);
            }else{ //new record
                //Check Menu existed base on this role or not
                $record_duplicate = RoleMenu::where('role_id',$input['role_id'])->where('menu_id', $input['menu_id'])->count();
                if($record_duplicate > 0){
                    $menu_name = Menu::select('name')->find($input['menu_id']);
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Menu '. $menu_name->name .' already exist for this role';
                    return Response::json($resultJson);
                }
            }
            $model_role_menu->fill($input);
            if(!$model_role_menu->save()){
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Error : Cant save menu role data!';
                return Response::json($resultJson);
            }else{
                $resultJson['result'] = 1;
                Session::flash('success', 'Save menu role success');

                //Update Record sort
                $record_menu = RoleMenu::select('id','position')->where('role_id', $input['role_id'])->orderBy('position', 'Asc')->get();
                $countMenu = 0;
                foreach ($record_menu as $key => $value) {
                    $value['position'] = $countMenu++;
                    $data = array();
                    $data['position'] = $value['position'];

                    $update_data_model = new RoleMenu();
                    $update_data_model = $update_data_model->find($value['id']);
                    $update_data_model->fill($data);
                    $update_data_model->save();
                }
                //End Update Record sort
            }
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'Invalid data during saving';
            return Response::json($resultJson);
        }

        return Response::json($resultJson);
    }

    public function deleteRoleMenu(Request $request){
        $access = RoleHelper::checkRoleAccess();
        if(!$access['result']){
            return $access['data'];
        }

        $input = $request->all();
        $required_data = array('id','role_id');
        foreach ($input as $key => $value) {
            if(in_array($key, $required_data)) {
                if(empty($value)){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = 'Missing requried data : '.$key;
                }
            }
        }

        $rolemenu_record = RoleMenu::find($input['id']);
        if(!empty($rolemenu_record)){
            if($rolemenu_record->delete()){
                $resultJson['result'] = 1;
                Session::flash('success', 'Delete role menu success');

                //Update Record sort
                $record_menu = RoleMenu::select('id','position')->where('role_id', $input['role_id'])->orderBy('position', 'Asc')->get();
                $countMenu = 0;
                foreach ($record_menu as $key => $value) {
                    $value['position'] = $countMenu++;
                    $data = array();
                    $data['position'] = $value['position'];

                    $update_data_model = new RoleMenu();
                    $update_data_model = $update_data_model->find($value['id']);
                    $update_data_model->fill($data);
                    $update_data_model->save();
                }
                //End Update Record sort
            }else{
                $resultJson['result'] = 0;
                $resultJson['errMsg'] = 'Delete role menu error, Please Inform Technical Team';
            }
        }else{
            $resultJson['result'] = 0;
            $resultJson['errMsg'] = 'There are no record for this role menu!';
        }
        return Response::json($resultJson);
    }
    /**** END MENU ****/

    public static function getRoleAdmin(){
        $records = Role::select('name', 'id')->where('status_id', 1)->whereNotIn('id',[2])->get();
        return $records;
    }
}
