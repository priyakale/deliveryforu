<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Response;

//HELPER
use App\Helpers\SettingHelper;
use App\Helpers\RoleHelper;

//Repositories
use ChargesRepo;

class ChargesController extends Controller
{

    public function __construct(ChargesRepo $chargesRepo )
    {
        $this->middleware('auth');
        $this->chargesRepo = $chargesRepo;
    }

    public function index(Request $request)
    {
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access

        $input = $request->all();
        $values['data'] = array(
            'page_title' => 'Charges' 
            ,'record' => $this->chargesRepo->find(1)
        );

        return view('admin.charges.index',$values);
    }

    public function save(Request $request){
        //Check access
        $access = RoleHelper::checkRoleAccess($request);
        if(!$access['result']){
            return $access['data'];
        }
        //end check access
        $data = $request->all();
        
        return $this->chargesRepo->save($data);
    }
}
