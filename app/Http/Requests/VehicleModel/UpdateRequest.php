<?php

namespace App\Http\Requests\VehicleModel;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_id' => 'integer|required|exists:vehicles,id',
            'model' => 'required|string',
            'status' => 'required|integer'
        ];
    }
}
