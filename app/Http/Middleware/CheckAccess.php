<?php

namespace App\Http\Middleware;

use App\Helpers\RoleHelper;
use Closure;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access = RoleHelper::checkRoleAccess();

        if(!$access['result']){
            return $access['data'];
        }

        return $next($request);
    }
}
