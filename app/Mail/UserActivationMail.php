<?php

namespace App\Mail;

use App\Models\UserActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var UserActivation
     */
    private $activation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($activation)
    {
        $this->activation = $activation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auth.activation')
            ->with([
                'activation' => $this->activation,
            ]);
    }
}
