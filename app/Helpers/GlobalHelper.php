<?php

if (!function_exists('convert_malaysia_date')) {
    /**
     * Convert to malaysia date format
     *
     * @param $date
     *
     * @return string|null
     */
    function convert_malaysia_date($date)
    {
        if ($date) {
            return Carbon::parse($date)->format('d-m-Y');
        }
    }
}

if (!function_exists('custom_asset')) {
    /**
     * Get Custom asset location
     *
     * @param $file
     *
     * @return mixed
     */
    function custom_asset($file)
    {
        //For HTTPS Purpose
        $secure = null;

        return app('url')->asset(config('app.asset_path'). '/' . $file, $secure);
    }
}

if (!function_exists('mask')) {
    /**
     * Masking function
     *
     * @param string $str
     * @param string $first
     * @param string $last
     *
     * @return string
     */
    function mask($str, $first, $last)
    {
        $len    = strlen($str);
        $toShow = $first + $last;
        return substr($str, 0, $len <= $toShow ? 0 : $first) . str_repeat("*", $len - ($len <= $toShow ? 0 : $toShow)) . substr($str, $len - $last, $len <= $toShow ? 0 : $last);
    }
}

if (!function_exists('mask_email')) {
    /**
     * Mask the email ie: iqmal.azim@i-serve.com.my to i*****azim@i******.com.my
     *
     * @param email $email
     *
     * @return mixed
     */
    function mask_email($email)
    {
        $mail_parts   = explode("@", $email);
        $domain_parts = explode('.', $mail_parts[1]);

        $mail_parts[0]   = mask($mail_parts[0], 2, 4); // show first 2 letters and last 1 letter
        $domain_parts[0] = mask($domain_parts[0], 1, 0); // same here
        $mail_parts[1]   = implode('.', $domain_parts);

        return implode("@", $mail_parts);
    }
}
