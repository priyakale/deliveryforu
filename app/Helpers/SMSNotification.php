<?php

namespace App\Helpers;

//TABLE
use App\Models\InvestorDetail;
use Log;
use Response;
use nusoap_client;
use Session;
use \Crypt;
use Carbon;

class SMSNotification {

    protected static $resendAfter = 5; //5 in minutes

    public static function generateMessage($phone_number, $type, $order_id, $amount) {

        switch ($type) {
            case 1:  //Successfull transaction For Merchant
                $message = "RM0.00 UPayMe: Transaction for order id-".$order_id." is successful. The amount of RM ".$amount." shall be credited to your account soon.
Thank you.";
                break;
            case 2:  //Successfull transaction For User
                $message = "RM0.00 UPayMe: Transaction for order id-".$order_id." is successful. The amount of RM ".$amount." has been debited from your account.
Thank you.";
                break;
            case 3:  //Failed transaction For User
                $message = "RM0.00 UPayMe: Transaction for order id-".$order_id." is unsuccessful.
Thank you.";
                break;
            default:
                $message = null;
                Log::useDailyFiles(storage_path() . '/logs/GENERATE_MESSAGE.log');
                Log::notice('Type fail: Type not found.');
        }//End Switch

        return $message;
    }

    public static function sendSMS($phone_number, $type, $order_id, $amount) {

        
        $resultJson['status'] = 1; //Failed

        if (!empty($phone_number)) {

            $message = SMSNotification::generateMessage($phone_number, $type, $order_id, $amount);
            
            //ini_set('max_execution_time', 300); //Increase processing time to 5 minute 

            $client = new nusoap_client(
                    'http://203.106.222.225/SMS_WS/Service1.asmx?op=RequestTAC', false
            );

            //socket_set_timeout( $client, 180); // Increase socket time out 3 minute
            $client->timeout = 180;
            $client->response_timeout = 180;

            $error = $client->getError();

            if (!$error) {
                $client->soap_defencoding = 'utf-8';
                $client->useHTTPPersistentConnection(); // Uses http 1.1 instead of 1.0
                $client->operation = "";
                $soapaction = "http://i-serve.com.my/RequestTAC";
                $request_xml = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <RequestTAC xmlns="http://i-serve.com.my/">
                      <phone_number>' . $phone_number . '</phone_number>
                      <message>' . $message . '</message>
                      <user_id>live</user_id>
                      <password>emoney2017</password>
                    </RequestTAC>
                  </soap:Body>
                </soap:Envelope>';

                if ($phone_number != '016425372712312') {
                    $response = $client->send($request_xml, $soapaction);
                } else {
                    $response = null;
                }
                //dd($response);
                Log::useDailyFiles(storage_path() . '/logs/SMS_SOAP_RESPONSE.log');
                Log::notice('SOAP Respone request_xml : ' . print_r($request_xml, true));

                $resultJson['result'] = $response;
                $resultJson['status'] = 0; //Success

                return Response::json($resultJson);
            } else {
                Log::notice('SOAP Result : ' . print_r($error, true));
                Log::notice('SOAP Error : ' . print_r($client->getError(), true));
                $resultJson['result'] = 'Failed SOAP Problem';

                return Response::json($resultJson);
            }//End Checking SOAP ERROR
        } else {
            $resultJson['result'] = "Mobile number is not yet set";

            return Response::json($resultJson);
        }
    }

}
