<?php

namespace App\Helpers;

use Log;
use Request;
use Carbon\Carbon;

class ErrorLogs {
    
   
    public function __construct() {
       

    }

    /*
     * Add Log for anything
     */

    public static function AddLog($logStorage = Null, $exception) {

        $msg['exception'] = get_class($exception); // Reflection might be better here
        $msg['message'] = $exception->getMessage();
        $msg['trace'] = $exception->getTrace();

        $msg['title'] = 'Other Exception';
        $data = array('logtype' => 'error', "dt" => $datetime, 'code' => 'ERROR-EXCEPTION', 'http_code' => $exception->getCode(), 'message' => $msg, 'headers' => '');
       
        $agent = new Agent(); ///jessengers
        $from_device = 'Unidentified';
        if($agent->isDesktop())
            $from_device = 'Dekstop';
        if($agent->isPhone())
            $from_device = 'Phone';
        if($agent->isRobot())
            $from_device = $agent->robot();///get robot name
       
        $logmsg = json_encode(array(
            'code' => get_class($exception),
            'http_code' => $exception->getCode(),
            'message' => $data['message'],
            'methods' => Request::method(),
            'api_link' => Request::fullUrl(),
            'ip_address' => Request::ip(),
            'platform' => $agent->platform(),
            'platform_version' => $agent->version($agent->platform()),
            'browser' => $agent->browser(),
            'browser_version' => $agent->version($agent->browser()),
            'device' => $from_device,
            'device_name' => $agent->device(),
            'headers' => $data['headers'],
            'parameters' => Request::all()  
        ));
       
         ///logs/Exception/ErrorLogs/201705 LOG purpose ..DAILY LOG
        if(is_null($logStorage)){
        $errorStorage = storage_path().'/logs/Others/ErrorLogs/'.carbon::now()->format('Ym').'/error.log';      
        }else{
        $errorStorage = $logStorage.carbon::now()->format('Ym').'/error.log';  
        }
        
        Log::useDailyFiles($errorStorage);
        Log::error($logmsg);
        
    }

}
