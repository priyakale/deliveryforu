<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Redirect;
use Route;
use Session;
use Lang;
use Response;

//TABLE
use App\Models\RoleAccessibility;
use App\Models\RoleMenu;
use App\Models\Role;
use App\Models\Menu;

class RoleHelper {
    
    public static function getUserRole(){
        $currentUser = RoleHelper::getCurrentUser();
        $role_name = 'Undefind Role';
        if(!empty($currentUser)){
            $role = Role::select('name')->where('id', $currentUser->role_id)->first();
            $role_name = $role->name;
        }
        return $role_name;
    }

    public static function getCurrentUser(){
        $currentUser = app('Illuminate\Contracts\Auth\Guard')->user();
    
        return $currentUser;
    }

    public static function getRoleMenu(){
        $menu_parent = array();
      //Session::forget('Menu');
        if(Session::has('Menu')){
            $session_menu = Session::get('Menu');
            $menu_parent = $session_menu;
        }else{
            $menu_parent = array();
            $currentUser = RoleHelper::getCurrentUser();
            if(!empty($currentUser)){
                $roleMenuData = RoleMenu::select('menu_id')
                ->where('role_id', $currentUser->role_id)
                ->orderBy('position', 'Asc')->get();

                foreach ($roleMenuData as $key => $value) {
                    if($value['menu'] == '0'){
                        $value['menu'] = 'Dashboard';
                    }else{
                        $menu_record_tmp= Menu::select('name','parent_id')
                        ->where('id', '=', trim($value['menu_id']))
                        ->orderBy('name', 'Asc')->get()
                        ->first();
                        if(!empty($menu_record_tmp)){
                            $value['menu'] = $menu_record_tmp['name'];
                            if(!empty($menu_record_tmp['parent_id'])){
                                $menu_record_tmp_parent = Menu::select('name')
                                ->where('id', '=', trim($menu_record_tmp['parent_id']))
                                ->first();
                                if(!empty($menu_record_tmp_parent)){
                                    $value['menu'] = $menu_record_tmp_parent['name'].'->'.$menu_record_tmp['name'];
                                }
                            }
                        }
                    }

                    $raw_data = explode("->", $value->menu);
                    if(count($raw_data)>1){
                        $resutl_menu_list = array();
                        $resutl_menu_list['menu_array'] = RoleHelper::getMenuList($value->menu);
                        $icon = Menu::select('icon')
                        ->where('name', '=', trim($raw_data[0]))
                        ->first();
                        $menu_parent['parent'][$raw_data[0].'||'.$icon['icon']][] = $resutl_menu_list;
                        unset($roleMenuData[$key]);
                    }else{
                        $menu_parent['menu_array'][] = RoleHelper::getMenuList($value->menu);
                    }
                }
            }
            Session::put('Menu', $menu_parent);
        }

        return $menu_parent;
    }

    public static function getMenuList($value = null){
        $menu_records = array();

        $menu_records_data = Menu::get();
        foreach ($menu_records_data as $key_menu => $value_menu) {
            if(!empty($value_menu['parent_id'])){
                $menu_record_tmp= Menu::select('name')
                ->where('id', '=', trim($value_menu['parent_id']))
                ->first();
                if(!empty($menu_record_tmp['name'])){
                    $menu_records[ $menu_record_tmp['name'].'->'.$value_menu['name'] ] =
                    array (
                        'link' => $value_menu['link_name']
                        , 'icon' => $value_menu['icon']
                        , 'name' => $value_menu['name']
                    );
                }
            }else{
                $menu_records[$value_menu['name']] = array (
                    'link' => $value_menu['link_name']
                    , 'icon' => $value_menu['icon']
                    , 'name' => $value_menu['name']
                );
            }
        }
        //Default menu
        $menu_records['Dashboard'] = array (
            'link' => 'home'
            , 'icon' => 'fa-windows'
            , 'name' => 'Dashboard'
            );

        if(empty($value)){
            return $menu_records;
        }else{
            return $menu_records[$value];
        }
    }

    public static function getAccess(){
        $resultArray = array();
        $route_detail = Route::getCurrentRoute()->getAction();
        $route_array_raw = explode("\\", $route_detail['uses']);
        $route_array = explode("@", $route_array_raw[count($route_array_raw)-1]);
        $controller_name = "Admin\\".$route_array[0];
        $function_name = $route_array[1];
        $currentUser = RoleHelper::getCurrentUser();

        if(!empty($currentUser)){
            $role = Role::select('status_id')
            ->where('id', $currentUser->role_id)
            ->where('status_id', 1)
            ->count();

            if($role > 0){ //This role is active
                $roleAccessData = RoleAccessibility::select('controller','function')
                ->where('role_id', $currentUser->role_id)
                ->where('controller', $controller_name)
                ->where('function', $function_name)
                ->count();
                if($roleAccessData > 0){ //This role have access to that page / post
                    $resultArray['result'] = 1;
                }else{
                    $resultArray['result'] = 0;
                    $errMsg = 'You dont have access to this page/action';
                    $resultArray['errMsg'] = $errMsg;
                }
            }else{
                $resultArray['result'] = 0;
                $errMsg = 'You role are not found @ inactive';
                $resultArray['errMsg'] = $errMsg;
            }//end role
        }else{
            $resultArray['result'] = 0;
            $errMsg = 'This role not active!';
            Session::flash('warning', $errMsg);
            $resultArray['errMsg'] = $errMsg;
        }
        return $resultArray;
    }

    public static function isAdministrator(){
        $currentUser = RoleHelper::getCurrentUser();
        return ($currentUser->role_id == 1);
    }

    public static function isCustomer(){
        $current_user = RoleHelper::getCurrentUser();
        $customer = 0;
        if($current_user->role_id == 6){
            $customer = 1;
        }

        return $customer;
    }

    public static function checkRoleAccess($request = null){
        $result = array();
        $result['result'] = 1;
         //Check access
        $accessResult = RoleHelper::getAccess();
        if(!$accessResult['result']){
            if(!empty($request)){
                if($request->method() == 'POST'){
                    $resultJson['result'] = 0;
                    $resultJson['errMsg'] = $accessResult['errMsg'];

                    $result['result'] = 0;
                    $result['data'] = response()->json(array('message' => $resultJson['errMsg']), 403);
                }else{
                    Session::flash('danger', $accessResult['errMsg']);
                    $result['result'] = 0;
                    $result['data'] = view('exception.access_denied');
                }
            }else{
                $result['result'] = 0;
                $result['data'] = view('exception.access_denied');
            }
        }
        //end check access
        return $result;
    }

}
