<?php

/*
 * Developer: Mohamad Khairil
 * Upay Error Types
 * 
 */

namespace App\Helpers;

use Log;
use Response;
//use Request;
//use Jenssegers\Agent\Agent;
//use Carbon\Carbon;

class HttpCode {

    public function __construct() {
        
    }
    
    
    public static function HttpType($httpCode) {
        
        
        switch ($httpCode) {
            case 200: //CREATE/POST
                $code = "SUCCESS";
                $groupCode = 0;
                break;
            case 201: //UPDATE/PUT
                $code = "SUCCESS";
                $groupCode = 0;
                break;
            case 400: //
                $code = "WRONG-ARGS";
                $groupCode = 1;
                break;
            case 401: //
                $code = "UNAUTHORIZED";
                $groupCode = 1;
                break;
            case 403: //
                $code = "FORBIDDEN";
                $groupCode = 1;
                break;
            case 404: //
                $code = "NOT-FOUND";
                $groupCode = 1;
                break;
            case 405: //
                $code = "METHOD-NOT-ALLOWED";
                $groupCode = 1;
                break;
            case 410: //
                $code = "GONE";
                $groupCode = 1;
                break;
            case 422: //
                $code = "UNPROCESSABLE";
                $groupCode = 1;
                break;
            case 431: //
                $code = "UNWILLING-TO-PROCESS";
                $groupCode = 1;
                break;
            case 500: //
                $code = "INTERNAL-ERROR";
                $groupCode = 1;
                break;
            default:
                $code = null;
                $groupCode = null;
                
        }//End Switch
        
        if($groupCode == 0)
            $groupCode = 'success';
        else
            $groupCode = 'error';
        
        $resultJson['code'] = $code;
        $resultJson['groupCode'] = $groupCode;
        return Response::json($resultJson);
        
    }
    

}
