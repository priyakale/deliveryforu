<?php

namespace App\Helpers;

//TABLE
use App\Models\User;
use Log;
use Response;
use nusoap_client;
use Session;
use \Crypt;
use Carbon;

class SmsHelper {

    public static function smsNotification($user_id, $type) {

        $contact_mobile = User::select('mobile_number')->find($user_id);
        $resultJson['status'] = 1; //Failed

        if (!empty($contact_mobile)) {

            if ($type == "Enrollment") {
                $message = "Welcome! You are now registered for the Yakin! Swap program. You will receive an e-mail confirmation shortly with a link to activate your user account.";
            } else {
                $message = "SMS Renewal Confirmation\nThank you for renewing your Yakin! program with us.  You may now log into your account and view the status of your subscription.";
            }

            //ini_set('max_execution_time', 300); //Increase processing time to 5 minute 

            $client = new nusoap_client(
                    'http://203.106.222.225/SMS_WS/Service1.asmx?op=RequestTAC', false
                    //'http://192.168.0.150/SMS_WS/Service1.asmx?op=RequestTAC', false
            );

            //socket_set_timeout( $client, 180); // Increase socket time out 3 minute
            $client->timeout = 180;
            $client->response_timeout = 180;

            $error = $client->getError();

            if (!$error) {
                $client->soap_defencoding = 'utf-8';
                $client->useHTTPPersistentConnection(); // Uses http 1.1 instead of 1.0
                $client->operation = "";
                $soapaction = "http://i-serve.com.my/RequestTAC";
                $request_xml = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <RequestTAC xmlns="http://i-serve.com.my/">
                      <phone_number>' . $contact_mobile['mobile_number'] . '</phone_number>
                      <message>' . $message . '</message>
                      <user_id>live</user_id>
                      <password>emoney2017</password>
                    </RequestTAC>
                  </soap:Body>
                </soap:Envelope>';

                if ($contact_mobile['mobile_number'] != '016425372755555') {
                    $response = $client->send($request_xml, $soapaction);
                } else {
                    $response = null;
                }
                
                Log::useDailyFiles(storage_path() . '/logs/SOAP_RESPONSE.log');
                Log::notice('SOAP Respone request_xml : ' . print_r($request_xml, true));

                $resultJson['result'] = $response;
                $resultJson['status'] = 0; //Success

                return Response::json($resultJson);
            } else {
                Log::useDailyFiles(storage_path() . '/logs/SOAP_RESPONSE_ERROR.log');
                Log::notice('SOAP Result : ' . print_r($error, true));
                Log::notice('SOAP Error : ' . print_r($client->getError(), true));
                $resultJson['result'] = 'Failed SOAP Problem';

                return Response::json($resultJson);
            }//End Checking SOAP ERROR
        } else {
            $resultJson['result'] = "Mobile number is not yet set";

            return Response::json($resultJson);
        }
    }

}
