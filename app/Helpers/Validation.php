<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Cache;

//TABLE
use App\Models\InputField;

class Validation {

    private $STATUS_FAILED;
    private $STATUS_SUCCESS;

    /**** GENERAL FUNCTION ****/
    public function __construct(){
        $this->STATUS_FAILED = 0;
        $this->STATUS_SUCCESS = 1;
    }
    /**** END GENERAL FUNCTION ****/

    public function validate($data = Null) {

        if (empty($data['merchant_id'])) {
            $result['status'] = $this->STATUS_FAILED; //Failed
            $result['message'] = 'Merchant id is missing';
            return $result;
        }

        $data['fields'] = json_decode($data['fields'],true);
 
        if ($data['merchant_id'] == 4) {////Indah Water Bill Acc. No. Validation
            $result = $this->IndahWaterValidation($data);
        } else if ($data['merchant_id'] == 2) {////Maxis Bill Acc. No. Validation
            $result = $this->MaxisValidation($data);
        } else if ($data['merchant_id'] == 6) {////Syabas Bill Acc. No. Validation
            $result = $this->SyabasValidation($data);
        } else if ($data['merchant_id'] == 5) {////TM Bill Acc. No. Validation
            $result = $this->TMValidation($data);
        } else if ($data['merchant_id'] == 3) {////UNIFI Bill Acc. No. Validation
            $result = $this->UnifiValidation($data);
        } else {
            $result['status'] = $this->STATUS_SUCCESS; //Success
            $result['message'] = 'No Validation for this merchant';
            return $result;
        }

        return $result;
    }

    public function IndahWaterValidation($data) {
        
        $account_number = null;

        if(!empty($data['fields'])){
            foreach ($data['fields'] as $key => $value) {
                if(!empty($value['value'])){
                    $record = InputField::find($value['field_id']);

                    if($record['name']=="Account Number (Numeric)"){
                        $account_number = $value['value'];
                    } 

                    if($record['name']=="Account Number"){
                        $account_number = $value['value'];
                    } 

                    if($record['name']=="Bill Number"){
                        $bill_number = $value['value'];
                    } 
                }
            }
        }

        if(!empty($bill_number)){
            $length = strlen((string)$bill_number);

            if(!$length==8){
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Only accept 8 digits for bill number';
                return $result;
            }
        }

        if(!empty($account_number)){
            $length = strlen((string)$account_number);

            if (!is_numeric($account_number)) {
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Only accept numeric for account number';
                return $result;
            }

            if($length>8 || $length<8){
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Only accept 8 digits for account number';
                return $result;
            }

            $last_digit = substr($account_number, -1);
            $number_arr = str_split($account_number);
            $sum = 0;

            for ($i=0; $i < $length - 1; $i++) { 

                if ($i % 2 == 0) {///Even number
                    $number_arr[$i] = $number_arr[$i] * 2;
                }
                else{///Odd number
                    $number_arr[$i] = $number_arr[$i] * 1;
                }

                $digit_length = strlen((string)$number_arr[$i]);

                if($digit_length>1){
                    $new_arr = str_split($number_arr[$i]);
                    $number_arr[$i] = $new_arr[0] + $new_arr[1];
                }

                $sum += $number_arr[$i];
            }

            $sum = $sum % 10;

            if ($sum != 0) {///Even number
                $sum = 10 - $sum;
            }

            if($sum == $last_digit){
                $result['status'] = $this->STATUS_SUCCESS; //Success
                $result['message'] = 'Account Number is valid';
                return $result;
            }
            else{
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }
        }
        else{
            $result['status'] = $this->STATUS_FAILED; //Failed
            $result['message'] = 'Fields Empty';
            return $result;
        }
              
    } 

    public function MaxisValidation($data) {
        
        $account_number = null;

        if(!empty($data['fields'])){
            foreach ($data['fields'] as $key => $value) {
                if(!empty($value['value'])){
                    $record = InputField::find($value['field_id']);

                    if($record['name']=="Account Number (Numeric)"){
                        $account_number = $value['value'];
                    } 
                }
            }
        }

        if(!empty($account_number)){
            $account_number = strrev($account_number); //reverse the account number
            $length = strlen((string)$account_number); //get length of the account number
            $number_arr = str_split($account_number);   //split into array
            $sum = 0;

            for ($i=0; $i < $length; $i++) { 

                if ($i % 2 == 0) {///Even number
                    $number_arr[$i] = $number_arr[$i] * 1;
                }
                else{///Odd number
                    $number_arr[$i] = $number_arr[$i] * 2;
                }

                $digit_length = strlen((string)$number_arr[$i]);

                if($digit_length>1){
                    $new_arr = str_split($number_arr[$i]);
                    $number_arr[$i] = $new_arr[0] + $new_arr[1];
                }

                $sum += $number_arr[$i];
            }

            $sum = $sum % 10;

            if($sum == 0){
                $result['status'] = $this->STATUS_SUCCESS; //Success
                $result['message'] = 'Account Number is valid';
                return $result;
            }
            else{
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }
        }
        else{
            $result['status'] = $this->STATUS_FAILED; //Failed
            $result['message'] = 'Fields Empty';
            return $result;
        }
              
    }   

    public function SyabasValidation($data) {
        
        $account_number = null;

        if(!empty($data['fields'])){
            foreach ($data['fields'] as $key => $value) {
                if(!empty($value['value'])){
                    $record = InputField::find($value['field_id']);

                    if($record['name']=="Account Number (Numeric)"){
                        $account_number = $value['value'];
                    } 
                }
            }
        }

        if(!empty($account_number)){
            $length = strlen((string)$account_number);

            if($length!=10 && $length!=13){
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Only accept 8 digits or 13 digits for account number';
                return $result;
            }

            $last_digit = substr($account_number, -1);
            $number_arr = str_split($account_number);
            $sum = 0;

            for ($i=0; $i < $length; $i++) { 

                if ($i % 2 == 0) {///Even number
                    $number_arr[$i] = $number_arr[$i] * 2;
                }
                else{///Odd number
                    $number_arr[$i] = $number_arr[$i] * 1;
                }

                $digit_length = strlen((string)$number_arr[$i]);

                if($digit_length>1){
                    $new_arr = str_split($number_arr[$i]);
                    $number_arr[$i] = ($new_arr[0]/10) + ($new_arr[1]%10);
                }

                $sum += $number_arr[$i];
            }

            $sum = 10 - ($sum % 10);

            if ($sum > 9) {///Even number
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }

            if($sum == $last_digit){
                $result['status'] = $this->STATUS_SUCCESS; //Success
                $result['message'] = 'Account Number is valid';
                return $result;
            }
            else{
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }
        }
        else{
            $result['status'] = $this->STATUS_FAILED; //Failed
            $result['message'] = 'Fields Empty';
            return $result;
        }
              
    }   

    public function TMValidation($data) {
        
        $account_number = null;

        if(!empty($data['fields'])){
            foreach ($data['fields'] as $key => $value) {
                if(!empty($value['value'])){
                    $record = InputField::find($value['field_id']);
                    
                    if($record['name']=="Account Number (Alphanumeric)"){
                        $account_number = $value['value'];
                    } 
                }
            }
        }

        if(!empty($account_number)){
            $length = strlen((string)$account_number);

            if($length!=13){
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Only accept 13 digits for account number';
                return $result;
            }

            $last_12_digits = substr($account_number, 1);
            $first_letter = strtoupper(substr($account_number, 0, 1));
            $last_2_digits = substr($account_number, -2);

            // if (strpos('AKUZCFNPLHYBGSD', $first_letter) == false) {
            //     $result['message'] = 'Account Number is not valid';
            //     return $result;
            // }

            $jul_date = substr($account_number, 3, 1).substr($account_number, 4, 1).substr($account_number, 5, 1);

            $tm_range = $this->checkTMRange($first_letter, $jul_date);

            if ($tm_range == false) {
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }

            $number_arr = str_split(substr($last_12_digits, 0, 10));
            $sum = 0;

            for ($i=0; $i < $length - 3; $i++) { 

                if ($i % 2 == 0) {///Even number
                    $number_arr[$i] = $number_arr[$i];
                }
                else{///Odd number
                    $number_arr[$i] = $number_arr[$i] * 2;
                }

                $digit_length = strlen((string)$number_arr[$i]);

                if($digit_length>1){
                    $number_arr[$i] = ($number_arr[$i]/10) + ($number_arr[$i]%10);
                }

                $sum += $number_arr[$i];
            }

            $sum = 10 - ($sum % 10);

            if($sum == $last_2_digits){
                $result['status'] = $this->STATUS_SUCCESS; //Success
                $result['message'] = 'Account Number is valid';
                return $result;
            }
            else{
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }
        }
        else{
            $result['status'] = $this->STATUS_FAILED; //Failed
            $result['message'] = 'Fields Empty';
            return $result;
        }
              
    }  

    public function UnifiValidation($data) {
        
        $account_number = null;

        if(!empty($data['fields'])){
            foreach ($data['fields'] as $key => $value) {
                if(!empty($value['value'])){
                    $record = InputField::find($value['field_id']);

                    if($record['name']=="Account Number (Numeric)"){
                        $account_number = $value['value'];
                    } 
                }
            }
        }

        if(!empty($account_number)){
            $length = strlen((string)$account_number);
            $number_arr = str_split($account_number);
            $sum = 0;

            for ($i=0; $i < $length; $i++) { 

                if ($i % 2 == 0) {///Even number
                    $number_arr[$i] = $number_arr[$i] * 2;
                }
                else{///Odd number
                    $number_arr[$i] = $number_arr[$i] * 1;
                }

                $digit_length = strlen((string)$number_arr[$i]);

                if($digit_length>1){
                    $number_arr[$i] = $number_arr[$i] - 9;
                }

                $sum += $number_arr[$i];
            }

            $sum = ($sum % 10);

            if($sum == 0){
                $result['status'] = $this->STATUS_SUCCESS; //Success
                $result['message'] = 'Account Number is valid';
                return $result;
            }
            else{
                $result['status'] = $this->STATUS_FAILED; //Failed
                $result['message'] = 'Account Number is not valid';
                return $result;
            }
        }
        else{
            $result['status'] = $this->STATUS_FAILED; //Failed
            $result['message'] = 'Fields Empty';
            return $result;
        }
              
    } 

    public function checkTMRange($first_letter, $jul_date) {
        $result = false;

        switch ($first_letter) {
            case 'A': //
                if ($jul_date >= 0 && $jul_date <= 25){
                    $result = true;
                }
                break;
            case 'K': //
                if ($jul_date >= 51 && $jul_date <= 75){
                    $result = true;
                }
                break;
            case 'U': //
                if ($jul_date >= 26 && $jul_date <= 50){
                    $result = true;
                }
                break;
            case 'Z': //
                if ($jul_date >= 76 && $jul_date <= 100){
                    $result = true;
                }
                break;
            case 'C': //
                if ($jul_date >= 101 && $jul_date <= 125){
                    $result = true;
                }
                break;
            case 'F': //
                if ($jul_date >= 126 && $jul_date <= 150){
                    $result = true;
                }
                break;
            case 'N': //
                if ($jul_date >= 151 && $jul_date <= 175){
                    $result = true;
                }
                break;
            case 'P': //
                if ($jul_date >= 176 && $jul_date <= 200){
                    $result = true;
                }
                break;
            case 'L': //
                if ($jul_date >= 201 && $jul_date <= 225){
                    $result = true;
                }
                break;
            case 'H': //
                if ($jul_date >= 226 && $jul_date <= 250){
                    $result = true;
                }
                break;
            case 'Y': //
                if ($jul_date >= 251 && $jul_date <= 275){
                    $result = true;
                }
                break;
            case 'B': //
                if ($jul_date >= 276 && $jul_date <= 300){
                    $result = true;
                }
                break;
            case 'G': //
                if ($jul_date >= 301 && $jul_date <= 325){
                    $result = true;
                }
                break;
            case 'S': //
                if ($jul_date >= 326 && $jul_date <= 350){
                    $result = true;
                }
                break;
            case 'D': //
                if ($jul_date >= 351 && $jul_date <= 999){
                    $result = true;
                }
                break;
            default:
                $result = false;
        }//End Switch

        return $result;
    }


    
}
