<?php

namespace App\Helpers;

//TABLE
use App\EmailSetting;
use App\Lookup;
use App\RequestTAC;
use App\ContactNumber;
use App\UserAccount;
use Log;
use Response;
use Session;
use \Crypt;
use Carbon;

class PushNotification {

    protected static $resendAfter = 5; //5 in minutes

    public static function generateMessage($user, $type, $order_id, $amount, $currency) {

        switch ($type) {
            case 1:  //Successfull transaction For Merchant
                $message = "UPayMe: Transaction for order id-".$order_id." is successful. The amount of ".$currency." ".$amount." shall be credited to your account soon.
Thank you.";
                break;
            case 2:  //Successfull transaction For User
                $message = "UPayMe: Transaction for order id-".$order_id." is successful. The amount of ".$currency." ".$amount." has been debited from your account.
Thank you.";
                break;
            case 3:  //Failed transaction For User
                $message = "UPayMe: Transaction for order id-".$order_id." is unsuccessful.
Thank you.";
                break;
            default:
                $message = null;
                Log::useDailyFiles(storage_path() . '/logs/GENERATE_MESSAGE.log');
                Log::notice('Type fail: Type not found.');
        }//End Switch

        return $message;
    }

    public function send($user,$type, $order_id, $amount, $currency){

        $message = $this->generateMessage($user, $type, $order_id, $amount, $currency);

        $registrationIds[] = $user['register_id'];
        
        $fields = array(
            'registration_ids'    => $registrationIds
            ,'notification' => array(
                'title' => 'UPAYME' 
                ,'text' =>  $message
            )
            ,'data' => array(
                'image' => ''
                ,'action' => ''
                ,"type" => $type
            )
            ,'sound' => 'default'
            ,'priority' => 'high'
        );
     
        $headers = array(
            'Authorization: key=AAAAFWDQAaM:APA91bEwFEQBVBTj8ozDdIehitox8RT5XM7wU1M-DoTWbnMNqBViQdkTaWPVPkIrAVfl12iXvvyumYZ_Nh9lugrNCxUcVQIXemGpQPtDSJhBsi619H2ci_coPeQtGxUM6XAecXUWHLW7',
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS,  json_encode($fields)  );

        $fcm_result = (array)json_decode(curl_exec($ch));

        Log::useDailyFiles(storage_path() . '/logs/GENERATE_MESSAGE.log');
        Log::notice($fcm_result);

        curl_close( $ch );

        return $fcm_result;
        //save message
        // $save_msg = array(
        //   'title' => $title
        //   ,'message' => $message
        //   ,'image_name' => $path
        //   ,'action' => $action_path
        // );

        // $message_log_id = '';
        // $message_log_model = New MessageLog();
        // $message_log_model->fill($save_msg);

        // if($message_log_model->save()){
        //   $message_log_id = $message_log_model->id;
        // }else{
        //   $jsonResult['errMsg'] = 'Save message problem!.. Send FCM abort!';
        //   return Response::json($jsonResult);
        // }
        // //end save message

        // //Save FCM LOG
        // $result_fcm = array('success' => $successSend , 'failure' => $failureSend);

        // $save_fcm_log = array(
        //   'user_id' => Auth::user()->id
        //   , 'message_log_id' => $message_log_id
        //   , 'result' => json_encode($fcm_result_raw)
        //   , 'result_fcm' => json_encode($result_fcm)
        //   , 'sent_to' => $input["sent_to"]
        // );

        // $fcm_log_model = New FcmLog();
        // $fcm_log_model->fill($save_fcm_log);
       
        // if($fcm_log_model->save()){
        //   //For Browser Type store the message that send into paylod table
        //     foreach ($registrationIds as $key => $id) {
        //       $payload = array();
        //       $payload['message_log_id'] = $message_log_id;
        //       $records = Application::select('platform')
        //       ->leftjoin('fcm as F','applications.id','=','F.application_id')
        //       ->where('F.register_id',$id)
        //       ->first();
        //       if ($records['platform']=="B"){
        //         $payload['register_id'] = $id;
        //         $payload_model = New Payload();
        //         $payload_model->fill($payload);
        //         $payload_model->save();
        //       }
        //     }
        //   //End save payload

        //   $jsonResult['result'] = 1;
        //   Session::flash('success', 'Send FCM complete and success');
        //   return Response::json($jsonResult);
        // }else{
        //   $jsonResult['errMsg'] = 'Save FCM Log problem !.. Send FCM abort!';
        //   return Response::json($jsonResult);
        // }
        // //END FCM LOG      
    }

}
