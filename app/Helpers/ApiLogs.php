<?php

/*
 * Developer: Mohamad Khairil
 * Upay API Logs
 * 
 */

namespace App\Helpers;

use Log;
use Request;
use Carbon\Carbon;
use App\Helpers\HttpCode;

class ApiLogs {
    
   
    public function __construct() {
       

    }

    public static function store($status_code, $message ='success') {
        $data = array('logtype'=>'success','code'=>'SUCCESS','http_code'=>$status_code,'message'=>$message);
        ApiLogs::AddLog($data);
    }

    /*
     * Add Log for API
     */

    public static function AddLog($data) {
        
        if(empty($data['dt']))//timestamp
           $data['dt'] = Carbon::now()->timestamp;
        $userLogin = request()->user();
        if(!is_null($userLogin))//user id
            $user_id = $userLogin->id;
        else
            $user_id = null;
        if(!empty($data['http_code']) || empty($data['code']))//code type
           $data['code'] = HttpCode::HttpType($data['http_code'])->getData()->code;
        if(!empty($data['http_code']) || empty($data['logtype']))//code group type
           $data['logtype'] = HttpCode::HttpType($data['http_code'])->getData()->groupCode;
        
       
        $from_device = 'Phone';
       
        
        $logmsg = json_encode(array(
            'dt' => $data['dt'],//datetime
            'user_id' => $user_id,
            'code' => $data['code'],
            'http_code' => $data['http_code'],
            'message' => $data['message'],
            'methods' => Request::method(),
            'api_link' => Request::fullUrl(),
            'ip_address' => Request::ip(),
            'parameters' => Request::all()  
        ));
       
         ///logs/Api/ErrorLogs/201705 LOG purpose ..DAILY LOG
        $errorStorage = storage_path().'/logs/Api/ErrorLogs/'.carbon::now()->format('Ym').'/error.log';  
        $successStorage = storage_path().'/logs/Api/SuccessLogs/'.carbon::now()->format('Ym').'/success.log';
        
        if($data['logtype'] == 'error'){
            Log::useDailyFiles($errorStorage);
            Log::error($logmsg);
        }
        if($data['logtype'] == 'success'){
            Log::useDailyFiles($successStorage);
            Log::info($logmsg);
        }
       
    }

}
