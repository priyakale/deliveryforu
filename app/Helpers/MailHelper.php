<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Cache;

//TABLE
use App\Models\EmailSetting;
use App\Models\Lookup;

class MailHelper {

    public static function getDriver(){
        
        $email_setting = EmailSetting::select('driver')->first();
        $driver_name = '';
        if(!empty($email_setting)){
            $type_record = Type::find($email_setting['driver']);
            $driver_name = $type_record->name;
        }
        
        return $driver_name;
    }

    public static function getName(){
        $email_setting = EmailSetting::select('name')->first();
        $name = '';
        if(!empty($email_setting)){
            $name = $email_setting->name;
        }
        
        return $name;
    }

    public static function getSMTPHost(){
        $email_setting = EmailSetting::select('smtp_host')->first();
        $smtp_host = '';
        if(!empty($email_setting)){
            $smtp_host = $email_setting->smtp_host;
        }
        
        return $smtp_host;
    }

    public static function getSMTPUsername(){
        $email_setting = EmailSetting::select('smtp_username')->first();
        $smtp_username = '';
        if(!empty($email_setting)){
            $smtp_username = $email_setting->smtp_username;
        }
        
        return $smtp_username;
    }

    public static function getSMTPPassword(){
        $email_setting = EmailSetting::select('smtp_password')->first();
        $smtp_password = '';
        if(!empty($email_setting)){
            $smtp_password = $email_setting->smtp_password;
        }
        
        return $smtp_password;
    }

    public static function getEncryption(){
    	$email_setting = EmailSetting::select('encryption')->first();
        $encryption = '';
        if(!empty($email_setting)){
            $type_record = Type::find($email_setting['encryption']);
            $encryption = $type_record->name;
        }
        
        return $encryption;
    }

    public static function getPort(){
        $email_setting = EmailSetting::select('port')->first();
        $port = '';
        if(!empty($email_setting)){
            $port = $email_setting->port;
        }
        
        return $port;
    }    

    public static function getSendmailPath(){
        $email_setting = EmailSetting::select('sendmail_path')->first();
        $sendmail_path = '';
        if(!empty($email_setting)){
            $sendmail_path = $email_setting->sendmail_path;
        }
        
        return $sendmail_path;
    }    

    public static function getEmailSetting(){
        
        $email_setting = Cache::remember('EmailSetting', 1440, function() {
            return EmailSetting::first();
        });

        if(!empty($email_setting)){
            $driver_name = Lookup::select('name')->find($email_setting['driver_id']);
            $email_setting['driver_id'] = $driver_name->name;

            $encryption = Lookup::select('name')->find($email_setting['encryption_id']);
            $email_setting['encryption_id'] = $encryption->name;
        }
        
        return $email_setting;
    }    
}
