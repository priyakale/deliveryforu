<?php

namespace App\Providers;

use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\Facades\Schema;
use Log;

//HELPER
use App\Helpers\MailHelper;


class CustomMailServiceProvider extends MailServiceProvider{

    public function register()
    {
        if (\Schema::hasTable('email_settings')) {
            $email_setting = MailHelper::getEmailSetting();
        }

        if(!empty($email_setting)){
            $this->app['config']['mail'] = [
                'driver'        => $email_setting['driver_id'],
                'host'          => $email_setting['smtp_host'],
                'port'          => $email_setting['port'],
                'from'          => [
                'address'   => $email_setting['smtp_username'],
                'name'      => $email_setting['name']
                ],
                'encryption'    => $email_setting['encryption_id'],
                'username'      => $email_setting['smtp_username'],
                'password'      => $email_setting['smtp_password'],
                'sendmail'      => $email_setting['sendmail_path'],
                'pretend'       => ''
            ];
            $this->app['config']['services'] = [
                'mailgun'          => [
                'domain'   => $email_setting['mailgun_domain'],
                'secret'      => $email_setting['mailgun_secret']
                ]
            ];
        }
        else{
            Log::useDailyFiles(storage_path().'/logs/Mail_Setting.log');
            Log::notice('Error Empty Mail Setting : Mail setting is empty');
        }

        $this->registerSwiftMailer();

        $this->registerIlluminateMailer();

        $this->registerMarkdownRenderer();
    }

}
