<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use StateRepo;

class StateController extends Controller
{
    public function __construct(StateRepo $repo)
    {
      $this->repo = $repo;
    }

    //Display the specified resource.
    public function index()
    {
        return $this->repo->getStateCache();
    }
}