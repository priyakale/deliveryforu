<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use CountryRepo;

class CountryController extends Controller
{
    public function __construct(CountryRepo $repo)
    {
      $this->repo = $repo;
    }

    //Display the specified resource.
    public function index()
    {
        return $this->repo->getCountryCache();
    }
}