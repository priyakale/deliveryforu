<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use LookupRepo;


class LookupController extends Controller
{
    public function __construct(LookupRepo $repo)
    {
      $this->repo = $repo;
    }

    //Display the specified resource.
    public function show($category)
    {
        //$this->repo->validator(array('category'=>$category),'exists')->validate();
        return $this->repo->getLookupCache($category);
    }
}