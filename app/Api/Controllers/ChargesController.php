<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use ChargesRepo;

class ChargesController extends Controller
{
    public function __construct(ChargesRepo $repo)
    {
      $this->repo = $repo;
    }

    //Display the specified resource.
    public function index()
    {
        return $this->repo->find(1);
    }
}