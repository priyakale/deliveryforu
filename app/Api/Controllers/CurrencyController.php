<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use CurrencyRepo;

class CurrencyController extends Controller
{
    public function __construct(CurrencyRepo $repo)
    {
      $this->repo = $repo;
    }

    //Display the specified resource.
    public function index()
    {
        return $this->repo->getCurrencyCache();
    }
}