<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use UserRepo;
use ImageRepo;
use App\Api\Response\Contracts\IResponse;
use Illuminate\Support\Facades\Crypt;
use GuzzleHttp\Client;

//HELPER
use App\Helpers\SettingHelper;

use App\Models\User;
use App\Models\InvestorDetail;


class UserController extends Controller
{    
    protected $repo;
    protected $eager_load = ['role','devices.transactions','devices.sku','devices.package_fee','images'];
    
    public function __construct(UserRepo $repo, ImageRepo $imageRepo)
    {
        $this->repo = $repo;
        $this->imageRepo = $imageRepo;
    }

    protected function validator(array $data, $method = null) {

        $messages = [
            
        ];
      
        if ($method == 'register') {
            $rule = [
                    'email' => 'required|max:100|unique:users',
                    'password' => 'required|between:6,25',
                    'client_id' => 'required',
                    'client_secret' => 'required',
            ];
        }elseif ($method == 'device_update') {
            $rule = [
                     'register_id'  =>'required',
            ];        
        }

        return Validator::make($data, $rule); 
    }

    //Display a list of users
    public function index(Request $request)
    {
        //Set default page size if page size not found in the request.
        $per_page = empty($request->ps) ?  SettingHelper::getPerPageApi() : $request->ps;
        return $this->repo->paginate($request->all(),$this->eager_load,$per_page);
    }

    //Display the specific user
    public function show($id)
    {
        $this->repo->validator(['id'=>$id],'show')->validate();
        return $this->repo->withRecord($this->repo->find($id,$this->eager_load)->toArray(), "Show User");
    }
    
    //Store a newly created user
    public function create(Request $request)
    {         
        $data = $request->input();

        $login_details = array(
            'password' => $request->password,
            'email' => $request->email,
            'client_id' => $request->cid,
            'client_secret' => $request->cp
        );

        $validator = $this->validator($login_details, 'register')->validate();

        $password = $data['password'];

        $data['password'] = bcrypt($data['password']);
        $data['role_id'] = 5;
        $data['activated'] = 1;
        $data['name'] = $request->fullname;
        $data['phone'] = $request->phone;      

        if(!empty($request->register_id)){
            $data['register_id'] = $request->register_id;
        }
        else{
            $data['register_id'] = null;
        }

        $user = User::create($data); //CREATE

        $data['user_id'] = $user->id;
        
        $investor = InvestorDetail::create($data); //CREATE

        $client = new Client();
        try {
            $res = $client->request('POST', env('OAUTH_TOKEN_URL'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $request->cid,
                    'client_secret' => $request->cp,
                    'username' => $request->email,
                    'password' => $password,
                    'scope' => ''
                ],
                'http_errors' => false // add this to return errors in json
            ]);
        } catch (\Exception $ex) 
        {
            return $ex;
        }        
        
        //$data = array('logtype' => 'success', 'code' => 'SUCCESS', 'http_code' => 200, 'message' => 'Login Success, Email: ' . $request->email, 'headers' => '');
        $data = [
                    'user_id' => $user->id,
                    'investor_id' => $investor->id,
                    'status_code' => 0,
                    'status_title' => 'Success',
                    'login_details' => json_decode((string) $res->getBody(), true)
        ];

        return response()->json($this->repo->withRecord($data, 'Create User'));
    }

    //Update the specified user
    public function update(Request $request)
    {
        return $this->repo->withRecord($this->repo->save($request->all())->toArray(), "Update User");
    }

    //Remove the specified user
    public function destroy($id)
    {
        return $this->repo->delete($id);
    }

    //Get the specified user from access token
    public function user(Request $request)
    {
        return $this->repo->withRecord(request()->user()->load('wallet','parent_referral')->toArray(), "Get Current User");
    }

    public function uploadImage(Request $request)
    {                
        return $this->imageRepo->save($request->all(), json_decode($request[0])->user_id, "user");
    }

    //Update the register id into database
    public function updateRegisterId(Request $request)
    {         
        $data = $request->all();
        
        $user = $request->user();
      
        $res['status'] = 'error';

        $device_details = array(
            'register_id'   =>$request->register_id
        );

        $validator = $this->validator($device_details, 'device_update')->validate();
        $data['user_id'] = $user->id;         
       
        $device_id = $this->repo->updateDevice($user->id, $data['register_id']);
      
        if($device_id){
            $res['status'] = 'success';
        }
        return response()->json($this->repo->withRecord($res, "Update Register Id"));         
    }    

    /*
     * Update password 
     * email can't update 
     */

    public function changePassword(Request $request) {

        $user = request()->user();
        if (is_null($user)) { ////IF NOT FOUND
            return $this->response->errorNotFound();
        }

        $data = $request->all();

        $this->repo->validator($data, 'change_password')->validate();

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json($error = [
                    "error" => [
                        "code" => 422,
                        "message" => "Invalid old password"
                    ]
                ], 422);
        }

        $user->password = bcrypt($request->new_password);
        $user->save();

        $result['status'] = 1;
        $result['message'] = "Password changed successfully.";
        
        return response()->json($this->repo->withRecord($result, "Update Register Id")); 
    } 
}
