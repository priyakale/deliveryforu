<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use TransactionRepo;
use UserRepo;
use Log;
use Carbon\Carbon;
use App\Helpers\PushNotification;

use App\Models\MerchantOutlet;
use App\Models\Merchant;

class TransactionController extends Controller
{        
    public function __construct(TransactionRepo $transactionRepo, UserRepo $userRepo, PushNotification $pushNotification)
    {
        $this->transactionRepo = $transactionRepo;
        $this->userRepo = $userRepo;
        $this->pushNotification = $pushNotification;
    }

    public function history(Request $request) 
    {

        $user = request()->user();

        $data = ['user_id'=>$user->id,'month'=>$request->month,'year'=>$request->year];

        if($user->role_id==6 || $user->role_id==7){
            $data['merchant_id'] = Merchant::whereUserId($user->id)->first()->id;
        }
        else if($user->role_id==8){
            $data['salesperson_id'] = $user->id;
        }

        $records = $this->transactionRepo->all($data, ['status','merchant'], ["id","amount","txn_description","currency","merchant_id","payment_type","status_id",'created_at'])->toArray();

        $response = [];

        if(!empty($records)){
            foreach ($records as $key => $value) {
                $path = asset('/uploads/merchants/MerchantId-' . $value['merchant']['id'].'/'.$value['merchant']['logo']);
                $records[$key]['merchant']['image_path'] = $path;

                $response[$key]['id'] = $value['id'];

                if($value['merchant']['type_id']==71){
                    $response[$key]['merchant_name'] = $value['merchant']['trading_name'];
                }
                else{
                    $response[$key]['merchant_name'] = $value['merchant']['individual_name'];
                }

                $response[$key]['payment_type'] = $value['payment_type'];
                $response[$key]['currency'] = $value['currency'];
                $response[$key]['amount'] = $value['amount'];
                $response[$key]['txn_description'] = $value['txn_description'];
                $response[$key]['status_name'] = $value['status']['name'];
                $response[$key]['status_id'] = $value['status_id'];
                $response[$key]['image_path'] = $path;
                $response[$key]['created_at'] = $value['created_at'];
            }
        }            

        return $this->transactionRepo->withRecord($response, 'Transaction History');
    }

    public function getMonth(Request $request) 
    {
        $user = request()->user();

        return $this->transactionRepo->withRecord($this->transactionRepo->getMonth(), 'Get Month');
    }
    
    /*
     * GET RESPONSE FROM UPAY GATEWAY AND UPDATE THE TRANSACTION STATUS
     */
    public function postUpay(Request $request) 
    {    
        return $this->transactionRepo->withRecord($this->transactionRepo->updateTransactionStatus($request->all())->toArray());
    }
}
