<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;

//Repositories
use MerchantInputFieldRepo;
use MerchantRepo;
use FavouriteMerchantRepo;
use App\Models\FavouriteMerchant;

class MerchantController extends Controller
{

     public function __construct(MerchantRepo $repo, MerchantInputFieldRepo $fieldRepo, FavouriteMerchantRepo $favouriteMerchantRepo)
    {
      $this->repo = $repo;
      $this->fieldRepo = $fieldRepo;
      $this->favouriteMerchantRepo = $favouriteMerchantRepo;
    }     
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $records = $this->repo->all(['status_id'=>1,'type_id'=>71],[],['id','user_id','company_registered_name','trading_name','logo','sub_merchant_ref','headquarters_address','charges_apply']);
        foreach ($records as $key => $value) {
            if($value['id']==1){
                $value['sub_merchant_ref'] = "";
            }
            $outlet_count = $this->repo->get_outlet_count($value['id']);
            if($outlet_count>0)
            {
                $records[$key]['outlet_count'] = "true";
            }
            else
            {
                $records[$key]['outlet_count'] = "false";
            }
            
            $path = asset('/uploads/merchants/MerchantId-' . $value['id'].'/'.$value['logo']);
            $records[$key]['company_registered_name'] = $records[$key]['trading_name'];
            $records[$key]['image_path'] = $path;

            $favourite = FavouriteMerchant::whereUserId($user['id'])
                            ->whereMerchantId($value['id'])
                            ->whereNull('salesperson_id')
                            ->first();

            if(!empty($favourite)){
                $records[$key]['favourite'] = "true";
            }
            else{
                $records[$key]['favourite'] = "false";
            }
        }

        return $this->repo->withRecord($records, "Merchant List");
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function individual_merchants(Request $request)
    {
        $user = $request->user();

        $records = $this->repo->all(['status_id'=>1,'type_id'=>72],[],['id','user_id','individual_name','trading_name','logo','sub_merchant_ref','individual_address','charges_apply']);
        
        foreach ($records as $key => $value) {
            if($value['id']==1){
                $value['sub_merchant_ref'] = "";
            }
            $path = asset('/uploads/merchants/MerchantId-' . $value['id'].'/'.$value['logo']);
            //$records[$key]['company_registered_name'] = $records[$key]['trading_name'];
            $records[$key]['individual_name'] = $records[$key]['individual_name'];
            $records[$key]['trading_name'] = $records[$key]['trading_name'];
            $records[$key]['image_path'] = $path;

            $favourite = FavouriteMerchant::whereUserId($user['id'])
                            ->whereMerchantId($value['id'])
                            ->whereNull('salesperson_id')
                            ->first();

            if(!empty($favourite)){
                $records[$key]['favourite'] = "true";
            }
            else{
                $records[$key]['favourite'] = "false";
            }
        }

        return $this->repo->withRecord($records, "Individual List");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_merchant_outlets(Request $request, $merchant_id)
    {
        $user = $request->user();
        $records = $this->repo->get_merchant_outlets($merchant_id);
        
        $data_array = array();
        foreach ($records as $key => $value){
            $path = asset('/uploads/outlets/'.$records[$key]->id.'/'.$records[$key]->outlet_logo);

            $favourite = FavouriteMerchant::whereUserId($user->id)->whereMerchantId($merchant_id)->where('salesperson_id', $value->user_id)->first();
            
            if(!empty($favourite)){
                $favourite = "true";
            }
            else{
                $favourite = "false";
            }

            $data_array[] = array(
                                'salesperson_id'=>$records[$key]->user_id,
                                'outlet_pic_name'=>$records[$key]->outlet_pic_name,
                                'image_path'=>$path,
                                'address'=>$value->outlet_address,
                                'favourite'=>$favourite
                            );
        }
        return $this->repo->withRecord($data_array, "Outlet List");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $records = $this->fieldRepo->all(['merchant_id'=>$id],['input_field.options'],['id','input_field_id','is_mandatory','position']);
        foreach ($records as $key => $value) {

            $value['name'] = $value['input_field']['name'];
            $value['data_type'] = $value['input_field']['data_type']['name'];
            $value['options'] = $value['input_field']['options'];
            unset($value['input_field']);
        }
        return $this->repo->withRecord($records, "Show Merchant");
    }

    /**
     * check_mobile exist or not
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function check_mobile(Request $request)
    {
        $user = $request->user();

        $res['is_exist'] = 1;

        if($user->role_id==5){
            $record = $this->repo->getPhoneNumber($user->id);

            if(empty($record['phone'])){
                $res['is_exist'] = 0;
            }
        }
            
        return response()->json($this->repo->withRecord($res, "Check Mobile"));
    }

    /**
     * update_mobile if not exist
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_image_msg(Request $request)
    {
        $data = $request->all();
        
        $user = $request->user();
        
        $res['res'] = 'error';

        if($user->role_id==5){
            $update_mobile = $this->repo->updateMobile($user->id, $data['phone']);

            if($update_mobile){
                $res['res'] = 'success';
            }
        }
            
        return response()->json($this->repo->withRecord($res, "Update Mobile"));
    }

    /**
     * getFavourite
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getFavourite(Request $request)
    {
        $data = $request->all();
        
        $user = $request->user();
        
        $data['user_id'] = $user['id'];

        $records = $this->favouriteMerchantRepo->all($data,['merchant' => function($query) {
            $query->select('id','user_id','company_registered_name','trading_name','logo','sub_merchant_ref','individual_name','type_id','headquarters_address','individual_address','charges_apply');
        },'salesperson'],['id','user_id','merchant_id','salesperson_id'])->toArray();

        $response = [];

        foreach ($records as $key => $value) {
          
            if($value['merchant']['id']==1){
                $value['merchant']['sub_merchant_ref'] = "";
            }

            $path = asset('/uploads/merchants/MerchantId-' . $value['merchant']['id'].'/'.$value['merchant']['logo']);
            $records[$key]['merchant']['image_path'] = $path;

            if($value['merchant']['type_id']==71){

                if(empty($value['salesperson_id'])){
                  
                    $outlet_count = $this->repo->get_outlet_count($value['merchant']['id']);
                    if($outlet_count>0)
                    {
                        $records[$key]['merchant']['outlet_count'] = "true";
                    }
                    else
                    {
                        $records[$key]['merchant']['outlet_count'] = "false";
                    }

                    $records[$key]['merchant']['salesperson_id'] = "";
                    $records[$key]['merchant']['salesperson_name'] = "";
                    $records[$key]['merchant']['salesperson_address'] = "";
                    $records[$key]['merchant']['merchant_address'] = $value['merchant']['headquarters_address'];

                }
                else{
                    $records[$key]['merchant']['outlet_count'] = "false";
                    $records[$key]['merchant']['salesperson_id'] = $value['salesperson_id'];
                    $records[$key]['merchant']['salesperson_name'] = $value['salesperson']['outlet_pic_name'];
                    $records[$key]['merchant']['salesperson_address'] = $value['salesperson']['outlet_address'];
                    $records[$key]['merchant']['merchant_address'] = $value['merchant']['headquarters_address'];
                }

            }
            else{
                $records[$key]['merchant']['trading_name'] = $records[$key]['merchant']['individual_name'];
                $records[$key]['merchant']['outlet_count'] = "false";
                $records[$key]['merchant']['salesperson_id'] = "";
                $records[$key]['merchant']['salesperson_name'] = "";
                $records[$key]['merchant']['salesperson_address'] = "";
                $records[$key]['merchant']['merchant_address'] = $value['merchant']['individual_address'];
            }
          
            $response[] = $records[$key]['merchant'];           

        }

        return $this->repo->withRecord($response, "Get Favourite");
    }

    /**
     * addFavourite
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addFavourite(Request $request)
    {
        $data = $request->all();
        
        $user = $request->user();
        
        $data['user_id'] = $user['id'];

        return $this->repo->withRecord($this->favouriteMerchantRepo->save($data), "Add Favourite");
    }

    /**
     * deleteFavourite
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFavourite(Request $request)
    {
        $data = $request->all();
        
        $user = $request->user();
        
        $data['user_id'] = $user['id'];

        $this->favouriteMerchantRepo->validator($data, "delete")->validate();

        if(!empty($data['salesperson_id'])){
          $record = FavouriteMerchant::whereUserId($data['user_id'])->whereMerchantId($data['merchant_id'])->where('salesperson_id', $data['salesperson_id'])->get();
        }
        else{
          $record = FavouriteMerchant::whereUserId($data['user_id'])
                      ->whereMerchantId($data['merchant_id'])
                      ->whereNull('salesperson_id')
                      ->get();
        }
          

        if(!empty($record)){
            if(!empty($data['salesperson_id'])){
              $delete = FavouriteMerchant::whereUserId($data['user_id'])->whereMerchantId($data['merchant_id'])->where('salesperson_id', $data['salesperson_id'])->delete();
            }
            else{
              $delete = FavouriteMerchant::whereUserId($data['user_id'])
                        ->whereMerchantId($data['merchant_id'])
                        ->whereNull('salesperson_id')
                        ->delete();
            }

            if($delete){
                return $this->favouriteMerchantRepo->success();
            }
            else{
                return $this->favouriteMerchantRepo->errorDelete();
            }
        }
        else{
            return $this->favouriteMerchantRepo->errorDelete();
        }

        
    }
}
