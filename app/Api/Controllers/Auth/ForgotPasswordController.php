<?php

namespace App\Api\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Api\Transformers\UserTransformer;
use Illuminate\Auth\Passwords\PasswordBroker;
use App\EmailNotification;
use App\Helpers\SettingControl;
use Notification as NF;
use App\Notifications\Email;
use UserRepo;
use Log;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {

    }

    /*
     * Validator
     */
    protected function validator(array $data) {
        $messages = [
            'ue.email' => 'The email must be a valid email address'
        ];

        return Validator::make($data, [
                        'ue' => 'required|email|exists:users,email',

                         ], $messages);
    }

    /*
     * Forgot password Check by Email
     * Reset password and send new password by email user
     */

    public function index(Request $request)
    {
         $data = array(
                       'ue' => $request->ue,
                        );

        $validator = $this->validator($data);

        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->messages());
        }


        $exists = User::where('email',$request->ue)->count();///get by email

        if ($exists == 0) { ////IF NOT FOUND
            return $this->response->errorNotFound('Email not exists');
        } else {
          $user = User::where('email', $request->ue)->first();
        if($user==""){
            return back()->with('danger', "Email does not exists.");
        }
        $password_broker = app(PasswordBroker::class); //so we can have dependency injection
        $token = $password_broker->createToken($user); //create reset password token

        //$this->validateEmail($request);
        $email_notification = EmailNotification::where('email_for_id', 103)->first();
        if (!empty($email_notification)) {
            $message['actionText'] = "Reset Password";
            $message['subject'] = $email_notification['subject'];
            $link = $message['actionUrl'] = url('password/reset',$token);
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            $message['description'] = str_replace("%USER_USERNAME", $user['username'], $email_notification['description']);
            $message['description'] = str_replace("%URL", $link, $message['description']);
            // //END REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA
            if ($smtp_response = SettingControl::check_email_settings(1) == 1) {
                NF::send($user, new Email($message));
                $response['status'] = 0; //Success
                $response['message'] = 'We have sent you an password reset email.Please check your email.';
                return $this->response->successDynamicMessage($user->id,'We have sent you an password reset email.Please check your email');
            } else {
                //echo $smtp_response;
                //exit;
                return $this->response->errorNotFound("Email cannot send, SMTP can't connect");
            }
        }

            return $this->response->successDynamicMessage($user->id,'We have sent you an password reset email.Please check your email');
        }
    }

    public function random_string($length) {

        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                . '0123456789'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';
        foreach (array_rand($seed, $length) as $k)
            $rand .= $seed[$k];

        return $rand;
    }
}
