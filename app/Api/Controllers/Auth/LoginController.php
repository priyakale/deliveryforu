<?php
/* 
 * LoginController with API structure
 */
namespace App\Api\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response as HttpResponse;
use Response;
use App\Api\Response\Contracts\IResponse;
use Illuminate\Support\Facades\Crypt;
use GuzzleHttp\Client;
use App\Channel;
use App\Helpers\ApiLogs;
//MODEL
use App\Models\Login;
use App\Models\InvestorDetail;

//REPOSITORY
use ActivationRepo;
use MerchantRepo;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $activationRepo;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Response $response, ActivationRepo $activationRepo, MerchantRepo $merchantRepo) {
        //$this->middleware('guest', ['except' => 'logout']);
        $this->response = $response;
        $this->activationRepo = $activationRepo;
        $this->merchantRepo = $merchantRepo;        
    }

    protected function validator(array $data, $method = null) {

        $messages = [
            
        ];
      
        if ($method == 'login') {
            $rule = [
                    'email' => 'required|max:100',
                    'password' => 'required|between:6,25',
                    'client_id' => 'required',
                    'client_secret' => 'required',
                         ];
        }

        return Validator::make($data, $rule); 
    }
    
    public function index(Request $request) 
    {   
      
        $data = array('password' => $request->password,
            'email' => $request->email,
            'client_id' => $request->cid,
            'client_secret' => $request->cp
        );

        $this->validator($data, 'login')->validate();        

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) == false) {
            //return $this->sendFailedLoginResponse($request);
            return response()->json(
              ["error" => [
                        "code" => 422,
                        "message" => ["err_msg" => "Email or password is incorrect"]
                    ]], 422);
        }

        $user = Auth::user();//get user data          
        
        if ($user->role_id != 5 && $user->role_id != 6 && $user->role_id != 7 && $user->role_id != 8){
            Auth::logout();
            return response()->json(
              ["error" => [
                        "code" => 422,
                        "message" => ["err_msg" => "Unauthorized Access."]
                    ]], 422);
        }

        // if ($user->activated == 0){
        //     Auth::logout();
        //     return response()->json(array('message' => ["err_msg" => 'You need to activate your account. We have sent you an activation code, please check your email.']), 401);
        // }
        
        if(!empty($user)){

            if(!empty($request->register_id)){
                $register_id = $request->register_id;
            }
            else{
                $register_id = null;
            }

            $user->register_id = $register_id;
            $user->save();
        }
          
        $client = new Client();
        try {
            $res = $client->request('POST', env('OAUTH_TOKEN_URL'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $request->cid,
                    'client_secret' => $request->cp,
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => ''
                ],
                'http_errors' => false // add this to return errors in json
            ]);
        } catch (\Exception $ex) 
        {
            return $ex;
        }        
        
        if($user->role_id==6){
          $merchant_data = $this->merchantRepo->findBy(['user_id'=>$user->id]);
          $user_id = $merchant_data->id;
        }
        elseif($user->role_id==7){
          $merchant_data = $this->merchantRepo->findBy(['user_id'=>$user->id]);
          $user_id = $merchant_data->id;
        }
        elseif($user->role_id==8){
          $merchant_data = $this->merchantRepo->findBy(['user_id'=>$user->referral_id]);
          $user_id = $merchant_data->id;          
        }
        else{
          $user_id = $user->id; 
        }
        
        $info = json_decode((string) $res->getBody(), true);
        $info['user_id']=$user_id;
        $info['role_id']=$user->role_id;

        $info['name']=$user->name;

        if($user->role_id==5){
            $info['contact_number'] = InvestorDetail::whereUserId($user->id)->first()->phone;
        }

        if(!empty($merchant_data))
        {
          $info['image_path'] = asset('/uploads/merchants/MerchantId-' . $merchant_data['id'].'/'.$merchant_data['logo']);

          if($user->role_id==7){
            $info['company_registered_name'] = $merchant_data->individual_name;
            $info['trading_name'] = $merchant_data->trading_name;
            $info['contact_number'] = $merchant_data->phone;
            $info['address'] = $merchant_data->individual_address;
          }
          elseif($user->role_id==6){
            $outlet_count = $this->merchantRepo->get_outlet_count($user_id);
            if($outlet_count>0)
            {
                $info['outlet_count'] = "true";
            }
            else
            {
                $info['outlet_count'] = "false";
            }
            $info['company_registered_name'] = $merchant_data->trading_name;
            $info['trading_name'] = $merchant_data->trading_name;
            $info['contact_number'] = $merchant_data->headquarters_contact_number;
            $info['address'] = $merchant_data->headquarters_address;
          }
          elseif($user->role_id==8){
            $salesperson = $this->merchantRepo->get_salesperson($user->email);  

            $info['contact_number'] = $salesperson->outlet_pic_mobile_number;        
            $info['salesperson'] = array(
                                        'id'=>$salesperson->user_id,
                                        'salesperson_name'=>$salesperson->outlet_pic_name,
                                        'salesperson_image_path'=>asset('/uploads/outlets/'.$salesperson->id.'/'.$salesperson->outlet_logo),
                                        'salesperson_address'=>$salesperson->outlet_address
                                      );
          }         

          $info['logo'] = $merchant_data->logo;
          $info['merchant_identifier'] = $merchant_data->merchant_identifier;
          if($merchant_data->id==1){
            $info['sub_merchant_ref'] = "";
          }
          else{
            $info['sub_merchant_ref'] = $merchant_data->sub_merchant_ref;
          }
            
          $info['verify_key'] = $merchant_data->verify_key;
        }

        return response()->json($this->merchantRepo->NullToEmpty($info,'Login'), $res->getStatusCode()); ///SUCCESS
       
    } 
   

}
