<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\InvestorDetail;
use App\Models\Project;
use App\Models\Lookup;
use LookupRepo;
use TransactionRepo;
use UserRepo;
use Log;
use Carbon\Carbon;
use DateTime;
use App\Notifications\Email;
use App\Helpers\SMSNotification;
use App\Helpers\PushNotification;
use Notification;
use App;
//use ImageRepo;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Merchant;
use App\Models\TransactionField;
use App\Models\InputField;
use App\Models\EmailNotification;



class UPaymeController extends Controller {

    //public function __construct(ImageRepo $imageRepo) {
    public function __construct(LookupRepo $lookupRepo, UserRepo $userRepo, TransactionRepo $transactionRepo, SMSNotification $smsNotification,PushNotification $pushNotification) {
        $this->lookupRepo = $lookupRepo;
        $this->userRepo = $userRepo;
        $this->transactionRepo = $transactionRepo;
        $this->smsNotification = $smsNotification;
        $this->pushNotification = $pushNotification;
       // $this->imageRepo = $imageRepo;
    }

    public function validator(array $data, $method = null)
    {
        $attributeNames = [
            'status_id' => 'status'
          ];

        if($method =='register'){
            $rule = [
              'fullname' => 'required|max:100' 
              ,'nationality' => 'required' 
              ,'id_no' => 'required' 
              ,'dob' => 'required' 
              ,'correspondence' => 'required' 
              ,'zipcode' => 'required' 
              ,'city' => 'required' 
              ,'state' => 'required' 
              ,'country' => 'required' 
              ,'phone' => 'required' 
              ,'email' => 'required' 
              ,'id_upload' => 'required' 
              ,'risk' => 'required' 
              ,'ack' => 'required' 
              ,'signature' => 'required' 
            ];

        }

        return Validator::make($data, $rule)->setAttributeNames($attributeNames);  
    }

    public function show(Request $request) {
        $user = request()->user()->load(['investor_details']);
        
        ////VIEW USER DATA////
        return $this->userRepo->withRecord($user, "Show User");     
    }

    //INVESTOR REGISTRATION
    public function register(Request $request) {
      
        $data = $request->input();
        
        $validator = $this->validator($data, 'register');
        
       /* if ($validator->fails()) {
           // dd($validator->messages());
           // return response()->json($validator->messages(),404);
            return response()->json([
                    'status_code' => 1,
                    'status_title' => 'Failed',
                    'details' => $validator->messages()
        ]);
        }*/
        
       
            
          /*$data_entry = array(
          'salutation' => $request->salutation,
          'gender' => $request->gender,
          'fullname' => $request->fullname,
          'nationality' => $request->nationality,
          'id_no' => $request->id_no,
          'passport' => $request->passport,
          'dob' => $request->dob,
          'correspondence' => $request->correspondence,
          'zipcode' => $request->zipcode,
          'city' => $request->city,
          'state' => $request->state,
          'country' => $request->country,
          'phone' => $request->phone,
          'email' => $request->email,
          //$id_upload = $request->id_upload;
          'risk' => $request->risk,
          'ack' => $request->ack,
          //$signature = $request->signature;
          );*/
       
        $investor = InvestorDetail::create($data); //CREATE
        

        $transaction_data = array(
            "investor_id" => $investor->id
            ,"merchant_id" => $data['merchant_id']
            ,"amount" => $data['amount']
            ,"signature" => $data['signature']
            ,"account_number" => $data['account_number']
            ,"invoice_number" => $data['invoice_number']
            ,"bill_number" => $data['bill_number']
            ,"invoice_img" => $data['invoice_img']
            ,"remarks" => $data['remarks']
        );

        $transaction = Transaction::create($transaction_data);

        /*
        $filebase64array = array('id_upload' => $data['id_upload'], 'signature' =>$data['signature']);
        $filebase64upload = $this->imageRepo->save($filebase64array, 'investor-id', $investor->id);

            foreach ($filebase64upload as $key => $value) 
            {
                $image_data = array(
                    'type_id' => $value['type_id']
                    ,'user_id' => $id
                    ,'name' => $value['name']
                );

                $result = $this->create($image_data);

                if(!$result)
                {
                    return $this->errorSave('Error saving the image!');
                }
            } 
            
        */

        $data = [
                    'investor_id' => $investor->id,
                    'transaction_id' => $transaction->id,
                    'status_code' => 0,
                    'status_title' => 'Success'
        ];

        return response()->json($this->userRepo->withRecord($data, "Register"));
    }

    public function createTransaction(Request $request) {
      
        $data = $request->all();

        $user = request()->user()->load(['investor_details','merchant_details']);

        $transaction_data = array(
            "user_id" => $user['id']
            ,"merchant_id" => $data['merchant_id']
            ,"amount" => $data['amount']
            ,"signature" => $data['signature']
            ,"remarks" => $data['remarks']
        );

        if(!empty($data['salesperson_id'])){
            $transaction_data['salesperson_id'] = $data['salesperson_id'];
        }
        else{
            $transaction_data['salesperson_id'] = null;
        }

        if(!empty($data['os_type'])){
            $transaction_data['os_type'] = $data['os_type'];
        }
        else{
            $transaction_data['os_type'] = 'ios';
        }

        if(!empty($data['currency'])){
            $transaction_data['currency'] = $data['currency'];
        }
        else{
            $transaction_data['currency'] = 'MYR';
        }

        //dd($transaction_data);
        $transaction = Transaction::create($transaction_data);

        $data['fields'] = json_decode($data['fields'],true);

        if(!empty($data['fields'])){
            foreach ($data['fields'] as $key => $value) {
                if(!empty($value['value'])){
                    $record = InputField::find($value['field_id']);

                    $transaction_fields_data['transaction_id'] = $transaction->id;
                    $transaction_fields_data['field_id'] = $value['field_id'];

                    if($record['data_type_id']!=65){
                        $transaction_fields_data['value'] = $value['value'];
                    }
                    else{
                        $transaction_fields_data['value'] = null;
                    }

                    $transaction_fields = TransactionField::create($transaction_fields_data);

                    if($record['data_type_id']==65){
                        $array['image'] = $value['value'];

                        $folder_type = "transaction-fields";
                        $resultSave = $this->userRepo->saveFile($array, $folder_type, $transaction_fields->id);
                        
                        $transaction_fields->value = $resultSave['image'];
                        $transaction_fields->save();
                    }
                }
            }
        }
        

        /*
        $filebase64array = array('id_upload' => $data['id_upload'], 'signature' =>$data['signature']);
        $filebase64upload = $this->imageRepo->save($filebase64array, 'investor-id', $investor->id);

            foreach ($filebase64upload as $key => $value) 
            {
                $image_data = array(
                    'type_id' => $value['type_id']
                    ,'user_id' => $id
                    ,'name' => $value['name']
                );

                $result = $this->create($image_data);

                if(!$result)
                {
                    return $this->errorSave('Error saving the image!');
                }
            } 
            
        */

        $data = [
                    'user_id' => $user['id'],
                    'transaction_id' => $transaction->id,
                    'status_code' => 0,
                    'status_title' => 'Success'
        ];

        return response()->json($this->userRepo->withRecord($data, "Create Transaction"));
    }

    ///GET PROJECTS/PORTFOLIO
    public function project(Request $request) {
        $input = $request->all();
        
        /*$logStorage = storage_path().'/logs/Api/MCM/PaymentStatus/'.carbon::now()->format('Ym').'/info.log';  
       
        Log::useDailyFiles($logStorage);
        Log::info($input);*/
        
        
        
        
        
        $project = Project::find(1); //GET PROJECT


        $data = [
                    "id" => $project->id
                    , "name" => $project->name
                    , "code" => $project->code
                    , "value" => $project->value
                    , "profit" => $project->profit
                    , "description" => $project->description
                    , "close" => $project->close
        ];

        return response()->json($this->userRepo->withRecord($data, "Project"));
    }

    ///UPDATE PAYMENT STATUS
    public function updatePaymentStatus(Request $request) {
        
        $input = $request->all();
        
         Log::debug($input);
        //crystal@i-serve.com.my; allangoh8@gmail.com
       
        $input["status_code"] = $input["STATUS_CODE"];
        unset($input["STATUS_CODE"]);
        $input["status_description"] = $input["STATUS_DESC"];
        unset($input["STATUS_DESC"]);
        $input["method"] = 'Upay Payment Gateway';
        $input["transaction_id"] = $input["TXN_ID"];
        unset($input["TXN_ID"]);
        $input["payment_type"] = $input["PAY_TYPE"];
        unset($input["PAY_TYPE"]);
        $input["order_id"] = $input["ORDER_ID"];
        unset($input["ORDER_ID"]);
        $input["testing"] = $input["IS_TEST"];
        unset($input["IS_TEST"]);
        $input["source_form"] = $input["SOURCE_FROM"];
        unset($input["SOURCE_FROM"]);
        $input["timestamp"] = $input["TXN_TIMESTAMP"];
        unset($input["TXN_TIMESTAMP"]);

        //unset($input["AMOUNT"]);
     
        //success status
        if (!empty($input["status_code"]) && ($input["status_code"] == "00" || $input["status_code"] == "0")) {
            Transaction::where('id', $input["order_id"])
                    ->update(
                            array('status_id' => 21,
                                'amount' => $input["AMOUNT"],
                                'payment_type' => $input["payment_type"],
                                'updated_at' => $input["timestamp"],
                                'txn_id' => $input["transaction_id"],
                                'txn_description' => $input["status_description"]
                            )
            );
            
            //$user = User::find(1);

            $transaction = $this->transactionRepo->find($input["order_id"],['fields','merchant']);

            $user = User::find($transaction["user_id"]);

            if($user->role_id==6){
                $user_details = Merchant::where('user_id',$transaction["user_id"])->first();
            }
            else{   
                $user_details = InvestorDetail::where('user_id',$transaction["user_id"])->first();
            }
                
            // $investor['total_paid'] = $transaction['amount'];
            // $investor["remarks"] = $transaction['remarks'];
            // $investor["updated_at"] = $transaction['updated_at'];

            // $project = Project::find($transaction['project_id']);
            // $investor['service_type'] = $project['name'];

            //print_r($user);die();
            ///if investor insert email send the email          

            
            if(!empty($user)){
                
                try {

                    if($transaction['merchant_id'] > 1 && $transaction['merchant_id'] < 13){
                        $this->callbackCreditControl($transaction['id']);
                    } 

                    //Send email and push notification to the user
                    if(!is_null($user->email) && $user->role_id!=6){
                        $this->sendMail($user, 1, $transaction, $user_details);
                        $this->pushNotification->send($user, 2, $transaction["id"], number_format($transaction["amount"],2),$transaction["currency"]);
                        //$this->smsNotification->sendSMS($user_details->phone, 2, $transaction["id"], $transaction["amount"]);
                    }

                    //Send email and push notification to merchant
                    $merchant = Merchant::find($transaction['merchant_id']);//merchant email
                    $merchant_user = User::find($merchant['user_id']);
                    $this->sendMail($merchant_user, 2, $transaction, $user_details);
                    $this->pushNotification->send($merchant_user, 1, $transaction["id"], number_format($transaction["amount"],2),$transaction["currency"]);
                    //$this->smsNotification->sendSMS($merchant->phone, 1, $transaction["id"], $transaction["amount"]);

                    //Send email and push notification to BOSS
                    $user1 = User::find(3);///allangoh8@gmail.com
                    $this->sendMail($user1, 2, $transaction, $user_details);
                    //$this->pushNotification->sendNotification($user->id, 2);
                    
                    //Send email and push notification to Finance
                    $user2 = User::find(4);///chingsiew@i-serve.com.my
                    $this->sendMail($user2, 2, $transaction, $user_details);
                    //$this->pushNotification->sendNotification($user->id, 2);
                    
                    //$user3 = User::find(5);///niban
                    //$this->sendMail($user3, 2, $transaction, $user_details);
                    //$this->pushNotification->sendNotification($user->id, 2);
                    
                } 
                catch (\Exception $exception) {
                    Log::useDailyFiles(storage_path().'/logs/Exception/send_email_error.log');
                    Log::notice('Exception : '.$exception->getMessage());
                }
            }
            
            
        }else{///status failed
            Transaction::where('id', $input["order_id"])
                    ->update(
                            array('status_id' => 22,
                                'amount' => $input["AMOUNT"],
                                'payment_type' => $input["payment_type"],
                                'updated_at' => $input["timestamp"],
                                'txn_id' => $input["transaction_id"],
                                'txn_description' => $input["status_description"]
                            )
            );

            $transaction = $this->transactionRepo->find($input["order_id"],['fields','merchant']);

            $user = User::find($transaction["user_id"]);

            if($user->role_id==6){
                $user_details = Merchant::where('user_id',$transaction["user_id"])->first();
            }
            else{   
                $user_details = InvestorDetail::where('user_id',$transaction["user_id"])->first();
            }
                
            // $investor['total_paid'] = $transaction['amount'];
            // $investor["remarks"] = $transaction['remarks'];
            // $investor["updated_at"] = $transaction['updated_at'];

            // $project = Project::find($transaction['project_id']);
            // $investor['service_type'] = $project['name'];

            //print_r($user);die();
            ///if investor insert email send the email
            
            if(!empty($user)){
                
                try {
                    if(!is_null($user->email) && $user->role_id!=6){
                        $this->smsNotification->sendSMS($user_details->phone, 3, $transaction["id"], $transaction["amount"]);
                        //$this->pushNotification->sendNotification($user->id, 2);
                    }
                } 
                catch (\Exception $exception) {
                    Log::useDailyFiles(storage_path().'/logs/Exception/send_email_error.log');
                    Log::notice('Exception : '.$exception->getMessage());
                }
            }
        }

        return response()->json([
                    'status_code' => 0,
                    'status_title' => 'Success'
        ]);
    }

    public function callbackCreditControl($transaction_id){                    

        $record = $this->transactionRepo->find($transaction_id,['fields.field','merchant','user']);

        if(App::environment(['production'])){
            $callback_url = "http://101.99.65.197:81/IServeApps/api/BillPaymentUpdate";
        }
        else{
            $callback_url = "http://101.99.65.197:81/IServeAppsUAT/api/BillPaymentUpdate";
        }            

        $data['ProductID'] = $record['merchant_id'];
        $data['Amount'] = $record['amount'];
        $data['TotalAmount'] = $record['amount'];
        $data['PayMethod'] = $record['payment_type'];
        $data['RefCode'] = $record['remarks'];
        $data['OsType'] = $record['os_type'];
        $data['MerchantName'] = '2';
        $data['TransactionID'] = $record['txn_id'];
        $data['Email'] = !empty($record['user']['email']) ? $record['user']['email'] : '';

        foreach ($record['fields'] as $key => $value) {
            if($value['field_id']==1){
                $data['AccountNo'] = $value['value'];
            }

            if($value['field_id']==6){
                $data['BillNo'] = $value['value'];
            }

            if($value['field_id']==3){
                $data['PhoneNo'] = $value['value'];
            }
        }
        
        Log::useDailyFiles(storage_path().'/logs/CREDIT_CONTROL_CALLBACK/'.carbon::now()->format('Ym').'/callback.log');
        Log::notice('Callback form data to CREDIT_CONTROL '.$callback_url.' '. print_r($data,true));
     
        $headers = array(
          'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, $callback_url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS,  json_encode($data)  );
        
        $post_result = (array)json_decode(curl_exec($ch));

        curl_close( $ch );

        return $post_result;
    }
    
    ///GET STATUS PAYMENT
    public function PaymentStatus(Request $request) {

        $transaction_id = $request->transaction_id;
        $transaction = Transaction::find($transaction_id); //GET PROJECT
        $payment_status = Lookup::find($transaction['status_id'])['name'];
       
        return response()->json([
                    "transaction_id" => $transaction_id
                    , "payment_status" => $payment_status
                    
        ]);
    }
    
    
    public function sendMail($user, $emailType, $transaction, $user_details)
    {   
        $result['status'] = 1; //Failed
                
            $emailNotification = EmailNotification::find($emailType);
            $datetime = $transaction['updated_at'];
            
            // $datetime = (DateTime::createFromFormat('Y-m-d H:i:s', $datetime))->format('d/m/Y H:i:s');


            $message['subject'] = $emailNotification['subject'];
            $message['actionText'] = "";
            $message['actionUrl'] = "";

            $message['description'] = $emailNotification['description'];
            // //REPLACE ALL THE %VARIABLES TO THE SPECIFIC DATA

            if(!empty($user_details['company_registered_name'])){
                $message['description'] = str_replace("%NAME",strtoupper($user_details['company_registered_name']),$message['description']);
            }
            else{
                $message['description'] = str_replace("%NAME",strtoupper($user_details['fullname']),$message['description']);
            }

            if(!empty($user_details['id_no'])){
                $message['description'] = str_replace("%PASSPORT",strtoupper($user_details['id_no']),$message['description']);
            }
            else{
                $message['description'] = str_replace("%PASSPORT",'-',$message['description']);
            } 
            
            $message['description'] = str_replace("%PHONE",strtoupper($user_details['phone']),$message['description']);
            $message['description'] = str_replace("%EMAIL",$user['email'],$message['description']);
            $message['description'] = str_replace("%PAID",strtoupper($transaction['currency']).' '.number_format($transaction['amount'],2),$message['description']);

            $message['description'] = str_replace("%ORDERID",strtoupper($transaction->id),$message['description']);
            $message['description'] = str_replace("%PAYMENTTYPE",strtoupper($transaction->payment_type),$message['description']);
            $message['description'] = str_replace("%SERVICETYPE",strtoupper($transaction->merchant['name']),$message['description']);

            $message['description'] = str_replace("%DATETIME",$datetime,$message['description']);
            
            if(!empty($transaction['remarks'])){
                $message['description'] = str_replace("%REMARKS",$transaction['remarks'],$message['description']);
            }
            else{
                $message['description'] = str_replace("%REMARKS",'-',$message['description']);
            }
            Notification::send($user, new Email($message));
            /*Notification::route('mail', $user['email'])
            ->route('nexmo', '5555555555')
            ->notify(new Email($message));*/
            
            /*Notification::send($user, new Email($message));
            $result['status'] = 0; //Success*/
       

        return $result;
    }

}
