<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use Validator;

//Helpers
use App\Helpers\Validation;

class ValidationController extends Controller
{

    public function __construct(Validation $validation)
    {
        $this->validation = $validation;
    }     
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $data = $request->input();

        $result = $this->validation->validate($data);

        return $result;
        
    }
}
