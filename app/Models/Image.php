<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = array(
    	"user_id"
        ,"type_id"
        ,"name"
  	);

  	/**
    * One To One relationship
    */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Lookup');
    }
}
