<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = array(
    	"icon"
      ,"name"
    	,"code"
      ,"symbols"
    	,"status_id"
  	);

  	/**
     * One To One inverse relationship
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }
}
