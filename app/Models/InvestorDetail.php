<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class InvestorDetail extends Model
{   
    use Notifiable;
    protected $fillable = array(
    	"user_id"
        ,"salutation"
    	,"gender"
    	,"fullname"
    	,"nationality"
    	,"id_no"
        ,"passport"
        ,"dob"
        ,"correspondence"
        ,"zipcode"
        ,"city"
        ,"state"
        ,"country"
        ,"phone"
        ,"email"
        ,"id_upload"
        ,"risk"
        ,"ack"
        ,"referrer"
    );
   
}
