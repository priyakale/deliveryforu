<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

//HELPER
use App\Helpers\SettingHelper;

class Merchant extends Model
{
    protected $fillable = array(
    	'user_id'
        ,'logo'
        ,'company_registered_name'
        ,'business_registration_number'
        ,'trading_name'
        ,'headquarters_address'
        ,'headquarters_contact_number'
        ,'type_of_ownership'
        ,'type_of_business'
        ,'years_in_business'
        ,'office_number'
        ,'fax_number'
        ,'phone'
        ,'salesperson_name'
        ,'position'
        ,'status_id'
        ,'sub_merchant_ref'
        ,'type_id'
        ,'ic_number'
        ,'individual_name'
        ,'individual_mobile_number'
        ,'individual_address'
        ,'sign_off'
        ,'end_date'
        ,'charges_apply'
        ,'agent_company'
        ,'agent_type'
        ,'agent_name'
        ,'agent_code'
	);	

    /**
     * One To One inverse relationship
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function type_of_ownership()
    {
        return $this->belongsTo('App\Models\Lookup', 'type_of_ownership');
    }

    public function type_of_business()
    {
        return $this->belongsTo('App\Models\Lookup', 'type_of_business');
    }

    /**
     * Get One To One relationship.
     */
    public function bank()
    {
        return $this->hasOne('App\Models\MerchantBankAccount');
    }

    /**
     * Get One To Many relationship.
     */
    public function input_fields()
    {
        return $this->hasMany('App\Models\MerchantInputField', 'merchant_id');
    }

    public function outlets()
    {
        return $this->hasMany('App\Models\MerchantOutlet', 'merchant_id');
    }

    /**
     * Accessors when we retrieve it
     */
    public function getCreatedAtAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format(SettingHelper::getDatetimeFormat());
        }
        return $value;
    }

    public function getSignOffAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d', $value))->format('d/m/Y');
        }
        return $value;
    }

    public function getEndDateAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d', $value))->format('d/m/Y');
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setSignOffAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['sign_off'] = $value;
    }

    public function setEndDateAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['end_date'] = $value;
    }
        
}
