<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputField extends Model
{
    protected $fillable = array(
        "id",
    	"name"
    	,"status_id"
    	,"data_type_id"
  	);

    public function data_type()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    /**
     * Get the comments for the blog post.
     */
    public function options()
    {
        return $this->hasMany('App\Models\InputFieldOption', 'field_id');
    }
}
