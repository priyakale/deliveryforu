<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmailSetting extends Model implements AuditableContract 
{
    use Auditable;
    protected $fillable = array(
    	"name"
    	,"driver"
    	,"smtp_host"
    	,"smtp_username"
    	,"smtp_password"
        ,"port"
        ,"encryption"
    	,"sendmail_path"
        ,"mailgun_domain"
        ,"mailgun_secret"
  	);
}
