<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Menu extends Model  implements AuditableContract 
{
	use Auditable;
    protected $fillable = array('name', 'link_name', 'parent_id', 'icon', 'status_id');
}
