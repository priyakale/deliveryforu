<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class UserDevice extends Model
{
    protected $fillable = array(
    	'user_id'
        ,'sku_id'
        ,'package_fee_id'
        ,'imei'
        ,'imei_2'
        ,'end_date'
        ,'signature'
        ,'swapped_old_device_id'
        ,'device_purchased_date'
        ,'swap_warranty_end'
        ,'status_id'
        ,'mobile_number'
  	);

    /**
     * One To One inverse relationship
     */
    public function sku()
    {
        return $this->belongsTo('App\Models\Sku');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function swapped_old_device()
    {
        return $this->belongsTo('App\Models\UserDevice');
    }

    /**
     * One To One inverse relationship
     */
    public function package_fee()
    {
        return $this->belongsTo('App\Models\PackageFee');
    }

    /**
     * Get the comments for the blog post.
     */
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'device_id');
    }

    /**
     * Accessors when we retrieve it
     */
    public function getDevicePurchasedDateAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d', $value))->format('d/m/Y');
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setDevicePurchasedDateAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['device_purchased_date'] = $value;
    }
}
