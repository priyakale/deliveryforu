<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
    public $table = "sku";
    protected $fillable = array(
    	'creation_date'
        ,'make'
        ,'model'
        ,'color'
        ,'client_brand_model_color'
        ,'client_inventory_client_conventions'
        ,'material_code'
        ,'material_description '
        ,'model_2'
        ,'mkt'
        ,'man'
        ,'col'
        ,'mod'
        ,'eid'
        ,'asurion_sku'
        ,'price'
        ,'tier'
  	);
}
