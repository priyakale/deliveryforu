<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageFee extends Model
{
    public $table = "package_fees";
    protected $fillable = array(
    	"package_id"
        ,"tier_id"
        ,"fee"
        ,"fee_wo_gst"
        ,"dealer_incentive"
        ,"dealer_renewal_incentive"
        ,"supply_fee"
        ,"incentive"
        ,"platform"
	);
	  
	/**
     * One To One inverse relationship
     */
    public function package()
    {
        return $this->belongsTo('App\Models\Package');
    }

    public function tier()
    {
        return $this->belongsTo('App\Models\Tier');
    }
}
