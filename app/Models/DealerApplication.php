<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class DealerApplication extends Model
{
	protected $fillable = array(
        'user_id'
        ,'company_name'
        ,'company_number'
        ,'company_gst_num'
        ,'date_incorporation'
        ,'num_of_employees'
        ,'trade_name'
        ,'phones_sold_monthly'            
        ,'form_9_or_13'
        ,'business_license'
        ,'phone_brands'
        ,'form_24'
        ,'proof_of_business'
        ,'contact_user_id'
        ,'same_authorised_person'
        ,'tnc'
        ,'yakin_salesman'
        ,'salesman_id'
        ,'distributer_id'
        ,'privacy_policy'
        ,'declaration'
        ,'qr_release'
        ,'saleskit_handover'
        ,'status_id'
  	);

    /**
     * One To One inverse relationship
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function contact_user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function salesman()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function distributer()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function contact_identification_type()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    /**
     * Accessors when we retrieve it
     */
    public function getDateIncorporationAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d', $value))->format('d/m/Y');
        }
        return $value;
    }

    public function getPhoneBrandsAttribute($value) 
    {
        if(!is_null($value)){
            $value = json_decode($value);
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setDateIncorporationAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['date_incorporation'] = $value;
    }

    public function setPhoneBrandsAttribute($value)
    {
        if(!is_null($value)){
            $value = json_encode($value);
        }

        $this->attributes['phone_brands'] = $value;
    }
    	
}
