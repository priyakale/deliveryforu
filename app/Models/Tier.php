<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tier extends Model
{
    public $table = "tiers";
    protected $fillable = array(
    	'name'
        ,'description'
        ,'status_id'
        ,'swap_fee'
  	);

  	/**
     * One To One inverse relationship
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }
}
