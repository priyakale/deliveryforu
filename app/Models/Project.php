<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = array(
    	"name"
    	,"code"
    	,"value"
    	,"profit"
    	,"description"
        ,"close"
        );
}
