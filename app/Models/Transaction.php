<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Transaction extends Model
{
    protected $fillable = array(
    	"user_id"
        ,"merchant_id"
        ,"amount"
        ,"signature"
        ,"status_id"
        ,"payment_type"
        ,"remarks"
        ,"invoice_img"
        ,"invoice_number"
        ,"account_number"
        ,"bill_number"
        ,"investor_id"
        ,"project_id"
        ,"payment_type"
        ,"os_type"
        ,"currency"
        ,'salesperson_id'
        ,'txn_id'
        ,'txn_description'
    );

    /**
     * Get the comments for the blog post.
     */
    public function fields()
    {
        return $this->hasMany('App\Models\TransactionField');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function investor()
    {
        return $this->belongsTo('App\Models\InvestorDetail');
    }

    /**
     * Accessors when we retrieve it
     */
    public function getCreatedAtAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format('d/m/Y H:i:s');
        }
        return $value;
    }

    public function getUpdatedAtAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format('d/m/Y H:i:s');
        }
        return $value;
    }
}
