<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavouriteMerchant extends Model
{
    protected $fillable = array(
    	"user_id"
      ,"merchant_id"
      ,"salesperson_id"
  	);

  	/**
     * One To One inverse relationship
     */
    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    public function salesperson()
    {
        return $this->belongsTo('App\Models\MerchantOutlet', 'salesperson_id', 'user_id');
    }
}
