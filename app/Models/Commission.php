<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
	protected $fillable = array(
    	'user_id'
    	, 'rate'
        , 'ra_rate'
    	, 'status_id'
	);

	/**
     * One To One inverse relationship
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
