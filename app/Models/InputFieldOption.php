<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputFieldOption extends Model
{
    protected $fillable = array(
        "id"
    	,"name"
    	,"field_id"
  	);
}
