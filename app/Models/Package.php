<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public $table = "packages";
    protected $fillable = array(
    	"name"
        ,"program_code"
    	,"customer_type_id"
        ,"package_type_id"
        ,"payment_month_id"
        ,"status_id"
	);
	  
	/**
     * One To One inverse relationship
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function customer_type()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function package_type()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function payment_month()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    /**
     * One To Many relationship
     */
    public function fee()
    {
        return $this->hasMany('App\Models\PackageFee');
    }
}
