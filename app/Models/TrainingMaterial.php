<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingMaterial extends Model
{
	public $table = "training_materials";
    protected $fillable = array(
    	'user_id'
	    ,'filename'
	    ,'thumbnail'
	    ,'material_type_id'
	    ,'description'
	    ,'status_id'
  	);

    /**
     * One To One inverse relationship
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    /**
     * One To One inverse relationship
     */
    public function material_type()
    {
        return $this->belongsTo('App\Models\Lookup');
    }
	    
}
