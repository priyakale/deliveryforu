<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDocumentChecklist extends Model
{
    protected $fillable = array(
    	'pkgid'
        ,'name'
        ,'description'
        ,'is_required'
    );
}
