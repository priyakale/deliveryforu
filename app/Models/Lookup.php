<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lookup extends Model
{
    /*
    * Return lookup data by id
    * 
    */ 

    protected $fillable = array(
      "name"
      ,"category"
      ,"status"
        ,"value"
    );

   public static function Name($id) {
       $result = Lookup::find($id);
       
       if(!is_null($result))
           $result = $result->name;
       else
          $result = null;
       return $result;
   }

   
    public static function getGeneralStatus(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','General Status')->get();
        return $result;
    }

    public static function getStatusHardcode($value = null){
        $record = array();
        $record['A'] = 'Active';
        $record['D'] = 'Disable';

        if(!empty($value) && $value != null){
            return $record[$value];
        }else{
            return $record;
        }
    }

    public static function getGender(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Gender')->get();
        return $result;
    }

    public static function getAddressType(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Address')->get();
        return $result;
    }

    public static function getRace(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Race')->get();
        return $result;
    }

    public static function getNationality(){
        $result = Nationality::select('id','name')->where('status_id', '1')->orderBy('order_by', 'asc')->get();
        return $result;
    }

    public static function getRegisterStatus(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Status_Register')->get();
        return $result;
    }

    public static function getApplicationStatus(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Application Status')->get();
        return $result;
    }

    public static function getMailDriver(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Mail Driver')->get();
        return $result;
    }

    public static function getMailEncryption(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Mail Encryption')->get();
        return $result;
    }

    public static function getEmailType(){
        $result = Lookup::select('id','name')->where('status', 'A')->where('category','Email Type')->get();
        return $result;
    }

    

}
