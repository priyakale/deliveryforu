<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Crypt;

class TransactionField extends Model
{
    protected $fillable = array(
    	"transaction_id"
        ,"field_id"
        ,"value"
    );

    /**
     * Get the comments for the blog post.
     */
    public function field()
    {
        return $this->belongsTo('App\Models\InputField');
    }

    /**
     * Accessors when we retrieve it
     */
    public function getValueAttribute($value) {
        if(!empty($value)){
            $value = Crypt::decrypt($value);
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setValueAttribute($value)
    {
        if(!empty($value)){
            $value = Crypt::encrypt($value);
        }

        $this->attributes['value'] = $value;
    }
}
