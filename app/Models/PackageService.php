<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

//HELPER
use App\Helpers\SettingHelper;

class PackageService extends Model
{
    protected $fillable = array(
        'pkgcode'
        ,'name'
        ,'description'
        ,'category_id'
        ,'status_id'
        ,'start'
        ,'end'
        ,'deal_method_id'
        ,'payment_type_id'
        ,'screen_flow'
        ,'logo'
        ,'insurance_id'
        ,'inhouse_id'
        ,'chaincode'
        ,'ruleid'
    );  

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function inhouse()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    /**
     * Get One To One relationship.
     */
    public function servicedocs()
    {
        return $this->hasOne('App\Models\ServiceDocumentChecklist','pkgid');
    }
    
    /**
     * Get One To Many relationship.
     */
    public function input_fields()
    {
        return $this->hasMany('App\Models\ServiceInputField', 'pkgid');
    }

    public function subservice()
    {
        return $this->hasMany('App\Models\SubService', 'pkgid');
    }

    public function status_id()
    {
        return $this->belongsTo('App\Models\Lookup','status_id');
    }

    public function category_id()
    {
        return $this->belongsTo('App\Models\Lookup','category_id');
    }

    public function deal_method_id()
    {
        return $this->belongsTo('App\Models\Lookup', 'deal_method_id');
    }  

    public function insurance_id()
    {
        return $this->belongsTo('App\Models\Lookup', 'insurance_id');
    }  

    public function inhouse_id()
    {
        return $this->belongsTo('App\Models\Lookup','inhouse_id');
    } 

    public function ruleid()
    {
        return $this->belongsTo('App\Models\Lookup','ruleid');
    } 

    public function payment_type_id()
    {
        return $this->belongsTo('App\Models\Lookup','payment_type_id');
    } 

    /**
     * Accessors when we retrieve it
     */
    public function getCreatedAtAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format(SettingHelper::getDatetimeFormat());
        }
        return $value;
    }

    public function getStartAttribute($value) 
    {   if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format('d/m/Y');
        }        
        return $value;
    }

    public function getEndAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format('d/m/Y');
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setStartAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['start'] = $value;
    }

    public function setEndAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['end'] = $value;
    }
        
}
