<?php

namespace App\Models;

use App\Notifications\PasswordResetMail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ACTIVATE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'referral_id',
        'last_login_at',
        'activated',
        'profile_pic',
        'status_id',
        'register_id',
        'phone_no',
        'google_id',
        'facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * One To One inverse relationship
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * Get One To One relationship.
     */
    public function investor_details()
    {
        return $this->hasOne('App\Models\InvestorDetail');
    }

    public function merchant_details()
    {
        return $this->hasOne('App\Models\Merchant');
    }

    public function outlet_details()
    {
        return $this->hasOne('App\Models\MerchantOutlet');
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetMail($token));
    }
}
