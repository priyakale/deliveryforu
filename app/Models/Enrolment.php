<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enrolment extends Model
{
    protected $fillable = array(
    	'user_id'
        ,'type_id'  
        ,'activation_date'
        ,'package_id'
        ,'dealer_id'
        ,'enrollment_termination_date'
        ,'status_id'
  	);
}
