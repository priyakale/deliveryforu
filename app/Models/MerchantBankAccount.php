<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantBankAccount extends Model
{
    protected $fillable = array(
    	'merchant_id'
        ,'bank_name'
        ,'acc_number'
        ,'acc_holder_name'
        ,'acc_finance_contact_number'
        ,'acc_contact_number'
        ,'acc_email'
        ,'branch_information'
	);	

	/**
     * One To One inverse relationship
     */
    public function bank_name()
    {
        return $this->belongsTo('App\Models\Lookup', 'bank_name');
    }

}
