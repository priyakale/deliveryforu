<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantOutlet extends Model
{
    protected $fillable = array(
    	'merchant_id'
        ,'outlet_name'
        ,'outlet_pic_name'
        ,'outlet_pic_id_num'
        ,'outlet_pic_mobile_number'
        ,'outlet_address'
        ,'outlet_logo'
        ,'outlet_pic_email'
        ,'user_id'
	);	
}
