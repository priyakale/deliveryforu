<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantInputField extends Model
{
   protected $fillable = ['merchant_id', 'input_field_id', 'is_mandatory', 'position', 'status'];

    /**
     * One To One inverse relationship
     */
    public function merchant()
    {
    	
        return $this->belongsTo('App\Models\Merchant');
    }

    public function input_field()
    {
        return $this->belongsTo('App\Models\InputField');
    }
}
