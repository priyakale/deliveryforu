<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    public $table = "nationalities";
    protected $fillable = array(
    	"name"
    	,"status_id"
    	,"value"
    	,"order_by"
  	);
    
    
    ///GET NATIONALITY BY ID
    public static function getNationalityById($id, $param = ['*']) {
        $result = Nationality::select($param)->find($id);
        return $result;
    }
}
