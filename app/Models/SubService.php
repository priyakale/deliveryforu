<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

//HELPER
use App\Helpers\SettingHelper;

class SubService extends Model
{
    protected $fillable = array(
    	'pkgid'
        ,'name'
        ,'description'
        ,'status'
        ,'start'
        ,'end'        
	);

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
    ];
    
	/**
     * Accessors when we retrieve it
     */
    public function getCreatedAtAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format(SettingHelper::getDatetimeFormat());
        }
        return $value;
    }

    public function getStartAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format('d/m/Y');
        }
        return $value;
    }

    public function getEndAttribute($value) 
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('Y-m-d H:i:s', $value))->format('d/m/Y');
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setStartAttribute($value)
    { 
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['start'] = $value;
    }

    public function setEndAttribute($value)
    {
        if(!is_null($value)){
            $value = (DateTime::createFromFormat('d/m/Y', $value))->format('Y-m-d');
        }

        $this->attributes['end'] = $value;
    }	
}
