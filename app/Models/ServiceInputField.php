<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceInputField extends Model
{
   protected $fillable = ['pkgid', 'input_field_id', 'is_mandatory', 'position', 'status'];

    /**
     * One To One inverse relationship
     */
    public function package()
    {
    	
        return $this->belongsTo('App\Models\Package');
    }

    public function input_field()
    {
        return $this->belongsTo('App\Models\InputField');
    }
}
