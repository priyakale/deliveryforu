<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Crypt;

class Wallet extends Model
{
    protected $fillable = array(
    	'user_id'
        ,'balance'
        ,'status_id'
  	);

  	/**
     * One To One inverse relationship
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Lookup');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Accessors when we retrieve it
     */
    public function getBalanceAttribute($value) 
    {
        if(!empty($value)){
            $value = number_format((float) Crypt::decrypt($value), 2, '.', '');
        }
        return $value;
    }

    /**
     * Mutators before save to db.
     */
    public function setBalanceAttribute($value)
    {
        if(!is_null($value)){
            $value = Crypt::encrypt($value);
        }

        $this->attributes['balance'] = $value;
    }
}
