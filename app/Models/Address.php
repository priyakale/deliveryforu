<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $table = "addresses";
    protected $fillable = array(
    	"type_id"
    	,"user_id"
        ,"street_1"
        ,"street_2"
        ,"city"
        ,"postal_code"
        ,"state_id"
        ,"country_code"
  	);

    /**
     * One To One inverse relationship
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * One To One inverse relationship
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_code','code');
    }
}
