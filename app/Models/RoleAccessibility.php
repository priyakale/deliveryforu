<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class RoleAccessibility extends Model  implements AuditableContract 
{
	use Auditable;
	public $table = "role_accessibilities";
    protected $fillable = array('controller', 'function', 'role_id');
}
