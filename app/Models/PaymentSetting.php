<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSetting extends Model
{
    protected $fillable = array(
    	"merchant_identify"
    	,"verify_key"
    	,"currency_id"
        ,"currency_rate"
        ,"url_type"
        ,"mode"
  	);

  	/**
     * One To One inverse relationship
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }
}
