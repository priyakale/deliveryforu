<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charges extends Model
{
	protected $fillable = array(
    	'convenience_fee'
    	, 'credit_card'
        , 'debit_card'
    	, 'fpx'
	);
}
