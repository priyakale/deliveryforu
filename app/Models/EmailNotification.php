<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailNotification extends Model
{
    protected $fillable = array(
    	"subject"
    	,"description"
    	,"email_for_id"
    	,"status_id"
  	);
}
