<?php

namespace App\Jobs\Auth;

use App\Mail\UserActivationMail;
use App\Models\UserActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendMailActivation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $activation = UserActivation::create([
            'user_id' => $this->user->id,
            'token'   => Str::random(40)
        ]);

        Mail::to($this->user)->send(new UserActivationMail($activation));
    }
}
