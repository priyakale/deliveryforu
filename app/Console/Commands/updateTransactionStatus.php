<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Log;

class updateTransactionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:transactionStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update transaction status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = date('Y-m-d',strtotime("-1 days"));
        $res = DB::table('transactions')
                        ->select('*')
                        ->whereRaw('date(created_at) = ?', $yesterday)
                        ->get();
        
        for($i=0;$i<count($res);$i++)
        {
            $mer_id = DB::table('merchants')
                        ->select('merchants.sub_merchant_ref')
                        ->whereRaw('merchants.id', $res[$i]->merchant_id)
                        ->first();
            
            $data = array('ORDER_ID'=>$res[$i]->id,'MERCHANT_IDENTIFIER'=>$mer_id->sub_merchant_ref);
            $callback = $this->get_transaction_status($data);
            $updated_status = json_decode( $callback, true );
            $status_code = "";
            if($updated_status['STATUS_CODE']!=0 || $updated_status['STATUS_CODE']!=00)
            {
                $status_code = 22;
            }
            else
            {
                $status_code = 21;
            }
            if(!empty($status_code))
            {
                $query = DB::table('transactions')
                        ->where('id',$res[$i]->id)
                        ->update(['status_id' => $status_code]);
            }
        }
    }

    public function get_transaction_status(array $data){                    
        
        $ORDER_ID = $data['ORDER_ID'];
        $MERCHANT_IDENTIFIER = $data['MERCHANT_IDENTIFIER'];
        $callback_url = "https://www.upay2us.com/iServeGateway_UAT/transaction_query";

        Log::useDailyFiles(storage_path().'/logs/Status/'.time().'/callback.log');
        Log::notice('Callback form data to UpayMe'. print_r($data,true));
     
        $headers = array(
          'Content-Type: application/json'
        );

        $ch = curl_init();
        $data_array = http_build_query($data);
        //echo $data_array;exit;
        $getUrl = $callback_url."?".$data_array;
        //echo $getUrl;exit;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
         
        $response = curl_exec($ch);
         
        if(curl_error($ch)){
            echo 'Request Error:' . curl_error($ch);
        }
        else
        {
            echo $response;
        }
         
        curl_close($ch);

        return $response;
    }
}
