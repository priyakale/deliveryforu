<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Email extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $message;
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   
        if(!empty($this->message['actionUrl'])){
            $actionUrl = $this->message['actionUrl'];
        }
        else{
            $actionUrl = '';
        }

        if(!empty($this->message['actionText'])){
            $actionText = $this->message['actionText'];
        }
        else{
            $actionText = '';
        }

        if(!empty($this->message['description'])){
            $description = $this->message['description'];
        }
        else{
            $description = '';
        }

        $values['data'] = array(
            'actionUrl' => $actionUrl
            ,'actionText' => $actionText
            ,'description' => $description
        );  
        return (new MailMessage)
                ->subject($this->message['subject'])
                ->markdown('admin.email.template.layout', $values);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
