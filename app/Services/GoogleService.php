<?php

namespace App\Services;

use Google_Client;
use Google_Service_Oauth2;
use Google_Service_Plus;

class GoogleService
{
    protected $client;

    /**
     * GoogleService constructor.
     */
    public function __construct()
    {
        $this->client = new Google_Client();
        $this->client->setAuthConfig(storage_path('google_oauth_consent.json'));
    }

    /**
     * Set scope for google plus information
     */
    public function GPlusScope()
    {
        $this->client->addScope(Google_Service_Plus::USERINFO_PROFILE);
        $this->client->addScope(Google_Service_Plus::PLUS_LOGIN);
        $this->client->addScope(Google_Service_Plus::PLUS_ME);
        $this->client->addScope(Google_Service_Plus::USERINFO_EMAIL);

        return $this;
    }

    /**
     * Generate Google Authentication URL and set callback
     *
     * @return string
     */
    public function createAuthUrl()
    {
        $this->client->setRedirectUri(route('auth.google.callback'));
        return $this->client->createAuthUrl();
    }

    /**
     * Fetch the user information
     *
     * @return \Google_Service_Oauth2_Userinfoplus
     */
    public function getUserInfo($code)
    {
        $this->client->fetchAccessTokenWithAuthCode($code);
        $oauth2 = new Google_Service_Oauth2($this->client);
        return $oauth2->userinfo->get();
    }
}
