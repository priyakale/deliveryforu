<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('auth/login', '\App\Api\Controllers\Auth\LoginController@index');
Route::post('auth/register', '\App\Api\Controllers\UserController@create');
Route::post('/auth/forgot-password', '\App\Api\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail');

Route::post('/investor-reg', '\App\Api\Controllers\UPaymeController@register');
Route::get('/projects', '\App\Api\Controllers\UPaymeController@project');
Route::get('/pstatus', '\App\Api\Controllers\UPaymeController@PaymentStatus');

Route::get('/country', '\App\Api\Controllers\CountryController@index');
Route::get('/currencies', '\App\Api\Controllers\CurrencyController@index');
Route::get('/state', '\App\Api\Controllers\StateController@index');


Route::post('/update-payment-status', '\App\Api\Controllers\UPaymeController@updatePaymentStatus');

Route::middleware('auth:api')->group(function () 
{  
	Route::get('/user', '\App\Api\Controllers\UPaymeController@show');

	///CHANGE PASSWORD
    Route::post('/user/cp', '\App\Api\Controllers\UserController@changePassword');


	Route::post('/transaction', '\App\Api\Controllers\UPaymeController@createTransaction');
	Route::get('/merchants', '\App\Api\Controllers\MerchantController@index');

	Route::get('/merchant/favourite', '\App\Api\Controllers\MerchantController@getFavourite');
	Route::post('/merchant/favourite/add', '\App\Api\Controllers\MerchantController@addFavourite');
	Route::post('/merchant/favourite/delete', '\App\Api\Controllers\MerchantController@deleteFavourite');

	Route::post('/bill-payment/validate', '\App\Api\Controllers\ValidationController@index');

	Route::get('/individual_merchants', '\App\Api\Controllers\MerchantController@individual_merchants');
	Route::get('/merchant-fields/{id}', '\App\Api\Controllers\MerchantController@show');
	Route::get('/merchant_outlets/{id}', '\App\Api\Controllers\MerchantController@get_merchant_outlets');
	Route::post('/check_mobile', '\App\Api\Controllers\MerchantController@check_mobile');
	Route::post('/update_mobile', '\App\Api\Controllers\MerchantController@update_mobile');
	Route::post('/update-register-id', '\App\Api\Controllers\UserController@updateRegisterId');

	Route::post('/transaction-history', '\App\Api\Controllers\TransactionController@history');

	Route::get('/get-month', '\App\Api\Controllers\TransactionController@getMonth');

	Route::get('/get-charges', '\App\Api\Controllers\ChargesController@index');
});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
