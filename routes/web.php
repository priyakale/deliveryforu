<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Route::get('/', function () {
    return view('landing');
});

Route::get('/reset-password-success', function () {
    return view('auth.reset_password_success');
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Auth::routes();
Route::get('/user/activation/{token}', 'Auth\LoginController@activation')->name('user.activation');
Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

//Social Authentication
Route::get('/login/google', 'Auth\LoginController@googleAuthLogin')->name('auth.google.login');
Route::get('/login/facebook', 'Auth\LoginController@facebookAuthLogin')->name('auth.facebook.login');

//Callback
Route::get('/oauth/google/redirect', 'Auth\LoginController@googleAuthCallback')->name('auth.google.callback');
Route::get('/oauth/facebook/redirect', 'Auth\LoginController@facebookCallback')->name('auth.facebook.callback');


Route::prefix('admin')->group(function () {

    # Home
    Route::get('/', 'HomeController@admin')->name('admin');
    # End Home

    # Menu
    Route::prefix('menu')->group(function () {
        Route::get('/', 'Admin\MenuController@index')->name('index_menu');
        Route::get('add', 'Admin\MenuController@add')->name('add_menu');
        Route::get('edit/{menu_id?}', 'Admin\MenuController@edit')->name('edit_menu');
        Route::post('save', 'Admin\MenuController@save')->name('save_menu');
        Route::get('delete/{menu_id?}', 'Admin\MenuController@delete')->name('delete_menu');
        Route::get('collapse', 'Admin\MenuController@setMenuCollapse')->name('collapse');
    });
    # End Menu

    # User
    Route::prefix('user')->group(function () {
        Route::get('/list', 'Admin\UserController@index')->name('index_user');
        Route::post('update_user', 'Admin\UserController@update_user')->name('update_user');
        Route::get('/user_detail/{user_id?}', 'Admin\UserController@user_detail')->name('user_detail');
        Route::get('/view/{user_id?}', 'Admin\UserController@view')->name('view_user');

    });
    # End User

    # Role
    Route::prefix('role')->group(function () {
        Route::get('/', 'Admin\RoleController@index')->name('index_role');
        Route::get('add', 'Admin\RoleController@add')->name('add_role');
        Route::get('edit/{role_id?}', 'Admin\RoleController@edit')->name('edit_role');
        Route::post('save', 'Admin\RoleController@save')->name('save_role');
        Route::post('delete', 'Admin\RoleController@delete')->name('delete_role');
        Route::get('add_function_model', 'Admin\RoleController@addFunction')->name('add_function_model');
        Route::post('delete_role_function', 'Admin\RoleController@deleteRoleFunction')->name('delete_role_function');
        Route::post('delete_role_function_selected', 'Admin\RoleController@deleteRoleFunctionSelected')->name('delete_role_function_selected');
        Route::post('save_role_function', 'Admin\RoleController@saveRoleFunction')->name('save_role_function');
        Route::get('add_menu_model/{menu_role_id?}', 'Admin\RoleController@addMenuModel')->name('add_menu_model');
        Route::post('save_role_menu', 'Admin\RoleController@saveRoleMenu')->name('save_role_menu');
        Route::post('delete_role_menu', 'Admin\RoleController@deleteRoleMenu')->name('delete_role_menu');
    });
    # End Role

    # Audit
    Route::prefix('audit')->group(function () {
        Route::get('/', 'Admin\AuditController@index')->name('index_audit');
    });
    # End Audit

    # Setting
    Route::prefix('setting')->group(function () {
        Route::get('/', 'Admin\SettingController@index')->name('index_setting');
        Route::post('save', 'Admin\SettingController@save')->name('save_setting');
    });
    # End Setting

    # Payment Setting
    Route::prefix('payment-setting')->group(function () {
        Route::get('/', 'Admin\PaymentSettingController@index')->name('index_payment_setting');
        Route::post('save', 'Admin\PaymentSettingController@save')->name('save_payment_setting');
    });
    # End Payment Setting

    # Email
    Route::prefix('notification')->group(function () {
        Route::get('/', 'Admin\NotificationController@index')->name('index_notification');
        Route::get('add', 'Admin\NotificationController@add')->name('add_notification');
        Route::get('edit/{menu_id?}', 'Admin\NotificationController@edit')->name('edit_notification');
        Route::post('save', 'Admin\NotificationController@save')->name('save_notification');
        Route::get('delete/{notification_id?}', 'Admin\NotificationController@delete')->name('delete_notification');
    });
    # End Email


    # Transaction
    Route::prefix('transaction')->group(function () {
        Route::get('/', 'Admin\TransactionController@index')->name('index_transaction');
        Route::get('view/{transaction_id?}', 'Admin\TransactionController@view')->name('view_transaction');
    });
    # End Transaction


    # Report
    Route::prefix('report')->group(function () {
        Route::get('/', 'Admin\ReportController@index')->name('index_report');
        Route::get('/transaction_report/{date_from?}/{date_to?}/{type?}', 'Admin\ReportController@transaction_report')->name('transaction_report');

        Route::post('/download_report', 'Admin\ReportController@download_report')->name('download_report');
    });
    # End Report

    # Email Setting
    Route::prefix('email')->group(function () {
        Route::get('/', 'Admin\EmailSettingController@index')->name('index_email');
        Route::post('save', 'Admin\EmailSettingController@save')->name('save_email');
    });
    # End Email Setting

    # Package
    Route::prefix('package')->group(function () {
        Route::get('/', 'Admin\PackageController@index')->name('index_packageservice');
        Route::get('add', 'Admin\PackageController@add')->name('add_package');
        Route::get('edit/{pkg_id?}', 'Admin\PackageController@edit')->name('edit_package');
        Route::post('save', 'Admin\PackageController@save')->name('save_package');
        Route::get('delete/{pkg_id?}', 'Admin\PackageController@delete')->name('delete_package');
        Route::post('remove-subservices', 'Admin\PackageController@removeSubServices')->name('remove_added_subservices');
    });
    # End Package


    # Merchant
    Route::prefix('merchant')->group(function () {
        Route::get('/', 'Admin\MerchantController@index')->name('index_merchant');
        Route::get('add', 'Admin\MerchantController@add')->name('add_merchant');
        Route::get('edit/{merchant_id?}', 'Admin\MerchantController@edit')->name('edit_merchant');
        Route::post('save', 'Admin\MerchantController@save')->name('save_merchant');
        Route::get('delete/{merchant_id?}', 'Admin\MerchantController@delete')->name('delete_merchant');
        Route::post('check_username', 'Admin\MerchantController@check_username')->name('check_username');
        Route::post('remove-outlet', 'Admin\MerchantController@removeOutlet')->name('remove_added_outlet');
    });
    # End Merchant

    # Individual Merchant
    Route::prefix('individual_merchant')->group(function () {
        Route::get('/add_individual_merchant', 'Admin\MerchantController@add_individual_merchant')->name('add_individual_merchant');
        Route::post('save_individual', 'Admin\MerchantController@save_individual')->name('save_individual_merchant');
        Route::get('edit_individual_merchant/{merchant_id?}', 'Admin\MerchantController@edit_individual_merchant')->name('edit_individual_merchant');
    });
    # End Individual Merchant

    # Input Field
    Route::prefix('input-field')->group(function () {
        Route::get('/', 'Admin\InputFieldController@index')->name('index_input_field');
        Route::get('add', 'Admin\InputFieldController@add')->name('add_input_field');
        Route::get('edit/{input_field_id?}', 'Admin\InputFieldController@edit')->name('edit_input_field');
        Route::post('save', 'Admin\InputFieldController@save')->name('save_input_field');
        Route::get('delete/{input_field_id?}', 'Admin\InputFieldController@delete')->name('delete_input_field');
    });
    # End Input Field

    # Charges
    Route::prefix('charges')->group(function () {
        Route::get('/', 'Admin\ChargesController@index')->name('index_charges');
        Route::post('save', 'Admin\ChargesController@save')->name('save_charges');
    });
    # End Charges

    //Vehicle
    Route::namespace('Admin')->group(function () {
        Route::resource('vehicle', 'VehicleController', [
            'as'     => 'admin',
            'except' => 'show'
        ]);

        Route::resource('vehicle-model', 'VehicleModelController', [
            'as'     => 'admin',
            'except' => 'show'
        ]);
    });

    //Route::get('/e', 'Admin\MerchantController@index')->name('index_input_field');
    // Route::get('/q', 'Admin\MerchantController@index')->name('index_convenience_charges');

    // Route::get('merchantData', 'Admin\MerchantController@merchantData')->name('merchant.data');

    // Route::prefix('input-field')->group(function () {
    //     Route::get('/', 'Admin\MerchantFieldController@index')->name('index_input_field');
    //     Route::get('/create', 'Admin\MerchantFieldController@create')->name('create-merchant-field');
    //     Route::post('/store', 'Admin\MerchantFieldController@store')->name('store-merchant-field');
    //     Route::get('/edit/{merchant_id}', 'Admin\MerchantFieldController@edit')->name('edit-merchant-field');

    //     Route::get('merchantFieldData', 'Admin\MerchantFieldController@merchantFieldData')->name('merchant.field.data');

    // });
});

Route::get('/home', 'HomeController@index')->name('home');
